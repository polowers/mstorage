﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalToolworks.Data.MStorage
{
    partial class MTable
    {
        /// <summary>
        /// Represents configuration for non persistent memory table
        /// </summary>
        public sealed class ConfigInfo : IDisposable
        {
            /// <summary>
            /// File containing the table metadata
            /// </summary>
            readonly MemoryMappedFile _metadataMapFile;
            readonly FileStream _metadataFileStream;

            bool _actionManagerEnabled;
         
            private ConfigInfo(MemoryMappedFile metadataMapFile, bool readOnly, long indexCapacityMb, long maxCapacityMb)
            {
                this.ReadOnly = readOnly;
                this.IndexCapacityMb = indexCapacityMb;
                this.MaxCapacityMb = maxCapacityMb;

                this.AutoDataInfoEnabled = true;
                this.InsertTypedValues = true;
                this.UseStaticColumnLength = false;

                this.IsPersistent = false;
                this.DataFolder = null;

                this.ActionManager = new GuiLabs.Undo.ActionManager();
                this.ActionManagerEnabled = false;

                _metadataMapFile = metadataMapFile;
               
                var field = typeof(MemoryMappedFile).GetField("_fileStream", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.GetField);
                _metadataFileStream = (FileStream)field.GetValue(metadataMapFile);
            }

            /// <summary>
            /// Creates in-memory table configuration 
            /// </summary>
            /// <param name="readOnly"></param>
            /// <param name="indexCapacityMb"></param>
            /// <param name="maxCapacityMb"></param>
            /// <returns></returns>
            public static ConfigInfo CreateMemory(bool readOnly = false, long indexCapacityMb = 100, long maxCapacityMb = 10000)
            {
                // Allocate in-memory mapped file, for the table meta data
                var metadata = MemoryMappedFile.CreateNew(null, Metadata.Header.MetaBlockSize + (1024L * 1024L * (Environment.Is64BitProcess ? indexCapacityMb : 1000L)));
                return new ConfigInfo(metadata, readOnly, indexCapacityMb, maxCapacityMb);
            }

            /// <summary>
            /// Creates configuration for persistent table
            /// </summary>
            /// <param name="persistencyRootFolder"></param>
            /// <param name="readOnly"></param>
            /// <param name="indexCapacityMb"></param>
            /// <param name="maxCapacityMb"></param>
            /// <returns></returns>
            public static ConfigInfo CreatePersistent(string persistencyRootFolder, bool readOnly = false, long indexCapacityMb = 100, long maxCapacityMb = 10000)
            {
                //  Ensures the root folder exists (creates if does not exist)
                //
                while (!Directory.Exists(persistencyRootFolder))
                    Directory.CreateDirectory(persistencyRootFolder);

                var metadataFile = new FileInfo(Path.Combine(persistencyRootFolder, "table.meta"));
                bool created;

                //  If the data already exists and the indexCapacity is smaller then the target file capacity, 
                //  truncate the indexCapacityMb to the capacity from the physical file. 
                //  The indexCapacityMb consist of the MetaBlockSize + Entries Index capacity
                //
                if (!(created = !metadataFile.Exists) && indexCapacityMb < ((metadataFile.Length - Metadata.Header.MetaBlockSize) / 1024L / 1024L))
                    indexCapacityMb = ((metadataFile.Length - Metadata.Header.MetaBlockSize) / 1024L / 1024L);

                //  Use memory mapped file for the table meta data (FileStream override is used to specify FileShare    
                //
                var metadataMapFile = MemoryMappedFile.CreateFromFile(
                    fileStream: new FileStream(
                        path: metadataFile.FullName,
                        mode: FileMode.OpenOrCreate,
                        access: FileAccess.ReadWrite,
                        share: FileShare.ReadWrite),
                    mapName: null,
                    capacity: Metadata.Header.MetaBlockSize + (indexCapacityMb * 1024L * 1024L),
                    access: MemoryMappedFileAccess.ReadWrite,
                    memoryMappedFileSecurity: null,
                    inheritability: HandleInheritability.Inheritable,
                    leaveOpen: false);

                return new ConfigInfo(metadataMapFile, readOnly, indexCapacityMb, maxCapacityMb)
                {
                    IsPersistent = true,
                    DataFolder = persistencyRootFolder,
                    NewlyCreated = created
                };
            }
           
            /// <summary>
            /// Enables automatic calculation of column data info statistics, upon data insersion, updates or deletion. Has performance impact when enabled
            /// </summary>
            public bool AutoDataInfoEnabled { get; set; }

            /// <summary>
            /// Whether the table is read only - this disables the row-block buffers and increases speed
            /// </summary>
            public bool ReadOnly { get; private set; }
            /// <summary>
            /// Whether the record values inserted are considered to be typed or untyped
            /// </summary>
            public bool InsertTypedValues { get; set; }

            /// <summary>
            /// Initial size of the data indexer
            /// </summary>
            public long IndexCapacityMb { get; private set; }

            /// <summary>
            /// Maximum capacity of this table storage
            /// </summary>
            public long MaxCapacityMb { get; private set; }

            /// <summary>
            /// Indicates whether the table is persistent on disk or it is in-memory table
            /// </summary>
            public bool IsPersistent { get; private set; }

            /// <summary>
            /// Indicates whether the persistent content was just created or it was loaded as existing
            /// </summary>
            internal bool NewlyCreated { get; private set; }

            /// <summary>
            /// The table column value lengths are static, fixed and maximum 255 bytes. They are not variants (variants are performance sensitive but takes less memory)
            /// </summary>
            internal bool UseStaticColumnLength { get; set; }

            /// <summary>
            /// Provides Undo and Redo functionality for table operations actions 
            /// </summary>
            public GuiLabs.Undo.ActionManager ActionManager { get; private set; }

            /// <summary>
            /// When ActionManager is set, it allows temporary suspend or resume the ActionsManager functionality
            /// </summary>
            public bool ActionManagerEnabled
            {
                get { return _actionManagerEnabled && ActionManager != null; }
                set { _actionManagerEnabled = value; }
            }

            /// <summary>
            /// Returns the underlaying metadata map file
            /// </summary>
            /// <returns></returns>
            internal MemoryMappedFile GetMetadataMap()
            {
                return _metadataMapFile;
            }

            void IDisposable.Dispose()
            {
                // Release any resources here

                _metadataMapFile.Dispose();
            }

            /// <summary>
            /// Path to the folder containing all metadata and content data for the table, if the table is in persistent mode
            /// </summary>
            internal string DataFolder { get; private set; }

            /// <summary>
            /// Forces the persistent metadata to flush its content to the file
            /// </summary>
            internal void FlushMetadata()
            {
                if (!Native.FlushFileBuffers(_metadataFileStream.SafeFileHandle))
                    throw new Win32Exception();

                _metadataFileStream.Flush(flushToDisk: true);
            }
        }

        /// <summary>
        /// Global Unique object identifier generator. Keeps track of already generated or explicitly registered identifiers
        /// </summary>
        public static class UID
        {
            private static SortedSet<int> uids = new SortedSet<int>();
            private static Random randomizer = new Random();

            /// <summary>
            /// Explicitly register object identifier in this UID container. This identifier will not be used for other NewUid calls
            /// </summary>
            /// <param name="uid"></param>
            public static int RegisterUid(int uid)
            {
                if (!uids.Contains(uid))
                    uids.Add(uid);
                else
                    throw new InvalidOperationException("UID: Unique object identier [" + uid + "] already registered");

                return uid;
            }

            /// <summary>
            /// Generates unique object identifier for this UID container scope
            /// </summary>
            /// <returns></returns>
            public static int NewUid()
            {
                int uid;
                do
                {
                    // Keep generating new id if the previous was already present in the list of IDs.
                    // This will less likely to happen
                    uid = randomizer.Next(int.MaxValue / 2, int.MaxValue);

                    // uid = (int)(uint)DateTime.UtcNow.ToFileTimeUtc();
                }
                while (uids.Contains(uid));

                UID.RegisterUid(uid);

                return uid;
            }
        }
    }
}
