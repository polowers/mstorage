﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DigitalToolworks.Data.MStorage
{
    partial class Query
    {
        /// <summary>
        /// Represent abstract syntacs filter clause tree
        /// </summary>
        public sealed class FilterClauseTree
        {
            readonly ParameterExpression _paramRecord = Expression.Parameter(typeof(Query.View), "record");
            readonly Dictionary<string, string> _referenceGroups;
            readonly Regex _regexEvaluator;
            readonly ITable _table;
            /// <summary>
            /// Columns involved in the tree
            /// </summary>
            readonly List<MTable.Column> _columns;

            // This pattern allows certain characters to be present in the left operands and skips the parenthesis characters
            readonly string _matchPattern =
            @"(?<binaryOperator>\s*(?<leftOperand>[\w\d\s-\@\!\~\%\$\^\+\=\*\.\#\[\]]+?)\s+(?<operation>" + BinaryRecordFieldEvaluator.SupportedBinaryOperations + @")\s+'?(?<rightOperand>.+?)'?)[\)]*(?:\s*$|\s+(?<unary>or|and|not)\s+)";
            //@"(?<binaryOperator>\s*(?<leftOperand>(?: \[.*\] | \w+ | \w+\(.*\) )+?)\s+(?<operation> " + BinaryRecordFieldEvaluator.SupportedBinaryOperations + @" )\s+'?(?<rightOperand>.+?)'?)[\)]*(?:\s*$|\s+(?<unary>or|and|not)\s+)";
            //@"(?<binaryOperator>\s*(?<leftOperand>(?: \[.*\] | \w+ | \w+\(.*\) )+?)\s+(?<operation> "+ BinaryRecordFieldEvaluator.SupportedBinaryOperations + @" )\s+'?(?<rightOperand>.+?)'?) | (?<functionCall>\w+\(.*\))[\)] *(?:\s*$|\s+(?<unary>or|and|not)\s+)";

            readonly string _matchPattern2 =
                            @"(?<binaryOperator> (?<leftOperand>.+?)\s+(?<operation>" + BinaryRecordFieldEvaluator.SupportedBinaryOperations + @")\s+'?(?<rightOperand>.+?)'?)
                              (?:\s*$|\s+(?<unary>or|and|not)\s+)";
            readonly IFunctionResolver _functionResolver;

            /// <summary>
            /// Represent abstract syntacs filter clause tree
            /// </summary>
            /// <param name="table"></param>
            /// <param name="clauseExpression"></param>
            public FilterClauseTree(ITable table, string clauseExpression, IFunctionResolver resolver)
            {
                // handle the parenthesis separately. 
                // for each binary operator, check for presence of parenthesis characters in the left and right operands
                // increase , decrease the nesting level counts. The final count has to be zero. 
                // once parenthesis is open within binary operator, it is expected to be closed after the last right operand in some of the following operators. otherwise, error occurs

                _functionResolver = resolver;
                _table = table;
                _columns = new List<MTable.Column>();
                _referenceGroups = new Dictionary<string, string>();

                this.GroupedClauseExpression = clauseExpression;
                this.ClauseExpression = clauseExpression;

                _regexEvaluator = new Regex(_matchPattern, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);

                var groupSpans = GetUnaryExpressionSpans(clauseExpression);

                // reorganized expressions, associsted with span group.
                // order the groups, by inner most, at top.

                var unarySpans = groupSpans
                    .OrderBy(o => o.Item2)
                    .Select(span => new { Start = span.Item1, Length = span.Item2 });

                //// debug: just print the spans expressions
                //foreach (var span in unarySpans)
                //    Console.WriteLine(clauseExpression.Substring(span.Start, span.Length));

                // create reference groups of expressions. 
                // replace each expression with special group token [ #GID# eq N ]

                int groupId = 0;
                foreach (var span in unarySpans)
                {
                    string gId = string.Format("[#GID#] eq {0}", groupId);

                    string groupedExpression = clauseExpression.Substring(span.Start, span.Length);

                    foreach (var reference in _referenceGroups)
                        groupedExpression = groupedExpression.Replace(reference.Value, reference.Key);

                    _referenceGroups.Add(gId, groupedExpression);
                    this.GroupedClauseExpression = this.GroupedClauseExpression.Replace(groupedExpression, gId);
                    groupId++;
                }

                // build the tree expression
                TreeExpression = ToCriteriaExpression();
            }

            /// <summary>
            /// Represents filter clause tree LINQ expression
            /// </summary>
            internal Expression TreeExpression { get; private set; }

            /// <summary>
            /// Gets the table columns involved into the clause tree
            /// </summary>
            internal IReadOnlyList<MTable.Column> Columns { get { return _columns; } }

            public override string ToString()
            {
                return this.ClauseExpression;
            }

            /// <summary>
            /// The root grouped reference clause expression
            /// </summary>
            internal string GroupedClauseExpression { get; private set; }

            /// <summary>
            /// The original filter clause expression
            /// </summary>
            public string ClauseExpression { get; private set; }

            /// <summary>
            /// Returns expression associated with the groupID
            /// </summary>
            /// <param name="groupID"></param>
            /// <returns></returns>
            internal string this[string groupID]
            {
                get
                {
                    return _referenceGroups[groupID];
                }
            }

            /// <summary>
            /// Builds groups tree from the nested parenthesis in the expression
            /// </summary>
            /// <param name="expression"></param>
            private static List<Tuple<int, int>> GetUnaryExpressionSpans(string expression)
            {
                // Results in list of spans (start and text locations), for every nested parenthesis
                List<Tuple<int, int>> spans = new List<Tuple<int, int>>();

                // contains stack of parenthesis start indexes
                Stack<int> nestingLevel = new Stack<int>();

                // count the parenthesis nesting level, determine the expression group span.
                // find the opening and closing parenthesis.
                // ignore parenthesis characters if they are located within escaped regions.
                // escaped regions are marked with single quote character ', or open and closed square brackets [ ] 

                bool withinQuoteEscapedRegion = false;
                bool withinBracketEscapedRegion = false;
                int escapedRegionCount = 0; // number of escape characters counted

                for (int index = 0; index < expression.Length; index++)
                {
                    char @char = expression[index];

                    // toggle escaped region
                    if (@char == '\'' && !withinBracketEscapedRegion)
                        withinQuoteEscapedRegion = !withinQuoteEscapedRegion;

                    // skip escaped regions
                    if (withinQuoteEscapedRegion)
                        continue;

                    if (!withinBracketEscapedRegion && !withinQuoteEscapedRegion)
                    {
                        // toggle escaped region
                        if (@char == '[')
                            withinBracketEscapedRegion = true;
                    }
                    else if(withinBracketEscapedRegion && !withinQuoteEscapedRegion)
                    {
                        if (@char == ']')
                            withinBracketEscapedRegion = false;
                        else
                            continue;  // skip escaped regions
                    }

                    // open parenthesis, push in stack
                    if (@char == '(')
                        nestingLevel.Push(index + 1);
                    else // close parenthesis, pop stack and add span
                        if (@char == ')')
                        {
                            if (nestingLevel.Count == 0)
                                throw new InvalidOperationException("Syntax error in the expression. Possible unbalanced parenthesis");

                            // Group closed, so use the start and this (end) indes to add Span
                            int start = nestingLevel.Pop();
                            int length = index - start;

                            spans.Add(Tuple.Create<int, int>(start, length));
                        }
                }

                // If the stack is not empty, syntax error
                if (nestingLevel.Count > 0)
                    throw new InvalidOperationException("Syntax error in the expression. Possible unbalanced parenthesis");

                return spans;
            }

            private static int TrimNesting(ref string expression)
            {
                // Determines the open parenthesis, calculates the nesting level and remove the new string
                // count the parenthesis nesting level

                int levelCount = 0;

                int open = -1;
                int close = -1;
                do
                {
                    open = expression.IndexOf('(', open + 1);
                    if (open > -1)
                    {
                        expression = expression.Substring(open + 1);
                        levelCount++;
                    }

                    close = expression.IndexOf(')', Math.Min(expression.Length, close + 1));
                    if (close > -1)
                    {
                        // Group closed, so use the start and this (end) indes to add Span
                        //  int startIndex = nestingLevel.Pop();

                        expression = expression.Substring(0, close);

                        int endIndex = close;
                        levelCount--;
                    }

                } while (open >= 0 || close >= 0);

                return levelCount;
            }

            /// <summary>
            /// Builds LINQ expression tree from the criteria
            /// </summary>
            /// <returns></returns>
            private Expression ToCriteriaExpression()
            {
                var expressionTreeBody = this.BuildExpressionFromReferencedClauseTree(this.GroupedClauseExpression);

                this.TreeExpression = expressionTreeBody;

                return expressionTreeBody;
            }

            /// <summary>
            /// Creates compiled lambda from the criteria expression tree
            /// </summary>
            /// <returns></returns>
            public Func<Query.View, bool> ToCriteriaLambda()
            {
                var compiled = Expression.Lambda<Func<Query.View, bool>>(this.TreeExpression, _paramRecord).Compile();
                return compiled;
            }

            private Expression BuildExpressionFromReferencedClauseTree(string groupedClause)
            {
                // Trim the trailing and leading parenthsis
                var match = _regexEvaluator.Match(groupedClause);

                Expression expressionBody = null;

                string previousUnary = string.Empty;

                while (match.Success)
                {
                    // find the span, where the left operand is after its start and right operand is before spand end

                    var binOp = match.Groups["binaryOperator"];

                    var leftOp = match.Groups["leftOperand"];
                    var rightOp = match.Groups["rightOperand"];

                    var op = match.Groups["operation"];
                    var unary = match.Groups["unary"];

                    // Process inner expressions (recursive). Extract the reference group from the left operand or right value operand.

                    if ("[#GID#]".Equals(leftOp.Value))
                    {
                        string expression = this[string.Format("[#GID#] eq {0}", rightOp)];

                        var innerBody = this.BuildExpressionFromReferencedClauseTree(expression);

                        if (expressionBody != null)
                        {
                            if (previousUnary.Equals("OR", StringComparison.InvariantCultureIgnoreCase))
                                expressionBody = Expression.OrElse(expressionBody, innerBody);
                            else
                                if (previousUnary.Equals("AND", StringComparison.InvariantCultureIgnoreCase))
                                expressionBody = Expression.AndAlso(expressionBody, innerBody);
                            else
                                if (previousUnary.Equals("NOT", StringComparison.InvariantCultureIgnoreCase))
                                expressionBody = Expression.Not(Expression.OrElse(expressionBody, innerBody));
                            else
                                throw new NotImplementedException("Unary [" + previousUnary + "] not implemented");
                        }
                        else
                            expressionBody = innerBody;
                    }
                    else
                    {
                        //// Check the right operand (value) for having group references and if so, extract the group value from it.
                        ///  var innerBody = this.BuildExpressionFromReferencedClauseTree(rightOp.Value);                        

                        // create binary operation evaluator and extract the associated column
                        var evaluator = BinaryRecordFieldEvaluator.Parse(_paramRecord, operatorText: binOp.Value, table: _table, resolver: _functionResolver);
                        _columns.AddRange(evaluator.Columns);

                        Expression binaryFieldExpression = evaluator.Body();

                        if (expressionBody != null)
                        {
                            if (previousUnary.Equals("OR", StringComparison.InvariantCultureIgnoreCase))
                                expressionBody = Expression.OrElse(expressionBody, binaryFieldExpression);
                            else if (previousUnary.Equals("AND", StringComparison.InvariantCultureIgnoreCase))
                                expressionBody = Expression.AndAlso(expressionBody, binaryFieldExpression);
                            else if (previousUnary.Equals("NOT", StringComparison.InvariantCultureIgnoreCase))
                                expressionBody = Expression.Not(Expression.OrElse(expressionBody, binaryFieldExpression));
                            else
                                throw new InvalidOperationException("Unary [" + previousUnary + "] not implemented");
                        }
                        else
                            expressionBody = binaryFieldExpression;
                    }

                    previousUnary = unary.Value;

                    match = match.NextMatch();
                }

                if (expressionBody != null)
                    return expressionBody;
                else
                    throw new InvalidOperationException("Invalid clause expression. Possible syntax error or unsupported operator. \r\nExpression: " + groupedClause + " ");
            }

            /// <summary>
            /// Represents records values evaluator for performing binary logical operations
            /// </summary>
            public class BinaryRecordFieldEvaluator
            {
                string _valueText;
                Func<dynamic, Query.View, bool> _binaryComparer;
                readonly MTable.MatchMode _operation;
                readonly MTable.Column _columnLeft;
                readonly ICollection<MTable.Column> _columns = new List<MTable.Column>();

                static string _supportedOps;

                Expression<Func<View, dynamic>> _expressFunctionCallLambda;
                ParameterExpression _parameterRecord = null;

                /// <summary>
                /// Gets list of supported binary operations
                /// </summary>
                internal static string SupportedBinaryOperations
                {
                    get
                    {
                        if (_supportedOps != null)
                            return _supportedOps;

                        // list of supported operations (used in the Regex bellow, eg: eq|ne|gt|ge|lt|le|add|sub|mul|div|mod ).
                        // extract the binary operation alialses from MatchMode BinaryOperatorAlias attributes.

                        var aliases = Enum.GetNames(typeof(MTable.MatchMode))
                            .SelectMany(n => typeof(MTable.MatchMode)
                                .GetField(n).GetCustomAttributes(false)
                                .OfType<MTable.BinaryOperatorAliasAttribute>()
                                .Select(a => a.Alias).Where(a => !string.IsNullOrWhiteSpace(a)).ToArray());

                        return _supportedOps = string.Join("|", aliases);
                    }
                }             

                private BinaryRecordFieldEvaluator(ParameterExpression paramRecord, ITable table, MTable.Column column, MTable.MatchMode operation, string valueText, bool isExpression, IFunctionResolver resolver)
                {
                    _operation = operation;
                    _columnLeft = column;
                    _valueText = valueText;
                    _parameterRecord = paramRecord;
                    _columns.Add(column);


                    // if function resolver is provided, check the value for presence of function calls. 
                    // extract the function call and try to resolve it, associating the comparableValue with the action function delegate resolved.
                    //
                    // preprocess the value, ensure it is of correct data type, convert if necesery
                    // determine the value type and convert to the target type if necesery 
                    // Note: handle the AnyOf or NoneOf operator value here, by extracting multiple-values from the value passed in form (val1, val2, val3)

                    if (!isExpression)  // Non expression, which means constant value of text, number or NULL (possibly any future additional keywords)
                    {
                        // Perform necesery value conversion if the column data types is not directly compareble to the value passed.
                        object comparableValue = valueText;  // default

                        if (operation == MTable.MatchMode.AnyOfList || operation == MTable.MatchMode.NoneOfList)
                        {
                            if (!TryResolveFunction(valueText, out comparableValue, resolver, scalarValueReturn: false))
                            {
                                // convert the input text comma separated values, into list of converted values
                                string[] comparableItems = valueText.Trim().Split(',');
                                comparableValue = comparableItems.Select(v => ConvertValue(v)).Distinct().ToArray();
                            }
                            else
                                comparableValue = ((Func<IEnumerable<object>>)comparableValue)().Select(v => ConvertValue(v.ToString())).Distinct();  // call the function delegate
                        }
                        else
                        {
                            if (!TryResolveFunction(valueText, out comparableValue, resolver, scalarValueReturn: true))
                                comparableValue = ConvertValue(valueText);
                            else
                                comparableValue = ConvertValue(((Func<object>)comparableValue)().ToString());  // call the function delegate
                        }

                        _binaryComparer = Extensions.GetWhereComparer(operation, record => comparableValue);
                    }
                    else
                    {
                        // Parse the expression, which may be function call, or columnname

                        // 1) try parse the column name in expression: [column Name Any character], or ColumnNameAlphaNumeric
                        // 2) try parse function call

                        if (TryParseColumnReference(table, valueText))
                        {
                            var columnRight = Columns.Last();
                            _binaryComparer = Extensions.GetWhereComparer(_operation, record => record[columnRight]);
                        }
                        else if (!TryParseFunctionCall(table, valueText, resolver))
                        {
                            object value;

                            if (operation == MTable.MatchMode.AnyOfList || operation == MTable.MatchMode.NoneOfList)
                            {
                                if (!TryParseListValue(out value, valueText))
                                    throw new NotImplementedException($"Expression [{valueText}] not implemented yet");
                            }
                            else if (!TryParseValue(out value, valueText))
                                throw new NotImplementedException($"Expression [{valueText}] not implemented yet");

                            _binaryComparer = Extensions.GetWhereComparer(_operation, record => value);
                        }                             
                    }
                }

                /// <summary>
                /// Tries to parse right operand value which may contain column reference
                /// </summary>
                /// <param name="valueText"></param>
                /// <returns></returns>
                private bool TryParseColumnReference(ITable table, string valueText)
                {
                    var match = Regex.Match(valueText, @"^(  \s*((\[{1}(?<columnName>.+?)\]{1}){1} |  (?<columnName>\w+){1})\s*  )$", RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase);
                    if (match.Success)
                    {
                        // Found column name, so execute the default record column value lookup function
                        // Get reference of the table column object and use that for the function. Temp: use the field name to do slower column lookup.

                        string columnName = match.Groups["columnName"].Value;
                        MTable.Column columnRight;
                        if (!table.Columns.Contains(columnName))
                            throw new KeyNotFoundException($"Column [{columnName}] not found in table [{table.Name}]");
                        else
                            columnRight = table.Columns[columnName].Single();

                        _columns.Add(columnRight);  // << adds this column too

                        if (_columnLeft.Name.Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                            throw new InvalidOperationException($"Invalid column reference: it is not allowed to have the same column [{columnName}] in both left and right side of binary operators");

                       // _binaryComparer = Extensions.GetWhereComparer(_operation, record => record[columnRight]);

                        return true;
                    }

                    return false;
                }

                /// <summary>
                /// Tries to parse the right operand value as function call containing zero or more parameters
                /// </summary>
                /// <param name="valueText"></param>
                /// <returns></returns>
                private bool TryParseFunctionCall(ITable table, string valueText, IFunctionResolver resolver)
                {
                    // This is the old arguments extraction pattern
                    // (?<argument> \w+ | '.*' | \[.*\] )\s*\,?\s*  
                    //

                    var match = Regex.Match(valueText, @"^( \s*((?<function>\w+) \((?<arguments> \s*( .* )*)\) ){1}\s* )$", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
                    if(match.Success)
                    {
                        // The arguments values should be determined using the same rules for the right operand value. 
                        // They may be constants of NULL, number, quotes text, or expressions like column reference, function or else.

                        // Find the function delegate using the resolver, 
                        // or try to find the function in the current running library, 
                        // or build expression tree for the function call

                        string functionName = match.Groups["function"].Value;

                        var matchArguments = Regex.Matches(match.Groups["arguments"].Value, @"(?<arg>[^,()]+((?:\((?>[^()]+|\((?<open>)|\)(?<-open>))*\)))? | \(\s*\))", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);

                        var arguments = new List<object>();

                        foreach (Match m in matchArguments)
                            if (m.Success && !string.IsNullOrWhiteSpace(m.Value))
                            {
                                // Convert the arguments texts into native values: If the argument is column name, obtain column reference.   
                                if (TryParseColumnReference(table, m.Value.Trim()))
                                    arguments.Add(this.Columns.Last());
                                else
                                {
                                    object value;
                                    if (TryParseValue(out value, m.Value.Trim()))
                                        arguments.Add(value);
                                    else
                                        arguments.Add(ConvertValue(m.Value.Trim()));
                                }
                            }

                        // The record evaluator delegate, takes one mandatory record parameter and the additional user provided parameters.     

                        // Find the method to evaluate, using the resolver instance. 
                        // - if the method found is static, create static method call expression, passing the type of resolver only. 
                        // - for instance methods, pass the resolver instance expression

                        System.Reflection.MethodInfo miFunction = resolver.GetType()
                            .FindMembers(System.Reflection.MemberTypes.Method, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.IgnoreCase | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Instance, (mi, criteria) => mi.Name.Equals(functionName, StringComparison.InvariantCultureIgnoreCase), null)
                            .OfType<System.Reflection.MethodInfo>().FirstOrDefault();
                        if (miFunction == null)
                            throw new InvalidOperationException($"Function {functionName} not found by the function provider");

                        MethodCallExpression expressCall;
                        if (miFunction.IsStatic)
                            expressCall = Expression.Call(miFunction, arguments: new Expression[] { _parameterRecord, Expression.Constant(arguments.ToArray()) });
                        else
                            expressCall = Expression.Call(Expression.Constant(resolver), miFunction, arguments: new Expression[] { _parameterRecord, Expression.Constant(arguments.ToArray()) });

                        _expressFunctionCallLambda = Expression.Lambda<Func<View, dynamic>>(expressCall, _parameterRecord);

                        var funcDelegate = _expressFunctionCallLambda.Compile();

                       // var funcDelegate = (Func<View, dynamic>)miFunction.CreateDelegate(typeof(Func<View, dynamic>), null);                       

                        _binaryComparer = Extensions.GetWhereComparer(_operation, record => ConvertValue(funcDelegate(record)));

                        return true;
                    }

                    return false;
                }

                private bool TryParseListValue(out object value, string valueText)
                {
                    value = null;

                    // Try parse multiple list value
                    // Split comma separated items
                    string[] comparableItems = valueText.Trim().Split(',');
                    var list = comparableItems.Select(v => ConvertValue((v.Trim().StartsWith("'") && v.Trim().EndsWith("'") ? v.Trim().Trim('\'') : v)))
                    .Distinct()
                    .ToArray();
                    if (list.Length > 0)
                    {
                        value = list;
                        return true;
                    }

                    return false;
                }
                /// <summary>
                /// Tries to parse the operand constant value from text
                /// </summary>
                /// <param name="value1"></param>
                /// <param name="value2"></param>
                /// <returns></returns>
                private bool TryParseValue(out object value, string valueText)
                {
                    // The value can be NULL, or Text (wrapped in single quotes), or Numeric

                    value = null;

                    var match = Regex.Match(valueText, @"^\s*(('{1}(?<valueText>.*)'{1}) | (?<valueNull>NULL) | (\[{1}(?<valueExpression>.+?)\]{1}) | (?<valueNumber>-?(\d+|\d+(,\d+)*(\.\d+(e\d+)?)* )))\s*$", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
                    if (!match.Success)
                        return false;

                    if (match.Groups["valueText"].Success)
                    {
                        value = ConvertValue(match.Groups["valueText"].Value);
                    }
                    else
                       if (match.Groups["valueNumber"].Success)
                        value = ConvertValue(match.Groups["valueNumber"].Value);
                    //else
                    //   if (match.Groups["valueExpression"].Success)
                    //{
                    //    // Split comma separated items
                    //    string[] comparableItems = match.Groups["valueExpression"].Value.Trim().Split(',');
                    //    value = comparableItems.Select(v => ConvertValue(v)).Distinct().ToArray();
                    //}
                    else
                       if (match.Groups["valueNull"].Success)
                        value = ConvertValue(string.Empty);
                    else
                        throw new InvalidOperationException($"Invalid binary operand value provided. Value = {value}");

                    return true;
                }

                //public LambdaExpression GetInnerLambda()
                //{
                //    ParameterExpression innerParam = Expression.Parameter(
                //                                        typeof(object),
                //                                        "innerParam");
                //    return Expression.Lambda(
                //            Expression.Block(
                //                Expression.Call(null,
                //                        typeof(ForSO).GetMethod("Write"),
                //                        Expression.Constant("Inner lambda start")),
                //                Expression.Call(null,
                //                        typeof(ForSO).GetMethod("Write"),
                //                        innerParam),
                //                Expression.Call(null,
                //                        typeof(ForSO).GetMethod("Write"),
                //                        Param),
                //                Expression.Call(null,
                //                        typeof(ForSO).GetMethod("Write"),
                //                        Expression.Constant("Inner lambda end"))
                //            ),
                //            innerParam
                //        );
                //}


                /// <summary>
                /// Tries to parse function call and resolve the function delegate. 
                /// If no resolver is provided, this method returns false. 
                /// Returns true only if function is found and assigned to the comparableValue.
                /// </summary>
                /// <param name="valueText"></param>
                /// <param name="comparableValue"></param>
                /// <returns></returns>
                static bool TryResolveFunction(string valueText, out object comparableValue, IFunctionResolver resolver, bool scalarValueReturn)
                {
                    comparableValue = valueText;  // default

                    if (resolver == null) return false;

                    // Parse the function name. The current format is, that the value text have to start and end with dollar sign. for eg: $MyFunction$
                    if(valueText.StartsWith("$") && valueText.EndsWith("$") && valueText.Length > 3)
                    {
                        string functionName = valueText.Substring(1).Substring(0, valueText.Length - 2);
                        if (scalarValueReturn)
                            comparableValue = resolver.ResolveScalarFunction(functionName);
                        else
                            comparableValue = resolver.ResolveCollectionFunction(functionName);

                        return true;
                    }

                    return false;
                }

                private bool TryResolveFunction(string valueText)
                {
                    throw new NotImplementedException();
                }

                /// <summary>
                /// Converts the input value text into the correct target type value
                /// </summary>
                /// <param name="valueText"></param>
                /// <returns></returns>
                private dynamic ConvertValue(dynamic valueText)
                {
                    //  Convert the value text into value of the target column type.
                    //  - for Text type: Empty value text is considered as NULL. Note: figureout how to handle empty value text differently.
                    //  - for Other types: Empty or whitespace value text are considered as NULL.

                    if (valueText is string)

                        switch (_columnLeft.DataType)
                        {
                            case MTable.Column.DataTypes.Text:
                                return string.IsNullOrEmpty(valueText) ? null : valueText;   // trim empty spaces and single quotes (if present)

                            case MTable.Column.DataTypes.Long:
                                return string.IsNullOrWhiteSpace(valueText) ? null : (object)Convert.ToInt64(valueText.Trim().Trim('\''));

                            case MTable.Column.DataTypes.Number:
                                return string.IsNullOrWhiteSpace(valueText) ? null : (object)Convert.ToDouble(valueText.Trim().Trim('\''));

                            case MTable.Column.DataTypes.Date:
                                return string.IsNullOrWhiteSpace(valueText) ? null : (object)Convert.ToDateTime(valueText.Trim().Trim('\''));

                            default:
                                throw new NotSupportedException($"Searching column with type {_columnLeft.DataType} is not supported yet");
                        }

                    return valueText;
                }

                /// <summary>
                /// Columns associated with this evaluator. Usually, the left operand and possibly columns present in the right expression operand
                /// </summary>
                internal MTable.Column[] Columns { get { return _columns.ToArray(); } }

                public bool Evaluate(Query.View record)
                {
                    // if Ordinal is present, query the record by ordinal (faster). otherwise, use the field name
                    //var value = _column != null ? record[_column] : record[_fieldName];

                    var value = record[_columnLeft];
                    return _binaryComparer(value, record);
                }

                /// <summary>
                /// Parses text represented binary operator, in form:  FieldName [Operation] 'Value'
                /// </summary>
                /// <param name="operatorText"></param>
                /// <returns></returns>
                public static BinaryRecordFieldEvaluator Parse(ParameterExpression paramRecord, string operatorText, ITable table, IFunctionResolver resolver)
                {
                    // Regex patters, which match left , operation, right operands
                    // Example input: name EQ '12EQ'
                    // http://regexhero.net/tester/?id=0a26931f-aaa3-4fa0-9fc9-1a67d34c16b3

                    string regexOperator = string.Format(@"\[?(?<LeftOperand>.+?)\]?\s+(?<Operator>{0})\s+ '{1}(?<RightOperand>.*)'{1}(?:\s*$|\s+)", SupportedBinaryOperations, "{1}");

                    // The folowing expression has special named value groups, depending on the operant type match
                    regexOperator = $@"\[?(?<LeftOperand>.+?)\]?\s+(?<Operator>{SupportedBinaryOperations})\s+ (?<RightOperand> ('{{1}}(?<valueText>.*)'{{1}}) | (?<valueNull>NULL) | (\[{{1}}(?<valueExpression>.+?)\]{{1}}) | (?<valueNumber>-?(\d+|\d+(,\d+)*(\.\d+(e\d+)?)*)) ) (?:\s*$|\s+)";

                    var regex = new Regex(regexOperator, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
                    var match = regex.Match(operatorText);
                    if (!match.Success)
                        throw new InvalidOperationException("Invalid clause binary operator or value " + operatorText + "\r\nSupported operations are:\r\n" + SupportedBinaryOperations + "");

                    var fieldName = match.Groups["LeftOperand"].Value;
                    var rightOperand = match.Groups["RightOperand"].Value;

                    var @operator = FindOperator(match.Groups["Operator"].Value);

                    // ### PARSE RULES FOR THE RIGHT OPERAND VALUE ###
                    //
                    // Certain rules determines value of the right operand, including.
                    // For Text columns:
                    // - the operand is considered as plaint value, if it starts and ends in single quotes , eg: 'my text'.
                    // For Number columns:
                    // - the operand is considered as plaint value, if number is present without any quotes.
                    // For all above:
                    // - the operand can be function call (in certain format explained bellow), column reference or expression. Column references must start and end with square brackets, eg: [colName].
                    // null value can be passed using special keywork NULL (no quotes)
                    //
                    // Function call parse rules:
                    // - function calls must be alphanumeric function name, followed by arguments parenthesis with zero or more comma separated arguments. For eg:  myFunction(), myOtherFunc(5, '15', [col1])
                    // - an function argument follows the same rules as above.. and can be another function call with arguments.

                    string operandValue = rightOperand;
                    bool isExpression = false;  // <<= Indicates if the right operand is expression, which is wrapped in square brackets [expression here], or constant value 'value', or nummer 1234.5 or NULL

                    if (match.Groups["valueText"].Success)
                        operandValue = match.Groups["valueText"].Value;
                    else
                        if (match.Groups["valueNumber"].Success)
                        operandValue = match.Groups["valueNumber"].Value;
                    else
                        if (match.Groups["valueExpression"].Success)
                    {
                        operandValue = match.Groups["valueExpression"].Value;
                        isExpression = true;
                    }
                    else
                        if (match.Groups["valueNull"].Success)
                        operandValue = string.Empty;
                    else
                        throw new InvalidOperationException($"Invalid binary operand value provided. Value = {operandValue}");

                    var column = table.Columns[fieldName].Single();

                    return new BinaryRecordFieldEvaluator(paramRecord, table, column, @operator, operandValue, isExpression, resolver);
                }

                /// <summary>
                /// Finds the binary operator match mode from keyword
                /// </summary>
                /// <param name="operatorKeyword"></param>
                /// <returns></returns>
                private static MTable.MatchMode FindOperator(string operatorKeyword)
                {
                    // find matchMode object, from operator keyword.
                    var type = typeof(MTable.MatchMode);

                    foreach (var name in Enum.GetNames(type))
                        foreach (var attribute in type.GetField(name).GetCustomAttributes(false).OfType<MTable.BinaryOperatorAliasAttribute>())
                            foreach (var keyword in attribute.Alias.Split('|'))
                                if (operatorKeyword.Equals(keyword, StringComparison.InvariantCultureIgnoreCase))
                                    return (MTable.MatchMode)Enum.Parse(type, name, true);
                    throw new InvalidOperationException("Binary operator " + operatorKeyword + " not supported");
                }

                public override string ToString()
                {
                    return string.Format("[{0} {1} {2}]", _columnLeft.Name, _operation, _valueText);
                }

                /// <summary>
                /// Gets expression body for this evaluator
                /// </summary>
                /// <returns></returns>
                internal Expression Body()
                {
                    // create a parameter expression that can be passed to the rules
                    var pEval = Expression.Constant(this, typeof(BinaryRecordFieldEvaluator));
                    var mi = this.GetType().GetMethod("Evaluate");
                    
                    return Expression.Call(pEval, mi, _parameterRecord);
                }

                public Expression<Func<Query.View, bool>> ToExpression(ParameterExpression paramRecord)
                {
                    _parameterRecord = paramRecord;

                    if (_parameterRecord == null)
                        _parameterRecord = Expression.Parameter(typeof(Query.View), "record");

                    Expression body = Body();

                    // get the expression body. This consists of all subgroups and rules
                    // Expression body = null; // GetExpressionFromSubgroup(this, t, param); 

                    return Expression.Lambda<Func<Query.View, bool>>(body: body, name: "FieldEvaluatorLambda", parameters: new[] { _parameterRecord });
                }
            }

        }

       
        /// <summary>
        /// Interface allowing resolving functions used in filter clause query
        /// </summary>
        public interface IFunctionResolver
        {
            /// <summary>
            /// Resolves function delegate which returns single value
            /// </summary>
            /// <param name="functionName"></param>
            /// <returns></returns>
            Func<object> ResolveScalarFunction(string functionName);

            /// <summary>
            /// Resolves function delegate which returns collection of values
            /// </summary>
            /// <param name="functionName"></param>
            /// <returns></returns>
           Func<IEnumerable<object>> ResolveCollectionFunction(string functionName);

        }
    }
}
