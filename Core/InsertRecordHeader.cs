﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Runtime;
using System.Runtime.CompilerServices;

namespace DigitalToolworks.Data
{
    [DebuggerDisplay("Header[ {Print()} ]")]
    public class InsertRecordHeader
    {
        /// <summary>
        /// Maximum number of fields supported
        /// </summary>
        public const int MaxFieldsCount = 1024;
        /// <summary>
        /// Maximum size of data supported, in bytes (1024Kb)
        /// </summary>
        public const int MaxDataSize = 1024 * 1024;
        /// <summary>
        /// Sequentially aligned data of all fields
        /// </summary>
        public byte[] Data = new byte[MaxDataSize];
        /// <summary>
        /// Number of bytes between each field in the data
        /// </summary>
        public int[] Offsets = new int[MaxFieldsCount];
        /// <summary>
        /// Individual field data sizes
        /// </summary>
        public int[] Lengths = new int[MaxFieldsCount];
        /// <summary>
        /// Total data size of all fields
        /// </summary>
        public int Length;
        /// <summary>
        /// Total number of fields in the header
        /// </summary>
        public int Count;

        static readonly ConcurrentDictionary<int, InsertRecordHeader> _cached = new ConcurrentDictionary<int, InsertRecordHeader>();
        static readonly BlockingCollection<InsertRecordHeader> _headerPool = new BlockingCollection<InsertRecordHeader>(new ConcurrentBag<InsertRecordHeader>(), 2);

        static readonly Common.ObjectPool<InsertRecordHeader> _headers = new Common.ObjectPool<InsertRecordHeader>(() =>
        {
            return new InsertRecordHeader();
        });

        /// <summary>
        /// The record position within the storage
        /// </summary>
        public long? Position;

        static InsertRecordHeader()
        {
            // allocate the row-header objects
            for (int i = 0; i < _headerPool.BoundedCapacity; i++)
                _headerPool.Add(new InsertRecordHeader());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        private static InsertRecordHeader GetCached()
        {
            var threadId = System.Threading.Thread.CurrentThread.ManagedThreadId;

            InsertRecordHeader header = null;
            if (!_cached.TryGetValue(threadId, out header))
                _cached.TryAdd(threadId, header = new InsertRecordHeader());

            return header;
        }

        /// <summary>
        /// Prints the record header data content for debug purposes
        /// </summary>
        /// <returns></returns>
        public string Print()
        {
            using (var ws = new StringWriter())
            {
                this.Print(ws);
                return ws.ToString();
            }
        }
        /// <summary>
        /// Prints the record header data content for debug purposes
        /// </summary>
        /// <param name="writer"></param>
        public void Print(StringWriter writer)
        {
            for (int offset = 0, idx = 0; idx < this.Count; idx++)
            {
                int length = this.Lengths[idx];
                string field = IDataStorage.Encoder.GetString(this.Data, offset, length);
                writer.Write("[");
                writer.Write(field);
                writer.Write("]  ");
                offset += length;
            }
        }
        public override string ToString()
        {
            return string.Format("Header: position:{0}, count:{1}, length:{2}", Position, Count, Length);
        }

        /// <summary>
        /// Gets free row header from the object pool. If no headers available, it will block untill available header gets released
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public static InsertRecordHeader GetFree()
        {
            // create header row on demand, only if the bounded capacity is not reached
            // if (_headerPool.Count < _headerPool.BoundedCapacity)
            //     return new RowHeader();

            return _headers.GetObject();
            //return _headerPool.Take();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int ParseCSVLine(byte[] line, int index, int length, ref int[] positions, ref int[] lengths, byte delimiter, byte quote)
        {
            unsafe
            {
                fixed (byte* pLine = &line[index])
                {
                    byte previous = (byte)'\0';
                    int field = 0;       // field idx
                    int fieldStart = 0;  // field start position in data.
                    int fieldEnd = 0;    // field end position in data
                    int advanceBy = 0;   // indicator for advancing trailing positions (inclusing number of control characters to skip at end fields)

                    // positions indicating if leading or traling quotes were found
                    int afterQuote = int.MaxValue;
                    int lastQuote = -1; // last quote position

                    bool quotedContent = false;  //  flag indicating if the current is within quoted content (content which has started with quote)

                    var end = pLine + length;

                    // Scan all characters in the current line. Current contains the current character
                    for (byte* pCurrent = pLine; pCurrent < end; pCurrent++)
                    {
                        byte current = *pCurrent;

                        // determine if the quotation should be incuded in the field data, or skipped as control character:
                        // if the current control character is quotation and the no previous control character found or the previous was delimiter (neighbour), it means that we start text content.

                        if(current == (byte)'\r')
                        {
                            // Skip trailing carrige returns
                            fieldStart++;
                        }
                        else if (current == quote)
                        {
                            // pass the end of line (possible end of quoted content), or after delimiter cc and not in quoted text
                            if (previous == (byte)'\0' || (previous == delimiter && !quotedContent))
                            {
                                afterQuote = (int)(pCurrent - pLine) + 1; // mark the position after the quote      

                                // start quoted content
                                quotedContent = true;
                            }

                            lastQuote = (int)(pCurrent - pLine); // get indexed position
                        }
                        else
                            // End of field was found. Allocate the field data by using the start and end field positions.      
                            // Note: delimiter has to be ignored if it is within quoted content.
                            if (current == delimiter)
                        {
                            // set the field end position
                            fieldEnd = (int)(pCurrent - pLine); // get indexed position   

                            // determine end of quoted content
                            if (!quotedContent || (previous == quote && fieldEnd > afterQuote))
                            {
                                quotedContent = false;

                                advanceBy = 1; // 1 = singe delimiter character

                                // deal with neighbour quotation
                                if (previous == quote)
                                {
                                    // if the position of the quote is after the last marked quote, trim the trailing and leading query characters. 
                                    // otherwise, consider the quotes as normal content
                                    if (fieldEnd > afterQuote)
                                    {
                                        // Here, the field contains trailing and leading quotes, which has to be trimmed
                                        fieldStart = afterQuote; // fieldStart + 1; // skip the leading

                                        int skip = fieldEnd - lastQuote; // skip trailing quotes, which may include white space
                                        fieldEnd = fieldEnd - skip;

                                        advanceBy += skip; // skip the trailing quote character
                                    }
                                }


                                positions[field] = fieldStart;    // mark the field start position
                                lengths[field] = fieldEnd - fieldStart; // set the field data length
                                field++;

                                // advance the field next start position (skipping the control characters count)
                                fieldStart = fieldEnd + advanceBy;

                                afterQuote = int.MaxValue; // reset the mark                                    
                            }

                        }

                        previous = current;
                    }

                    // ###
                    // Process the trailing field, which is not terminated by delimiter, but the end of the line
                    fieldEnd = length; // same as "length"

                    // additionally, deal with neighbour quotation
                    if (previous == quote)
                    {
                        // if the position of the quote is after the last marked quote, trim the trailing and leading query characters. 
                        // otherwise, consider the quotes as normal content
                        if (fieldEnd > afterQuote)
                        {
                            // Here, the field contains trailing and leading quotes, which has to be trimmed
                            fieldStart = afterQuote; // skip the leading

                            int skip = fieldEnd - lastQuote; // skip trailing quotes, which may include white space
                            fieldEnd = fieldEnd - skip;
                        }
                    }

                    // Allocate the trailing field
                    positions[field] = fieldStart;    // mark the field start position                  
                    lengths[field] = fieldEnd - fieldStart; /// fieldEnd // set the field data length
                    field++;

                    // total number of parsed fields
                    return field;
                }
            }
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int ParseCSVLine(string line, int length, ref int[] positions, ref int[] lengths, char delimiter, char quote)
        {
            unsafe
            {
                fixed (char* pLine = line)
                {
                    char previous = '\0';
                    int field = 0;       // field idx
                    int fieldStart = 0;  // field start position in data.
                    int fieldEnd = 0;    // field end position in data
                    int advanceBy = 0;   // indicator for advancing trailing positions (inclusing number of control characters to skip at end fields)

                    // positions indicating if leading or traling quotes were found
                    int afterQuote = int.MaxValue;
                    int lastQuote = -1; // last quote position

                    bool quotedContent = false;  //  flag indicating if the current is within quoted content (content which has started with quote)

                    // Scan all characters in the current line. Current contains the current character
                    for (char* pCurrent = pLine; *pCurrent != '\0'; pCurrent++)
                    {
                        char current = *pCurrent;

                        // determine if the quotation should be incuded in the field data, or skipped as control character:
                        // if the current control character is quotation and the no previous control character found or the previous was delimiter (neighbour), it means that we start text content.

                        if (current == quote)
                        {
                            // pass the end of line (possible end of quoted content), or after delimiter cc and not in quoted text
                            if (previous == '\0' || (previous == delimiter && !quotedContent))
                            {
                                afterQuote = (int)(pCurrent - pLine) + 1; // mark the position after the quote      

                                // start quoted content
                                quotedContent = true;
                            }

                            lastQuote = (int)(pCurrent - pLine); // get indexed position
                        }
                        else
                            // End of field was found. Allocate the field data by using the start and end field positions.      
                            // Note: delimiter has to be ignored if it is within quoted content.
                            if (current == delimiter)
                            {
                                // set the field end position
                                fieldEnd = (int)(pCurrent - pLine); // get indexed position   

                                // determine end of quoted content
                                if (!quotedContent || (previous == quote && fieldEnd > afterQuote))
                                {
                                    quotedContent = false;

                                    advanceBy = 1; // 1 = singe delimiter character

                                    // deal with neighbour quotation
                                    if (previous == quote)
                                    {
                                        // if the position of the quote is after the last marked quote, trim the trailing and leading query characters. 
                                        // otherwise, consider the quotes as normal content
                                        if (fieldEnd > afterQuote)
                                        {
                                            // Here, the field contains trailing and leading quotes, which has to be trimmed
                                            fieldStart = afterQuote; // fieldStart + 1; // skip the leading

                                            int skip = fieldEnd - lastQuote; // skip trailing quotes, which may include white space
                                            fieldEnd = fieldEnd - skip;

                                            advanceBy += skip; // skip the trailing quote character
                                        }
                                    }


                                    positions[field] = fieldStart;    // mark the field start position
                                    lengths[field] = fieldEnd - fieldStart; // set the field data length
                                    field++;

                                    // advance the field next start position (skipping the control characters count)
                                    fieldStart = fieldEnd + advanceBy;

                                    afterQuote = int.MaxValue; // reset the mark                                    
                                }

                            }

                        previous = current;
                    }

                    // ###
                    // Process the trailing field, which is not terminated by delimiter, but the end of the line
                    fieldEnd = length; // same as "length"

                    // additionally, deal with neighbour quotation
                    if (previous == quote)
                    {
                        // if the position of the quote is after the last marked quote, trim the trailing and leading query characters. 
                        // otherwise, consider the quotes as normal content
                        if (fieldEnd > afterQuote)
                        {
                            // Here, the field contains trailing and leading quotes, which has to be trimmed
                            fieldStart = afterQuote; // skip the leading

                            int skip = fieldEnd - lastQuote; // skip trailing quotes, which may include white space
                            fieldEnd = fieldEnd - skip;
                        }
                    }

                    // Allocate the trailing field
                    positions[field] = fieldStart;    // mark the field start position                  
                    lengths[field] = fieldEnd - fieldStart; /// fieldEnd // set the field data length
                    field++;

                    // total number of parsed fields
                    return field;
                }
            }
        }

        [Obsolete]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int ParseCsvRow(char[] buffer, int index, int length, ref int[] positions, ref int[] lengths, char delimiterField, char delimiterText)
        {
            unsafe
            {
                fixed (char* rowData = &buffer[index])
                {
                    int lengthField = 0, position = 0, positionField = 0, currentField = 0;
                    bool withinText = false; // whether current position is located within text
                    bool skipEnd = false;

                    while (currentField < 1024 && position <= length)
                    {
                        lengthField = 0;

                        char* current = (char*)(rowData + positionField);

                        skipEnd = false;

                        // find the end of the string
                        while ((*current != delimiterField || withinText) && position < length)
                        {
                            // find start and end of text field
                            if (*current == delimiterText)
                            {
                                withinText = !withinText;

                                if (withinText)
                                    positionField = positionField + 1; // skip starting Text delimiter    
                                else
                                    skipEnd = true; // skip the ending Text delimiter

                                /// Skip the text delimiter character
                                ///if(withinText)
                                ///    walk++;
                                ///position++;
                            }

                            current++;
                            position++;
                        }

                        lengthField = (int)(current - (char*)(rowData + positionField));

                        // create string from current position, up to len.. advance position

                        positions[currentField] = positionField;
                        lengths[currentField] = lengthField - (skipEnd ? 1 : 0);

                        // new string(rowData, positionField, lengthField);

                        currentField++;

                        positionField += lengthField + 1;
                        position++;

                        //if (withinText)
                        //    System.Diagnostics.Debug.Assert(!withinText, "The text field has not been terminated. Malformed CSV.");
                    }

                    return currentField;
                }
            }
        }

        [Obsolete]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int ParseCsvRow(byte[] buffer, int index, int length, ref int[] positions, ref int[] lengths, byte delimiterField, byte delimiterText)
        {
            unsafe
            {
                fixed (byte* rowData = &buffer[index])
                {
                    int lengthField = 0, position = 0, positionField = 0, currentField = 0;
                    bool withinText = false; // whether current position is located within text
                    bool skipEnd = false;

                    while (currentField < 1024 && position <= length)
                    {
                        lengthField = 0;

                        byte* current = (byte*)(rowData + positionField);

                        skipEnd = false;

                        // find the end of the string
                        while ((*current != delimiterField || withinText) && position < length)
                        {
                            // find start and end of text field
                            if (*current == delimiterText)
                            {
                                withinText = !withinText;

                                if (withinText)
                                    positionField = positionField + 1; // skip starting Text delimiter    
                                else
                                    skipEnd = true; // skip the ending Text delimiter

                                /// Skip the text delimiter character
                                ///if(withinText)
                                ///    walk++;
                                ///position++;
                            }

                            current++;
                            position++;
                        }

                        lengthField = (int)(current - (byte*)(rowData + positionField));

                        // create string from current position, up to len.. advance position

                        positions[currentField] = positionField;
                        lengths[currentField] = lengthField - (skipEnd ? 1 : 0);

                        // new string(rowData, positionField, lengthField);

                        currentField++;

                        positionField += lengthField + 1;
                        position++;

                        //if (withinText)
                        //    System.Diagnostics.Debug.Assert(!withinText, "The text field has not been terminated. Malformed CSV.");
                    }

                    return currentField;
                }
            }
        }


        [Obsolete]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int ParseCsvRow(string row, int length, ref int[] positions, ref int[] lengths, char delimiterField, char delimiterText)
        {
            unsafe
            {

                fixed (char* rowData = row)
                {
                    int lengthField = 0, position = 0, positionField = 0, currentField = 0;
                    bool withinText = false; // whether current position is located within text
                    bool skipEnd = false;

                    while (currentField < 1024 && position <= length)
                    {
                        lengthField = 0;

                        char* current = (char*)(rowData + positionField);

                        skipEnd = false;

                        // find the end of the string
                        while ((*current != delimiterField || withinText) && position < length)
                        {
                            // find start and end of text field
                            if (*current == delimiterText)
                            {
                                withinText = !withinText;

                                if (withinText)
                                    positionField = positionField + 1; // skip starting Text delimiter    
                                else
                                    skipEnd = true; // skip the ending Text delimiter

                                /// Skip the text delimiter character
                                ///if(withinText)
                                ///    walk++;
                                ///position++;
                            }

                            current++;
                            position++;
                        }

                        lengthField = (int)(current - (char*)(rowData + positionField));

                        // create string from current position, up to len.. advance position

                        positions[currentField] = positionField;
                        lengths[currentField] = lengthField - (skipEnd ? 1 : 0);

                        // new string(rowData, positionField, lengthField);

                        currentField++;

                        positionField += lengthField + 1;
                        position++;

                        //if (withinText)
                        //    System.Diagnostics.Debug.Assert(!withinText, "The text field has not been terminated. Malformed CSV.");
                    }

                    return currentField;
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static InsertRecordHeader GetRowHeader(string row, char delimiter, bool useSimpleParser, bool useEncoder = true)
        {
            var header = InsertRecordHeader.GetFree();

            header.Length = row.Length;

            if(!useSimpleParser)
                header.Count = ParseCSVLine(row, row.Length, ref  header.Offsets, ref header.Lengths, delimiter, '"');
            else
                header.Count = ParseCsvRow(row, row.Length, ref  header.Offsets, ref header.Lengths, delimiter, '"');

#if true // A
            unsafe
            {
                fixed (byte* dest = header.Data)
                {
                    fixed (char* src = row)
                    {
                        int rowLength = 0;

                        for (int field = 0; field < header.Count; field++)
                        {
                            var srcOffset = header.Offsets[field];
                            var length = header.Lengths[field];

                            if (!useEncoder)
                            {
                                //(byte*)(pDest + destIndex)
                                Native.Utils.Memory.Memcpy(dest + rowLength, (byte*)(src + srcOffset), length * 2);
                            }
                            else
                            {
                                IDataStorage.Encoder.GetBytes(src + srcOffset, length, dest + rowLength, length);
                            }

                            rowLength += length;
                        }

                        header.Length = rowLength;
                    }
                }
            }
#else

            
                        int rowLength = 0;

                        for (int field = 0; field < header.Count; field++)
                        {
                            var srcOffset = header.Offsets[field];
                            var length = header.Lengths[field];

                            var encodedLength = IDataStorage.Encoder.GetBytes(row, srcOffset, length, header.Data, rowLength);

                            System.Diagnostics.Debug.Assert(encodedLength == length);

                            rowLength += encodedLength;
                        }

                        header.Length = rowLength;
                   
                
            
#endif

            

            return header;
        }




        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static InsertRecordHeader GetRowHeader(char[] buffer, int startInBuffer, int lengthInBuffer, char delimiter, bool useEncoder = true)
        {
            var header = InsertRecordHeader.GetFree();

            header.Length = lengthInBuffer;

            header.Count = ParseCsvRow(buffer, startInBuffer, lengthInBuffer, ref header.Offsets, ref header.Lengths, delimiter, '"');

#if true // A
            unsafe
            {
                fixed (byte* dest = header.Data)
                {
                    fixed (char* src = buffer)
                    {
                        int rowLength = 0;

                        for (int field = 0; field < header.Count; field++)
                        {
                            var srcOffset = header.Offsets[field];
                            var length = header.Lengths[field];

                            if (!useEncoder)
                            {
                                //(byte*)(pDest + destIndex)
                                Native.Utils.Memory.Memcpy(dest + rowLength, (byte*)(src + srcOffset), length * 2);
                            }
                            else
                            {
                                IDataStorage.Encoder.GetBytes((char*)src + srcOffset, length, dest + rowLength, length);
                            }

                            rowLength += length;
                        }

                        header.Length = rowLength;
                    }
                }
            }
#else

            
                        int rowLength = 0;

                        for (int field = 0; field < header.Count; field++)
                        {
                            var srcOffset = header.Offsets[field];
                            var length = header.Lengths[field];

                            var encodedLength = IDataStorage.Encoder.GetBytes(row, srcOffset, length, header.Data, rowLength);

                            System.Diagnostics.Debug.Assert(encodedLength == length);

                            rowLength += encodedLength;
                        }

                        header.Length = rowLength;
                   
                
            
#endif



            return header;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static InsertRecordHeader GetRowHeader(byte[] buffer, int startInBuffer, int lengthInBuffer, byte delimiter, byte delimiterText = (byte)'"')
        {
            var header = InsertRecordHeader.GetFree();

            header.Length = lengthInBuffer;

            header.Count = ParseCsvRow(buffer, startInBuffer, lengthInBuffer, ref header.Offsets, ref header.Lengths, delimiter, delimiterText);

            //header.Count = ParseCSVLine(buffer, startInBuffer, lengthInBuffer, ref header.Offsets, ref header.Lengths, delimiter, delimiterText);

#if true // A
            unsafe
            {
                fixed (byte* dest = header.Data)
                {
                    fixed (byte* src = &buffer[startInBuffer])
                    {
                        int rowLength = 0;

                        for (int field = 0; field < header.Count; field++)
                        {
                            var srcOffset = header.Offsets[field];
                            var length = header.Lengths[field];

                           
                                //(byte*)(pDest + destIndex)
                                Native.Utils.Memory.Memcpy(dest + rowLength, (byte*)(src + srcOffset), length+1);
                           
                            
                               // IDataStorage.Encoder.GetBytes((char*)src + srcOffset, length, dest + rowLength, length);
                             

                            rowLength += length;
                        }

                        header.Length = rowLength;
                    }
                }
            }
#else

            
                        int rowLength = 0;

                        for (int field = 0; field < header.Count; field++)
                        {
                            var srcOffset = header.Offsets[field];
                            var length = header.Lengths[field];

                            var encodedLength = IDataStorage.Encoder.GetBytes(row, srcOffset, length, header.Data, rowLength);

                            System.Diagnostics.Debug.Assert(encodedLength == length);

                            rowLength += encodedLength;
                        }

                        header.Length = rowLength;
                   
                
            
#endif



            return header;
        }


        /// <summary>
        /// Release the row header block, so it will become availalbe for others
        /// </summary>
        public void Release()
        {
            _headers.PutObject(this);

           // _headerPool.Add(this);
        }
    }
}