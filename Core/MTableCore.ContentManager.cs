﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DigitalToolworks.Data
{
    partial class MTableCore
    {
        public sealed unsafe partial class ContentManager : IDisposable
        {
            private readonly MTableCore storage;
            private readonly Block blocks;
            private readonly Indexer indexer;
            private byte[] bufferTransfer;

            private ITransaction transactionCurrent = null;

            /// <summary>
            /// Whether to run storage defragmentation automaticly upon executing operations considering various data condition rules
            /// </summary>
            public bool AutoDefragment { get; set; }

            internal ContentManager(MTableCore storage, AlignmentMode mode)
            {
                this.mode = mode;
                this.storage = storage;
                this.indexer = storage.Index;
                this.blocks = storage.Blocks;
                this.bufferTransfer = new byte[1024 * 64]; // 64KB transfer buffer, initial size. It is adjusted on demand by operations.

                this.AutoDefragment = false;

                this.EnableParallelSearch = false;

                if (this.EnableParallelSearch)
                    Task.Factory.StartNew(() =>
                    {
                        Thread.CurrentThread.Name = "SearchPool." + Thread.CurrentThread.ManagedThreadId;

                        foreach (var index in Enumerable.Range(0, parallelDegree))
                        {
                            Task.Factory.StartNew(() =>
                            {
                                Thread.CurrentThread.Name = "Search." + Thread.CurrentThread.ManagedThreadId;

                                while (!_workActions.IsCompleted)
                                {
                                    try
                                    {
                                        var action = _workActions.Take();
                                        action.Invoke();

                                        lock (_signalWork)
                                        {
                                            if (_signalWork.CurrentCount > 0)
                                                _signalWork.Signal();
                                        }
                                    }
                                    catch (OperationCanceledException)
                                    {

                                    }
                                }
                            }, TaskCreationOptions.LongRunning);
                        }
                    }, TaskCreationOptions.LongRunning);
            }

            /// <summary>
            /// Gets the number of the items currently queued into the current transaction if there is such
            /// </summary>
            public int TransactionQueueCount
            {
                get
                {
                    if (transactionCurrent != null)
                    {
                        if (transactionCurrent is InsertTransation)
                            return ((InsertTransation)transactionCurrent).PendingCount;
                    }

                    return 0;
                }
            }

            /// <summary>
            /// Indicates that a transation is currently running
            /// </summary>
            public bool InTransation
            {
                get
                {
                    return transactionCurrent != null;
                }
            }

            /// <summary>
            /// Calculates the size of the entry header metadata section (lengths storage only, exluding the size of the header itself if using RecordHeaderFormat.HybridFixed). 
            /// It depends on the number of fields and the size needed to store their lengths.
            /// </summary>
            /// <param name="header"></param>
            /// <returns></returns>
            protected long GetHeaderMetaSize(InsertRecordHeader header)
            {
                //  By default, at least the number of fields (one byte per field)   
                if (storage.RecordHeaderFormat == RecordHeaderFormats.Fixed)
                    return header.Count;

                long headerSize = 0;

                // Add 2 bytes for the header size which are at the header section start (in hybridFixed mode only)
                if (storage.RecordHeaderFormat == RecordHeaderFormats.HybridFixed)
                    headerSize += 2;

                // test the lengths of each field and if larger then variant
                // get the number of bytes needed to store this value using variant
                // TODO: note, temporary we will use the number size calculation but the correct approach is the user variant size determination (performance matters)

                foreach (var length in Enumerable.Range(0, header.Count).Select(idx => header.Lengths[idx]))
                    if (length < Variant.MaxSingleByteValue)
                        headerSize += 1;
                    else
                        if (length < (Variant.MaxSingleByteValue * 2))
                        headerSize += 2;
                    else
                        headerSize += Native.Utils.GetNumberSize(length * 2);   // multiplied by two , in order to calculate the real variant size (half of byte)

                //headerSize += header.Lengths.Where(length => length >= Variant.MaxSingleByteValue).Select(length => Native.Utils.GetNumberSize(length)).Sum();

                return headerSize;
            }

            /// <summary>
            /// Ensures that there will be enougth room in this block to fit the data required. 
            /// The process could reorganize descending data blocks and it will ensure just exact space needed, learing no space the block was full and reorganized.
            /// </summary>
            /// <param name="block"></param>
            /// <param name="requiredAmount"></param>
            /// <returns>Information about the operation performed</returns>
            private CapacityOperationInfo EnsureBlockCapacity(long block, long requiredAmount)
            {
                int blocksAffected = 0;
                long sizeReorganized = 0;
                long neededAmount = requiredAmount;

                // 1. this block does not have enougth space to fit the modified data.
                // 2. check the next blocks for free space. if free space is found in the next block, mark the next block for movement N bytes.
                // 3. check all subsquent blocks and once space is found in one, mark that block and all blocks up to that block , for movement by N bytes right
                // then move all of those blocks. that will free up the needed space for this block to fit its content

                long currentBlock = block; // starting block to check for free space

                while (neededAmount > 0 && currentBlock < blocks.Blocks && block + 1 < blocks.Blocks)
                {
                    long freeAmount = blocks.GetBlockSpaceFree(currentBlock);

                    if (freeAmount < 0)
                        throw new InvalidOperationException("Free space for block " + currentBlock + " is negative value of " + freeAmount + "");

                    if (neededAmount > freeAmount)
                    {
                        neededAmount -= freeAmount;
                        currentBlock++;
                    }
                    else if (currentBlock > block) // ignore rewriting this block in case there is enougth free space in it
                    {
                        // reserve the block free space to be used as data shift amount
                        // here is the final block. get the amount left to shift, 
                        // do block shift and keep iterating back thru all blocks, filling up their free spaces

                        this.MoveBlock(currentBlock, (int)neededAmount);
                        sizeReorganized += neededAmount;
                        blocksAffected++;

                        while (--currentBlock > block)
                        {
                            // move the current block data to right

                            freeAmount = blocks.GetBlockSpaceFree(currentBlock);

                            if (freeAmount < 0)
                                throw new InvalidOperationException("Free space for block " + currentBlock + " is negative value of " + freeAmount + "");

                            this.MoveBlock(currentBlock, (int)freeAmount);
                            blocksAffected++;
                            sizeReorganized += neededAmount;
                        }

                        break; // blocks reorganization done. exit the routine
                    }
                    else
                        break;
                }

                if (this.AutoDefragment)
                    this.DefragmentationIfNeeded(blocksAffected, block);

                return new CapacityOperationInfo(blocksAffected, sizeReorganized, requiredAmount);
            }

            /// <summary>
            /// Ensures that there will be enougth room in this block to fit the data required. 
            /// The process could reorganize descending data blocks and it will ensure just exact space needed, learing no space the block was full and reorganized.
            /// </summary>
            /// <param name="block"></param>
            /// <param name="requiredAmount"></param>
            /// <returns>Information about the operation performed</returns>
            private CapacityOperationInfo __EnsureBlockCapacity(long block, long requiredAmount)
            {
                int blocksAffected = 0;
                long sizeReorganized = 0;

                // 1. this block does not have enougth space to fit the modified data.
                // 2. check the next blocks for free space. if free space is found in the next block, mark the next block for movement N bytes.
                // 3. check all subsquent blocks and once space is found in one, mark that block and all blocks up to that block , for movement by N bytes right
                // then move all of those blocks. that will free up the needed space for this block to fit its content

                long currentBlock = block; // starting block to check for free space

                while (requiredAmount > 0 && currentBlock < blocks.Blocks && block + 1 < blocks.Blocks)
                {
                    long freeAmount = blocks.GetBlockSpaceFree(currentBlock);

                    if (freeAmount < 0)
                        throw new InvalidOperationException("Free space for block " + currentBlock + " is negative value of " + freeAmount + "");

                    if (requiredAmount > freeAmount)
                    {
                        requiredAmount -= freeAmount;
                        currentBlock++;
                    }
                    else if (currentBlock > block) // ignore rewriting this block in case there is enougth free space in it
                    {
                        // reserve the block free space to be used as data shift amount
                        // here is the final block. get the amount left to shift, 
                        // do block shift and keep iterating back thru all blocks, filling up their free spaces

                        MoveBlock(currentBlock, (int)requiredAmount);
                        sizeReorganized += requiredAmount;
                        blocksAffected++;

                        while (--currentBlock > block)
                        {
                            // move the current block data to right

                            freeAmount = blocks.GetBlockSpaceFree(currentBlock);

                            if (freeAmount < 0)
                                throw new InvalidOperationException("Free space for block " + currentBlock + " is negative value of " + freeAmount + "");

                            MoveBlock(currentBlock, (int)freeAmount);
                            blocksAffected++;
                            sizeReorganized += requiredAmount;
                        }

                        break; // blocks reorganization done. exit the routine
                    }
                    else
                        break;
                }

                if (this.AutoDefragment)
                    DefragmentationIfNeeded(blocksAffected, block);

                return new CapacityOperationInfo(blocksAffected, sizeReorganized, requiredAmount);
            }

            /// <summary>
            /// Record block as being cause for blocks rewriting. If there are more times for block triggering rewrites, return true to potencially signal the 
            /// </summary>
            /// <param name="block"></param>
            private void DefragmentationIfNeeded(int blocksAffected, long block)
            {
                // Automaticly trigger the defragmentation if too many row-blocks were affected. 
                // As rule, if more then 30% of the rowblocks are affected.
                // Finally, if there are more then 30% blocks with no space, run the defragmentation

                if (blocksAffected > 0)
                {
                    var affectedThreshold = (float)blocksAffected / blocks.Blocks;

                    if (affectedThreshold > 0.2f)
                    {
                        this.DefragmentStorage(block);
                        _blocksTrigeringRewrites.Clear();
                    }
                    else
                    {
                        // Register the block as trigger counter
                        int triggerCount = 0;
                        if (_blocksTrigeringRewrites.TryGetValue(block, out triggerCount))
                            _blocksTrigeringRewrites[block] = ++triggerCount;
                        else
                            _blocksTrigeringRewrites[block] = ++triggerCount;

                        //if (triggerCount > 20)
                        //{
                        //    this.DefragmentStorage(block);
                        //    _blocksTrigeringRewrites.Clear();
                        //}
                        //else


                        {
                            float emptyBlocks = 0;
                            for (var b = 0; b < blocks.Blocks; b++)
                                if (blocks.GetBlockSpaceFree(b) <= 0)
                                    emptyBlocks++;

                            var emptyThreshold = emptyBlocks / blocks.Blocks;
                            if (emptyThreshold > 0.2f)
                            {
                                this.DefragmentStorage();
                                _blocksTrigeringRewrites.Clear();
                            }
                        }
                    }
                }
            }

            /// <summary>
            /// Updates column field value in the data storage
            /// </summary>
            /// <param name="record">The field row</param>
            /// <param name="column">The field column</param>
            /// <param name="valueNew">The new value to be updated</param>
            public CapacityOperationInfo ModifyRecord(long record, int column, int currentValueLength, byte[] valueNew, int columnCount = 0)
            {
                // Validate value length
                if (valueNew.Length > MaxValueLength && storage.RecordHeaderFormat == RecordHeaderFormats.Fixed)
                    throw new InvalidOperationException(string.Format("Size of the value exceed the max allowed. StaticFieldsLength=true, Size: {0}, Max: {1}", valueNew.Length, MaxValueLength));

                // Check for transation presence
                var transation = transactionCurrent as ModifyTransation;
                if (transation != null && !transation.IsProcessing)
                {
                    transation.Modify(record, column, currentValueLength, valueNew);
                    return CapacityOperationInfo.Empty;
                }

                if (_contentBeginModifyCallback != null)
                    _contentBeginModifyCallback();

                // The table updated the storage can have different number of columns then the storage itself
                if (columnCount > 0)
                    storage.SetColumnCount(columnCount);

                long sizeUpdated = 0;

                var entry = (mode == AlignmentMode.RowMode ? record : record * column);

                var block = blocks.GetBlock(entry);

                var entryOldSize = blocks.GetEntryIndex(entry).Length;

                var entriesValues = new Dictionary<long, byte[]>();

                if (mode == AlignmentMode.RowMode)
                    entriesValues.Add(entry, PrepareRecordUpdate(entry, column, valueNew));
                else
                    entriesValues.Add(entry, valueNew);

                sizeUpdated += valueNew.Length;

                var requiredAmount = blocks.GetBlockRequiredSize(entriesValues);

                var info = this.EnsureBlockCapacity(block, requiredAmount);

                this.RewriteBlock(block, entriesValues);

                // Unregistrer all entries first then register them back again with the new lengths
                var entryNewSize = blocks.GetEntryIndex(entry).Length;
                blocks.RegisterEntry(entryNewSize - entryOldSize);

                // Adjust the storage data size too. 
                // NOTE: This should include only the data size, without the entry header size, but currently we use the entry size. Inspection might be needed in future
                storage.IncreaseDataSize(entryNewSize - entryOldSize);
                if (storage.DataSize < 0)
                    throw new InvalidOperationException("Storage data size dropped bellow zero (" + storage.DataSize + "). Implementation inspection suggested.");

#if VERBOSE_MANAGER

                blocks.PrintStats(row, true);

                var checkValue = this.GetValue(row, column);
                System.Diagnostics.Debug.Assert(string.Equals(checkValue, value), "The updated value differs from the value gotten! updatedValue={0}, gottenValue={1}", value, checkValue);
#endif

                this.SetModified();

                return new CapacityOperationInfo(info.BlocksAffected, info.SizeReorganized, sizeUpdated);
            }

            /// <summary>
            /// Moves the content of the block to different location
            /// </summary>
            /// <param name="block">The block number to move</param>
            /// <param name="movementOffset">The amount offset of data to move</param>
            private void MoveBlock(long block, int movementOffset)
            {
                // move each entry and update its Index
                long entry = (blocks.EntryBlock * block);
                RewriteBlockDescendant(block, entry, movementOffset);
            }

            /// <summary>
            /// Updates complete row data, by altering only the column with the value provided
            /// </summary>
            /// <param name="rowEntry">Entry number for the row</param>
            /// <param name="column">The column to alter</param>
            /// <param name="value">Cell value for the row</param>
            /// <returns>An updated version of the row data</returns>
            private byte[] PrepareRecordUpdate(long rowEntry, int column, byte[] value)
            {
                // 1. Get the existing row data
                // 2. Locate the column using NUL teminators
                // 3. Write the value at that location and transfer the remaining row data

                var length = 0;
                var rowData = this.GetEntryData(rowEntry, out length);
                return this.EditFieldData(rowData, column, value);

            }

            /// <summary>
            /// Updates complete row data, by altering the list of columns with their values
            /// </summary>
            /// <param name="recordEntry">Entry number for the record</param>        
            /// <param name="cells">Cells columns and values values for the row</param>
            /// <param name="oldEntryLength">Contains the old size of the record entry</param>
            /// <param name="oldHeaderLength">Contains the old size of the record header only</param>
            /// <param name="newHeaderLength">Contains the new size of the record header only</param>
            /// <param name="newEntryLength">Contains the new size of the record entry</param>
            /// <returns>An updated version of the row data</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private byte[] PrepareRecordUpdate(long recordEntry, Dictionary<int, byte[]> cells, out int oldEntryLength, out int oldHeaderLength, out int newEntryLength, out int newHeaderLength)
            {
                newEntryLength = 0;
                var recordData = this.GetEntryData(recordEntry, out oldEntryLength);

                // Update the row-header meta-data for the affected fields entire row.  
                // Contains the values of all cells in the row.
                // Replace the updated values only. 
                // Combine all of the values back into record data and set the record header meta accordingly

                var recordValues = this.ExtractFieldData(recordData, out oldHeaderLength);

                // updates the modified value here
                foreach (var value in cells)
                    recordValues[value.Key] = value.Value;


                if (storage.RecordHeaderFormat == RecordHeaderFormats.Fixed)
                {
                    // NOTE: Currenty, the optimization considers the fields lengths are static, and each field has max size of 255 (1 byte)

                    newEntryLength = recordValues.Length;   // Header length has size of number of cells in the record (considering we have fixed static field lengths, of 1 byte

                    foreach (var cell in cells)
                    {
                        if (cell.Value.Length > 255)
                            throw new InvalidOperationException("Cell value length exceeds limit of one byte (length=" + cell.Value.Length + "). Expected static field length sizes limited to one byte.");

                        recordData[cell.Key] = (byte)cell.Value.Length;
                    }
                }
                else
                {
                    // Set the new record header meta: 
                    // Note: Optimization can be done, by executing single meta set for all fields 

                    for (int idx = 0; idx < recordValues.Length; idx++)
                        newEntryLength = this.SetFieldMeta(recordData, idx, recordValues[idx].Length);
                }

                //  Set the new header len here
                newHeaderLength = newEntryLength;

                byte[] entryData = new byte[newEntryLength + recordValues.Sum(value => value.Length)];

                Buffer.BlockCopy(recordData, 0, entryData, 0, newEntryLength);

                // Allocate and entry buffer and Append the record values after the record header in the record data
                for (int idx = 0; idx < recordValues.Length; idx++)
                {
                    var value = recordValues[idx];
                    if (value.Length > 0)
                    {
                        Buffer.BlockCopy(value, 0, entryData, newEntryLength, value.Length);
                        newEntryLength = newEntryLength + value.Length;
                    }
                }

                return entryData;
            }

            /// <summary>
            /// Edit data value of field inside row data
            /// </summary>
            /// <param name="rowData">Row data</param>
            /// <param name="column">Column field to edit</param>
            /// <param name="value">Value to use for the field data</param>
            /// <returns>New updated version of the row data</returns>
            private byte[] EditFieldData(byte[] rowData, int column, byte[] value)
            {
#if MODE_LEN_LIST

                // Remove the current data for the field and replace it with the new data.
                // The remove / replace can be achieved by pushing the fields content after the new value
                // Then, update the row-header meta-data for the affected fields entire row.                

                int positionField, lengthField, lengthHeader, lengthData;
                GetFieldMeta(rowData, column, out positionField, out lengthField, out lengthHeader, out lengthData);

                positionField += lengthHeader; // offset the position of the field, after the header

                // preserve the current row data first, then remove the old value data from the row , then insert the new value data in the row

                // allocate and ascending and descending buffers                
                var ascendingData = new byte[positionField - lengthHeader];
                var descendingData = new byte[(lengthHeader + lengthData) - (positionField + lengthField)];

                // preserve the content into the buffers
                Buffer.BlockCopy(rowData, lengthHeader, ascendingData, 0, ascendingData.Length);
                Buffer.BlockCopy(rowData, positionField + lengthField, descendingData, 0, descendingData.Length);

                // get the header-row length, to be used for starting offset of the row data, then insert the new row header
                int headerSize = SetFieldMeta(rowData, column, value.Length);

                // allocate entry data buffer, for the modified content
                var entryData = new byte[headerSize + ascendingData.Length + value.Length + descendingData.Length];

                // copy all parts back to the entry data. parts includes: row-header, ascending, value and descending data
                Buffer.BlockCopy(rowData, 0, entryData, 0, headerSize); // copy row-header
                Buffer.BlockCopy(ascendingData, 0, entryData, headerSize, ascendingData.Length);    // copy ascending data
                Buffer.BlockCopy(value, 0, entryData, headerSize + ascendingData.Length, value.Length);
                Buffer.BlockCopy(descendingData, 0, entryData, headerSize + ascendingData.Length + value.Length, descendingData.Length);

                return entryData;
#endif

#if MODE_NUL_DMT
                var row = new StringBuilder(rowData);

                // 2. Locate the column using NUL teminators
                // 3. Write the value at that spot and transver the remaining row data

                // for each of the field , get its value determined by null terminator
                int field = 0, position = 0, delimiter = 0, length = rowData.Length;
                do
                {
                    delimiter = rowData.IndexOf('\0', position);

                    if (delimiter < 0)
                        delimiter = length;
                    {
                        if (field == column) // cell value found
                        {
                            rowData = row.Remove(position, delimiter - position).Insert(position, value).ToString();
                            return rowData;
                        }

                        field++;

                        position = delimiter + 1;
                    }

                }
                while (position < length);
#endif

                throw new Exception("No field found for the column " + column + ".");
            }

            /// <summary>
            /// Gets the value for cell from the data storage
            /// </summary>
            /// <param name="row">The cell row</param>
            ///  <param name="column">The cell column</param>
            /// <returns>The data value of the cell</returns>
            public string GetValue(long row, int column)
            {
                var entry = (mode == AlignmentMode.RowMode ? row : row * column);

                var length = 0;
                var data = this.GetEntryData(entry, out length);

                if (mode == AlignmentMode.RowMode)
                    data = ExtractFieldData(data, column);

                return IDataStorage.Encoder.GetString(data, 0, data.Length);
            }

            public string PrintRecords(params long[] records)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(new string('-', 100));
                foreach (var record in records)
                {
                    sb.AppendFormat("{0} > ", record);

                    for (int idx = 0; idx < this.storage.ColumnCount; idx++)
                        sb.AppendFormat("|{0}", this.GetValue(record, idx));

                    sb.Append("|");
                    sb.AppendLine();
                }
                sb.AppendLine(new string('-', 100));
                System.Diagnostics.Debug.WriteLine(sb.ToString());
                return sb.ToString();
            }

            /// <summary>
            /// Get the complete data of an entry
            /// </summary>
            /// <param name="entry">Entry number</param>
            /// <param name="length">Value length returned</param>
            /// <returns>The value data for the entry</returns>
            private byte[] GetEntryData(long entry, out int length)
            {
                length = this.ReadEntry(entry, ref bufferTransfer);
                return bufferTransfer;
            }

            /// <summary>
            /// Gets the lengths of the row-header and the data of row entry
            /// </summary>
            /// <param name="entry">Entry data</param>
            /// <param name="lengthHeader">Size of the header</param>
            /// <param name="lengthData">Size of the data</param>
            /// <param name="columnCount">The total number of columns, used to terminate the header parsing process</param>
            /// <param name="entryLength">Complete length of the entry</param>
            private void GetEntryMetaLengths(byte[] entry, out int lengthHeader, out int lengthData, int columnCount, int entryLength)
            {
                if (storage.RecordHeaderFormat == RecordHeaderFormats.HybridFixed)
                {
                    // Just extract the header-length from the first 2 bytes and use the entryLength to calculate the remaining
                    fixed (byte* buffer = entry)
                        lengthHeader = (int)Native.Utils.BytesToNumberOpt(buffer, 2);
                    lengthData = entryLength - lengthHeader;
                }
                else
                    if (storage.RecordHeaderFormat == RecordHeaderFormats.Variant)
                {
                    unsafe
                    {
                        lengthHeader = 0;
                        lengthData = 0;

                        int positionHeader = 0, currentField = 0;

                        fixed (byte* dataPtr = entry)
                        {
                            while (currentField < columnCount)
                            {
                                // iterate over the row header to gather field length
                                // store the field set position and length..

                                var lengthField = Variant.ReadVInt(dataPtr + positionHeader, out lengthHeader);
                                lengthData += lengthField;
                                positionHeader += lengthHeader;
                                currentField++;
                            }

                            lengthHeader = positionHeader; // set the final length of the header
                        }
                    }
                }
                else
                    throw new NotImplementedException();
            }

            /// <summary>
            /// Gets the metadata (location and size) of the field data in the memory
            /// </summary>
            /// <param name="header">Data containing the row-header where the metadata is located</param>
            /// <param name="field">The number of the field to get its meta data</param>
            /// <param name="position">The memory location of the field data</param>
            /// <param name="length">The length of the field data</param> 
            /// <param name="lengthHeader">The length of the header data</param> 
            /// <param name="lengthData">The length of the entry content data</param> 
            private void GetFieldMeta(byte[] header, int field, out int position, out int length, out int lengthHeader, out int lengthData)
            {
                // 1. Read the row-heading which contains the cells lenghts
                // 2. Locate the column cell and calcuate its position and length

                position = 0;
                length = 0;
                lengthHeader = 0;
                lengthData = 0;

                /* Determines the fields positions and lengths from the row-header list  */

                unsafe
                {
                    int positionHeader = 0, currentField = 0, positionField = 0;

                    fixed (byte* dataPtr = header)
                    {
                        while (currentField < storage.ColumnCount)
                        {
                            // iterate over the row header to gather field length
                            // store the field set position and length..

                            var lengthField = Variant.ReadVInt(dataPtr + positionHeader, out lengthHeader);

                            if (currentField == field)
                            {
                                length = lengthField;
                                position = positionField;
                            }

                            positionHeader += lengthHeader;
                            positionField += lengthField;
                            lengthData += lengthField;
                            currentField++;
                        }

                        lengthHeader = positionHeader;
                    }
                }
            }

            /// <summary>
            /// Gets the metadata (location and size) of all fields in the record
            /// </summary>
            /// <param name="header">Data containing the row-header where the metadata is located</param>          
            /// <param name="positions">The memory location of the field data</param>
            /// <param name="lengths">The length of the field data</param> 
            /// <param name="lengthHeader">The length of the header data</param> 
            /// <param name="lengthData">The length of the entry content data</param> 
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private void GetFieldsMeta(byte[] header, out int[] positions, out int[] lengths, out int lengthHeader, out int lengthData)
            {
                // 1. Read the row-heading which contains the cells lenghts
                // 2. Locate the column cell and calcuate its position and length

                int totalFieldsCount = storage.ColumnCount;
                positions = new int[totalFieldsCount];
                lengths = new int[totalFieldsCount];
                lengthHeader = 0;
                lengthData = 0;

                /* Determines the fields positions and lengths from the row-header list  */

                unsafe
                {
                    int positionHeader = 0, currentField = 0, positionField = 0;

                    fixed (byte* dataPtr = header)
                    {
                        if (storage.RecordHeaderFormat == RecordHeaderFormats.HybridFixed)
                        {
                            // get the header length from the first 2 bytes
                            lengthHeader = (int)Native.Utils.BytesToNumberOpt(dataPtr, 2);
                            positionHeader = 2;  // increase the pointer by 2 bytes
                        }

                        while (currentField < totalFieldsCount)
                        {
                            // iterate over the row header to gather field length
                            // store the field set position and length..

                            var lengthField = Variant.ReadVInt(dataPtr + positionHeader, out lengthHeader);

                            lengths[currentField] = lengthField;
                            positions[currentField] = positionField;

                            positionHeader += lengthHeader;
                            positionField += lengthField;
                            lengthData += lengthField;
                            currentField++;
                        }

                        lengthHeader = positionHeader;
                    }
                }
            }

            /// <summary>
            /// Gets the metadata (location and size) of the fields data in the memory
            /// </summary>
            /// <param name="header">Data containing the row-header where the metadata is located</param>
            /// <param name="fields">The number of the field to get its meta data</param>
            /// <param name="positions">The memory location of the field data</param>
            /// <param name="lengths">The length of the field data</param> 
            /// <param name="lengthHeader">The length of the header data</param> 
            /// <param name="lengthData">The length of the entry content data</param> 
            private void GetFieldsMeta(byte[] header, int[] fields, out int[] positions, out int[] lengths, out int lengthHeader, out int lengthData)
            {
                // 1. Read the row-heading which contains the cells lenghts
                // 2. Locate the column cell and calcuate its position and length

                int idx = 0;
                int count = fields.Length;
                positions = new int[count];
                lengths = new int[count];
                lengthHeader = 0;
                lengthData = 0;

                /* Determines the fields positions and lengths from the row-header list  */

                unsafe
                {
                    int positionHeader = 0, currentField = 0, positionField = 0;

                    fixed (byte* dataPtr = header)
                    {
                        while (currentField < storage.ColumnCount)
                        {
                            // iterate over the row header to gather field length
                            // store the field set position and length..

                            var lengthField = Variant.ReadVInt(dataPtr + positionHeader, out lengthHeader);

                            if (currentField == fields[idx])
                            {
                                lengths[idx] = lengthField;
                                positions[idx] = positionField;

                                idx++;
                            }

                            positionHeader += lengthHeader;
                            positionField += lengthField;
                            lengthData += lengthField;
                            currentField++;
                        }

                        lengthHeader = positionHeader;
                    }
                }
            }

            /// <summary>
            /// Sets the field length into the row header meta data and returns the size of the row header
            /// </summary>
            /// <param name="header">Data containing the row-header where the metadata is located</param>
            /// <param name="field">The number of the field to set its meta data</param>
            /// <param name="lengthField">The length of the data</param>
            /// <returns>The size of the row header after setting the field length</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private int SetFieldMeta(byte[] header, int field, int lengthField)
            {
                /* Determines the fields positions and lengths from the row-header list  */
                // 1. Reads all fields lengths from the row header
                // 2. Writes lengths back for fields up to this field, then write this field length
                // 3. Write the lengths of the remaining fields.
                // 4. If the new header size is different then the original header size, adjust the row data size and push the content

                unsafe
                {
                    int totalFieldCount = storage.ColumnCount;
                    int[] lengths = new int[totalFieldCount];
                    int positionHeader = 0;

                    if (storage.RecordHeaderFormat == RecordHeaderFormats.HybridFixed)
                        positionHeader = 2; // position after the first 2 bytes reserved the storing the header length

                    // reads the original row-header
                    fixed (byte* headerPtr = header)
                    {
                        for (int currentField = 0; currentField < totalFieldCount; currentField++)
                        {
                            // iterate over the row header to gather fields lengths
                            var lengthHeader = 0;
                            lengths[currentField] = Variant.ReadVInt(headerPtr + positionHeader, out lengthHeader);
                            positionHeader += lengthHeader;
                        }

                        // writes the field length into buffer
                        fixed (byte* fieldPtr = new byte[32])
                        {
                            int lengthHeader = 0;
                            if (storage.RecordHeaderFormat == RecordHeaderFormats.HybridFixed)
                                lengthHeader += 2; // reserve the first 2 bytes for the header length

                            var length = Variant.WriteVInt(fieldPtr, lengthField);

                            // calculate the new size for the row-header. If different then the original, adjust the original data buffer and copy its content backwards, then write the new header at the head.
                            if (length > lengths[field])
                            {
                            }
                            if (length < lengths[field])
                            {
                            }

                            // write the precending fields, then this field, then remaining into the row-header
                            for (int currentField = 0; currentField < lengths.Length; currentField++)
                            {
                                if (currentField == field) // this field length
                                {
                                    Native.CopyMemory(headerPtr + lengthHeader, fieldPtr, length);
                                    lengthHeader += length;
                                }
                                else
                                {
                                    lengthHeader += Variant.WriteVInt(headerPtr + lengthHeader, lengths[currentField]);
                                }
                            }

                            if (storage.RecordHeaderFormat == RecordHeaderFormats.HybridFixed)
                                Native.Utils.NumberToBytes(headerPtr, lengthHeader, 2);  // << write the header length in the first 2 bytes

                            return lengthHeader;
                        }
                    }
                }
            }

            /// <summary>
            /// Gets the value data of a fields, within row data
            /// </summary>
            /// <param name="rowData">The complete row data, containing all fields</param>
            /// <param name="columns">Column to extract the field value for</param>
            /// <returns>The field value data extracted</returns>
            private byte[][] __ExtractFieldData(byte[] rowData, int[] columns)
            {
                int[] positionsField, lengthsField;
                int lengthHeader, lengthData;
                this.GetFieldsMeta(rowData, columns, out positionsField, out lengthsField, out lengthHeader, out lengthData);

                byte[][] data = new byte[columns.Length][];
                for (int idx = 0; idx < data.Length; idx++)
                {
                    // extract the data using the field position and length
                    data[idx] = new byte[lengthsField[idx]];
                    Buffer.BlockCopy(rowData, lengthHeader + positionsField[idx], data[idx], 0, lengthsField[idx]);
                }
                return data;
            }

            /// <summary>
            /// Gets the value data of all fields, within row data
            /// </summary>
            /// <param name="rowData">The complete row data, containing all fields</param>
            /// <param name="columns">Column to extract the field value for</param>
            /// <param name="lengthHeader">Contains the length of the record header</param>
            /// <returns>The field value data extracted</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private byte[][] ExtractFieldData(byte[] rowData, out int lengthHeader)
            {
                int[] positionsField, lengthsField;
                int lengthData;
                this.GetFieldsMeta(rowData, out positionsField, out lengthsField, out lengthHeader, out lengthData);

                byte[][] data = new byte[positionsField.Length][];
                for (int idx = 0; idx < data.Length; idx++)
                {
                    // extract the data using the field position and length
                    data[idx] = new byte[lengthsField[idx]];
                    Buffer.BlockCopy(rowData, lengthHeader + positionsField[idx], data[idx], 0, lengthsField[idx]);
                }
                return data;
            }

            /// <summary>
            /// Gets the value data of a field, within row data
            /// </summary>
            /// <param name="rowData">The complete row data, containing all fields</param>
            /// <param name="column">Column to extract the field value for</param>
            /// <returns>The field value data extracted</returns>
            private byte[] ExtractFieldData(byte[] rowData, int column)
            {

#if MODE_LEN_LIST

                int positionField, lengthField, lengthHeader, lengthData;
                this.GetFieldMeta(rowData, column, out positionField, out lengthField, out lengthHeader, out lengthData);

                // extract the data using the field position and length
                var data = new byte[lengthField];
                Buffer.BlockCopy(rowData, lengthHeader + positionField, data, 0, lengthField);
                return data;
#endif

#if MODE_NUL_DMT
                // 1. Locate the cell by NUL teminators

                // for each of the field , get its value determined by null terminator
                int field = 0, position = 0, delimiter = 0, length = rowData.Length;
                do
                {
                    delimiter = rowData.IndexOf('\0', position);

                    if (delimiter < 0)
                        delimiter = length;
                    {

                        if (field == column) // cell value found
                        {
                            string value = rowData.Substring(position, delimiter - position);
                            return value;
                        }

                        field++;

                        position = delimiter + 1;
                    }

                }
                while (position < length);
#endif

                throw new Exception("No field found for the column " + column + ".");
            }

            /// <summary>
            /// Starts data blocks defragmentation operation. This ensures every block has needed buffer
            /// </summary>
            public void DefragmentStorage(long startBlock = 0)
            {
                var elapsed = System.Diagnostics.Stopwatch.StartNew();

                // Iterate thru all row blocks and get the free buffer space, then calculate the required buffer space for each of them, considering the percentage and minimum buffer required

                // amount needed for the entire data storage
                Dictionary<long, long> blocksNeededAmounts = new Dictionary<long, long>();
                for (long block = startBlock; block < blocks.Blocks; block++)
                {
                    long freeSpace = blocks.GetBlockSpaceFree(block);
                    long usedSpace = blocks.GetBlockSpaceUsed(block);

                    long neededBlockSize = Math.Max((long)((Block.BufferSizePercentage / 100d) * (double)usedSpace), Block.MinBufferSizeBytes);

                    // temporary, use the min buffer size here. 
                    if (freeSpace < neededBlockSize)
                        blocksNeededAmounts.Add(block, neededBlockSize - freeSpace);
                    else
                        blocksNeededAmounts.Add(block, 0);
                }

                if (blocksNeededAmounts.Values.Sum() > 0)
                {
                    // Start iterating the blocks backwards, shifting by the amount needed

                    int totalBlocks = 0;
                    long totalData = 0;
                    for (long block = blocks.Blocks - 1; block > startBlock; block--)
                    {
                        var moveBy = blocksNeededAmounts.Values.Sum();

                        this.MoveBlock(block, (int)moveBy);

                        blocksNeededAmounts.Remove(block - 1);

                        totalBlocks++;

                        totalData += moveBy;
                    }

                    System.Diagnostics.Debug.WriteLine("Storage Defragmented! Affected blocks {0}, with total data {1}, records {3}, elapsed {2}ms", totalBlocks, totalData, elapsed.ElapsedMilliseconds, storage.RowCount);
                    elapsed.Stop();
                }
            }

            /// <summary>
            /// Defragments the entries in the block, by realigning them sequentially and ensuring the proper block buffer space. Returns the number of entries affected
            /// </summary>
            /// <param name="complete">Whether to defragment complete block or only up to the first disfragmented entry</param>
            /// <param name="block">The block to defragment</param>
            private DefragmentationOperationInfo DefragmentBlock(long block, bool complete, bool backwards = false, bool returnFullInfo = false)
            {
                // Rewrite the entries to be sequential
                // Iterate thru entries indexes and get their position and lengths.
                // The next entry position should be just after the current entry.
                // If the next entry position is away from the current entry, calculate the needed offset and shift the next entry,

                int entriesAffected = 0;
                int entriesProcessed = 0;
                Dictionary<long, long> entriesAffectedList = null;

                if (!backwards)
                {
                    // Process forwards 
                    for (long entry = blocks.GetBlockFirstEntry(block); entry < blocks.GetBlockLastEntry(block) && entry + 1 < (long)storage.Index.IndexCount; entry++)
                    {
                        entriesProcessed++;

                        int writeOffset = this.indexer.GetAncendantGap(entry);

                        //   var currentIndex = blocks.GetEntryIndex(entry);
                        //    var nextIndex = blocks.GetEntryIndex(entry + 1);
                        //    int writeOffset = (int)((currentIndex.Position + currentIndex.Length) - nextIndex.Position);

                        if (writeOffset < 0)
                        {
                            // read from original location
                            var readen = ReadEntry(entry + 1, ref bufferTransfer);
                            // write to new location (original + offset)
                            var offset = WriteEntry(entry + 1, bufferTransfer, readen, writeOffset);

                            entriesAffected++;

                            if (returnFullInfo)
                            {
                                if (entriesAffectedList == null)
                                    entriesAffectedList = new Dictionary<long, long>();
                                entriesAffectedList.Add(entry, writeOffset);
                            }
                        }
                        else
                            if (writeOffset > 0)
                            throw new InvalidOperationException("The position of entry " + entry + " overlaps entry " + (entry + 1) + " by " + writeOffset + " bytes. Implementation inspection suggested.");
                        else
                                if (!complete)
                            break;  // only process one entry, for optinization purposese. this may cause bugs, inspect it in such case
                    }
                }
                else
                {
                    for (long entry = blocks.GetBlockLastEntry(block); entry > blocks.GetBlockFirstEntry(block) && entry - 1 >= 0 && entry < (long)storage.Index.IndexCount; entry--)
                    {
                        entriesProcessed++;

                        int writeOffset = this.indexer.GetDescendantGap(entry);

                        // var currentIndex = blocks.GetEntryIndex(entry);
                        //  var prevIndex = blocks.GetEntryIndex(entry - 1);
                        // int writeOffset = (int)(currentIndex.Position - (prevIndex.Position + prevIndex.Length));

                        if (writeOffset > 0)
                        {
                            // read from original location
                            var readen = ReadEntry(entry - 1, ref bufferTransfer);
                            // write to new location (original + offset)
                            var offset = WriteEntry(entry - 1, bufferTransfer, readen, writeOffset);

                            entriesAffected++;

                            if (returnFullInfo)
                            {
                                if (entriesAffectedList == null)
                                    entriesAffectedList = new Dictionary<long, long>();
                                entriesAffectedList.Add(entry, writeOffset);
                            }
                        }
                        else
                            if (writeOffset < 0)
                            throw new InvalidOperationException("The position of entry " + entry + " overlaps entry " + (entry + 1) + " by " + writeOffset + " bytes. Implementation inspection suggested.");
                        else
                                if (!complete)
                            break;  // only process one entry, for optinization purposese. this may cause bugs, inspect it in such case
                    }
                }

                return new DefragmentationOperationInfo(entriesAffectedList, entriesAffected, entriesProcessed);
            }


            /// <summary>
            /// Rewrite and reorganize data content of an entry block, overwriting the entries supplied
            /// </summary>
            /// <param name="block">Block number to process</param>
            /// <param name="entriesValues">Entries and their values to overwrite and reorganize</param>
            /// <param name="ignoreReductionsRewrites">Whether to rewrite the descendant block entries if the values were reduces (shifted negatively)</param>
            private void RewriteBlock(long block, Dictionary<long, byte[]> entriesValues, bool ignoreReductionsRewrites = false)
            {
                long[] entriesRewrite = entriesValues.Keys.ToArray();   // entries to overwrite and reorganize

                foreach (var entry in entriesRewrite)
                {
                    var dataValue = entriesValues[entry];

                    // temp
                    if (bufferTransfer.Length < dataValue.Length)
                        bufferTransfer = new byte[dataValue.Length];

                    // 1. Determine data shift offset 
                    var dataLength = dataValue.Length;
                    var index = blocks.GetEntryIndex(entry);
                    var shiftOffset = dataLength - index.Length;

                    // 2. If the operation needs shifing, rewrite the descendant block entries if the write has caused data shifting
                    if (shiftOffset < 0)
                    {
                        var writeOffset = WriteEntry(entry, dataValue);
                        if (!ignoreReductionsRewrites)
                            this.RewriteBlockDescendant(block, entry + 1, writeOffset);
                    }
                    else
                        if (shiftOffset > 0)
                    {
                        this.RewriteBlockDescendant(block, entry + 1, shiftOffset);
                        var writeOffset = WriteEntry(entry, dataValue);
                    }
                    else
                    {
                        // 3. Finally, write the modified value
                        var writeOffset = WriteEntry(entry, dataValue);
                    }
                }
            }

            /// <summary>
            /// Rewrite and reorganize data content of an entry block, for all entries descendant to the startEntry including itself, shifting the data location by writeOffset
            /// </summary>
            /// <param name="block">Block number to process</param>
            /// <param name="startEntry">Process all descendant entries starting from this entry</param>
            /// <param name="writeOffset">Process all block entries after this entry</param>
            private void RewriteBlockDescendant(long block, long startEntry, int writeOffset, long endEntry = 0)
            {
                // all block entries, Reversed or Forward, based on writeOffset!

                long endRangeEntry = Math.Min(endEntry == 0 ? blocks.GetBlockLastEntry(block) : endEntry, (long)indexer.IndexCount - 1);

                if (startEntry <= endRangeEntry)
                {
                    // Grab the entire block data and shift it by the write offset. Use the starting and ending entry indexes.
                    // Then, shift the entire area of indexes accordingly

                    var startIndex = blocks.GetEntryIndex(startEntry);
                    var endIndex = blocks.GetEntryIndex(endRangeEntry);

                    if (OverflowsMapView(startIndex.Position, endIndex.Position, endIndex.Position + writeOffset))
                    {
                        // shift entries one by one, in backwards order (use the old __RewriteBlockDescendant)
                        this.__RewriteBlockDescendant(block, startEntry, writeOffset);
                        return;
                    }

                    /*
                    // Determine if the end entry is withing the same mapped view as the startEntry.
                    // If the endRangeEntry is on another view, limit the rate only to the startEntry view's last entry.
                    //
                    // IMPORTANT: This logic considers that the caller will take care of processing the remaining view range explicitly.
                    //
                    if (this.storage.MapViews.GetViewId(startIndex.Position) != this.storage.MapViews.GetViewId(endIndex.Position))
                    {
                        // Find the last entry from the group, which is aligned in this map view
                        long lastViewEntry = startEntry;
                        long blockViewId = this.storage.MapViews.GetViewId(startIndex.Position);
                        while(lastViewEntry < endRangeEntry)
                        {
                            var index = blocks.GetEntryIndex(lastViewEntry);
                            if (storage.MapViews.GetViewId(index.Position) == blockViewId) // same view , keep looping
                            {
                                lastViewEntry++;
                            }
                            else
                            {
                                // this entry is within the other map view, so break the loop and allocate the endRangeEntry and endIndex
                                // perform rewrite for remaining part of the range first (backwards order). Call this same routine with narrowed range.

                                endRangeEntry = lastViewEntry;
                              //  endIndex = blocks.GetEntryIndex(endRangeEntry);

                                // process second part first
                                this.RewriteBlockDescendant(block, endRangeEntry + 1, writeOffset, endEntry); 

                                // process first part next
                                this.RewriteBlockDescendant(block, startEntry, writeOffset, endRangeEntry);

                                return;
                            }
                        }                      
                    }

                    */


                    // The length of the entire block region which will be shifted 
                    var regionLength = (endIndex.Position + endIndex.Length) - startIndex.Position;

                    if (regionLength < 0)
                        throw new InvalidOperationException("Data block writing region is negative value (" + regionLength + "). This must not happen, immideate investigation needed");

#if (DEBUG || RELEASE) 
                    // If the region is aligned between two map views, split the operation for each maped view separately
                    //#####################################################################################
                    var startMapView = this.storage.MapViews.GetView(startIndex.Position, false);
                    var endMapView = this.storage.MapViews.GetView(endIndex.Position, false);
                    if (startMapView.Id != endMapView.Id)
                        throw new InvalidOperationException("Data block writing region is aligned between two map view pages (viewId=" + startMapView.Id + " and viewId=" + endMapView.Id + "). This must not happen, immideate investigation needed");
                    //#####################################################################################
#endif

                    // Ensures the transfer buffer is large enougth for the operation
                    if (bufferTransfer.Length < regionLength)
                        bufferTransfer = new byte[regionLength];

                    
                    if (true)   //  Faster approach to rewrite descendant block data
                    {
                        // MUST: 
                        //  At this point the operation MUST be performed ONLY on single memory map view.
                        //  It is considered that the range of data is properly split and delegated to to fit on single map view prior getting to this point.
                        //  Having this under consideration, we can perform the entire range data movement with single fast native memory move.
                        storage.MapViews.RepositionViewData(startIndex.Position, writeOffset, (int)regionLength);
                    }
                    else
                    {
                        // This is the prior slower approach to write descendant block data

                        int readen = storage.MapViews.Read(bufferTransfer, startIndex.Position, (int)regionLength);
                        if (readen != regionLength)
                            throw new InvalidOperationException("The region length was not fully readen (regionLength=" + regionLength + ", readen=" + readen + ")");

                        this.storage.MapViews.Write(bufferTransfer, 0, startIndex.Position + writeOffset, (int)regionLength);
                    }

                    this.indexer.RepositionRange(startEntry, endRangeEntry, writeOffset);

                    if (System.Diagnostics.Debugger.IsAttached && false)
                        System.Diagnostics.Debug.WriteLine("startRangeEntry:{0}, endRangeEntry:{1}, entries:{3}, writeOffset:{2}", startEntry, endRangeEntry, writeOffset, endRangeEntry - startEntry);
                }
            }

            /// <summary>
            /// Determine whether the data locations passed, overflows single memory map view. With other words, whether those locations are withn the same map view.
            /// </summary>
            /// <param name="locations"></param>
            /// <returns></returns>
            protected bool OverflowsMapView(params long[] locations)
            {
                long currentViewId = -1;
                bool overflowsMapView = false;
                foreach (var location in locations)
                {
                    var viewId = storage.MapViews.GetViewId(location);
                    if (currentViewId >= 0 && currentViewId != viewId)
                    {
                        overflowsMapView = true;
                        break;
                    }

                    currentViewId = viewId;
                };

                return overflowsMapView;
            }

            /// <summary>
            /// Rewrite and reorganize data content of an entry block, for all entries descendant to the startEntry including itself, shifting the data location by writeOffset
            /// </summary>
            /// <param name="block">Block number to process</param>
            /// <param name="startEntry">Process all descendant entries starting from this entry</param>
            /// <param name="writeOffset">Process all block entries after this entry</param>
            private void __RewriteBlockDescendant(long block, long startEntry, int writeOffset, long endEntry = 0)
            {
#if VERBOSE_MANAGER
                System.Diagnostics.Debug.WriteLine("OperationManager: RewriteBlockDescendant: block={0}, startEntry={1}, writeOffset={2}", block, startEntry, writeOffset);
#endif

                // all block entries, Reversed or Forward, based on writeOffset!

                var entriesDescendant = blocks.GetBlockEntries(block).Where(entry => entry >= startEntry && (endEntry == 0 || entry < endEntry)).ToList();
                if (writeOffset > 0)
                    entriesDescendant.Reverse();

                // Read the entry data content and write it into destination
                foreach (var entry in entriesDescendant)
                {
                    // read from original location
                    var readen = ReadEntry(entry, ref bufferTransfer);

                    // write to new location (original + offset)
                    var offset = WriteEntry(entry, bufferTransfer, readen, writeOffset);
                }
            }

            /// <summary>
            /// Writes entry data value content. It returns amount of data needed to be shifted (offset), into the data source stream. This offset is used for reorganization of the subsequent entries
            /// </summary>
            /// <param name="entry">Entry number</param>            
            /// <param name="buffer">Buffer to write data from</param>
            /// <param name="length">Size fo the entry data</param>
            /// <param name="writeOffset">The offset from the position in the data store target stream, to write at</param>
            /// <returns>Number of bytes needed to be shifted after this entry was writen</returns>
            private int WriteEntry(long entry, byte[] buffer, int length, int writeOffset)
            {
#if VERBOSE_MANAGER_
                System.Diagnostics.Debug.WriteLine("OperationManager: WriteEntry: entry={0}, length={1}, writeOffset={2}", entry, length, writeOffset);
#endif

                var index = blocks.GetEntryIndex(entry);
                var position = index.Position + writeOffset;

                //var writer = storage.GetWriteAccessor(position);
                //writer.Position = position;
                //writer.Write(buffer, 0, length);

                storage.MapViews.Write(buffer, 0, position, length);

                // if the new entry data size is different from the existing or the data is relocated, update the index with the new length
                if (index.Length != length || position != index.Position)
                    blocks.SetEntryIndex(entry, position, length);

                //// Advance the global writer position to the far most point
                //if (position + length > storage._writerPosition)
                //    storage._writerPosition = position + length;

#if VERBOSE_MANAGER_W

                var checkIndex = blocks.GetEntryIndex(entry);
                var checkWriter = storage.GetWriteAccessor(checkIndex.Position);
                checkWriter.Position = checkIndex.Position;
                var checkReaden = checkWriter.Read(buffer, 0, checkIndex.Length);
                var chechValue = IDataStorage.Encoder.GetString(buffer, 0, checkReaden);
                 
                System.Diagnostics.Debug.WriteLine("WriteEntry Buffer: entry={0}, length={1}, writeOffset={2}\r\nbufferContent={3}\r\nstreamContent={4}",
                    entry, length, writeOffset, IDataStorage.Encoder.GetString(buffer, 0, length).Replace("/0","//0"),
                    chechValue);
#endif

                // return the shifting offset size difference
                return length - index.Length;
            }

            /// <summary>
            /// Writes entry data value content. It returns amount of data needed to be shifted (offset), into the data source stream. This offset is used for reorganization of the subsequent entries
            /// </summary>
            /// <param name="entry">Entry number</param>            
            /// <param name="data">The value of the data for the entry</param>
            /// <returns>Number of bytes needed to be shifted after this entry was writen</returns>
            private int WriteEntry(long entry, byte[] data)
            {
                //var length = IDataStorage.Encoder.GetBytes(value, 0, value.Length, bufferTransfer, 0);
                return WriteEntry(entry, data, data.Length, 0);
            }

            /// <summary>
            /// Reads the data content of an Entry
            /// </summary>
            /// <param name="entry">Entry number</param>
            /// <param name="buffer">Buffer to read the data into</param>
            /// <returns>Number of bytes readen</returns>
            private int ReadEntry(long entry, ref byte[] buffer)
            {
                var index = blocks.GetEntryIndex(entry);

                // Ensures the transfer buffer is large enougth for the operation
                if (buffer.Length < index.Length)
                    buffer = new byte[index.Length];

                // read the entry data in the transfer buffer   
                int readen = storage.MapViews.Read(buffer, index.Position, index.Length);
#if DEBUG_
                System.Diagnostics.Debug.Assert(readen == index.Length, string.Format("readen ({0}) differs from index.Length ({1})", readen, index.Length));
#endif

                return readen;
            }

            public enum MatchMode { StartsWith, EndsWith, Contains, Equals }

            public unsafe IDictionary<long, int[]> MatchRows(string matchText, bool ignoreCase, MatchMode matchMode, bool firstFieldOnly, Records.FieldSet fieldSet, bool directMatch, long startRow, long endRow, IEnumerable<long> matchInRecords, bool forceParallel, bool dataIsSorted, Func<long, long> sortedRecordCallback = null)
            {
                byte[] matchData = IDataStorage.Encoder.GetBytes(matchText ?? string.Empty);
                byte[] matchAltData = IDataStorage.Encoder.GetBytes(!ignoreCase ? string.Empty : (matchText ?? string.Empty).ToLower());

                return MatchRows(matchData, matchAltData, matchData.Length, matchMode, firstFieldOnly, fieldSet, directMatch, startRow, endRow, matchInRecords, forceParallel, dataIsSorted, sortedRecordCallback);
            }

            public unsafe IDictionary<long, int[]> MatchRows(byte[] matchData, byte[] matchAltData, int matchLength, MatchMode matchMode, bool firstFieldOnly, Records.FieldSet fieldSet, bool directMatch, long startRow, long endRow, IEnumerable<long> matchInRecords, bool forceParallel, bool dataIsSorted, Func<long, long> sortedRecordCallback = null)
            {
                fixed (byte* matchPtr = matchData)
                fixed (byte* matchAltPtr = matchAltData)
                {
                    if (forceParallel)
                    {
                        IDictionary<long, int[]> rr = null;
                        long count = 1; // 100000;
                        while (count-- > 0)
                            rr = ExecuteParallelMatch(fieldSet, matchPtr, matchAltPtr, matchLength, matchMode, directMatch, firstFieldOnly);
                        return rr;
                    }
                    else
                    {
                        var resultRows = new Dictionary<long, int[]>();

                        if (matchInRecords != null)
                        {
                            foreach (var row in matchInRecords)
                            {
                                var fields = fieldSet.GetFields(row);
                                MatchRow(ref resultRows, ref _resultFields, fields, directMatch, row, matchPtr, matchAltPtr, matchLength, matchMode, firstFieldOnly, false);
                            }
                        }
                        else
                        {
                            if (dataIsSorted)
                            {
                                // Run optmized search, where the current value is cheched with the next value and the last result is provided based on the equity of the values

                                int lastValueSize = -1;
                                byte* lastValuePtr = (byte*)0;
                                bool lastFound = false;

                                for (long row = startRow; row < endRow; row++)
                                {
                                    var sorted = sortedRecordCallback.Invoke(row);

                                    var fields = fieldSet.GetFields(sorted);
                                    var field = fields[0];

                                    if (lastValueSize != field.Size || !DataCompare64_1(lastValuePtr, field.Pointer, lastValueSize))
                                    {
                                        if (MatchRow(ref resultRows, ref _resultFields, fields, directMatch, sorted, matchPtr, matchAltPtr, matchLength, matchMode, firstFieldOnly, false))
                                        {
                                            lastFound = true;
                                        }
                                        else
                                            lastFound = false;

                                        lastValueSize = field.Size;
                                        lastValuePtr = field.Pointer;
                                    }
                                    else if (lastFound)
                                    {
                                        resultRows.Add(sorted, new int[] { 0 });
                                    }
                                }
                            }
                            else
                                for (long row = startRow; row < endRow; row++)
                                {
                                    var fields = fieldSet.GetFields(row);
                                    MatchRow(ref resultRows, ref _resultFields, fields, directMatch, row, matchPtr, matchAltPtr, matchLength, matchMode, firstFieldOnly, false);
                                }

                        }

                        return resultRows;
                    }
                }
            }

            private static int parallelDegree = Environment.ProcessorCount;
            private readonly BlockingCollection<Action> _workActions = new BlockingCollection<Action>(new ConcurrentBag<Action>(), parallelDegree);
            private readonly CountdownEvent _signalWork = new CountdownEvent(parallelDegree);

            private unsafe IDictionary<long, int[]> ExecuteParallelMatch(Records.FieldSet fieldSet, byte* matchPtr, byte* matchAltPtr, int matchLength, MatchMode matchMode, bool directMatch, bool firstFieldOnly)
            {
                lock (_signalWork)
                    _signalWork.Reset();

                var resultRows = new Dictionary<long, int[]>();

                var partitioner = System.Collections.Concurrent.Partitioner.Create(0, storage.RowCount, storage.RowCount / parallelDegree);

                var unprocessedItems = parallelDegree;

                foreach (var rowsRange in partitioner.GetDynamicPartitions())
                {
                    Action action = () =>
                    {
                        for (long row = rowsRange.Item1, endRow = rowsRange.Item2; row < endRow; row++)
                            MatchRow(ref resultRows, ref _resultFieldsThreadSafe, fieldSet.GetFields(row), directMatch, row, matchPtr, matchAltPtr, matchLength, matchMode, firstFieldOnly, true);
                    };

                    unprocessedItems--;

                    _workActions.Add(action);
                }

                // signals any unprocessed workers
                if (unprocessedItems > 0)
                    _signalWork.Signal(unprocessedItems);

                _signalWork.Wait();

                /*
                Parallel.ForEach(partitioner, new ParallelOptions { MaxDegreeOfParallelism = parallelDegree },
                    rowsRange =>
                    {
                        foreach (var row in this.storage.GetRows(rowsRange.Item1, rowsRange.Item2, fieldSet))
                        {
                            SearchRow(ref resultRows, fieldSet, row, searchPtr, searchAltPtr, searchLength, searchMode, ignoreCase, firstFieldOnly);
                        }
                    });
                 */

                /*
                var partitioner = System.Collections.Concurrent.Partitioner.Create<IRow>(this.storage.GetRows(fieldSet), EnumerablePartitionerOptions.None);
                Parallel.ForEach<IRow>(partitioner, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, row =>
                {
                    SearchRow(ref resultRows, fieldSet, row, searchPtr, searchAltPtr, searchLength, searchMode, ignoreCase, firstFieldOnly);
                });
                */

                _searchLocker.EnterReadLock();
                try
                {
                    return new SortedDictionary<long, int[]>(resultRows);
                }
                finally
                {
                    _searchLocker.ExitReadLock();
                }
            }

            private readonly ReaderWriterLockSlim _searchLocker = new ReaderWriterLockSlim();

            [ThreadStatic]
            private static List<int> _resultFieldsThreadSafe;
            private List<int> _resultFields;
            private bool _modified;
            private readonly Dictionary<long, int> _blocksTrigeringRewrites = new Dictionary<long, int>();

            private AlignmentMode mode;

            /// <summary>
            /// Maximum size for single data value in bytes. At this version, it is 32Mb.
            /// </summary>
            internal const long MaxValueLength = 255; //<- 255 is temp!!!  32L * 1024L * 1024L;  // 32MB Max value size

            /// <summary>
            /// Maximum size for full entry (record entry) in bytes. At this version, it is 64Mb. Note: this depends on the map-view buffers per view page. it may increase in future versions.
            /// </summary>
            internal const long MaxEntryLength = 64L * 1024L * 1024L;  // 64MB Max value size

            private Action _contentModifiedCallback;
            private Action _contentBeginModifyCallback;

            private unsafe bool MatchRow(ref Dictionary<long, int[]> resultRows, ref List<int> resultFields, Records.FieldSet.Field[] fields, bool directMatch, long rowNumber, byte* matchPtr, byte* matchAltPtr, int matchLength, MatchMode matchMode, bool firstFieldOnly, bool threadSafe)
            {
                ushort fieldIndex = 0;
                bool found = false;
                bool hasMatch = false;

                if (resultFields != null)
                    resultFields.Clear();
                else
                    resultFields = new List<int>();

                // Thread.SpinWait(500);

                foreach (var field in fields)
                {
                    byte* fieldValue = field.Pointer;

                    /* if (ignoreCase) // convert the field value to lower-case
                     {
                         int position = 0;
                         int length = field.Size;
                         while (position < length)
                         {                                   
                            //if (char.IsUpper( (char)(*(fieldValue) )))
                             {
                               //  toLower(fieldValue);
                                 toLowerPtr(fieldValue);
                                       
                             }

                            fieldValue++;

                             position++;
                         }
                     }*/

                    // if the field match the search term, interupt the row search and add the row into the results
                    //if (false)
                    if ((directMatch && MatchFieldDirect(matchPtr, matchAltPtr, matchLength, fieldValue, field.Size, matchMode)) || (!directMatch && MatchFieldInverted(matchPtr, matchAltPtr, matchLength, fieldValue, field.Size, matchMode)))
                    {
                        hasMatch = true;

                        if (firstFieldOnly)
                        {
                            if (threadSafe)
                            {
                                _searchLocker.EnterWriteLock();
                                try
                                {
                                    resultRows.Add(rowNumber, new int[] { fields[fieldIndex].Ordinal });
                                }
                                finally
                                {
                                    _searchLocker.ExitWriteLock();
                                }
                            }
                            else
                                resultRows.Add(rowNumber, new int[] { fields[fieldIndex].Ordinal });

                            break;
                        }
                        else
                        {
                            found = true;
                            resultFields.Add(fields[fieldIndex].Ordinal);
                        }
                    }

                    fieldIndex++;
                }

                if (found)
                {
                    var fieldsArr = resultFields.ToArray();

                    if (threadSafe)
                    {
                        _searchLocker.EnterWriteLock();
                        try
                        {
                            resultRows.Add(rowNumber, fieldsArr);
                        }
                        finally
                        {
                            _searchLocker.ExitWriteLock();
                        }
                    }
                    else
                    {
                        resultRows.Add(rowNumber, fieldsArr);
                    }
                }

                return hasMatch;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
            private static byte toUpper(byte ch)
            {
                if (ch < 'a' || ch > 'z')
                {
                    return ch;
                }
                return (byte)(ch - ('a' - 'A'));
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
            private static byte toLower(byte ch)
            {
                if (ch < 'A' || ch > 'Z')
                {
                    return ch;
                }
                return (byte)(ch + ('a' - 'A'));
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
            private unsafe static void toLower(byte* ch)
            {
                if (*ch < (byte)'A' || *ch > (byte)'Z')
                    return;

                ch = (ch + ((byte)'a' - (byte)'A'));
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
            private unsafe static void toLowerPtr(byte* p)
            {
                switch ((char)*p)
                {
                    case 'A': *p = (byte)'a'; return;
                    case 'B': *p = (byte)'b'; return;
                    case 'C': *p = (byte)'c'; return;
                    case 'D': *p = (byte)'d'; return;
                    case 'E': *p = (byte)'e'; return;
                    case 'F': *p = (byte)'f'; return;
                    case 'G': *p = (byte)'g'; return;
                    case 'H': *p = (byte)'h'; return;
                    case 'I': *p = (byte)'i'; return;
                    case 'J': *p = (byte)'j'; return;
                    case 'K': *p = (byte)'k'; return;
                    case 'L': *p = (byte)'l'; return;
                    case 'M': *p = (byte)'m'; return;
                    case 'N': *p = (byte)'n'; return;
                    case 'O': *p = (byte)'o'; return;
                    case 'P': *p = (byte)'p'; return;
                    case 'Q': *p = (byte)'q'; return;
                    case 'R': *p = (byte)'r'; return;
                    case 'S': *p = (byte)'s'; return;
                    case 'T': *p = (byte)'t'; return;
                    case 'U': *p = (byte)'u'; return;
                    case 'V': *p = (byte)'v'; return;
                    case 'W': *p = (byte)'w'; return;
                    case 'X': *p = (byte)'x'; return;
                    case 'Y': *p = (byte)'y'; return;
                    case 'Z': *p = (byte)'z'; return;
                };
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static unsafe bool MatchFieldInverted(byte* matchValue, byte* matchValueAlt, int matchLength, byte* fieldValue, int fieldLength, MatchMode mode)
            {
                bool ignoreCase = IntPtr.Zero != (IntPtr)matchValueAlt;

                switch (mode)
                {
                    case MatchMode.StartsWith:
                        {
                            if (matchLength < fieldLength)
                                return false;
#if MODE_FIND_OPT1
                            int dp = 0;
                            while (dp < matchLength)
                            {
                                if (*(fieldValue + dp) != *(matchValue + dp))
                                    return false;
                                else
                                    dp++;
                            }

                            return true;
#elif MODE_FIND_OPT2
                            return ignoreCase ? DataCompare64_1(matchValue, fieldValue, matchValueAlt, fieldLength) : DataCompare64_1(matchValue, fieldValue, fieldLength);
#elif MODE_FIND_OPT3
                            return Native.memcmp(fieldValue, matchValue, matchLength) == 0;
#elif MODE_FIND_OPT4
                            return DataCompare64_2(fieldValue, matchValue, matchLength);
#endif
                        }
                    case MatchMode.EndsWith:
                        {
                            if (matchLength < fieldLength)
                                return false;
#if MODE_FIND_OPT1
                            int tp = fieldLength - matchLength;
                            int dp = 0;
                            while (dp < matchLength)
                            {
                                if (*(fieldValue + tp + dp) != *(matchValue + dp))
                                    return false;
                                else
                                    dp++;
                            }

                            return true;
#elif MODE_FIND_OPT2
                            return DataCompare64_1(matchValue + (matchLength - fieldLength), fieldValue, fieldLength);
#elif MODE_FIND_OPT3
                            return Native.memcmp(fieldValue + (fieldLength - matchLength), matchValue, matchLength) == 0;                              
#elif MODE_FIND_OPT4
                            return DataCompare64_2(fieldValue + (fieldLength - matchLength), matchValue, matchLength);
#endif
                        }
                    case MatchMode.Contains:
                        {
                            if (matchLength < fieldLength)
                                return false;
#if MODE_FIND_OPT1
                            int tp = 0;
                            int dp = 0;

                            while (tp <= fieldLength - matchLength)
                            {
                                if (*(fieldValue + tp + dp) != *(matchValue + dp))
                                    tp++;
                                else
                                    dp++;

                                if (dp == matchLength)
                                    return true;
                            }

                            return false;
#elif MODE_FIND_OPT2
                            int dp = 0;
                            while (dp + fieldLength <= matchLength)
                            {
                                if (DataCompare64_1(matchValue + dp, fieldValue, fieldLength))
                                    return true;
                                dp++;
                            }

                            return false;
#elif MODE_FIND_OPT3

                            int dp = 0;
                            while (dp + matchLength <= fieldLength)
                            {
                                if (Native.memcmp(fieldValue + dp, matchValue, matchLength) == 0)
                                    return true;
                                dp++;
                            }

                            return false;
#elif MODE_FIND_OPT4
                            int dp = 0;
                            while (dp + matchLength <= fieldLength)
                            {
                                if (DataCompare64_2(fieldValue + dp, matchValue, matchLength))
                                    return true;
                                dp++;
                            }

                            return false;
#endif
                        }
                    case MatchMode.Equals:
                        {
                            if (matchLength != fieldLength)
                                return false;
#if MODE_FIND_OPT1
                            int dp = 0;
                            while (dp < matchLength)
                            {
                                if (*(fieldValue + dp) != *(matchValue + dp))
                                    return false;
                                else
                                    dp++;
                            }

                            return true;
#elif MODE_FIND_OPT2
                            return DataCompare64_1(matchValue, fieldValue, fieldLength);
#elif MODE_FIND_OPT3
                            return Native.memcmp(fieldValue, matchValue, matchLength) == 0;
#elif MODE_FIND_OPT4
                            return DataCompare64_2(fieldValue, matchValue, matchLength);
#endif
                        }
                    default:
                        throw new InvalidOperationException("Search mode not implemented");
                }
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static unsafe bool MatchFieldDirect(byte* matchValue, byte* matchValueAlt, int matchLength, byte* fieldValue, int fieldLength, MatchMode mode)
            {
                bool ignoreCase = IntPtr.Zero != (IntPtr)matchValueAlt;

                switch (mode)
                {
                    case MatchMode.StartsWith:
                        {
                            if (fieldLength < matchLength)
                                return false;
#if MODE_FIND_OPT1
                            int dp = 0;
                            while (dp < matchLength)
                            {
                                if (*(fieldValue + dp) != *(matchValue + dp))
                                    return false;
                                else
                                    dp++;
                            }

                            return true;
#elif MODE_FIND_OPT2
                            return ignoreCase ? DataCompare64_1(fieldValue, matchValue, matchValueAlt, matchLength) : DataCompare64_1(fieldValue, matchValue, matchLength);
#elif MODE_FIND_OPT3
                            return Native.memcmp(fieldValue, matchValue, matchLength) == 0;
#elif MODE_FIND_OPT4
                            return DataCompare64_2(fieldValue, matchValue, matchLength);
#endif
                        }
                    case MatchMode.EndsWith:
                        {
                            if (fieldLength < matchLength)
                                return false;
#if MODE_FIND_OPT1
                            int tp = fieldLength - matchLength;
                            int dp = 0;
                            while (dp < matchLength)
                            {
                                if (*(fieldValue + tp + dp) != *(matchValue + dp))
                                    return false;
                                else
                                    dp++;
                            }

                            return true;
#elif MODE_FIND_OPT2
                            return DataCompare64_1(fieldValue + (fieldLength - matchLength), matchValue, matchLength);
#elif MODE_FIND_OPT3
                            return Native.memcmp(fieldValue + (fieldLength - matchLength), matchValue, matchLength) == 0;                              
#elif MODE_FIND_OPT4
                            return DataCompare64_2(fieldValue + (fieldLength - matchLength), matchValue, matchLength);
#endif
                        }
                    case MatchMode.Contains:
                        {
                            if (fieldLength < matchLength)
                                return false;
#if MODE_FIND_OPT1
                            int tp = 0;
                            int dp = 0;

                            while (tp <= fieldLength - matchLength)
                            {
                                if (*(fieldValue + tp + dp) != *(matchValue + dp))
                                    tp++;
                                else
                                    dp++;

                                if (dp == matchLength)
                                    return true;
                            }

                            return false;
#elif MODE_FIND_OPT2
                            int dp = 0;
                            while (dp + matchLength <= fieldLength)
                            {
                                if (DataCompare64_1(fieldValue + dp, matchValue, matchLength))
                                    return true;
                                dp++;
                            }

                            return false;
#elif MODE_FIND_OPT3

                            int dp = 0;
                            while (dp + matchLength <= fieldLength)
                            {
                                if (Native.memcmp(fieldValue + dp, matchValue, matchLength) == 0)
                                    return true;
                                dp++;
                            }

                            return false;
#elif MODE_FIND_OPT4
                            int dp = 0;
                            while (dp + matchLength <= fieldLength)
                            {
                                if (DataCompare64_2(fieldValue + dp, matchValue, matchLength))
                                    return true;
                                dp++;
                            }

                            return false;
#endif
                        }
                    case MatchMode.Equals:
                        {
                            if (fieldLength != matchLength)
                                return false;
#if MODE_FIND_OPT1
                            int dp = 0;
                            while (dp < matchLength)
                            {
                                if (*(fieldValue + dp) != *(matchValue + dp))
                                    return false;
                                else
                                    dp++;
                            }

                            return true;
#elif MODE_FIND_OPT2
                            return DataCompare64_1(fieldValue, matchValue, matchLength);
#elif MODE_FIND_OPT3
                            return Native.memcmp(fieldValue, matchValue, matchLength) == 0;
#elif MODE_FIND_OPT4
                            return DataCompare64_2(fieldValue, matchValue, matchLength);
#endif
                        }
                    default:
                        throw new InvalidOperationException("Search mode not implemented");
                }
            }


            // Copyright (c) 2008-2013 Hafthor Stefansson
            // Distributed under the MIT/X11 software license
            // Ref: http://www.opensource.org/licenses/mit-license.php.
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            // [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
            private static unsafe bool DataCompare64_1(byte* x1, byte* x2, int length)
            {
                for (int i = 0; i < length / 8; i++, x1 += 8, x2 += 8)
                    if (*((long*)x1) != *((long*)x2)) return false;

                if ((length & 4) != 0) { if (*((int*)x1) != *((int*)x2)) return false; x1 += 4; x2 += 4; }

                if ((length & 2) != 0) { if (*((short*)x1) != *((short*)x2)) return false; x1 += 2; x2 += 2; }

                if ((length & 1) != 0) if (*((byte*)x1) != *((byte*)x2)) return false;

                return true;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static unsafe int DataValueCompare(byte* buffer1, byte* buffer2, int length)
            {
                // var result = StringComparer.Ordinal.Compare(buffer1, buffer2);
                return 0;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
            private static unsafe bool DataCompare64_1(byte* data, byte* lcase, byte* ucase, int length)
            {
                for (int i = 0; i < length / 8; i++, data += 8, lcase += 8)
                    if (*((long*)data) != *((long*)lcase) && *((long*)data) != *((long*)ucase)) return false;

                if ((length & 4) != 0) { if (*((int*)data) != *((int*)lcase) && *((int*)data) != *((int*)ucase)) return false; data += 4; lcase += 4; }

                if ((length & 2) != 0) { if (*((short*)data) != *((short*)lcase) && *((short*)data) != *((short*)ucase)) return false; data += 2; lcase += 2; }

                if ((length & 1) != 0) if (*((byte*)data) != *((byte*)lcase) && *((byte*)data) != *((byte*)ucase)) return false;

                return true;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
            private static unsafe bool DataCompare64_2(byte* buff1, byte* buff2, int length)
            {
                byte* chPtr = buff1;
                byte* chPtr2 = buff2;
                while (length >= 20)
                {
                    if ((((*(((int*)chPtr)) != *(((int*)chPtr2))) ||
                    (*(((int*)(chPtr + 4))) != *(((int*)(chPtr2 + 4))))) ||
                    ((*(((int*)(chPtr + 8))) != *(((int*)(chPtr2 + 8)))) ||
                    (*(((int*)(chPtr + 12))) != *(((int*)(chPtr2 + 12)))))) ||
                    (*(((int*)(chPtr + 16))) != *(((int*)(chPtr2 + 16)))))
                        break;

                    chPtr += 20;
                    chPtr2 += 20;
                    length -= 20;
                }

                while (length >= 4)
                {
                    if (*(((int*)chPtr)) != *(((int*)chPtr2))) break;
                    chPtr += 4;
                    chPtr2 += 4;

                    length -= 4;
                }

                while (length > 0)
                {
                    if (*chPtr != *chPtr2) break;
                    chPtr++;
                    chPtr2++;
                    length--;
                }

                return (length <= 0);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
            private static unsafe bool DataCompare64_3(byte* buff1, byte* buff2, int length)
            {
                byte* chPtr = buff1;
                byte* chPtr2 = buff2;
                byte* chPtr3 = chPtr;
                byte* chPtr4 = chPtr2;

                while (length >= 10)
                {
                    if ((((*(((int*)chPtr3)) != *(((int*)chPtr4))) || (*(((int*)(chPtr3 + 2))) != *(((int*)(chPtr4 + 2))))) || ((*(((int*)(chPtr3 + 4))) != *(((int*)(chPtr4 + 4)))) || (*(((int*)(chPtr3 + 6))) != *(((int*)(chPtr4 + 6)))))) || (*(((int*)(chPtr3 + 8))) != *(((int*)(chPtr4 + 8)))))
                    {
                        break;
                    }
                    chPtr3 += 10;
                    chPtr4 += 10;
                    length -= 10;
                }

                while (length > 0)
                {
                    if (*(((int*)chPtr3)) != *(((int*)chPtr4)))
                    {
                        break;
                    }
                    chPtr3 += 2;
                    chPtr4 += 2;
                    length -= 2;
                }

                return (length <= 0);
            }

            public void RestoreData(string folder, bool mergeDataFiles = false, bool allowMissingDataFiles = true)
            {
                var backupFolder = Path.Combine(folder, storage.Name);
                if (!Directory.Exists(backupFolder))
                    throw new IOException("Backup folder does not exist");

                //  Restore the table index data 
                //  Restore the mmf files content into the map views

                using (var stream = File.OpenRead(Path.Combine(backupFolder, "table.meta")))
                    this.RestoreMeta(stream);

                var dataFiles = Directory.EnumerateFiles(backupFolder, "*.data").OrderBy(file => file).ToArray();
                if (!allowMissingDataFiles && dataFiles.Length == 0)
                    throw new IOException("Storage map view data files are missing");

                storage.MapViews.AssignMapFiles(dataFiles);
            }

            internal void RestoreMeta(Stream stream)
            {
                //  Restore the table index data 
                //  Restore the mmf files content into the map views
                using (var reader = new BinaryReader(stream, Encoding.Default, true))
                {
                    storage.SetRowCount(reader.ReadInt64());
                    storage.SetColumnCount(reader.ReadInt32());
                    storage.IncreaseDataSize((-storage.DataSize) + reader.ReadInt64());

                    storage.Blocks.RestoreData(reader);

                    // storage.Index.Restore(reader);
                }
            }

            public void BackupData(string folder, bool mergeDataFiles = false, bool overwriteExistingStructure = false)
            {
                lock (this)
                {
                    var backupFolder = new DirectoryInfo(Path.Combine(folder, storage.Name));

                    // Gets map page files which were not persistent yet and needs to be.
                    // The folder location of the file can be used to determine of the file is peristent or page file
                    var unpresistedMapFiles = storage.MapViews.GetMapFiles()
                        .Where(filePath => Path.GetDirectoryName(filePath) != backupFolder.FullName);

                    if (unpresistedMapFiles.Count() > 0)
                    {
                        if (overwriteExistingStructure)
                            while (Directory.Exists(backupFolder.FullName))
                            {
                                backupFolder.Delete(true);
                                Thread.Sleep(220);
                            }

                        // Ensure the backup folder path is created
                        while (!Directory.Exists(backupFolder.FullName))
                        {
                            backupFolder.Create();
                            Thread.Sleep(220);
                        }

                        // Close all views to release memory and the flush pending changes
                        storage.MapViews.ReleaseViews();

                        int totalDataFiles = 0;

                        if (mergeDataFiles)
                        {
                            // Merge all data files in one

                            throw new NotImplementedException();
                        }
                        else
                        {
                            // Store the mmf files to physical storage location
                            foreach (var pageMapFile in unpresistedMapFiles)
                            {
                                var persistentMapFile = Path.Combine(backupFolder.FullName, Path.GetFileName(pageMapFile));

                                File.Copy(pageMapFile, persistentMapFile, true);

                                totalDataFiles++;
                            }
                        }
                    }

                    this.BackupMeta(folder);
                }
            }

            /// <summary>
            /// Backup the table meta data into stream
            /// </summary>
            /// <param name="stream"></param>
            internal void BackupMeta(Stream stream)
            {
                lock (this)
                {
                    // Write the Table Meta data header and content 
                    using (var writer = new BinaryWriter(stream))
                    {
                        // Store table state, like row and column count
                        writer.Write(storage.RowCount);
                        writer.Write(storage.ColumnCount);
                        writer.Write(storage.DataSize);

                        storage.Blocks.BackupData(writer);

                        // Store the table index data
                        //storage.Index.Backup(writer);

                        //writer.Write(storage.MapViews.GetMapFiles().Length);
                    }

                    _modified = false;
                }
            }

            /// <summary>
            /// Backup the table meta data into folder location
            /// </summary>
            /// <param name="folder"></param>
            public void BackupMeta(string folder)
            {
                lock (this)
                {
                    var backupFolder = Path.Combine(folder, storage.Name);
                    while (!Directory.Exists(backupFolder))
                    {
                        Directory.CreateDirectory(backupFolder);
                        Thread.Sleep(220);
                    }

                    this.BackupMeta(File.Open(Path.Combine(backupFolder, "table.meta"), FileMode.Create));
                }
            }

            /// <summary>
            /// Gets the operation info associated with the transation if supported
            /// </summary>
            /// <param name="transation"></param>
            /// <returns></returns>
            public CapacityOperationInfo GetOperationInfo(ITransaction transation)
            {
                if (transation is ModifyTransation)
                    return ((ModifyTransation)transation).OperationInfo;
                throw new NotSupportedException();
            }

            public struct CapacityOperationInfo
            {
                public readonly int BlocksAffected;
                public readonly long SizeReorganized;
                public readonly long SizeUpdated;
                public readonly static CapacityOperationInfo Empty = new CapacityOperationInfo();

                internal CapacityOperationInfo(int blocksAffected, long sizeReorganized, long sizeUpdated)
                {
                    BlocksAffected = blocksAffected;
                    SizeReorganized = sizeReorganized;
                    SizeUpdated = sizeUpdated;
                }

                public override string ToString()
                {
                    return string.Format("BlocksAffected: {0}, SizeReorganized: {1}, SizeUpdated: {2}", BlocksAffected, SizeReorganized, SizeUpdated);
                }
            }

            public struct DefragmentationOperationInfo
            {
                public readonly Dictionary<long, long> EntriesAffected;
                public readonly int EntriesAffectedCount;
                public readonly int EntriesProcessed;

                internal DefragmentationOperationInfo(Dictionary<long, long> entriesAffected, int entriesAffectedCount, int entriesProcessed)
                {
                    this.EntriesAffected = entriesAffected;
                    this.EntriesAffectedCount = entriesAffectedCount;
                    this.EntriesProcessed = entriesProcessed;
                }

                public override string ToString()
                {
                    if (EntriesAffected != null)
                    {
                        var sb = new StringBuilder();
                        foreach (var item in EntriesAffected)
                            sb.Append(item.Key).Append(":").Append(item.Value).Append(", ");

                        return string.Format("Entries Total: {0}, Affected: {1}, List: {2}", EntriesProcessed, EntriesAffectedCount, sb.ToString());
                    }
                    return string.Format("Entries Total: {0}, Affected: {1}", EntriesProcessed, EntriesAffectedCount);
                }
            }

            public ITransaction BeginModifyTransaction()
            {
                if (transactionCurrent != null)
                    throw new InvalidOperationException("Failed to begin transaction: There is already active transaction");
                return transactionCurrent = new ModifyTransation(this);
            }

            public ITransaction BeginInsertTransaction()
            {
                if (transactionCurrent != null)
                    throw new InvalidOperationException("Failed to begin transaction: There is already active transaction");
                return transactionCurrent = new InsertTransation(this);
            }

            public ITransaction BeginExtendTransaction(int columnsToExtend)
            {
                if (transactionCurrent != null)
                    throw new InvalidOperationException("Failed to begin transaction: There is already active transaction");
                return transactionCurrent = new ExtendTransation(this, columnsToExtend);
            }

            /// <summary>
            /// Begins records remove transaction
            /// </summary>
            /// <param name="defragmentOnCommit">On transation commit, data defragmentation will be executed which will free the undelaying space taken by the data removed</param>
            /// <returns></returns>
            public ITransaction BeginRemoveTransaction(bool defragmentOnCommit = true)
            {
                if (transactionCurrent != null)
                    throw new InvalidOperationException("Failed to begin transaction: There is already active transaction");
                return transactionCurrent = new RemoveTransaction(this, defragmentOnCommit);
            }
  
          
            public void Dispose()
            {
                _workActions.CompleteAdding();
            }

            /// <summary>
            /// Indicates if the data has been changed and needs to be commited if the storage is in persistent mode
            /// </summary>
            public bool Modified { get { return _modified; } }

            public void SetModified()
            {
                _modified = true;

                if (_contentModifiedCallback != null)
                    _contentModifiedCallback();
            }


            /// <summary>
            /// Removes entry from the data storage
            /// </summary>
            /// <param name="record">Entry to remove</param>
            /// <returns>Returns the complete data of this entry after it has been removed</returns>
            public byte[] RemoveRecord(long record)
            {
                // Check for transation presence
                var transation = transactionCurrent as RemoveTransaction;
                if (transation != null && !transation.IsProcessing)
                {
                    transation.Remove(record);
                    return null;
                }

                if (_contentBeginModifyCallback != null)
                    _contentBeginModifyCallback();

                // Removes the entry index
                var entryIndex = blocks.GetEntryIndex(record);

                // Determines and entry header and data lengths for unregitration purposes
                byte[] buffer = new byte[entryIndex.Length];
                ReadEntry(record, ref buffer);
                int lengthHeader;
                int lengthData;
                this.GetEntryMetaLengths(buffer, out lengthHeader, out lengthData, storage.ColumnCount, entryIndex.Length);

                long block = blocks.GetBlock(record);

                // Rewrite all block entries, shifting them over this entry data
                this.RewriteBlockDescendant(block, record + 1, -1 * entryIndex.Length);
                storage.Index.Remove(record, 0, blocks.RowEntries);

                // Defragment this and all descending blocks
                int lengthDefragmented = 0;
                for (long currentBlock = block; currentBlock < blocks.Blocks; currentBlock++)
                    lengthDefragmented += this.DefragmentBlock(currentBlock, true, false).EntriesAffectedCount;

#if DEBUG_
                System.Diagnostics.Debug.WriteLine("Blocks Defragmented: {0}, Entries Affected: {1}", blocks.Blocks - block, lengthDefragmented);
#endif

                storage.SetRowCount(storage.RowCount - 1);

                blocks.UnregisterEntry(entryIndex.Length);
                storage.IncreaseDataSize(-1 * lengthData);  // unregister only the data size of the entry, without the header size


                if (storage.DataSize < 0)
                    throw new InvalidOperationException("Storage data size dropped bellow zero (" + storage.DataSize + "). Implementation inspection suggested.");

                this.SetModified();

                return buffer;
            }

            /// <summary>
            /// Insert entry into the data storage. The operation ensures there is room for the entry in the entry block, but it does not actually write the entry content.
            /// </summary>
            /// <param name="nullable"></param>
            internal void InsertEntry(long entry, long position, int lengthData)
            {
                // Validate maximum entry size bounds
                if (lengthData > ContentManager.MaxEntryLength)
                    throw new InvalidOperationException(string.Format("Size of the full entry exceed the max allowed. Size: {0}, Max: {1}", lengthData, ContentManager.MaxEntryLength));

                bool currentInTransation = !(transactionCurrent == null || !(transactionCurrent is InsertTransation) || !((InsertTransation)transactionCurrent).IsProcessing);

                var block = blocks.GetBlock(entry);

                if (!currentInTransation)
                {
                    if (_contentBeginModifyCallback != null)
                        _contentBeginModifyCallback();

                    var opinfo = this.EnsureBlockCapacity(block, lengthData);
                    if (currentInTransation && opinfo.BlocksAffected > 0)
                    {
                        // double check for the actual free space
                        long actuallyFree = blocks.GetBlockSpaceFree(block);
                        System.Diagnostics.Debug.WriteLine("\t\t\t  WARN! Re-Ensuring capacity block {0}, currentRecords {1}, currentFree {2}  [{3}]", block, storage.RowCount, actuallyFree, opinfo);
                    }
                }

                if (!currentInTransation)
                {
                    // Pushes all descending entries to ensure there is anougth room for this entry data, in the corresponding entry block
                    // Rewrite all block entries, shifting them to make room for this entry data, but excluding the last block entry
                    RewriteBlockDescendant(block, entry, lengthData);
                }

                storage.Index.Insert(entry, 0, 1, position, lengthData);

                if (!currentInTransation)
                {
                    // Defragment this and all descending blocks, only if there is no insert transation running currently.
                    // Otherwise, execute thie in group into the transation accumulation
                    int lengthDefragmented = 0;
                    for (long currentBlock = block; currentBlock < blocks.Blocks; currentBlock++)
                        lengthDefragmented += this.DefragmentBlock(currentBlock, true, true).EntriesAffectedCount;

                    this.SetModified();
                }

#if DEBUG_
                System.Diagnostics.Debug.WriteLine("Blocks Defragmented: {0}, Entries Affected: {1}", blocks.Blocks - block, lengthDefragmented);
#endif

            }

            /// <summary>
            /// Updates values for one or more fields in record belonging to certain block
            /// </summary>           
            /// <param name="record"></param>
            /// <param name="values">List of columns as keys and the associated values</param>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private void UpdateBlockRecordValues(long record, Dictionary<int, byte[]> values, ref Dictionary<long, byte[]> entriesValues)
            {
                int oldEntryLength, oldHeaderLength, newEntryLength, newHeaderLength;

                byte[] entryData = this.PrepareRecordUpdate(record, values, out oldEntryLength, out oldHeaderLength, out newEntryLength, out newHeaderLength);

                var affectedEntryLength = newEntryLength - oldEntryLength;
                var affectedHeaderLength = newHeaderLength - oldHeaderLength;

                entriesValues.Add(record, entryData);

                // Unregistrer all entries first then register them back again with the new lengths

                blocks.RegisterEntry(affectedEntryLength);

                // Adjust the storage data size too. 
                // NOTE: This should include only the data size, without the entry header size, but currently we use the entry size. Inspection might be needed in future
                storage.IncreaseDataSize(affectedEntryLength); // + newHeaderLength);                
            }

            /// <summary>
            /// Updates values for one or more fields in record belonging to certain block
            /// </summary>
            /// <param name="block">The block containing the record</param>
            /// <param name="record"></param>
            /// <param name="values">List of columns as keys and the associated values</param>
            private void UpdateBlockRecordValues(long block, long record, Dictionary<int, byte[]> values)
            {
                int oldEntryLength, oldHeaderLength, newEntryLength, newHeaderLength;

                byte[] entryData = this.PrepareRecordUpdate(record, values, out oldEntryLength, out oldHeaderLength, out newEntryLength, out newHeaderLength);

                var affectedEntryLength = newEntryLength - oldEntryLength;
                var affectedHeaderLength = newHeaderLength - oldHeaderLength;

                var entriesValues = new Dictionary<long, byte[]>();

                entriesValues.Add(record, entryData);

                this.RewriteBlock(block, entriesValues);

                // Unregistrer all entries first then register them back again with the new lengths

                blocks.RegisterEntry(affectedEntryLength);

                // Adjust the storage data size too. 
                // NOTE: This should include only the data size, without the entry header size, but currently we use the entry size. Inspection might be needed in future
                storage.IncreaseDataSize(affectedEntryLength); // + newHeaderLength);                
            }

            /// <summary>
            /// Inserts record entry into the data store
            /// </summary>
            /// <param name="header"></param>
            public void InsertRecord(InsertRecordHeader header)
            {
                // Check for transation presence
                var transation = transactionCurrent as InsertTransation;
                if (transation != null && !transation.IsProcessing)
                {
                    transation.Insert(header);
                    return;
                }

                storage.AddRow(header);

                if (transation == null || !transation.IsProcessing)
                    storage.Complete();
            }

            /// <summary>
            /// Extends the entire range of records storage for new column
            /// </summary>
            /// <param name="record"></param>
            /// <param name="columnCount"></param>
            /// <returns></returns>
            public CapacityOperationInfo ExtendRecords(long recordStart, long recordCount, int extendByColumnCount)
            {
                if (storage.RecordHeaderFormat != RecordHeaderFormats.HybridFixed)
                    throw new NotImplementedException("Record extension for HeaderFormat other then HybridFixed has to be implemented");

                /*  APPROACH    ****************************************************/
                //  1. The record header has to be modified to fit the meta data for the new extended column, which is 0 bytes by default
                //  2. Move all block descendant entries with 1 byte, which is the size of the column offset into the header
                //  3. Move the record content data by 1 byte too.
                //  4. Append the column offset meta data into the record header
                /*******************************************************************/

                //  Notify content modification is about to start so it can be backed up by the versioning manager
                if (_contentBeginModifyCallback != null)
                    _contentBeginModifyCallback();

                //  WORKAROUND:
                //      Force table defragmentation regardless the AutoDefragment setting.
                //      It is workaround for cases where the table has zero block buffers despite having them configured to non zero. Normally this is observed when the table has not AutoDefragment set.
                //      So this can be considered as bug and needs to be inspected. 
                this.DefragmentStorage(0);

                // Ensure the extended column count first
                storage.SetColumnCount(storage.ColumnCount + extendByColumnCount);

                long blockPrev = -1;

                for (long record = recordStart; record < recordStart + recordCount; record++)
                {
                    var block = blocks.GetBlock(record);
                    if (block > blockPrev)
                    {
                        // Ensure capacity for all column offsets for all block records

                        var opinfo = this.EnsureBlockCapacity(block, blocks.EntryBlock * (extendByColumnCount * 3));   // Purposely request larger portion to reduce number of blocks relocations
                        if (/*currentInTransation && */ opinfo.BlocksAffected > 0)
                        {
                            // double check for the actual free space
                            long actuallyFree = blocks.GetBlockSpaceFree(block);
                            System.Diagnostics.Debug.WriteLine("\t\t\t  WARN! Re-Ensuring capacity block {0}, currentRecords {1}, currentFree {2}  [{3}]", block, storage.RowCount, actuallyFree, opinfo);
                        }

                        blockPrev = block;
                    }

                    this.RewriteBlockDescendant(block, record + 1, extendByColumnCount);

                    int length = 0, lengthHeader = 0, lengthData;

                    byte[] entryDataOrig = this.GetEntryData(record, out length);
                    byte[] entryDataNew = new byte[length + extendByColumnCount];

                    this.GetEntryMetaLengths(entryDataOrig, out lengthHeader, out lengthData, storage.ColumnCount - extendByColumnCount, length);

                    System.Diagnostics.Debug.Assert(length == lengthData + lengthHeader);

                    //  The lengthHeader here, contains the original header length, which will be used as starting point to rewrite the record content data
                    //  Move the record content data by one byte and append the new column offset into the header

                    Buffer.BlockCopy(entryDataOrig, 0, entryDataNew, 0, lengthHeader);   // Copy the header first
                    Buffer.BlockCopy(entryDataOrig, lengthHeader, entryDataNew, lengthHeader + extendByColumnCount, lengthData);  // Copy the record content next, shifting it by one byte

                    //  Alter the header-length meta with the new value (HybridFixed format only)
                    fixed (byte* buffer = entryDataNew)
                        Native.Utils.NumberToBytes(buffer, lengthHeader + extendByColumnCount, 2);

                    this.WriteEntry(record, entryDataNew, entryDataNew.Length, 0); // The -1 is for setting this record back to the original position
                }

                blocks.RegisterEntry(recordCount * extendByColumnCount);
                storage.IncreaseDataSize(recordCount * extendByColumnCount);


                //  Indicate the content is modified so it can be handled by the versioning manager
                this.SetModified();

                return new CapacityOperationInfo();
            }


            /// <summary>
            /// Extends a record for new column, by appending column meta data into it.
            /// </summary>
            /// <param name="record"></param>
            /// <param name="columnCount"></param>
            /// <returns></returns>
            [Obsolete("Used for test or benchmark purposes. Use the other overload for range of records to expand")]
            private CapacityOperationInfo ExtendRecord(long record, int columnCount)
            {
                /*  APPROACH    ****************************************************/
                //  1. The record header has to be modified to fit the meta data for the new extended column, which is 0 bytes by default
                //  2. Move all block descendant entries with 1 byte, which is the size of the column offset into the header
                //  3. Move the record content data by 1 byte too.
                //  4. Append the column offset meta data into the record header
                /*******************************************************************/

                // Ensure the extended column count first
                storage.SetColumnCount(columnCount);

                var block = blocks.GetBlock(record);

                var opinfo = this.EnsureBlockCapacity(block, 1);
                if (/*currentInTransation && */ opinfo.BlocksAffected > 0)
                {
                    // double check for the actual free space
                    long actuallyFree = blocks.GetBlockSpaceFree(block);
                    System.Diagnostics.Debug.WriteLine("\t\t\t  WARN! Re-Ensuring capacity block {0}, currentRecords {1}, currentFree {2}  [{3}]", block, storage.RowCount, actuallyFree, opinfo);
                }

                this.RewriteBlockDescendant(block, record + 1, 1);

                int length = 0, lengthHeader = 0, lengthData;

                byte[] entryDataOrig = this.GetEntryData(record, out length);
                byte[] entryDataNew = new byte[length + 1];

                this.GetEntryMetaLengths(entryDataOrig, out lengthHeader, out lengthData, storage.ColumnCount - 1, length);

                System.Diagnostics.Debug.Assert(length == lengthData + lengthHeader);

                //  The lengthHeader here, contains the original header length, which will be used as starting point to rewrite the record content data
                //  Move the record content data by one byte and append the new column offset into the header

                Buffer.BlockCopy(entryDataOrig, 0, entryDataNew, 0, lengthHeader);   // Copy the header first
                Buffer.BlockCopy(entryDataOrig, lengthHeader, entryDataNew, lengthHeader + 1, lengthData);  // Copy the record content next, shifting it by one byte

                this.WriteEntry(record, entryDataNew);

                blocks.RegisterEntry(1);
                storage.IncreaseDataSize(1);

                return new CapacityOperationInfo();
            }

            public bool EnableParallelSearch { get; set; }

            /// <summary>
            /// Restores persistent data files
            /// </summary>
            /// <param name="p"></param>
            internal void RestoreDataFiles(string dataFolder)
            {
                if (!Directory.Exists(dataFolder))
                    throw new IOException("Data folder does not exist");

                // Attach the persistent data map files
                var dataFiles = Directory.EnumerateFiles(dataFolder, "*.data").OrderBy(file => file).ToArray();
                storage.MapViews.AssignMapFiles(dataFiles);
            }

            /// <summary>
            /// Provide delegate which will be called when table content changed. Usually, this happends after every successfull transation commit operation.
            /// </summary>
            internal void OnContentModified(Action callback)
            {
                _contentModifiedCallback = callback;
            }

            /// <summary>
            /// Provide delegate which will be called when table content is about to be modified. Usually, this happends on every transation begin operation.
            /// </summary>
            internal void OnBeginContentModification(Action callback)
            {
                _contentBeginModifyCallback = callback;
            }
        }

    }
}
