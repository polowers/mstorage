﻿using System.IO;
using System.Runtime;
using System.Runtime.CompilerServices;

namespace DigitalToolworks.Data
{
    public static class Variant
    {
        /// <summary>
        /// The maximum value (exluding) which can be store in single byte variant structure. Equals to 128.
        /// </summary>
        public const int MaxSingleByteValue = 128;

        /// <summary>Writes an long in a variable-length format.  Writes between one and five
        /// bytes.  Smaller values take fewer bytes.  Negative numbers are not
        /// supported.
        /// </summary>
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte WriteVLong(Stream memory, long value)
        {
            byte[] buffer = new byte[9];
            byte length = 0;
            while ((value & ~0x7F) != 0)
            {
                buffer[length] = (byte)((value & 0x7f) | 0x80);
                value = value >> 7;
                length++;
            }
            if (length == 0)
            {
                memory.WriteByte((byte)value);
                length++;
            }
            else
            {
                buffer[length] = (byte)value;
                length++;
                memory.Write(buffer, 0, length);
            }
            return length;
        }

        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte WriteVInt(Stream memory, int value)
        {
            byte[] buffer = new byte[5];
            byte length = 0;
            while ((value & ~0x7F) != 0)
            {
                buffer[length] = (byte)((value & 0x7f) | 0x80);
                value = value >> 7;
                length++;
            }
            if (length == 0)
            {
                memory.WriteByte((byte)value);
                length++;
            }
            else
            {
                buffer[length] = (byte)value;
                length++;
                memory.Write(buffer, 0, length);
            }
            return length;
        }

        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe static int WriteVInt(byte* memory, int value)
        {
            //  byte[] buffer = new byte[5];
            int length = 0;
            while ((value & ~0x7F) != 0)
            {
                memory[length] = (byte)((value & 0x7f) | 0x80);
                value = value >> 7;
                length++;
            }

            if (length == 0)
            {
                memory[length] = (byte)value;
                length++;
            }
            else
            {
                memory[length] = (byte)value;
                length++;
            }

            return length;
        }

        /// <summary>Reads a long stored in variable-length format.  Reads between one and
        /// nine bytes.  Smaller values take fewer bytes.  Negative numbers are not
        /// supported. 
        /// </summary>
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static long ReadVLong(Stream memory)
        {
            byte b = (byte)memory.ReadByte();
            long i = b & 0x7F;
            for (int shift = 7; (b & 0x80) != 0; shift += 7)
            {
                b = (byte)memory.ReadByte();
                i |= (b & 0x7FL) << shift;
            }
            return i;
        }

        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe static int ReadVInt(byte* memory, out int length)
        {
            length = 1;
            byte b = *(memory);
            int value = b & 0x7F;
            for (int shift = 7; (b & 0x80) != 0; shift += 7)
            {
                b = memory[length++];
                value |= (b & 0x7F) << shift;
            }
            return value;
        }

        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe static int ReadVIntOpt(byte* memory, out int length)
        {
            length = 1;
            byte b = *memory;
            if ((b & 0x80) == (byte)0)
                return b & 0x7F;

            int value = b & 0x7F;
            for (int shift = 7; (b & 0x80) != 0; shift += 7)
            {
                b = memory[length++];
                value |= (b & 0x7F) << shift;
            }

            return value;
        }

        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe static ushort ReadVUInt16Opt(byte* memory, out ushort length)
        {
            length = 1;
            byte b = *memory;
            if ((b & 0x80) == 0)
                return (ushort)(b & 0x7F);

            ushort value = (ushort)(b & 0x7F);
            for (ushort shift = 7; (b & 0x80) != 0; shift += 7)
            {
                b = memory[length++];
                value |= (ushort)((b & 0x7F) << shift);
            }

            return value;
        }

        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int ReadVInt(Stream memory)
        {
            byte b = (byte)memory.ReadByte();
            int i = b & 0x7F;
            for (int shift = 7; (b & 0x80) != 0; shift += 7)
            {
                b = (byte)memory.ReadByte();
                i |= (b & 0x7F) << shift;
            }
            return i;
        }
    }
}
