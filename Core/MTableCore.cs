﻿#define OPT_SET_TIELD_META

using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;

namespace DigitalToolworks.Data
{
    public sealed unsafe partial class MTableCore : IDataStorage
    {
        readonly long Capacity;
        readonly string _name;
        readonly Block _block;
        readonly ContentManager _contentManager;

        internal readonly MapViewManager _mapViewManager;
        internal readonly Indexer _indexer;

        /// <summary>
        /// Counter for number of MTableCore instances. This is global application counter. Once the counter is zero, the Records queue can be disposed.
        /// </summary>
        static int instancesCount = 0;
        RecordQueueProcessor RecordQueue;

        /// <summary>
        /// The root metadata CORE Header object
        /// </summary>
        readonly MStorage.Metadata.Header.Core* HEADER;

        [ThreadStatic]
        byte[] _bufferEntry;
        char[] _bufferField;

        /// <summary>
        /// Data current position of the insert / append entries writer
        /// </summary>
        long _writerPosition;
        long _headerSize;

        public MTableCore(string name, string filename, long capacity /* 80000L; GB 80 */, long indexCapacityMb, bool persistMapFiles = false, AlignmentMode mode = AlignmentMode.RowMode, MemoryMappedFile metadataMapFile = null)
        {
            this.RecordHeaderFormat = RecordHeaderFormats.HybridFixed;

            _name = name;

            filename = filename ?? "storage.mmf";

            Capacity = Native.Utils.GetNearestSizeToAllocationGranularity(Environment.Is64BitProcess ? capacity : Math.Min(1024L * 1024L * 2000L, capacity));

            // use external map view stream for backing the metadata, including the data indexer
            if (metadataMapFile != null)
            {
                _metadataMapView = metadataMapFile.CreateViewStream(MStorage.Metadata.Header.MetaBlockSize, indexCapacityMb * 1024L * 1024L);

                // Allocate the CORE HEADER
                this.HEADER = (MStorage.Metadata.Header.Core*)Native.Utils.Memory.AcquirePointer(_metadataMapView);
                _indexer = new Indexer(&HEADER->Indexer);

                // Map only the indexer portion, which is after the table meta data fixed block size                
                //_indexer = new Indexer(metadataMapFile.CreateViewStream(MetaBlockSize, indexCapacityMb * 1024L * 1024L));
            }
            else
            {
                // Create in-memory metadata mmf stream
                metadataMapFile = MemoryMappedFile.CreateNew(null, MStorage.Metadata.Header.MetaBlockSize + (1024L * 1024L * (Environment.Is64BitProcess ? indexCapacityMb : 1000L)));

                _metadataMapView = metadataMapFile.CreateViewStream(MStorage.Metadata.Header.MetaBlockSize, indexCapacityMb * 1024L * 1024L);

                // Allocate the CORE HEADER
                this.HEADER = (MStorage.Metadata.Header.Core*)Native.Utils.Memory.AcquirePointer(_metadataMapView);
                _indexer = new Indexer(&HEADER->Indexer);

                ///_indexer = new Indexer(metadataMapFile.CreateViewStream(MetaBlockSize, indexCapacityMb * 1024L * 1024L));
            }

            _block = new Block(this, mode);
            _contentManager = new ContentManager(this, mode);
            _mapViewManager = new MapViewManager(this, filename, Capacity, persistMapFiles: persistMapFiles);
        }

        /// <summary>
        /// Format type of the record header meta. Defines how the sizes of the fields data are stored in the record header section
        /// </summary>
        public enum RecordHeaderFormats
        {
            /// <summary>
            /// Fields lengths are fixed to single byte (maximum length of 255 bytes)
            /// </summary>
            Fixed,
            /// <summary>
            /// Fields lengths are stores as variants. There is no limitation to the field length stored (slower on performance)
            /// </summary>
            Variant,
            /// <summary>
            /// Fields lengths are stored as single bytes, if their value is smaller then 128 and stored as variants, if larger.
            /// </summary>
            Hybrid,
            /// <summary>
            /// Fields lengths are stored as single bytes, if their value is smaller then 128 and stored as variants, if larger. The header length is fixed 2 bytes. This mode is faster, but it takes 2 bytes per record (64Kb max header size)
            /// </summary>
            HybridFixed
        }

        public enum AlignmentMode { RowMode, FieldMode }

        public ContentManager Manager { get { return _contentManager; } }
        public MapViewManager MapViews
        {
            get
            {
                return _mapViewManager;
            }
        }

        internal void SetColumnCount(int columnCount)
        {
            this.HEADER->Fields = columnCount;

            // Dont call the base implementation
        }

        public override long DataSize
        {
            get
            {
                return HEADER->DataSize;

                // Dont call the base implementation
            }
        }

        protected override void SetRowCount(long rowCount)
        {
            this.HEADER->Records = rowCount;

            // Dont call the base implementation
        }

        public override long RowCount
        {
            get
            {
                return HEADER->Records;

                // Dont call the base implementation
            }
        }

        protected override long IncreaseCount(int increaseBy = 1)
        {
            return (HEADER->Records += increaseBy);

            // Dont call the base implementation
        }

        protected override void IncreaseDataSize(long increaseBy)
        {
            if ((HEADER->DataSize += increaseBy) < 0)
                throw new InvalidOperationException("Storage data size dropped bellow zero (" + this.DataSize + "). Implementation inspection suggested.");

            // Dont call the base implementation
        }

        public override void Setup()
        {
            _bufferField = new char[1024 * 1024 * 4];
#if DEBUG_
            // Fast Allocate buffers and fill with test data
            unsafe
            {
                fixed (char* dest = _bufferField, src = "A")
                    Native.memset((byte*)dest, (int)'A', _bufferField.Length * sizeof(char));
            }
#endif
            instancesCount++;

            RecordQueue = RecordQueueProcessor.Instance;
        }

        internal Indexer Index
        {
            get
            {
                return _indexer;
            }
        }

        public Block Blocks { get { return _block; } }

        public override void Release()
        {
            instancesCount--;

            // Dispose the recordQueue only if the count is zero
            if (instancesCount == 0)
                this.RecordQueue.Dispose();

            ((IDisposable)this.MapViews).Dispose();

            this.Manager.Dispose();

            this.Index.Dispose();

            if (_metadataMapView != null)
            {
                _metadataMapView.SafeMemoryMappedViewHandle.ReleasePointer();
                _metadataMapView.Dispose();
                _metadataMapView = null;
            }
        }

        public override void Complete()
        {
            _insertLocker.EnterWriteLock();

            try
            {

                _block.RegisterEntryComplete();

                _writerPosition = 0;
                // reset the writer position to begin. on insertion this will be set to the proper position based on the last data index
                _insertionInProgress = false;
            }
            finally
            {
                _insertLocker.ExitWriteLock();
            }
        }

        public override string Name
        {
            get { return _name ?? "[MMFTable Storage]"; }
        }

        public struct Records : IEnumerable<Records.Record>
        {
            public struct Record
            {
                internal long number;
                internal FieldSet.Field[] fields;

                public long Number
                {
                    get { return number; }
                }

                public FieldSet.Field[] Fields
                {
                    get { return fields; }
                }
            }

            private readonly long _startRow;
            private readonly long _endRow;
            private readonly FieldSet _fieldSet;

            private Record _current;

            internal Records(MTableCore table, long startRow, long endRow, FieldSet fieldSet)
            {
                _startRow = startRow;
                _endRow = endRow;
                _fieldSet = fieldSet;

                //_current = new Row(table, -1, fieldSet.ordinals, (ushort)fieldSet.totalFields);
                _current = new Record();

                /// allocate field sets buffers
                /// Row.AllocateFieldSet(_current, fieldSet.Count);
            }

            public IEnumerator<Record> GetEnumerator()
            {
                for (var rowNumber = _startRow; rowNumber < _endRow; rowNumber++)
                {
                    _current.number = rowNumber;
                    _current.fields = _fieldSet.GetFields(rowNumber);
                    yield return _current;
                }

                yield break;
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }

            public struct FieldSet : IEquatable<long>        // We support passing entry key for equity comparision
            {
                readonly static EqualityComparer<FieldSet> Comparer = new EquityComparer();

                static char[] _bufferChars;  // Thread UNSAFE

                /// <summary>
                /// Total number of all fields in the table
                /// </summary>
                internal readonly int totalFields;

                readonly int _explicitFieldsLength;

                /// <summary>
                /// List of fields ordinals represented by this fieldset
                /// </summary>
                internal readonly int[] ordinals;

                readonly Field[] _fields;

                /// <summary>
                /// Cache of assigned fields used for the equity comparer
                /// </summary>
                readonly Field[] _fieldsComparable;

                /// <summary>
                /// Contains index-accessed collection of all fields. The purpose is fast accessing the field associated with ordinal
                /// </summary>
                readonly Field[] _accessorByOrdinal;

                readonly Indexer _indexer;
                readonly MapViewManager _mapViews;
                readonly RecordHeaderFormats _headerFormat;

                /// <summary>
                /// Number of fields represented by this fieldset
                /// </summary>
                public readonly int Count;

                long _currentEntry;

                private FieldSet(MTableCore table, int[] ordinals, int totalFields, ushort[] fieldLenghts)
                {
                    Count = (ushort)ordinals.Length;
                    this.totalFields = totalFields;
                    this.ordinals = ordinals;

                    _indexer = table.Index;
                    _mapViews = table.MapViews;
                    _headerFormat = table.RecordHeaderFormat;

                    if (fieldLenghts != null && fieldLenghts.Length > 0)
                    {
                        _explicitFieldsLength = fieldLenghts.Sum(length => length);
                        _fields = ordinals.Select(ordinal => new Field(ordinal, fieldLenghts[ordinal])).ToArray();
                        _fieldsComparable = ordinals.Select(ordinal => new Field(ordinal, fieldLenghts[ordinal])).ToArray();
                    }
                    else
                    {
                        _explicitFieldsLength = 0;
                        _fields = ordinals.Select(ordinal => new Field(ordinal, 0)).ToArray();
                        _fieldsComparable = ordinals.Select(ordinal => new Field(ordinal, 0)).ToArray();
                    }

                    //      Allocate the fields accessor to total number of fields.
                    //      No need to assign values, since this is collection of structs (already allocated).
                    //      Initialize the Ordinals field only.
                    //
                    //      NOTE: The following is not the approach mentioned above. It is normal complete initialization. Optimize this if needed.

                    _accessorByOrdinal = Enumerable.Range(0, totalFields).Select(ordinal => new Field(ordinal, 0)).ToArray();

                    _currentEntry = long.MinValue;

                    _bufferChars = new char[1024 * 8]; // Thread UNSAFE !! 

                    /*  _accessorByOrdinal = new Field[this.totalFields];
                        fixed (Field* pAccessor = _accessorByOrdinal)
                        {
                            for (int ordinal = 0; ordinal < totalFields; ordinal++)
                                pAccessor[ordinal].Ordinal = ordinal;
                        }
                    */
                }

                /// <summary>
                /// Returns prepared field by ordinal. Field associated with the ordinal which has to be already prepared and populated with the memory location and data size.
                /// </summary>
                /// <param name="ordinal"></param>
                /// <returns>Field associated with the ordinal which has to be already prepared and populated with the memory location and data size</returns>              
                public Field this[int ordinal]
                {
                    get
                    {
                        // Use the accessor for fast accessing fields by orginal
                        return _accessorByOrdinal[ordinal];
                    }
                }


                [Obsolete("Use ReadEntry, then access by ordinal")]
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                public Field[] GetFields(long entry, bool threadLock = false)
                {
                    ReadEntry(entry, threadLock);

                    for (int i = 0; i < Count; i++)
                        _fields[i] = this[ordinals[i]];

                    return _fields;
                }

                /// <summary>
                /// Prepares and populates the entry fields with the associated meta data, memory pointers and data sizes
                /// </summary>
                /// <param name="entry"></param>
                /// <returns></returns>
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                public unsafe void ReadEntry(long entry, bool threadLock = false)
                {
                    // 1. get the row bytes.
                    // 2. scan the data to determine the fields positions and lengths
                    // 3. copy the fields data

                    long position = _indexer.GetPosition(entry);
                    byte* entryData = _mapViews.Read(position, threadLock);

                    if (_headerFormat == RecordHeaderFormats.Variant)
                    {
                        // determine the fields lengths by processing the entry header considering the metadata lengths are stored as variant numbers                      
                        ProcessFieldsVariantHeader(entryData);
                    }
                    else if (_headerFormat == RecordHeaderFormats.Hybrid)
                    {
                        // determine the fields lengths by processing the entry header considering the metadata lengths are stored as variant numbers or fixed singe bytes (hybrid header)
                        ProcessFieldsHeaderHybrid(entryData);
                    }
                    else if (_headerFormat == RecordHeaderFormats.HybridFixed)
                    {
                        // determine the fields lengths by processing the entry header considering the metadata lengths are stored as variant numbers or fixed singe bytes (hybrid header)
                        // The header-meta starts with header section length, stored in 2-bytes
                        ProcessFieldsHeaderHybridFixed(entryData);
                    }
                    else
                    {
                        /* Determines the fields positions and lengths from the row-header list  */

                        fixed (Field* accessor = _accessorByOrdinal)
                        fixed (int* pOrdinals = ordinals)
                        {
                            int currentFieldSet = 0, lengthField = 0, positionField = 0, currentField = 0;

                            for (;;)
                            {
                                lengthField = *(entryData + currentField);

                                if (currentField == pOrdinals[currentFieldSet])
                                {
                                    // Assign the field to the accessor

                                    Field* field = (&accessor[currentField]);

                                    field->Pointer = entryData + (totalFields + positionField);
                                    field->Size = (ushort)lengthField;

                                    currentFieldSet++;

                                    if (currentFieldSet == Count)
                                        break;
                                }

                                /*
                                if (currentField == pOrdinals[currentFieldSet])
                                {
                                    Field* field = (&fieldMemory[currentFieldSet]);

                                    field->Pointer = entryData + (totalFields + positionField);
                                    field->Size = (ushort)lengthField;

                                    // Assign the field to the accessor
                                    accessor[currentField] = (*field);

                                    currentFieldSet++;

                                    if (currentFieldSet == Count)
                                        break;
                                }*/

                                currentField++;
                                positionField += lengthField;

                                if (currentField == totalFields)
                                    break;
                            }
                        }
                    }

                    // Set the current entry to the last entry readen
                    this._currentEntry = entry;
                }

                /// <summary>
                /// Extracts the record header metadata, using hubrid fields lengths determination.
                /// </summary>
                /// <param name="entryData"></param>
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                private unsafe void ProcessFieldsHeaderHybridFixed(byte* entryData)
                {
                    //   HYBRID HEADER LENGTHS #################
                    //
                    //   Treat the fields as single byte lengths, if value is up to maxumum 127 (half of single byte)
                    //   For larger values, extract the field length as variant, then adjust the data-start position accordicly, after the header.
                    // 
                    // - [_fields]: contains exact number of elements as allocated in this field set.
                    // - [_accessorByOrdinal]: contains all elements for this table (total fields).
                    // - [ordinals]: contains exact number of elemens ordinals, as allocated in this field set.

                    // The header length is stored in first 2 bytes.. max 64Kb of header size is supported


                    /* Determines the fields positions and lengths from the row-header list  */


                    fixed (Field* accessor = _accessorByOrdinal)
                    fixed (int* pOrdinals = ordinals)
                    {
                        // Normal
                        {
                            int currentFieldSet = 0, lengthField = 0, positionField = 0, currentField = 0,
                                lengthHeader = 0; // the current length of the metadata header

                            // READ THE HEADER LENGTH FIRST: 
                            // [ LEN(2) ..... ] [ - content - ]

                            var headerMetaSize = ((int)((*entryData) + (entryData[1] << 8))) - 2; // << remove itself

                            // increase the pointer, by length size (offset position to the first field)
                            entryData += 2;

                            for (;;)
                            {
                                // get field data length from the entry header section
                                lengthField = *(entryData + lengthHeader);

                                // If the length is larger then max of 127, process it as Variant. Otherwise, process it as fixed-single-byte.
                                if (lengthField < 128)
                                    lengthHeader += 1; // advance the header length, by one byte (fixed single byte)
                                else
                                {
                                    // extract the variant from the field length
                                    int variantSize;
                                    lengthField = Variant.ReadVIntOpt(entryData + lengthHeader, out variantSize);

                                    // the variant size contains the actual size of the length, so can be 1 or more. 
                                    // If 1, just pass it. Otherwise, keep track on the header offset (number of bytes the following fields has to be shifted)
                                    // normally, this should be always more then one.. but we check for any case.

                                    lengthHeader += variantSize; // advance the header length, by variant size.
                                }

                                // Locate the requested ordinal here and assign the field pointer. 
                                // If the current field is listed in the ordinals list, process it and continue with the rest of the fields
                                if (currentField == pOrdinals[currentFieldSet])
                                {
                                    Field* field = &accessor[currentField];

                                    field->Pointer = entryData + headerMetaSize + positionField;  // position of the field data, located after the header-meta
                                    field->Size = lengthField;
                                    currentFieldSet++;

                                    // all requested fields obtained. break it imideately
                                    if (currentFieldSet == this.Count)
                                        break;
                                }

                                currentField++;
                                positionField += lengthField;

                                //  reach the end of the full fieldset

                                if (currentField == totalFields)
                                    break;

                            }
                        }
                    }
                }

                /// <summary>
                /// Extracts the record header metadata, using hubrid fields lengths determination.
                /// </summary>
                /// <param name="entryData"></param>
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                private unsafe void ProcessFieldsHeaderHybrid(byte* entryData)
                {
                    //   HYBRID HEADER LENGTHS #################
                    //
                    //   Treat the fields as single byte lengths, if value is up to maxumum 127 (half of single byte)
                    //   For larger values, extract the field length as variant, then adjust the data-start position accordicly, after the header.
                    // 
                    // - [_fields]: contains exact number of elements as allocated in this field set.
                    // - [_accessorByOrdinal]: contains all elements for this table (total fields).
                    // - [ordinals]: contains exact number of elemens ordinals, as allocated in this field set.


                    /* Determines the fields positions and lengths from the row-header list  */

                    fixed (Records.FieldSet.Field* fieldMemory = _fields)
                    fixed (Field* accessor = _accessorByOrdinal)
                    fixed (int* pOrdinals = ordinals)
                    {
                        // Normal
                        {
                            int currentFieldSet = 0, lengthField = 0, positionField = 0, currentField = 0,
                                lengthHeader = 0; // the current length of the metadata header

                            for (;;)
                            {
                                // get field data length from the entry header section
                                lengthField = *(entryData + lengthHeader);

                                // If the length is larger then max of 127, process it as Variant. Otherwise, process it as fixed-single-byte.
                                if (lengthField < 128)
                                    lengthHeader += 1; // advance the header length, by one byte (fixed single byte)
                                else
                                {
                                    // extract the variant from the field length
                                    int variantSize;
                                    lengthField = Variant.ReadVIntOpt(entryData + lengthHeader, out variantSize);

                                    // the variant size contains the actual size of the length, so can be 1 or more. 
                                    // If 1, just pass it. Otherwise, keep track on the header offset (number of bytes the following fields has to be shifted)
                                    // normally, this should be always more then one.. but we check for any case.
                                    if (variantSize > 0)
                                    {
                                        lengthHeader += variantSize; // advance the header length, by variant size.
                                    }
                                }

                                // Locate the requested ordinal here and assign the field pointer. 
                                // If the current field is listed in the ordinals list, process it and continue with the rest of the fields
                                if (currentField == pOrdinals[currentFieldSet])
                                {
                                    Field* field = &fieldMemory[currentFieldSet];

                                    field->Pointer = entryData + (totalFields + positionField);  // position of the field data, located afte the header-meta
                                    field->Size = (ushort)lengthField;
                                    currentFieldSet++;
                                }

                                currentField++;
                                positionField += lengthField;

                                if (currentField == totalFields)
                                {
                                    //  reach the end of the full fieldset

                                    //  Data-Section Adjustment needed? If headerLength is larger then totalFields, 
                                    //  that means there were variants in the header, so offset the data-section by the difference

                                    //  variants might be found were found, adjust the data-section
                                    AdjustDataSectionAndAssignAccessor(fieldMemory, accessor, lengthHeader - totalFields);

                                    break;
                                }
                            }
                        }
                    }
                }

                /// <summary>
                /// Moves the fields positions (data section), after the end of the record header-section if needed.
                /// Additionally, assigns the accessors with the corresponding fields pointers
                /// </summary>
                private void AdjustDataSectionAndAssignAccessor(Field* fieldMemory, Field* accessor, int offset)
                {
                    // move (adjust) all fields positions after the row-header. 
                    // This includes the accessor too
                    // Note: future optimization, determine whether accessors OR fields are used, so deal with only one of them, not both of them.

                    // Use the Ordinals to locate the correct accessor objects, within all accessor objects array. The fields array is already limited to the actual count
                    {
                        int index = this.Count;
                        for (;;)
                        {
                            // decrement the index first, before anything, so the correct index is passed to the pointer
                            Records.FieldSet.Field* field = &fieldMemory[--index];

                            // only if there is sense
                            if (offset > 0)
                                field->Pointer = field->Pointer + offset;

                            // Assign the field to the accessor
                            // use the field ordinal, to locate the correct accesor object and
                            // assign the field to the accessor
                            accessor[field->Ordinal] = (*field);

                            if (index == (byte)0)
                                break;
                        }
                    }
                }


                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                private unsafe void ProcessFieldsVariantHeader(byte* entryData)
                {
                    /* Determines the fields positions and lengths from the row-header list  */

                    fixed (Records.FieldSet.Field* fieldMemory = _fields)
                    fixed (Field* accessor = _accessorByOrdinal)
                    fixed (int* pOrdinals = ordinals)
                    {
                        int currentFieldSet = 0, lengthHeader = 0, byteCount = 0, lengthField = 0, positionField = 0, currentField = 0;

                        var totalFields = this.totalFields;

                        for (;;)
                        {
                            // iterate over the row header to gather all fields lengths
                            // If there are available field lenghts, skip them and add their lenght back to the counter

                            lengthField = Variant.ReadVIntOpt(entryData + lengthHeader, out byteCount);
                            lengthHeader += byteCount;

                            // store the field set position and length..

                            if (currentField == *(pOrdinals + currentFieldSet))
                            {
                                Records.FieldSet.Field* field = (&fieldMemory[currentFieldSet]);

                                field->Pointer = entryData + positionField;
                                field->Size = (ushort)lengthField;

                                // Assign the field to the accessor (not here, but at the end)
                                //accessor[currentField] = (*field);

                                currentFieldSet++;
                            }

                            currentField++;
                            positionField += lengthField;

                            if (currentField == totalFields)
                                break;
                        }

                        // move (adjust) all fields positions after the row-header. 
                        // This includes the accessor too
                        // Note: future optimization, determine whether accessors OR fields are used, so deal with only one of them, not both of them.

                        // Use the Ordinals to locate the correct accessor objects, within all accessor objects array. The fields array is already limited to the actual count
                        {
                            int index = this.Count;
                            for (;;)
                            {
                                // decrement the index first, before anything, so the correct index is passed to the pointer
                                Records.FieldSet.Field* field = &fieldMemory[--index];
                                field->Pointer = field->Pointer + lengthHeader;

                                // use the field ordinal, to locate the correct accesor object and
                                // assign the field to the accessor
                                accessor[field->Ordinal] = (*field);

                                if (index == (byte)0)
                                    break;
                            }
                        }
                    }
                }


                public override string ToString()
                {
                    return string.Format("ordinals: {0}", String.Join(",", this.ordinals));
                }

                /// <summary>
                /// Constructs full field set for all fields in the data storage
                /// </summary>
                /// <param name="dataStorage"></param>
                /// <returns></returns>
                public static FieldSet Allocate(MTableCore table)
                {
                    return new Records.FieldSet(table, Enumerable.Range(0, table.ColumnCount).ToArray(), table.ColumnCount, null);
                }

                /// <summary>
                /// Constructs fields set for the specified field ordinals
                /// </summary>
                /// <param name="table"></param>
                /// <param name="fields"></param>
                /// <returns></returns>
                internal static FieldSet Allocate(MTableCore table, int[] fields)
                {
                    return new Records.FieldSet(table, fields, table.ColumnCount, null);
                }

                /// <summary>
                /// Constructs fields set for the specified fields ordinals and using explicit total number of fields counts
                /// </summary>
                /// <param name="table"></param>
                /// <param name="fields"></param>
                /// <param name="fieldsTotal"></param>
                /// <returns></returns>
                internal static FieldSet Allocate(MTableCore table, int[] fields, int fieldsTotal)
                {
                    return new Records.FieldSet(table, fields, fieldsTotal, null);
                }

              
                /// <summary>
                /// Compare this field set with fields associated with the entry key
                /// </summary>
                /// <param name="other"></param>
                /// <returns></returns>
                bool IEquatable<long>.Equals(long entry)
                {
                    // Preserve the currently assigned fieldset entries then fetch fields the entry 
                    for (var idx = 0; idx < Count; idx++)
                        _fieldsComparable[idx] = this[ordinals[idx]];

                    // Fetch the fields for the entry key being compared
                    ReadEntry(entry);

                    // At this point we use the current fields and the comparable cached fields to determine equity
                    // Start comparing each of the fields from the fieldsets sequentially.
                    // If any of the field equity test fails consider the entire fieldset as failed.
                    //
                    for (var idx = 0; idx < this.Count; idx++)
                    {
                        var field1 = _fieldsComparable[idx];
                        var field2 = this[ordinals[idx]];

                        //  Fail of the first non matched equity
                        if (!field1.Equals(field2))
                            return false;
                    }

                    return true;
                }

                public unsafe struct Field : IComparable<Field>, IEquatable<Field>, IComparable<long>
                {
                    internal readonly int Ordinal;
                    internal byte* Pointer;
                    public int Size;

                    /// <summary>
                    /// Fast create clone to this field
                    /// </summary>
                    /// <returns></returns>
                    public Field Clone()
                    {
                        return new Field(Ordinal, Size, Pointer);
                    }

                    internal Field(int ordinal, int size)
                    {
                        Pointer = null;
                        Size = size;
                        Ordinal = ordinal;
                    }

                    internal Field(int ordinal, int size, byte* pointer)
                        : this(ordinal, size)
                    {
                        Pointer = pointer;
                    }

                    [MethodImpl(MethodImplOptions.AggressiveInlining)]
                    [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                    public unsafe bool TryGetNumber(out long number)
                    {
                        number = 0;

                        if (Size > 0)
                        {
                            number = Native.Utils.BytesToNumberOpt(Pointer, Size);
                            return true;
                        }

                        return false;
                    }


                    [MethodImpl(MethodImplOptions.AggressiveInlining)]
                    [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                    public unsafe bool TryGetTextNumber(out long number)
                    {
                        number = 0;
                        long result;
                        if (Size > 0 && Native.Utils.totypeAtoi64(Pointer, &result, (int)Size))
                        {
                            number = result;
                            return true;
                        }

                        return false;
                    }

                    [MethodImpl(MethodImplOptions.AggressiveInlining)]
                    [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                    public unsafe bool TryGetTextNumber(out double number)
                    {
                        number = 0;
                        double result;
                        if (Size > 0 && Native.Utils.totypeAtoF(Pointer, &result, (int)Size))
                        {
                            number = result;
                            return true;
                        }

                        return false;
                    }

                    [MethodImpl(MethodImplOptions.AggressiveInlining)]
                    [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                    public string GetText()
                    {
                        if (Size > 0)
                            unsafe
                            {
                                // Obtain character encoding buffer large enougth to fit the field size
                                if (_bufferChars.Length < Size)
                                    _bufferChars = new char[Size];

                                fixed (char* charPtr = _bufferChars)
                                {
                                    var encoded = IDataStorage.Encoder.GetChars(Pointer, Size, charPtr, Size);
                                    return new string(charPtr, 0, encoded);
                                }
                            }
                        else
                            return string.Empty;
                    }

                    [MethodImpl(MethodImplOptions.AggressiveInlining)]
                    [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                    public static unsafe void Read(MTableCore table, long row, Field[] fields, int totalFields, int[] fieldSet)
                    {
                        // 1. get the row bytes.
                        // 2. scan the data to determine the fields positions and lengths
                        // 3. copy the fields data

                        long position = table._indexer.GetPosition(row);
                        byte* entryData = table._mapViewManager.Read(position, false);

                        /* Determines the fields positions and lengths from the row-header list  */

                        fixed (Field* fieldMemory = &fields[0])
                        // fixed (int* pFieldSet = fieldSet)
                        {
                            int currentFieldSet = 0, lengthField = 0, positionField = 0, currentField = 0;

                            for (;;)
                            {
                                lengthField = *(entryData + currentField);

                                if (currentField == fieldSet[currentFieldSet])
                                {
                                    Field* field = (&fieldMemory[currentFieldSet]);

                                    field->Pointer = entryData + (totalFields + positionField);
                                    field->Size = (ushort)lengthField;

                                    currentFieldSet++;
                                }

                                currentField++;
                                positionField += lengthField;

                                if (currentField == totalFields)
                                    break;
                            }
                        }
                    }

                    /// <summary>
                    ///  Calculates fast hash code for this field, by using the fields underlaying data
                    /// </summary>
                    /// <returns></returns>
                    [MethodImpl(MethodImplOptions.AggressiveInlining)]
                    [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                    public long CalculateHashKey()
                    {
                        long hash = 0;

                        hash = (long)Native.Utils.Hashing.BSon.Calculate(Pointer, Size); // <- Absolute winner
                        return hash;

                        var bb = Native.Murmur3.Instance.ComputeHash(Pointer, Size); // <- Next Winner
                        hash = (long)Native.Murmur3.IntHelpers.GetUInt64(bb, 0);
                        return hash;

                        hash = Native.Utils.Hashing.CalculateIII(this.Pointer, this.Size); // <-- 2 and 3 very good
                        return hash;
                    }

                    /// <summary>
                    /// Returns calculated hash code using the Field.CalculageHashKey function. This is suitable only for smaller values, for hash codes in INT range
                    /// </summary>
                    /// <returns></returns>
                    public override int GetHashCode()
                    {
                        return (int)CalculateHashKey();
                    }

                    public override string ToString()
                    {
                        return string.Format("ordinal: {0}, size: {1}", this.Ordinal, this.Size);
                    }

                    [MethodImpl(MethodImplOptions.AggressiveInlining)]
                    [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                    public bool TryGetNumber(out double real)
                    {
                        real = 0;
                        long number;

                        // Gets the long value first and convert it to double, using the BitConverter
                        if (TryGetNumber(out number))
                        {
                            real = BitConverter.Int64BitsToDouble(number);
                            return true;
                        }

                        return false;
                    }

                    /// <summary>
                    ///  Optimized and fast comparision of this field memory content with other.  
                    /// </summary>
                    /// <param name="value"></param>
                    /// <returns></returns>
                    public int CompareTo(byte[] value)
                    {
                        fixed (byte* p = value)
                            return this.CompareTo(new Field { Size = (ushort)value.Length, Pointer = p });
                    }

                    /// <summary>
                    /// Optimized and fast comparision of this field memory content with other.  
                    /// </summary>
                    /// <param name="other"></param>
                    /// <returns></returns>
                    public int CompareTo(Field other)
                    {
                        #region Alternative comparisions
                        // var difference = Native.strncmp(Pointer, other.Pointer, Size);

                        //var difference = Native.memoryCompare1(Pointer, other.Pointer, Size);
                        //var difference = Native.memoryCompare4(Pointer, other.Pointer, Size);
                        //  var difference = Native.memoryCompare5(Pointer, other.Pointer, Size); // MS - good
                        // var difference = Native._tcsncmp(Pointer, other.Pointer, Size);
                        /// var difference = Native.CompareRaven(Pointer, other.Pointer, Size);
                        ///   // var difference = Native.memcmp(Pointer, other.Pointer, Size);

                        // WORKS PROPERLY
                        //var difference0 = System.StringComparer.OrdinalIgnoreCase.Compare(GetText(), other.GetText());
                        // return difference0;

                        #endregion


                        if (this.Size != other.Size)
                        {
                            // Determine the position from null and non null comparisions
                            if (this.Size == 0) return -1;
                            if (other.Size == 0) return 1;
                        }
                        else
                            if (Size == 0) return 0; // both sizes are zeros

                        // Comparision length to the smaller size
                        var length = Math.Min(Size, other.Size);

                        var difference = Native.MemoryUtils.CompareVoron(Pointer, other.Pointer, length); // GOOD

                        // If the compared range is equal, return the difference in the fields sizes
                        if (difference != 0)
                            return difference;

                        return Size - other.Size;
                    }

                    /// <summary>
                    ///  Optimized and fast equity comparision of this field memory content with other.  
                    /// </summary>
                    /// <param name="value"></param>
                    /// <returns></returns>
                    public bool Equals(byte[] value)
                    {
                        if (this.Size != value.Length)
                            return false;

                        fixed (byte* p = value)
                            return this.CompareTo(value) == 0;
                    }

                    /// <summary>
                    /// Optimized and fast equity comparision of this field memory content with other.  
                    /// </summary>
                    /// <param name="other"></param>
                    /// <returns></returns>
                    public bool Equals(Field other)
                    {
                        // compares only if sizes are different
                        if (Size != other.Size)
                            return false;

                        return CompareTo(other) == 0;
                    }

                    public override bool Equals(object obj)
                    {
                        return this.Equals((Field)obj);
                    }

                    /// <summary>
                    /// Optimized and fast comparision of this field memory content represented as number with other.  
                    /// </summary>
                    /// <param name="other"></param>
                    /// <returns></returns>
                    public int CompareTo(long other)
                    {
                        long number1 = 0;
                        if (Size > 0)
                            number1 = Native.Utils.BytesToNumberOpt(Pointer, Size);

                        return (int)(number1 - other);
                    }
                }


                /// <summary>
                /// Represent equity comparer of fieldset's allocated and assigned values
                /// /// </summary>
                private sealed class EquityComparer : EqualityComparer<FieldSet>
                {
                    internal EquityComparer()
                    {

                    } 

                    /// <summary>
                    /// The equity check is performed on the allocated and assigned fields only and non self
                    /// </summary>
                    /// <param name="x"></param>
                    /// <param name="y"></param>
                    /// <returns></returns>
                    public override bool Equals(FieldSet x, FieldSet y)
                    {
                        // Dont allow comparing on self instance. 
                        // This is struct type and the values are most likely invalid or self updating, learing to always positive result.

                        if (ReferenceEquals(x, y))
                            throw new InvalidOperationException("Unpermited self-referenced FieldSet instance involved in an equity comparision.");

                        if (x.Count != y.Count)
                            return false;

                        // Start comparing each of the fields from the fieldsets sequentially.
                        // If any of the field equity test fails consider the entire fieldset as failed.
                        //
                        for (var idx = 0; idx < x.Count; idx++)
                        {
                            var field1 = x[x.ordinals[idx]];
                            var field2 = y[y.ordinals[idx]];

                            //  Fail of the first non matched equity
                            if (!field1.Equals(field2))
                                return false;
                        }

                        // All fields were equal so return positive
                        return true;
                    }

                    /// <summary>
                    /// The hash code is calculated by taking the allocated and assigned fields only into account
                    /// </summary>
                    /// <param name="obj"></param>
                    /// <returns></returns>
                    public override int GetHashCode(FieldSet fieldset)
                    {
                        // Access the fields in most performant way and calcualte the hash values

                        unchecked
                        {
                            if (fieldset.Count > 1)
                            {
                                long hash = 17;

                                foreach (var ordinal in fieldset.ordinals)
                                {
                                    var field = fieldset[ordinal];

                                    // get hash code for all items in array

                                    hash = hash * 23 + field.CalculateHashKey();
                                }

                                return (int)hash;
                            }
                            else
                            {
                                var field = fieldset[fieldset.ordinals[0]];

                                return (int)field.CalculateHashKey();
                            }
                        }
                    }
 
                }



            }


            
        }

#if MODE_SCAN_OPTIMIZED

        public unsafe class Row : IRow
        {
            private readonly Indexer _indexer;

            private readonly char[] _bufferField;
            private readonly ushort _fields;


            private readonly MapViewManager _viewManager;

            private readonly MTableCore _table;

            private readonly ushort _fieldSetSize;

            private readonly ushort[] _fieldExplicitLenghts;

            internal ushort[] FieldSet;

            // [ThreadStatic]
            private static Field[] _fieldMemory;
            // [ThreadStatic]
            private static string[] _fieldValues;
            //  [ThreadStatic]
            private static byte[][] _fieldData;
            // [ThreadStatic]
            private static ushort[] _fieldSetPositions;
            //  [ThreadStatic]
            private static ushort[] _fieldSetLengths;

            [ThreadStatic]
            private static Row _currentRow;

            internal Field[] _valueFields;

            // [MethodImpl(MethodImplOptions.AggressiveInlining)]
            // [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            internal static void AllocateFieldSet(int fieldSetSize)
            {
                if (fieldSetSize == 0)
                    throw new Exception("No fields are configured to be obtained");

                bool initialize = false;

                if (_fieldSetLengths == null || _fieldSetLengths.Length != fieldSetSize)
                {
                    _fieldSetLengths = new ushort[fieldSetSize];
                    initialize = true;
                }

                if (_fieldSetPositions == null || _fieldSetPositions.Length != fieldSetSize)
                {
                    _fieldSetPositions = new ushort[fieldSetSize];
                    initialize = true;
                }

                if (initialize)
                {
                    _fieldMemory = new Field[fieldSetSize];
                    for (int i = 0; i < fieldSetSize; i++)
                        _fieldMemory[i] = new Field();
                }
            }

            // new IndexRow(this, rowNumber, this.ColumnCount, _bufferField, fieldSet, null, _bufferEntry, _indexer, IntPtr.Zero, _mode, MapViews);
            internal Row(MTableCore table, long number, ushort[] fieldSet, ushort[] fieldExplicitLenghts)
                : base(number)
            {
                _table = table;
                _indexer = table._indexer;
                _fields = (ushort)table.ColumnCount;
                _bufferField = table._bufferField;
                _fieldExplicitLenghts = fieldExplicitLenghts;

                _viewManager = table._mapViewManager;
                _fieldSetSize = (ushort)fieldSet.Length;

                FieldSet = fieldSet;
            }

            public override string GetRowData()
            {
                throw new NotImplementedException();
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static void ScanFieldSetOptimized(byte* rowData, ushort[] fieldSetPositions, ushort[] fieldSetLengths, ushort totalFields, ushort fieldSetSize, ushort[] fieldSet, ushort[] fieldExplicitLenghts)
            {
                /* Determines the fields positions and lengths from the row-header list  */


                ushort /*positionHeader = 0,*/ currentField = 0, currentFieldSet = 0, lengthHeader = 0, lengthField = 0, positionField = 0;

                unsafe
                {
                    fixed (ushort* p_fieldExplicitLenghts = fieldExplicitLenghts)
                    {
                        ushort* ptrFEL = p_fieldExplicitLenghts;

                        while (currentField < totalFields) // && currentFieldSet < fieldSetSize)
                        {
                            // iterate over the row header to gather all fields lengths
                            // If there are available field lenghts, skip them and add their lenght back to the counter

                            lengthField = (*ptrFEL++); //[currentField];

                            // if (p_fieldExplicitLenghts != null && (lengthField = p_fieldExplicitLenghts[currentField]) > 0)

                            if (lengthField == 0)
                                lengthField = Variant.ReadVUInt16Opt(rowData + lengthHeader, out lengthHeader);
                            else
                                lengthHeader += lengthField;

                            //      positionHeader += lengthHeader;

                            // store the field set position and length..

                            if (currentFieldSet < fieldSetSize && currentField == fieldSet[currentFieldSet])
                            {
                                fieldSetPositions[currentFieldSet] = positionField;
                                fieldSetLengths[currentFieldSet] = lengthField;

                                currentFieldSet++;
                            }

                            currentField++;
                            positionField += lengthField;
                        }
                    }
                }

                // move all fields positions after the row-header
                for (ushort index = 0; index < fieldSetSize; index++)
                    fieldSetPositions[index] += lengthHeader;

            }

            public override IEnumerator<string> GetEnumerator()
            {
                throw new NotImplementedException();
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static void Unmanaged_Encode(byte* buffer, int startIndex, char* target, int destIndex, int length)
            {
                do
                {
                    *((target + destIndex) + length) = (char)*((buffer + startIndex) + length);
                }
                while (length-- > 0);
            }




            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public sealed override byte[][] GetFieldsData()
            {
                // 1. get the row bytes.
                // 2. scan the data to determine the fields positions and lengths
                // 3. copy the fields data

                var rowIndex = _indexer.Get(Number, 0, 1);
                int length = rowIndex.Length;

                byte* pointer = (byte*)_viewManager.Read(rowIndex.Position, length).ToPointer();

                ScanFieldSetOptimized(pointer, _fieldSetPositions, _fieldSetLengths, _fields, _fieldSetSize, FieldSet, _fieldExplicitLenghts);

                AllocateFieldSetData(pointer, _fieldSetPositions, _fieldSetLengths);

                return _fieldData;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public sealed override string[] GetFields()
            {
                // 1. get the row bytes.
                // 2. scan the data to determine the fields positions and lengths
                // 3. encode the fields found only

                var rowIndex = _indexer.Get(Number, 0, 1);
                int length = rowIndex.Length;

                byte* pointer = (byte*)_viewManager.Read(rowIndex.Position, length).ToPointer();

                ScanFieldSetOptimized(pointer, _fieldSetPositions, _fieldSetLengths, _fields, _fieldSetSize, FieldSet, _fieldExplicitLenghts);

                AllocateFieldSetText(pointer, _fieldSetPositions, _fieldSetLengths);

                return _fieldValues;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private void AllocateFieldSetData(byte* pointer, ushort[] _fieldSetPositions, ushort[] _fieldSetLengths)
            {
                for (ushort currentFieldSet = 0; currentFieldSet < _fieldSetSize; currentFieldSet++)
                {
                    // encode and allocate each of the field , using its location and length

                    ushort position = _fieldSetPositions[currentFieldSet];
                    ushort length = _fieldSetLengths[currentFieldSet];

                    byte[] fieldData = new byte[length];

                    Marshal.Copy(new IntPtr(pointer + position), fieldData, 0, length);

                    _fieldData[currentFieldSet] = fieldData;
                }
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static void AllocateFieldSetMemory(byte* pointer, ushort[] fieldSetPositions, ushort[] fieldSetLengths, IRow.Field[] fieldMemory, ushort fieldSetSize)
            {
                for (ushort currentFieldSet = 0; currentFieldSet < fieldSetSize; currentFieldSet++)
                {
                    // encode and allocate each of the field , using its location and length

                    ushort offset = fieldSetPositions[currentFieldSet];
                    ushort length = fieldSetLengths[currentFieldSet];

                    var fm = fieldMemory[currentFieldSet];

                    fm.Pointer = pointer + offset;
                    fm.Size = length;
                }
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private void AllocateFieldSetText(byte* pointer, ushort[] _fieldSetPositions, ushort[] _fieldSetLengths)
            {
                fixed (char* rowData = _bufferField)
                {
                    for (ushort currentFieldSet = 0; currentFieldSet < _fieldSetSize; currentFieldSet++)
                    {
                        // encode and allocate each of the field , using its location and length

                        ushort position = _fieldSetPositions[currentFieldSet];
                        ushort length = _fieldSetLengths[currentFieldSet];

                        Unmanaged_Encode(pointer, position, rowData, 0, length);

                        _fieldValues[currentFieldSet] = new string(rowData, 0, length);
                    }
                }
            }

            public override byte[,] GetFields(params ushort[] columns)
            {
                throw new NotImplementedException();
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            protected IRow.Field[] GetFieldsMemory(bool threadSafe)
            {
                // 1. get the row bytes.
                // 2. scan the data to determine the fields positions and lengths
                // 3. copy the fields data

                var rowIndex = _indexer.Get(Number, 0, 1);
                int length = rowIndex.Length;

                var pointer = (byte*)_viewManager.Read(rowIndex.Position, length, threadSafe).ToPointer();

                ScanFieldSetOptimized(pointer, _fieldSetPositions, _fieldSetLengths, _fields, _fieldSetSize, FieldSet, _fieldExplicitLenghts);

                AllocateFieldSetMemory(pointer, _fieldSetPositions, _fieldSetLengths, _fieldMemory, _fieldSetSize);

                return _fieldMemory;
            }

            public IRow.Field[] Fields
            {
                get
                {
                    if (_valueFields != null)
                        return _valueFields;

                    return _valueFields = GetFieldsMemory(false);
                }
            }

            internal static Row SetCurrent(MTableCore table, long row, ushort[] fieldSet)
            {
                if (_currentRow != null)
                {
                    _currentRow.Number = row;
                    _currentRow.FieldSet = fieldSet;
                    _currentRow._valueFields = null;
                    return _currentRow;
                }

                _currentRow = new Row(table, row, fieldSet, null);

                return _currentRow;
            }
        }

#else

        [Obsolete("Possible no need to have it the component")]
        private unsafe class Row : IRow
        {
            private readonly Indexer _indexer;
            private readonly MapViewManager _viewManager;
            private readonly MTableCore _table;

            private readonly char[] _bufferField;

            private readonly ushort[] _fieldExplicitLenghts;

            internal int[] FieldSet;

            private readonly int _fields;
            private readonly int _fieldSetSize;

            private Records.FieldSet.Field[] _fieldMemory;
            private int[] _fieldSetPositions;
            private int[] _fieldSetLengths;

            // [ThreadStatic]
            private static Row _currentRow;

            internal Records.FieldSet.Field[] _valueFields;

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            internal static void AllocateFieldSet(Row row, int fieldSetSize)
            {
                if (fieldSetSize == 0)
                    throw new Exception("No fields are configured to be obtained");

                bool initialize = false;

                if (row._fieldSetLengths == null || row._fieldSetLengths.Length != fieldSetSize)
                {
                    row._fieldSetLengths = new int[fieldSetSize];
                    initialize = true;
                }

                if (row._fieldSetPositions == null || row._fieldSetPositions.Length != fieldSetSize)
                {
                    row._fieldSetPositions = new int[fieldSetSize];
                    initialize = true;
                }

                if (initialize)
                {
                    row._fieldMemory = new Records.FieldSet.Field[fieldSetSize];
                    for (int i = 0; i < fieldSetSize; i++)
                        row._fieldMemory[i] = new Records.FieldSet.Field();
                }
            }

            internal Row(MTableCore table, long number, int[] fieldSet, ushort totalFields)
                : base(number)
            {
                _table = table;
                _indexer = table._indexer;
                _fields = totalFields;
                _bufferField = table._bufferField;
                _fieldExplicitLenghts = null; // fieldExplicitLenghts;
                //_explicitFieldsLength = 0;
                //_explicitFieldsLength = fieldExplicitLenghts != null ? (ushort)fieldExplicitLenghts.Sum(length => (ushort)length) : 0;

                _viewManager = table._mapViewManager;
                _fieldSetSize = fieldSet.Length;

                FieldSet = fieldSet;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private unsafe void ProcessRowFieldsHeaderNonVariant(byte* rowData, int[] fieldSet)
            {
                /* Determines the fields positions and lengths from the row-header list  */

                fixed (Records.FieldSet.Field* fieldMemory = &_fieldMemory[0])
                fixed (int* pFieldSet = fieldSet)
                {
                    int currentFieldSet = 0, lengthField = 0, positionField = 0, currentField = 0;

                    for (;;)
                    {
                        lengthField = *(rowData + currentField);

                        if (currentField == pFieldSet[currentFieldSet])
                        {
                            Records.FieldSet.Field* field = (&fieldMemory[currentFieldSet]);

                            field->Pointer = rowData + (this._fields + positionField);
                            field->Size = (ushort)lengthField;

                            currentFieldSet++;
                        }

                        currentField++;
                        positionField += lengthField;

                        if (currentField == this._fields)
                            break;
                    }
                }
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private unsafe void ProcessRowFieldsHeaderVariant(byte* rowData, int* fieldSet)
            {
                /* Determines the fields positions and lengths from the row-header list  */

                fixed (Records.FieldSet.Field* fieldMemory = &_fieldMemory[0])
                {
                    int currentFieldSet = 0, lengthHeader = 0, byteCount = 0, lengthField = 0, positionField = 0, currentField = 0;

                    var totalFields = this._fields;

                    for (;;)
                    {
                        // Opt Future Note: iterate over the row header to gather all fields lengths
                        // If there are available field lenghts, skip them and add their lenght back to the counter

                        lengthField = Variant.ReadVIntOpt(rowData + lengthHeader, out byteCount);
                        lengthHeader += byteCount;

                        // store the field set position and length..

                        if (currentField == *(fieldSet + currentFieldSet))
                        {
                            Records.FieldSet.Field* field = (&fieldMemory[currentFieldSet]);

                            field->Pointer = rowData + positionField;
                            field->Size = (ushort)lengthField;

                            currentFieldSet++;
                        }

                        currentField++;
                        positionField += lengthField;

                        if (currentField == totalFields)
                            break;
                    }

                    // move all fields positions after the row-header

                    ushort index = (ushort)this._fieldSetSize;
                    for (;;)
                    {
                        Records.FieldSet.Field* field = (&fieldMemory[--index]);
                        field->Pointer = field->Pointer + lengthHeader;

                        if (index == (byte)0)
                            break;
                    }
                }
            }


            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static void ScanFieldSet(byte* rowData, int rowLength, int[] fieldSetPositions, int[] fieldSetLengths, int totalFields, int fieldSetSize, int[] fieldSet, ushort[] fieldExplicitLenghts)
            {
                /* Determines the fields positions and lengths from the row-header list  */

                int positionHeader = 0, currentField = 0, currentFieldSet = 0, lengthHeader = 0, lengthField = 0, positionField = 0;

                unsafe
                {
                    fixed (ushort* p_fieldExplicitLenghts = fieldExplicitLenghts)
                    {
                        ushort* ptrFEL = p_fieldExplicitLenghts;

                        while (currentField < totalFields) // && currentFieldSet < fieldSetSize)
                        {
                            // iterate over the row header to gather all fields lengths
                            // If there are available field lenghts, skip them and add their lenght back to the counter

                            lengthField = (*ptrFEL++); //[currentField];

                            // if (p_fieldExplicitLenghts != null && (lengthField = p_fieldExplicitLenghts[currentField]) > 0)

                            if (lengthField > 0)
                                lengthHeader += lengthField;
                            else
                                lengthField = Variant.ReadVIntOpt(rowData + positionHeader, out lengthHeader);

                            positionHeader += lengthHeader;

                            // store the field set position and length..

                            if (currentFieldSet < fieldSetSize && currentField == fieldSet[currentFieldSet])
                            {
                                fieldSetPositions[currentFieldSet] = positionField;
                                fieldSetLengths[currentFieldSet] = lengthField;

                                currentFieldSet++;
                            }

                            currentField++;
                            positionField += lengthField;
                        }
                    }
                }

                // move all fields positions after the row-header
                for (ushort index = 0; index < fieldSetSize; index++)
                    fieldSetPositions[index] += positionHeader;
            }


            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static void ScanFieldSetOptimized(byte* rowData, int rowLength, ushort[] fieldSetPositions, ushort[] fieldSetLengths, int totalFields, int fieldSetSize, ushort[] fieldSet, ushort[] fieldExplicitLenghts)
            {
                /* Determines the fields positions and lengths from the row-header list  */

                ushort positionHeader = 0, currentField = 0, currentFieldSet = 0, lengthHeader = 0, lengthField = 0, positionField = 0;

                unsafe
                {
                    fixed (ushort* p_fieldExplicitLenghts = fieldExplicitLenghts)
                    {
                        ushort* ptrFEL = p_fieldExplicitLenghts;

                        while (currentField < totalFields) // && currentFieldSet < fieldSetSize)
                        {
                            // iterate over the row header to gather all fields lengths
                            // If there are available field lenghts, skip them and add their lenght back to the counter

                            lengthField = (*ptrFEL++); //[currentField];

                            // if (p_fieldExplicitLenghts != null && (lengthField = p_fieldExplicitLenghts[currentField]) > 0)

                            if (lengthField > 0)
                                lengthHeader += lengthField;
                            else
                                lengthField = Variant.ReadVUInt16Opt(rowData + positionHeader, out lengthHeader);

                            positionHeader += lengthHeader;

                            // store the field set position and length..

                            if (currentFieldSet < fieldSetSize && currentField == fieldSet[currentFieldSet])
                            {
                                fieldSetPositions[currentFieldSet] = positionField;
                                fieldSetLengths[currentFieldSet] = lengthField;

                                currentFieldSet++;
                            }

                            currentField++;
                            positionField += lengthField;
                        }
                    }
                }

                // move all fields positions after the row-header
                for (ushort index = 0; index < fieldSetSize; index++)
                    fieldSetPositions[index] += positionHeader;
            }

            public override IEnumerator<string> GetEnumerator()
            {
                throw new NotImplementedException();
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static void Unmanaged_Encode(byte* buffer, int startIndex, char* target, int destIndex, int length)
            {
                do
                {
                    *((target + destIndex) + length) = (char)*((buffer + startIndex) + length);
                }
                while (length-- > 0);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public sealed override string[] GetFields()
            {
                // 1. get the row bytes.
                // 2. scan the data to determine the fields positions and lengths
                // 3. encode the fields found only

                var rowIndex = _indexer.Get(Number, 0, 1);
                int length = rowIndex.Length;

                byte* pointer = _viewManager.Read(rowIndex.Position);

                ScanFieldSet(pointer, length, _fieldSetPositions, _fieldSetLengths, _fields, _fieldSetSize, FieldSet, _fieldExplicitLenghts);

                AllocateFieldSetText(pointer, _fieldSetPositions, _fieldSetLengths);

                return null; // _fieldValues;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private void AllocateFieldSetData(byte* pointer, int[] _fieldSetPositions, int[] _fieldSetLengths)
            {
                for (ushort currentFieldSet = 0; currentFieldSet < _fieldSetSize; currentFieldSet++)
                {
                    // encode and allocate each of the field , using its location and length

                    int position = _fieldSetPositions[currentFieldSet];
                    int length = _fieldSetLengths[currentFieldSet];

                    byte[] fieldData = new byte[length];

                    Marshal.Copy(new IntPtr(pointer + position), fieldData, 0, length);

                    ///_fieldData[currentFieldSet] = fieldData;
                }
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static void AllocateFieldSetMemory(byte* pointer, int[] fieldSetPositions, int[] fieldSetLengths, Records.FieldSet.Field[] fieldMemory, int fieldSetSize)
            {
                for (ushort currentFieldSet = 0; currentFieldSet < fieldSetSize; currentFieldSet++)
                {
                    // encode and allocate each of the field , using its location and length

                    int offset = fieldSetPositions[currentFieldSet];
                    int length = fieldSetLengths[currentFieldSet];

                    var field = fieldMemory[currentFieldSet];
                    field.Pointer = pointer + offset;
                    field.Size = (ushort)length;
                }
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private void AllocateFieldSetText(byte* pointer, int[] _fieldSetPositions, int[] _fieldSetLengths)
            {
                fixed (char* rowData = _bufferField)
                {
                    for (uint currentFieldSet = 0; currentFieldSet < _fieldSetSize; currentFieldSet++)
                    {
                        // encode and allocate each of the field , using its location and length

                        var position = _fieldSetPositions[currentFieldSet];
                        var length = _fieldSetLengths[currentFieldSet];

                        Unmanaged_Encode(pointer, position, rowData, 0, length);

                        ///_fieldValues[currentFieldSet] = new string(rowData, 0, length);
                    }
                }
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            protected Records.FieldSet.Field[] GetFieldsMemory(bool threadSafe)
            {
                // 1. get the row bytes.
                // 2. scan the data to determine the fields positions and lengths
                // 3. copy the fields data

                long position = _indexer.GetPosition(Number);

                var pointer = _viewManager.Read(position, threadSafe);

                if (_table.RecordHeaderFormat == RecordHeaderFormats.Fixed)
                {
                    ProcessRowFieldsHeaderNonVariant(pointer, FieldSet);
                }
                else
                    if (_table.RecordHeaderFormat == RecordHeaderFormats.Variant)
                {
                    fixed (int* pFieldSet = FieldSet)
                        ProcessRowFieldsHeaderVariant(pointer, pFieldSet);
                }
                else
                    throw new NotSupportedException();

                return _fieldMemory;
            }

            public Records.FieldSet.Field[] Fields
            {
                get
                {
                    if (_valueFields != null)
                        return _valueFields;

                    return _valueFields = GetFieldsMemory(false);
                }
            }

            internal static Row SetCurrent(MTableCore table, long row, int[] fieldSet, ushort totalFields)
            {
                if (_currentRow != null)
                {
                    _currentRow.Number = row;
                    _currentRow.FieldSet = fieldSet;
                    _currentRow._valueFields = null;

                    // allocate field sets buffers
                    Row.AllocateFieldSet(_currentRow, fieldSet.Length);

                    return _currentRow;
                }

                _currentRow = new Row(table, row, fieldSet, totalFields);

                // allocate field sets buffers
                Row.AllocateFieldSet(_currentRow, fieldSet.Length);

                return _currentRow;
            }
        }
#endif
 
        public override IDataStorage.ITransaction BeginTransation()
        {
            throw new NotImplementedException();
        }

        public IDataStorage.IRow GetRow(long row, ushort totalFields, params int[] fields)
        {
            var current = Row.SetCurrent(this, row, fields, totalFields);

            /// allocate field sets buffers
            ///Row.AllocateFieldSet(current, fieldSet.Length);

            return current; // new IndexRow(this, row, fieldSet);
        }

        [Obsolete("This is not really used any longer")]
        public IEnumerable<IDataStorage.IRow> GetRows(long[] rows, params int[] fields)
        {
            // allocate field sets buffers
            // Row.AllocateFieldSet(fieldSet.Length);

            foreach (var rowNumber in rows)
                yield return Row.SetCurrent(this, rowNumber, fields, (ushort)ColumnCount);

            yield break;
        }

        public override IEnumerable<IDataStorage.IRow> GetRows(long startRow, long endRow, params int[] fields)
        {
            // Skip validation for more performances
            /*
            // perform field set valudation. ensure and order the fieldSet
            fieldSet = fieldSet != null ? fieldSet.Distinct().OrderBy(field => field).ToArray() : Enumerable.Range(0, this.ColumnCount).Select(field => (ushort)field).ToArray();
            */

            // allocate field sets buffers
            //Row.AllocateFieldSet((fieldSet.Length);

            for (var rowNumber = startRow; rowNumber < endRow; rowNumber++)
            {
                yield return Row.SetCurrent(this, rowNumber, fields, (ushort)ColumnCount);
            }

            yield break;
        }

        public long HeaderSize { get { return _headerSize; } }

        private readonly ReaderWriterLockSlim _insertLocker = new ReaderWriterLockSlim();

        private bool _insertionInProgress;
        private bool _appending;

        /// <summary>
        /// Map view containing the meta data for the , contatining the indexes, storage meta and blocks buffer
        /// </summary>
        private MemoryMappedViewStream _metadataMapView;

        /// <summary>
        /// Format type of the record header meta. Defines how the sizes of the fields data are stored in the record header section. 
        /// </summary>
        public RecordHeaderFormats RecordHeaderFormat { get; set; }

        public override sealed void AddRow(InsertRecordHeader header)
        {
            int lengthData = 0;
            int blockBufferSize = 0; // the size of buffer to write for block
            long writerPosition = 0;

            _insertLocker.EnterWriteLock();

            try
            {
                // Set the columns count on the initial insert
                if (ColumnCount == 0)
                    SetColumnCount(header.Count);


                // Set the writer position to be after the last data index.
                if (!_insertionInProgress)
                {
                    if (_writerPosition > 0)
                        throw new InvalidOperationException(
                            "The previous insertion has not been completed. Make sure EndImport or Complete was called.");

                    this.SetWriterPosition(header.Position);

                    _insertionInProgress = true;
                }

                var currentRow = this.IncreaseCount() - 1;

                unsafe
                {
                    int lengthHeader = 0; // the total length of the row-header, which contains the fields offsets
                    int lengthFields = 0; // total length of the row field values

                    fixed (byte* bufferPtr = GetEntryBuffer(header.Length))
                    {
                        // Create the fields values lengths list , in the row header
                        if (RecordHeaderFormat == RecordHeaderFormats.Fixed)
                        {
                            // Fixed Header. Max field length is 255.

                            for (int fpos = 0; fpos < header.Count; fpos++)
                            {
                                var length = header.Lengths[fpos];

                                // write the length value at current header position
                                bufferPtr[lengthHeader] = (byte)length;

                                lengthHeader += 1;
                                lengthFields += length;

                                // Validate value length
                                if (length > ContentManager.MaxValueLength)
                                    throw new InvalidOperationException(string.Format("Size of the value exceed the max allowed. StaticFieldsLength=true, Size: {0}, Max: {1}", length, ContentManager.MaxValueLength));
                            }
                        }
                        else
                            if (RecordHeaderFormat == RecordHeaderFormats.Variant)
                        {
                            // Variant Header
                            for (int fpos = 0; fpos < header.Count; fpos++)
                            {
                                var length = header.Lengths[fpos];

                                // write the length value at current header position
                                lengthHeader += Variant.WriteVInt(bufferPtr + lengthHeader, length);
                                lengthFields += length;
                            }
                        }
                        else
                                if (RecordHeaderFormat == RecordHeaderFormats.HybridFixed)
                        {
                            // Hybrid Header: alues larger then 127 are writen as variants
                            // The header length is stored in the first 2 bytes

                            lengthHeader = 2; // << reserve the first 2 bytes for the header length

                            for (int fpos = 0; fpos < header.Count; fpos++)
                            {
                                var length = header.Lengths[fpos];
                                // write the length value at current header position

                                if (length < Variant.MaxSingleByteValue)
                                {
                                    // fixed-single-byte

                                    bufferPtr[lengthHeader] = (byte)length;
                                    lengthHeader += 1;
                                }
                                else
                                {
                                    // variant
                                    lengthHeader += Variant.WriteVInt(bufferPtr + lengthHeader, length);
                                }

                                lengthFields += length;
                            }

                            // store the header length in the first 2 bytes
                            Native.Utils.NumberToBytes(bufferPtr, lengthHeader, 2);
                        }
                        else
                        {
                            // Hybrid Header: alues larger then 127 are writen as variants

                            for (int fpos = 0; fpos < header.Count; fpos++)
                            {
                                var length = header.Lengths[fpos];

                                // write the length value at current header position

                                if (length < 128)
                                {
                                    // fixed-single-byte

                                    bufferPtr[lengthHeader] = (byte)length;
                                    lengthHeader += 1;
                                }
                                else
                                {
                                    // variant
                                    lengthHeader += Variant.WriteVInt(bufferPtr + lengthHeader, length);
                                }

                                lengthFields += length;
                            }
                        }

                        // _insertLocker.EnterWriteLock();
                        try
                        {
                            blockBufferSize = _block.RegisterEntry(lengthData = lengthHeader + lengthFields);
                        }
                        finally
                        {
                            // _insertLocker.ExitWriteLock();
                        }

                        // Create Index, Write the row-header and fields-content into the memory

                        try
                        {
                            // _insertLocker.EnterWriteLock();

                            writerPosition = _writerPosition;

                            if (_appending)
                                _writerPosition += (lengthHeader + header.Length + blockBufferSize);
                            else
                                _writerPosition += (lengthHeader + header.Length);
                        }
                        finally
                        {
                            // _insertLocker.ExitWriteLock();
                        }



                        // Write the entry location and size to the index. 
                        // If the current header position and current row match, append the index. 
                        // Otherwise, consider the operation as Insertion
                        if (header.Position != null && (ulong)header.Position < _indexer.IndexCount)
                        {
                            // TEMPS, use the insetion just for testing, to use the InsertedClustered approach
                            if (_insertionInProgress && Manager.InTransation)
                            {
                                // it means, the index is created, but is not assigned, so assign it using the writer position and len
                                _indexer.SetAllocated((long)header.Position, writerPosition, lengthData);
                            }
                            else
                            {
                                // Push the descending block entries to make room for this record
                                Manager.InsertEntry((long)header.Position, writerPosition, lengthData);
                            }
                        }
                        else
                        {
                            _indexer.Add(currentRow, 0, 1, writerPosition, lengthData);
                        }

                        //**********************************************************************

                        // Get the map view writer and manage the writing here, the header and entry content has to be writen on same view.

                        var writerView = this.MapViews.GetView(writerPosition, true);

                        // Check for capacity (optimize: it can be removed, to increase performances)
                        if (writerView.Position + lengthHeader + header.Length > this.Capacity)
                            throw new InvalidOperationException(string.Format("Not enougth space in the data storage. Capacity of {0:n0}MB exceeded.", this.Capacity / 1024 / 1024));

                        this.MapViews.BeginWrite(writerView);

                        // Write the entry header first
                        Native.CopyMemory(writerView.Pointer + writerView.Position, bufferPtr, lengthHeader);

                        // Write the entry content data second
                        fixed (byte* dataPtr = header.Data)
                            Native.CopyMemory(writerView.Pointer + writerView.Position + lengthHeader, dataPtr, header.Length);

                        writerPosition += (lengthHeader + header.Length); // Advance the writer position

                        header.Release();  // Return header asap , for better insert performances

                        writerView.SetLastWrite();  //  Mark the last write operation

                        //**********************************************************************
                    }

                    _headerSize += lengthHeader; // increase number of the total header size

                    this.IncreaseDataSize(lengthFields + lengthHeader);
                    // increase the size by the real fields data size only, exluding the row header size
                }


                // Write empty block buffer if needed
                if (blockBufferSize > 0 && _appending)
                {
                    byte[] buffer = new byte[blockBufferSize];

                    MapViews.Write(buffer, 0, writerPosition, blockBufferSize);

#if DEBUG_
                    if (System.Diagnostics.Debugger.IsAttached)
                        System.Diagnostics.Debug.WriteLine("Block Buffer Writen: {0} @ {1}", blockBufferSize, writerPosition);
#endif
                    //_writerPosition += blockBufferSize;
                }

            }
            finally
            {
                _insertLocker.ExitWriteLock();

                // release the header row
              //  header.Release();
            }

            //this.IncreaseCount();
        }

        /// <summary>
        /// Sets the position of the writer based on the currently provided record number. If can be positioned for insertion or for appending
        /// </summary>
        /// <param name="entry"></param>
        private Indexer.Index SetWriterPosition(long? entry)
        {
            //  The writer position is set afte the last index, considering this is appending. 
            //  On insertion , the writer is set to different location, based on the insertion point index.
            //  IMPORTANT: If there is no real index for the startentry, it means that we are at the end of the data storage and the operaton should be considered as Appending. 
            //  therefore, get the last index for teh data storage and position to writer after that index.

            _writerPosition = 0;
            _appending = true;

            if (Blocks.DataEntries > 0)
            {
                // determine the writer position, which have to be set after all of the indexed data in append more, or at the place of the interted entry in insert mode

                if (entry != null && entry < Blocks.DataEntries)    // Insert mode
                {
                    var index = Blocks.GetEntryIndex((long)entry);
                    _writerPosition = index.Position;   // Set the writer at the position of the existing index entry
                    _appending = false;
                    return index;
                }
                else    // Append mode
                {
                    var index = Blocks.GetEntryIndex(Blocks.DataEntries - 1);
                    _writerPosition = index.Position + index.Length;    // Set the writer position at the end
                    return index;
                }
            }

            return Indexer.Index.Empty;
        }

        /// <summary>
        /// Gets pre-allocated buffer for entry operations. If existing buffer is smaller then the required size, large buffer is re-created.
        /// </summary>
        /// <param name="requiredSize"></param>
        /// <returns></returns>
        private byte[] GetEntryBuffer(int requiredSize)
        {
            if (_bufferEntry != null && requiredSize <= _bufferEntry.Length)
                return _bufferEntry;

            if (requiredSize > ContentManager.MaxEntryLength)
                throw new InvalidOperationException(string.Format("Size of the full entry exceed the max allowed. Size: {0}, Max: {1}", requiredSize, ContentManager.MaxEntryLength));

            // allocate buffers and fill with test data
            _bufferEntry = new byte[Math.Max(requiredSize, 1024 * 64)]; // min 64Kb

            for (int i = 0; i < _bufferEntry.Length; i++)
                _bufferEntry[i] = 127;

            return _bufferEntry;
        }

        public override void QueueRow(InsertRecordHeader header)
        {
            RecordQueue.QueueRecord(this, header);
        }

        public Records QueryRows(long startRow, long endRow, int[] fields)
        {
            return new Records(this, startRow, endRow, Records.FieldSet.Allocate(this, fields));
        }

        public Records QueryRows(long startRow, long endRow, Records.FieldSet fieldSet)
        {
            return new Records(this, startRow, endRow, fieldSet);
        }

        public IEnumerable<IDataStorage.IRow> GetRows()
        {
            return GetRows(0, RowCount, Enumerable.Range(0, this.ColumnCount).Select(field => field).ToArray());
        }

        public IEnumerable<IDataStorage.IRow> GetRows(params int[] fields)
        {
            return GetRows(0, RowCount, fields);
        }

        public override int ColumnCount
        {
            get { return HEADER->Fields; }
        }

        public string PrintIndex(bool distantOnly = false, bool returnOnly = false)
        {
            return Index.PrintIndexes(distantOnly: distantOnly, returnOnly: returnOnly, indexes: null);
        }


        /// <summary>
        /// Represents queue for fast parallel importing of record headers for multiple tables
        /// </summary>
        public class RecordQueueProcessor : IDisposable
        {
            private static RecordQueueProcessor _instance;

            /// <summary>
            /// Gets singleton static instance for the processor
            /// </summary>
            public static RecordQueueProcessor Instance
            {
                get
                {
                    if (_instance != null)
                        return _instance;
                    return _instance = new RecordQueueProcessor();
                }
            }

            /// <summary>
            /// Number of working threads used by the queue processor
            /// </summary>
            public const int NumberOfThreads = 1; // It was 2 threads before, but that cause sequence ordering problem

            /// <summary>
            /// Number of queue items allocated for the processor. 
            /// Any attempt to queue item after this number is reached, will block the operation until the processor releases queue item
            /// </summary>
            public const int NumberOfMaxQueueItems = 1024 * 10;

            private readonly BlockingCollection<TRQueueItem> _recordsQueue;
            private bool _disposed;

            private RecordQueueProcessor()
            {
                _recordsQueue = new BlockingCollection<TRQueueItem>(NumberOfMaxQueueItems);

                // Start the queue insertion task
                for (var idx = 0; idx < NumberOfThreads; idx++)
                    new Thread(() =>
                    {
                        try
                        {
                            foreach (var queueItem in _recordsQueue.GetConsumingEnumerable())
                            {
                                queueItem.Table.AddRow(queueItem.Record);
                            }
                        }
                        catch (OperationCanceledException)
                        {
                        }
                        catch (ObjectDisposedException)
                        {

                        }

                    })
                    { Name = "RecordQueueProcessor.Thread." + (idx + 1) }.Start();
            }

            /// <summary>
            /// Adds record for a table into the queue processor. The attempt will block temporary, if the processor is busy processing other items.
            /// </summary>
            /// <param name="table"></param>
            /// <param name="record"></param>
            public void QueueRecord(MTableCore table, InsertRecordHeader record)
            {
                // Note: Performance can be improved by using ObjectPool for the TableRecordItem, to avoid the object creation overhead
                _recordsQueue.Add(new TRQueueItem
                {
                    Table = table,
                    Record = record
                });
            }

            /// <summary>
            /// Object containing the Table and Record being queued
            /// </summary>
            private struct TRQueueItem
            {
                internal MTableCore Table;
                internal InsertRecordHeader Record;
            }

            public void Dispose()
            {
                ((IDisposable)this).Dispose();
            }

            void IDisposable.Dispose()
            {
                if (!_disposed)
                {
                    _recordsQueue.CompleteAdding();
                    //_recordsQueue.Dispose();
                    _disposed = true;

                    _instance = null;
                }
            }
        }
    }
}
