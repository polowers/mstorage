﻿using System;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace DigitalToolworks.Data
{
    partial class MTableCore
    {
        public sealed unsafe class Indexer : IDisposable
        {
            private MemoryMappedViewStream _memoryMapView;

            /// <summary>
            /// The underlaying metadata indexer header
            /// </summary>
            private readonly MStorage.Metadata.Header.Core.DataIndexer* HEADER;

            /// <summary>
            /// Create pure in-memory based data indexer
            /// </summary>
            /// <param name="capacityMb"></param>
            internal Indexer(long capacityMb = 6000L)
                : this(MemoryMappedFile.CreateNew(null, 1024L * 1024L * (Environment.Is64BitProcess ? capacityMb : 1000L))
                .CreateViewStream())
            {

            }

            /// <summary>
            /// Create data indexer associated with memory map view
            /// </summary>
            /// <param name="mapView"></param>
            internal Indexer(MemoryMappedViewStream mapView)
                : this((MStorage.Metadata.Header.Core.DataIndexer*)Native.Utils.Memory.AcquirePointer(mapView))   // Map the header to the allocated memory
            {
                _memoryMapView = mapView;
            }

            /// <summary>
            /// Create data indexer associated with its DataIndexer Header
            /// </summary>
            /// <param name="headerIndexer"></param>
            internal Indexer(MStorage.Metadata.Header.Core.DataIndexer* headerIndexer)
            {
                this.HEADER = headerIndexer;

                // Allocate the content if it's not allocated yet [ if (this.header->Content == (byte*)0) ]
                this.HEADER->Content = ((byte*)HEADER) + sizeof(MStorage.Metadata.Header.Core.DataIndexer);
            }

            internal string PrintIndexes(bool distantOnly, bool returnOnly, params long[] indexes)
            {
                // Print indexes information. It can display all intexes or only those who has distance different then zero

                var sb = new StringBuilder();
                sb.AppendLine(new string('-', 100));
                if (indexes != null && indexes.Length > 0)
                {
                    Index previous = Index.Empty;

                    foreach (var record in indexes)
                    {
                        var index = Get(record, 0, 1);

                        if (!previous.Equals(Index.Empty))
                        {
                            var distance = index.Position - (previous.Position + previous.Length);

                            if (!distantOnly || distance != 0)
                                sb.AppendLine(string.Format("  {0}) {1}  -> {2}", record, index.ToString(), distance));
                        }
                        else
                            sb.AppendLine(string.Format("  {0}) {1}", record, index.ToString()));

                        previous = index;
                    }
                }
                else
                {
                    Index previous = Index.Empty;

                    for (var record = 0; record < (int)this.IndexCount; record++)
                    {
                        var index = Get(record, 0, 1);

                        if (!previous.Equals(Index.Empty))
                        {
                            var distance = index.Position - (previous.Position + previous.Length);

                            if (!distantOnly || distance != 0)
                                sb.AppendLine(string.Format("  {0}) {1}  -> {2}", record, index.ToString(), distance));
                        }
                        else
                            sb.AppendLine(string.Format("  {0}) {1}", record, index.ToString()));

                        previous = index;
                    }
                }
                sb.AppendLine(new string('-', 100));

                if (System.Diagnostics.Debugger.IsAttached && !returnOnly)
                    System.Diagnostics.Debug.WriteLine(sb.ToString());

                return sb.ToString();
            }

            /// <summary>
            /// Total number of index entries
            /// </summary>
            public ulong IndexCount { get { return HEADER->Indexes; } }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public void Add(long rowNumber, int columnNumber, int columnCount, long dataLocation, int dataLength)
            {
                var positionIndex = (rowNumber * columnCount + columnNumber) * 8;

                this.Add(positionIndex, dataLocation, dataLength);
            }

            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            private void Add(long positionIndex, long dataLocation, int dataLength)
            {
#if VERBOSE_INDEXER
                System.Diagnostics.Debug.WriteLine("Indexer Add: idx={0}  pos={3}  dataLoc={1}  dataLen={2}", IndexCount, dataLocation, dataLength, _memory.Position);
#endif

                IndexToPointer(HEADER->Content + positionIndex, dataLocation, dataLength);

                HEADER->Indexes++;
                HEADER->IndexedDataSize += dataLength;

                if (HEADER->IndexedDataSize < 0)
                    throw new InvalidOperationException("Indexer size bellow zero");
            }

            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public unsafe void GetIndexes(long rowNumber, int columnCount, ref long[] position, ref int[] length)
            {
                for (int column = 0; column < columnCount; column++)
                {
                    var positionIndex = (rowNumber * columnCount + column) * 8; // -7;

                    PointerToIndex(HEADER->Content + positionIndex, out position[column], out length[column]);
                }
            }

            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public Index Get(long rowNumber, int columnNumber, int columnCount)
            {
                long dataLocation;
                int dataLength;

                var positionIndex = (rowNumber * columnCount + columnNumber) * 8;

                PointerToIndex(HEADER->Content + positionIndex, out dataLocation, out dataLength);

                var index = new Index(dataLocation, dataLength);

#if VERBOSE_INDEXER
                    System.Diagnostics.Debug.WriteLine("Indexer Get: [{0}-{1}-{2}]   pos={3}  dataLoc={4}  dataLen={5}", rowNumber, columnNumber, columnCount,  position, dataLocation, dataLength);
#endif
                return index;
            }

            public void Remove(long rowNumber, int columnNumber, int columnCount)
            {
                // remove the index entry and shift all descenting entries

                var positionIndex = (rowNumber * columnCount + columnNumber) * 8;
                long position;
                int length;
                PointerToIndex(HEADER->Content + positionIndex, out position, out length);

                // overwrite the current index with all descending indexes

                var destination = (rowNumber * columnCount + columnNumber) * 8;
                var source = ((rowNumber + 1) * columnCount + columnNumber) * 8;
                var descendingLength = (int)((long)IndexCount - rowNumber) * 8;

                Native.memcpy(HEADER->Content + destination, HEADER->Content + source, descendingLength);

                HEADER->Indexes = HEADER->Indexes - 1;
                HEADER->IndexedDataSize = HEADER->IndexedDataSize - length;
            }

            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Set(long rowNumber, int columnNumber, int columnCount, Index index)
            {
                // calculate the index pointer data
                // locate the pointer block in the memory to write into

                var positionIndex = (rowNumber * columnCount + columnNumber) * 8;

                int lengthOld;
                long position;
                PointerToIndex(HEADER->Content + positionIndex, out position, out lengthOld);

                IndexToPointer(HEADER->Content + positionIndex, index.Position, index.Length);

                // update the total size, based on the old and new lengths
                HEADER->IndexedDataSize += (index.Length - lengthOld);

                if (HEADER->IndexedDataSize < 0)
                    throw new InvalidOperationException("Indexer size bellow zero");
            }

            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public Index Set(long rowNumber, int columnNumber, int columnCount, long position, int length)
            {
                var index = new Indexer.Index(position, length);

                this.Set(rowNumber, columnNumber, columnCount, index);

                return index;
            }

            public void Dispose()
            {
                if (_memoryMapView != null)
                {
                    _memoryMapView.SafeMemoryMappedViewHandle.ReleasePointer();
                    _memoryMapView.Dispose();
                }
                _memoryMapView = null;
            }

            void IDisposable.Dispose()
            {
                if (_memoryMapView != null)
                {
                    _memoryMapView.SafeMemoryMappedViewHandle.ReleasePointer();
                    _memoryMapView.Dispose();
                }
                _memoryMapView = null;
            }

            private static byte[] WriteIndex(Index index)
            {
                int rawsize = Marshal.SizeOf(index);
                byte[] rawdata = new byte[rawsize];
                GCHandle handle = GCHandle.Alloc(rawdata, GCHandleType.Pinned);

                try
                {
                    Marshal.StructureToPtr(index, handle.AddrOfPinnedObject(), false);

                    return rawdata;
                }
                finally
                {
                    handle.Free();
                }
            }

            private static Index ReadIndex(byte[] index)
            {
                GCHandle handle = GCHandle.Alloc(index, GCHandleType.Pinned);

                try
                {
                    Index value = (Index)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(Index));

                    return value;
                }
                finally
                {
                    handle.Free();
                }
            }

            /// <summary>
            /// Reads the index header meta data from the memory
            /// </summary>
            private void ReadMetaHeader()
            {
                /* [ META HEADER LAYOUT ] (16 bytes total fixed header)
                    
                    1] _indexCount  ulong   8bytes
                    2] _dataSize    long    8bytes                  
                 --------------------------------------------------*/

                // approach 1: operate with casts directly
                ////__indexCount = *(((ulong*)__metaPtr));
                ////__dataSize = *(((long*)(__metaPtr + 8)));

                // approach 2: use native convertion routines               
                //  _indexCount = (ulong)Native.Utils.BytesToNumberOpt(_metaPtr, 8);
                //  _dataSize = Native.Utils.BytesToNumberOpt(_metaPtr + 8, 8);

                ////__metaIndexCount = __indexCount;
                ////__metaDataSize = __dataSize;
            }

            /// <summary>
            /// Writes the index header meta data in the memory
            /// </summary>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private void WriteMetaHeader()
            {

                /* META HEADER LAYOUT (16 bytes total fixed header)
                    
                  1] _indexCount  ulong   8bytes
                  2] _dataSize    long    8bytes                  
               --------------------------------------------------*/

                // Note: performace improvement, but make sure the check does not cost more then the actuall assignment

                ////////if (__indexCount != __metaIndexCount)
                ////////{
                ////////    // approach 1: operate with casts directly
                ////////    *((ulong*)(__metaPtr)) = __indexCount;

                ////////    // approach 2: use native convertion routines
                ////////    //  Native.Utils.NumberToBytes(_metaPtr, (long)_indexCount, 8);

                ////////    __metaIndexCount = __indexCount;
                ////////}

                ////////if (__dataSize != __metaDataSize)
                ////////{
                ////////    // approach 1: operate with casts directly
                ////////    *((long*)(__metaPtr + 8)) = __dataSize;

                ////////    // approach 2: use native convertion routines
                ////////    //  Native.Utils.NumberToBytes(_metaPtr + 8, (long)_dataSize, 8);

                ////////    __metaDataSize = __dataSize;
                ////////}
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static void _IndexToPointer(long position, int length, ref byte[] pointerBuffer)
            {
                pointerBuffer[0] = (byte)position;
                pointerBuffer[1] = (byte)(position >> 8);
                pointerBuffer[2] = (byte)(position >> 16);
                pointerBuffer[3] = (byte)(position >> 24);
                pointerBuffer[4] = (byte)(position >> 32);


                pointerBuffer[5] = (byte)length;
                pointerBuffer[6] = (byte)(length >> 8);
                pointerBuffer[7] = (byte)(length >> 16);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static void IndexToPointer(byte* pointer, long position, int length)
            {
                pointer[0] = (byte)position;
                pointer[1] = (byte)(position >> 8);
                pointer[2] = (byte)(position >> 16);
                pointer[3] = (byte)(position >> 24);
                pointer[4] = (byte)(position >> 32);


                pointer[5] = (byte)length;
                pointer[6] = (byte)(length >> 8);
                pointer[7] = (byte)(length >> 16);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static void PointerToIndex(byte[] pointerBuffer, out long position, out int length)
            {
                position = (((long)pointerBuffer[4] << 32) | ((long)pointerBuffer[3] << 24) | ((long)pointerBuffer[2] << 16) | ((long)pointerBuffer[1] << 8) | (long)pointerBuffer[0]);
                length = ((int)pointerBuffer[7] << 16 | (int)pointerBuffer[6] << 8 | (int)pointerBuffer[5]);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private static void PointerToIndex(byte* pointer, out long position, out int length)
            {
                position = (((long)pointer[4] << 32) | ((long)pointer[3] << 24) | ((long)pointer[2] << 16) | ((long)pointer[1] << 8) | (long)pointer[0]);
                length = ((int)pointer[7] << 16 | (int)pointer[6] << 8 | (int)pointer[5]);
            }

            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Index
            {
                internal static readonly Index Empty = new Index();

                public readonly long Position;
                public readonly int Length;

                public Index(long position, int length)
                {
                    this.Position = position;
                    this.Length = length;
                }

                private static Index FromBytes(byte[] value)
                {
                    long position = (((long)value[4] << 32) | ((long)value[3] << 24) | ((long)value[2] << 16) | ((long)value[1] << 8) | (long)value[0]);
                    int length = ((int)value[7] << 16 | (int)value[6] << 8 | (int)value[5]);
                    return new Index(position, length);
                }

                public override string ToString()
                {
                    return string.Format("Position: {0}, Length: {1}", this.Position, this.Length);
                }
            }


            /// <summary>
            /// Total size of the data indexed
            /// </summary>
            public long IndexedDataSize { get { return HEADER->IndexedDataSize; } }

            /// <summary>
            /// Total memory size taken by the indexer
            /// </summary>
            public long IndexContentSize { get { return (long)IndexCount * 8L; } }

            public void Backup(BinaryWriter writer)
            {
                writer.Write(this.IndexedDataSize);
                writer.Write(this.IndexCount);

                _memoryMapView.Position = 0;

                byte[] copyBuffer = new byte[1024 * 1024 * 8];
                while (_memoryMapView.Position < this.IndexContentSize)
                {
                    var readen = _memoryMapView.Read(copyBuffer, 0, Math.Min(copyBuffer.Length, (int)(this.IndexContentSize - _memoryMapView.Position)));
                    writer.Write(copyBuffer, 0, readen);
                }
            }

            public void Restore(BinaryReader reader)
            {
                HEADER->IndexedDataSize = reader.ReadInt64();
                HEADER->Indexes = reader.ReadUInt64();

                byte[] copyBuffer = new byte[1024 * 1024 * 8];
                _memoryMapView.Position = 0;

                long indexContentLength = 0;
                while (indexContentLength < this.IndexContentSize)
                {
                    var readen = reader.Read(copyBuffer, 0, Math.Min(copyBuffer.Length, (int)(this.IndexContentSize - indexContentLength)));
                    _memoryMapView.Write(copyBuffer, 0, readen);
                    indexContentLength += readen;
                }

                System.Diagnostics.Debug.Assert(indexContentLength == IndexContentSize);
            }

            /// <summary>
            /// Gets the data memory position only from the index entry
            /// </summary>
            /// <param name="rowNumber">Row entry number</param>
            /// <returns></returns>
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public long GetPosition(long rowNumber)
            {
                byte* entry = HEADER->Content + (rowNumber * 8);

                return (((long)*(entry + 4) << 32) | ((long)*(entry + 3) << 24) | ((long)*(entry + 2) << 16) | ((long)*(entry + 1) << 8) | (long)*(entry));

                //long position = (((long)entry[4] << 32) | ((long)entry[3] << 24) | ((long)entry[2] << 16) | ((long)entry[1] << 8) | (long)entry[0]);
                // return position;
            }

            public void Validate(long entry)
            {
                int dof = GetDescendantGap(entry);
                if (dof < 0)
                    throw new InvalidOperationException("Index validation failed");

                int aof = GetAncendantGap(entry);
                if (aof > 0)
                    throw new InvalidOperationException("Index validation failed");
            }

            /// <summary>
            /// Allocates continous number of indexes. Inserts group of continous indexes at the record position, with zero lengths
            /// </summary>
            /// <param name="recordStart"></param>
            /// <param name="allocateCount"></param>
            public void Allocate(long recordStart, int allocateCount)
            {
                // Move all descending indexes forward. If the starting record is at the end, it means appending
                if (recordStart < (long)IndexCount)
                {
                    var source = recordStart * 8;

                    var destination = source + (allocateCount * 8);

                    var shiftAmount = (int)((long)IndexCount - recordStart) * 8;

                    Native.memcpy(HEADER->Content + destination, HEADER->Content + source, shiftAmount);
                }

                HEADER->Indexes += (ulong)allocateCount;
            }

            public void Insert(long rowNumber, int columnNumber, int columnCount, long dataLocation, int dataLength)
            {
                // Make room for the new entry first, by shifting all of the descending entries.
                // Then, use the normal addition procedure to place this entry 

                HEADER->Indexes++;
                HEADER->IndexedDataSize += dataLength;
                if (HEADER->IndexedDataSize < 0)
                    throw new InvalidOperationException("Indexer size bellow zero");

                var source = (rowNumber * columnCount + columnNumber) * 8;
                {
                    var destination = source + 8;

                    var descendingLength = (int)((long)IndexCount - rowNumber) * 8;

                    Native.memcpy(HEADER->Content + destination, HEADER->Content + source, descendingLength);
                }

                // SET THE INSERTED DATA  ##############################
                var pointer = HEADER->Content + source;

                pointer[0] = (byte)dataLocation;
                pointer[1] = (byte)(dataLocation >> 8);
                pointer[2] = (byte)(dataLocation >> 16);
                pointer[3] = (byte)(dataLocation >> 24);
                pointer[4] = (byte)(dataLocation >> 32);

                pointer[5] = (byte)dataLength;
                pointer[6] = (byte)(dataLength >> 8);
                pointer[7] = (byte)(dataLength >> 16);
                // ############################################################
            }

            /// <summary>
            /// Gets the bytes offset between this and the next index, most optimized
            /// </summary>
            /// <param name="entry"></param>
            internal int GetAncendantGap(long entry)
            {
                if (entry + 1 < (long)IndexCount)
                {
                    byte* current = HEADER->Content + (entry * 8);
                    byte* next = current + 8; // _memoryPtr + ((entry + 1) * 8);

                    var currentPosition = (((long)*(current + 4) << 32) | ((long)*(current + 3) << 24) | ((long)*(current + 2) << 16) | ((long)*(current + 1) << 8) | (long)*(current));
                    var currentLength = ((int)current[7] << 16 | (int)current[6] << 8 | (int)current[5]);

                    var nextPosition = (((long)*(next + 4) << 32) | ((long)*(next + 3) << 24) | ((long)*(next + 2) << 16) | ((long)*(next + 1) << 8) | (long)*(next));

                    int gapOffset = (int)((currentPosition + currentLength) - nextPosition);

                    return gapOffset;
                }

                return 0;
            }

            /// <summary>
            /// Gets the bytes offset between this and the previous index, most optimized
            /// </summary>
            /// <param name="entry"></param>
            internal int GetDescendantGap(long entry)
            {
                if (entry > 0)
                {
                    byte* current = HEADER->Content + (entry * 8);
                    byte* prev = current - 8; // _memoryPtr + ((entry - 1) * 8);

                    var currentPosition = (((long)*(current + 4) << 32) | ((long)*(current + 3) << 24) | ((long)*(current + 2) << 16) | ((long)*(current + 1) << 8) | (long)*(current));

                    var prevPosition = (((long)*(prev + 4) << 32) | ((long)*(prev + 3) << 24) | ((long)*(prev + 2) << 16) | ((long)*(prev + 1) << 8) | (long)*(prev));
                    var prevLength = ((int)prev[7] << 16 | (int)prev[6] << 8 | (int)prev[5]);

                    int gapOffset = (int)(currentPosition - (prevPosition + prevLength));
                    return gapOffset;
                }

                return 0;
            }

            /// <summary>
            /// Reposition the locations of all indexes in the range by certain bytes offset
            /// </summary>
            /// <param name="entryStart">Range start index</param>
            /// <param name="entryEnd">Range end index</param>
            /// <param name="writeOffset">Amount to reposition</param>
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            internal void RepositionRange(long entryStart, long entryEnd, int writeOffset)
            {
                // Move te entire block of memory between the start and end indexes

                byte* current = HEADER->Content + (entryStart * 8);
                while (entryStart <= entryEnd)
                {
                    //#################################################
                    // get the current position
                    var position = (((long)*(current + 4) << 32) | ((long)*(current + 3) << 24) | ((long)*(current + 2) << 16) | ((long)*(current + 1) << 8) | (long)*(current));

                    // change it by the offset 
                    position += writeOffset;

                    current[0] = (byte)position;
                    current[1] = (byte)(position >> 8);
                    current[2] = (byte)(position >> 16);
                    current[3] = (byte)(position >> 24);
                    current[4] = (byte)(position >> 32);
                    //#################################################

                    current = current + 8;
                    entryStart++;
                }
            }

            /// <summary>
            /// Move the data positioin for the entry index, by the certain data offset (optimized)
            /// </summary>
            /// <param name="entry"></param>
            /// <param name="offset"></param>
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            private void Reposition(long entry, int offset)
            {
                byte* current = HEADER->Content + (entry * 8);

                // get the current position
                var position = (((long)*(current + 4) << 32) | ((long)*(current + 3) << 24) | ((long)*(current + 2) << 16) | ((long)*(current + 1) << 8) | (long)*(current));

                // change it by the offset 
                position += offset;

                current[0] = (byte)position;
                current[1] = (byte)(position >> 8);
                current[2] = (byte)(position >> 16);
                current[3] = (byte)(position >> 24);
                current[4] = (byte)(position >> 32);
            }


            /// <summary>
            /// Allocates index which has been created by clusered index creation, but not assigned yet.
            /// </summary>
            /// <param name="record"></param>
            /// <param name="dataLocation"></param>
            /// <param name="lengthData"></param>
            internal void SetAllocated(long record, long dataLocation, int lengthData)
            {
                var pointer = HEADER->Content + (record * 8);

                // SET THE INSERTED DATA  ############################## 
                pointer[0] = (byte)dataLocation;
                pointer[1] = (byte)(dataLocation >> 8);
                pointer[2] = (byte)(dataLocation >> 16);
                pointer[3] = (byte)(dataLocation >> 24);
                pointer[4] = (byte)(dataLocation >> 32);

                pointer[5] = (byte)lengthData;
                pointer[6] = (byte)(lengthData >> 8);
                pointer[7] = (byte)(lengthData >> 16);
                // ############################################################

                HEADER->IndexedDataSize += lengthData;
            }

            /// <summary>
            /// Removes continous block of indexes
            /// </summary>
            /// <param name="recordStart"></param>
            /// <param name="p"></param>
            internal void Remove(long recordStart, long removeCount)
            {
                // Shift the descending indexes over the start index 

                // Move all descending indexes forward
                var source = (recordStart + removeCount) * 8;
                var destination = recordStart * 8;

                var shiftAmount = (int)((long)IndexCount - (recordStart + removeCount)) * 8;

                Native.memcpy(HEADER->Content + destination, HEADER->Content + source, shiftAmount);

                HEADER->Indexes -= (ulong)removeCount;
            }
        }
    }
}
