﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalToolworks.Data
{
    partial class MTableCore
    {
        partial class ContentManager
        {
            /// <summary>
            /// Base class for storage managed transation
            /// </summary>
            /// <typeparam name="T"></typeparam>
            internal abstract class ManagerTransaction<T> : ITransaction
            {
                protected readonly ContentManager manager;
                int _pendingCount;
                readonly List<T> _queue;

                readonly System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

                internal ManagerTransaction(ContentManager operationManager, IDataStorage storage)
                    : base(storage)
                {
                    manager = operationManager;
                    _queue = new List<T>(QueueSize);
                    IsProcessing = false;

                    if (manager._contentBeginModifyCallback != null)
                        manager._contentBeginModifyCallback();

                    if (LogTransaction)
                    {
                        System.Diagnostics.Debug.WriteLine(">>> BEGIN TRANSACTION >>>");
                        sw.Start();
                    }
                }

                /// <summary>
                /// Number of operations pending to be processed
                /// </summary>
                public int PendingCount
                {
                    get { return _pendingCount; }
                }

                /// <summary>
                /// Number of total queue items, to be accumulated the processed in groups. 
                /// </summary>
                protected abstract int QueueSize { get; }
                protected abstract void ProcessAccumulation(IEnumerable<T> queue);

                protected void Queue(T item)
                {
                    _queue.Add(item);
                    _pendingCount++;

                    /// Process the queued items once the the queue is full
                    if (_pendingCount == QueueSize)
                        ProcessAccumulationCore();
                }


                /// <summary>
                /// Gets whether the current transation is executing the accumulation process currently
                /// </summary>
                public bool IsProcessing { get; private set; }

                /// <summary>
                /// Process the transation for the queue accumulated to this point
                /// </summary>
                private void ProcessAccumulationCore()
                {
                    if (this.PendingCount > 0)
                    {
                        if (LogTransaction)
                        {
                            System.Diagnostics.Debug.WriteLine("\t\t- Accumulation Processing (Queue {0}) ", PendingCount);
                            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
                        }

                        this.IsProcessing = true;

                        this.ProcessAccumulation(_queue);
                        _pendingCount = 0;
                        _queue.Clear();
                        this.IsProcessing = false;

                        if (LogTransaction)
                        {
                            System.Diagnostics.Debug.WriteLine("\t\t- Accumulation Processed ({0} ms)", (int)sw.ElapsedMilliseconds);
                            sw.Stop();
                        }
                    }
                }

                public override void Commit()
                {
                    base.Commit();

                    this.ProcessAccumulationCore();
                    manager.transactionCurrent = null;

                    if (LogTransaction)
                    {
                        System.Diagnostics.Debug.WriteLine(">>> END TRANSACTION ({0} ms) >>>", (int)sw.ElapsedMilliseconds);
                        sw.Stop();
                    }

                    manager.SetModified();
                }

                public override void Rollback()
                {
                    manager.transactionCurrent = null;
                }

                /// <summary>
                /// Groups elements in groups by custom predicate
                /// </summary>
                /// <typeparam name="T"></typeparam>
                /// <param name="source"></param>
                /// <param name="predicate"></param>
                /// <returns></returns>
                internal static IEnumerable<IEnumerable<E>> GroupAdjacentBy<E>(IEnumerable<E> source, Func<E, E, bool> predicate)
                {
                    using (var e = source.GetEnumerator())
                    {
                        if (e.MoveNext())
                        {
                            var list = new List<E> { e.Current };
                            var pred = e.Current;
                            while (e.MoveNext())
                            {
                                if (predicate(pred, e.Current))
                                {
                                    list.Add(e.Current);
                                }
                                else
                                {
                                    yield return list;
                                    list = new List<E> { e.Current };
                                }
                                pred = e.Current;
                            }
                            yield return list;
                        }
                    }
                }


            }


            /// <summary>
            /// Transaction for removing records
            /// </summary>
            internal sealed class RemoveTransaction : ManagerTransaction<long>
            {
                readonly int _queueSize;
                int _removedRecordsCount;
                long _removedDataSize;
                /// <summary>
                /// On transation commit, data defragmentation will be executed which will free the undelaying space taken by the data removed
                /// </summary>
                bool _defragmentOnCommit;

                public RemoveTransaction(ContentManager operationManager, bool defragmentOnCommit)
                    : base(operationManager, operationManager.storage)
                {
                    // Arbitrary size for remove bulk operation, 2K records
                    _queueSize = 2000;
                    _defragmentOnCommit = defragmentOnCommit;
                }

                protected override int QueueSize
                {
                    get { return _queueSize; }
                }

                /// <summary>
                /// Splits the source by groups of continous values
                /// </summary>
                /// <typeparam name="T"></typeparam>
                /// <param name="records"></param>
                /// <returns></returns>
                private IEnumerable<IEnumerable<long>> GetContinuations(IEnumerable<long> records)
                {
                    var continuations = GroupAdjacentBy<long>(records.OrderBy(record => record), (rc1, rc2) => rc1 + 1 == rc2);
                    return continuations;
                }

                protected override void ProcessAccumulation(IEnumerable<long> queue)
                {
                    //  GROUP RECORDS BY "BLOCKS"
                    var blocks = queue
                        .GroupBy(record => manager.blocks.GetBlock(record), record => record, (key, group) =>
                            new
                            {
                                Block = key,
                                BlockRecords = group
                            });

                    //  PROCESS THE BLOCKS RECORDS
                    foreach (var block in blocks)
                    {
                        if (LogTransaction)
                            System.Diagnostics.Debug.WriteLine("\r\n\t\t> Block {0}, {1} records, process start.", block.Block, block.BlockRecords.Count());

                        //  Split the block records by continuations
                        foreach (var continuation in this.GetContinuations(block.BlockRecords))
                        {
                            // GET THE LAST AND FIRST ENTRY FROM THE CONTINUATION (Consider the queue is in ordered continuation at this point, temporary)
                            long entryStart = continuation.First() - _removedRecordsCount;
                            long entryEnd = continuation.Last() - _removedRecordsCount;

                            //  Set the entries to the correct bounds
                            if (entryStart < 0)
                                entryStart = continuation.First();
                            if (entryEnd < entryStart)
                                entryEnd = continuation.Last();

                            var indexStart = manager.blocks.GetEntryIndex(entryStart);
                            var indexEnd = manager.blocks.GetEntryIndex(entryEnd);
                            var removeCount = (int)(entryEnd - entryStart) + 1;

                            long shiftAmount = (indexEnd.Position + indexEnd.Length) - indexStart.Position;

                            manager.indexer.Remove(entryStart, removeCount);

                            manager.blocks.RegisterEntry(shiftAmount * -1);

                            _removedRecordsCount += removeCount;
                            _removedDataSize += shiftAmount;
                        }
                    }
                }

                public override void Commit()
                {
                    if (LogTransaction)
                        System.Diagnostics.Debug.WriteLine("\t\t> Total removed records {0:n0}, size {1}", _removedRecordsCount, Common.Utils.FormatData(_removedDataSize));

                    base.Commit();

                    //  Post-Commit operation:
                    //  Here all removed records count and data size is commited to the data source

                    if (_defragmentOnCommit)
                    {
                        //  Realign all block entries properly, for all descending blocks.
                        //  The process ensures that entries are placed one to each other and no spaces between them.

                        // !!!! IMPORTANT .. CURRENTLY I HAVE IGNORED THE CURRENT BLOCK (currentBlock = block.Block+1), BUT THIS MAY CAUSE PROBLEMS  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        int lengthDefragmented = 0;
                        for (long currentBlock = 0; currentBlock < manager.blocks.Blocks; currentBlock++)
                        {
                            var opinfo = manager.DefragmentBlock(currentBlock, true, false, returnFullInfo: false);
                            if (LogTransaction && opinfo.EntriesAffectedCount > 0)
                                System.Diagnostics.Debug.WriteLine("\t\t\t> Block {0} Defragmented, {1}", currentBlock, opinfo);
                            lengthDefragmented += opinfo.EntriesAffectedCount;
                        }
                    }

                    manager.storage.IncreaseCount(_removedRecordsCount * -1);
                    manager.storage.IncreaseDataSize(_removedDataSize * -1);

                    /// DEBUG CHECK
                    /// Here the number of records in the storage has to match the number of indexes in the Indexer
                    if (manager.storage.RowCount != (long)manager.indexer.IndexCount)
                        throw new InvalidOperationException("Storage and Indexer records count missmatch");
                }

                /// <summary>
                /// Removes record from the data storage
                /// </summary>
                /// <param name="record"></param>
                public void Remove(long record)
                {
                    this.Queue(record);
                }
            }

            /// <summary>
            /// Transaction for inserting data records
            /// </summary>
            internal sealed class InsertTransation : ManagerTransaction<InsertRecordHeader>
            {
                private readonly int _queueSize;

                public InsertTransation(ContentManager operationManager)
                    : base(operationManager, operationManager.storage)
                {
                    // For the insert transation, this number is size of 3 row blocks
                    _queueSize = operationManager.blocks.EntryBlock * 3;
                }

                protected override int QueueSize
                {
                    get { return _queueSize; }
                }

                /// <summary>
                /// Splits the source by groups of continous values, which are alighed within same memory map view
                /// </summary>
                /// <typeparam name="T"></typeparam>
                /// <param name="headers"></param>
                /// <returns></returns>
                private IEnumerable<IEnumerable<InsertRecordHeader>> GetContinuationsWithinMapView(IEnumerable<InsertRecordHeader> headers)
                {
                    var continuations = GroupAdjacentBy(headers.OrderBy(header => header.Position),
                        (rh1, rh2) =>
                            rh1.Position + 1 == rh2.Position &&
                            manager.storage.MapViews.GetViewId(manager.indexer.GetPosition((long)rh1.Position + 1)) == manager.storage.MapViews.GetViewId(manager.indexer.GetPosition((long)rh2.Position)));
                    return continuations;
                }

                protected override void ProcessAccumulation(IEnumerable<InsertRecordHeader> queue)
                {
                    var viewManager = manager.storage.MapViews;

                    // 1. Order the records ascending.
                    // 2. Group them by row blocks.. 
                    // 3. Ensure the required row blocks buffers for each of the blocks
                    // 4. Determine the records continuations to insert them in chunks                   

                    //  GROUP HEADER BY "MAP VIEWS" ( headers which are aligned within same memory map view )

                    foreach (var mapView in queue.GroupBy(header => viewManager.GetViewId(manager.indexer.GetPosition((long)header.Position)), header => header, (key, group) => new { MapViewId = key, ViewHeaders = group }))
                    {
                        //  GROUP HEADERS BY "BLOCKS"
                        var blocks = mapView.ViewHeaders
                            .GroupBy(header => manager.blocks.GetBlock((long)header.Position), header => header, (key, group) =>
                                new
                                {
                                    Block = key,
                                    BlockHeaders = group,
                                    DataSize = (group.Sum(header => header.Length + manager.GetHeaderMetaSize(header))) // + Block.MinBufferSizeBytes) * 1
                                    /* Data size, temporary multiplied by 3. The correct approach is to
                                     * calculate how many bytes the record-meta header will take considering the actually record lengths
                                     */
                                });

                        //  PROCESS THE BLOCKS HEADERS
                        foreach (var block in blocks)
                        {
                            if (LogTransaction)
                                System.Diagnostics.Debug.WriteLine("\r\n\t\t> MView {2}, Block {0}, {1} headers, process start.", block.Block, block.BlockHeaders.Count(), mapView.MapViewId);

                            var blockHasEntries = manager.blocks.HasEntries(block.Block);

                            long insertionBlock = Math.Max(0, blockHasEntries ? block.Block : block.Block - 1);
                            var freeSpace = manager.blocks.GetBlockSpaceFree(insertionBlock);
                            if (freeSpace < block.DataSize + Block.MinBufferSizeBytes)
                            {
                                var opinf = manager.EnsureBlockCapacity(insertionBlock, block.DataSize + Block.MinBufferSizeBytes);

                                // double check the space
                                freeSpace = manager.blocks.GetBlockSpaceFree(insertionBlock);
                                if (LogTransaction)
                                    System.Diagnostics.Debug.WriteLine("\t\t\t> Ensuring capacity block {0}, currentRecords {1}, currentFree {2}  [{3}]", insertionBlock, manager.storage.RowCount, freeSpace, opinf);
                            }

                            //  Spit the block headers into continuations
                            foreach (var continuation in this.GetContinuationsWithinMapView(block.BlockHeaders))
                            {
                                //  Calculate the continuation size only 
                                long continuationDataSize = continuation.Sum(header => header.Length + manager.GetHeaderMetaSize(header)); // Optimiaion: re-organize the continuations and blocks so the calculations will be done only once

                                // ###################################################################################################################
                                //      At this point, there is enougth room in the block for insertion of all its records. 
                                //      Considering the records MUST BE continued, first insert their indexes into the array.
                                //      Move all of the descendant entries from this block, pushing them for the amount of space requested.
                                //      The operation has the goal to execute the descendant block movement only once, per block
                                //      Position the writer to the first index (insertion position) *before* rewriting descendant blocks
                                //      Rewrite all block entries, shifting them to make room for all records processed in this block.

                                long entryStart = (long)continuation.First().Position;
                                long entryEnd = (long)continuation.Last().Position;
                                // 
                                //      DETERMINEE BLOCK INSERTION / WRITER POSITION !
                                //      Position the writer to the first start entry index only once
                                //      and once the block is processed, the storage.Complate will reset the insertion state and the position.

                                //      RANGE CHECK: If the start entry is larger then the current index count, make it to be the maxumum allowed, which should be the last index
                                if (entryStart > (long)manager.indexer.IndexCount)
                                    throw new InvalidOperationException("Insert position out of bounds of the data storage");

                                var index = manager.storage.SetWriterPosition(entryStart);
                                manager.storage._insertionInProgress = true;    // <<< Indicate insertion start

                                if (LogTransaction)
                                {
                                    System.Diagnostics.Debug.WriteLine("\t\t\t  - start entry {0}, index [{1}], writer {2}", entryStart, index, manager.storage._writerPosition);
                                    System.Diagnostics.Debug.WriteLine("\t\t\t  - rewriting block {0} @ {1}, for continuation with {2} bytes", block.Block, entryStart, continuationDataSize);
                                }

                                // ########################################################################################################
                                //    Relocate the descendant entries to make free insersion room for the continuation:
                                //    First: ensure that the continuation will not span two map views. 
                                //    In case it does, split the continuation in two parts. Each of them has to fit on the two map views.
                                //    Logic: 
                                //    - check if the continuation will overflow at all first. If not, process normal. If yes, apply the following logic.
                                //    - determine the last entryEnd which can be repositioned (right shifted), being on the single view, without overflowing to next view.
                                //    - rewrite descendant for that entry range, then rewrite descendants for the remaining entry range.
                                //    Finally: rewrite block descendants in single or two steps, regards whether the continuation can fit in single or multiple map views.


                                manager.RewriteBlockDescendant(block.Block, entryStart, (int)continuationDataSize); //  Move the desdendant for this "continuation"

                                /*
                                bool willFitInSameMapView = true;
                                {
                                    // var indexEnd = manager.blocks.GetEntryIndex(entryEnd);
                                    // long endPosition = indexEnd.Position + indexEnd.Position + continuationDataSize;

                                    long startPosition = manager.indexer.GetPosition(entryStart) + continuationDataSize;
                                    long endPosition = manager.indexer.GetPosition(entryEnd) + continuationDataSize;
                                    willFitInSameMapView = viewManager.GetViewId(startPosition) == viewManager.GetViewId(endPosition);
                                }

                                if (willFitInSameMapView)   // relocate descendant normal
                                    manager.RewriteBlockDescendant(block.Block, entryStart, (int)continuationDataSize); //  Move the desdendant for this "continuation"
                                else
                                {
                                    //  Need to run two relocations on splited continuation (read above Logic description), in *Backwards* order.
                                    //  Split logic: determine the last entryEnd which can be repositioned (right shifted), being on the single view, without overflowing to next view.

                                    long lastViewId = -1;
                                    foreach (var header in continuation)
                                    {
                                        var viewId = viewManager.GetViewId(manager.indexer.GetPosition((long)header.Position) + continuationDataSize);
                                        if (viewId > lastViewId && lastViewId > -1)
                                        {
                                            // Split found. This entry will overflow on next view. break and use the last narrowed entryEnd.

                                            // Perform the first partial descendant rewrite, using the remaining range first (backwards order). 
                                            // Use the current entry as entryStart and continuation last entry as entryEnd.
                                            manager.RewriteBlockDescendant(block: block.Block, startEntry: (long)header.Position, writeOffset: (int)continuationDataSize, endEntry: (long)continuation.Last().Position);

                                            // Then perform the second partial descendant rewrite, using current entryStart and entryEnd.
                                            manager.RewriteBlockDescendant(block: block.Block, startEntry: entryStart, writeOffset: (int)continuationDataSize, endEntry: entryEnd);

                                            break;
                                        }

                                        // narrow the entryEnd to current entry.
                                        entryEnd = (long)header.Position;
                                        lastViewId = viewId;
                                    }
                                }
                                */

                                if (LogTransaction)
                                {
                                    index = manager.blocks.GetEntryIndex(entryStart);
                                    System.Diagnostics.Debug.WriteLine("\t\t\t  - index after rewrite [{0}]", index);
                                }

                                // ###################################################################################################################

                                manager.indexer.Allocate(entryStart, continuation.Count());

                                foreach (var header in continuation)
                                    manager.InsertRecord(header);
                            }

                            //  Realign all block entries properly, for all descending blocks.
                            //  The process ensures that, entries are placed one to each other and no spaces between them.

                            // !!!! IMPORTANT .. CURRENTLY I HAVE IGNORED THE CURRENT BLOCK (currentBlock = block.Block+1), BUT THIS MAY CAUSE PROBLEMS  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                            int lengthDefragmented = 0;
                            for (long currentBlock = block.Block + (manager.blocks.Blocks > 1 ? 0 : 0); currentBlock < manager.blocks.Blocks; currentBlock++)
                            {
                                var opinfo = manager.DefragmentBlock(currentBlock, true, true, returnFullInfo: false);
                                if (LogTransaction && opinfo.EntriesAffectedCount > 0)
                                    System.Diagnostics.Debug.WriteLine("\t\t\t> Block {0} Defragmented, {1}", currentBlock, opinfo);
                                lengthDefragmented += opinfo.EntriesAffectedCount;
                            }

                            manager.storage.Complete(); /*  << End of block processing. Finishing processing of this block headers only */
                        }
                    }
                }

                public void Insert(InsertRecordHeader header)
                {
                    this.Queue(header);
                }
            }


            /// <summary>
            /// Transaction for extending table columns
            /// </summary>
            internal sealed class ExtendTransation : ManagerTransaction<long>
            {
                private readonly int _queueSize;
                readonly int _columnsCountToExtend;                

                public ExtendTransation(ContentManager manager, int columnsCountToExtend)
                    : base(manager, manager.storage)
                {
                    // NOTE: 
                    //          This is special kind of transation. It does not require adding items in the queue.
                    //          It uses the records from the entire table in the process so we can the protected ProcessAcumulation base class method to manually execute the entire table for processing. 
                    //          Also another thing is that we initially use preparation step which sets the number of columns to extended number too

                    _columnsCountToExtend = columnsCountToExtend;
                    _queueSize = (int)this.manager.blocks.DataEntries;

                    //  Set the new extended number of columns in the target table
                    manager.storage.SetColumnCount(manager.storage.ColumnCount + columnsCountToExtend);

                    //  Manually invoke the managed transation ProcessAcumulation method
                    this.ProcessAccumulation(Enumerable.Range(0, (int)manager.storage.RowCount).Select(record => (long)record));
                }

                protected override int QueueSize
                {
                    get { return _queueSize; }
                }

                protected unsafe override void ProcessAccumulation(IEnumerable<long> queue)
                {
                    var blocks = manager.blocks;
                    var storage = manager.storage;
                    long blockPrev = -1;

                    /*  APPROACH    ****************************************************/
                    //
                    //      1. The record header has to be modified to fit the meta data for the new extended N columns, which is 0 bytes by default
                    //      2. Move all block descendant entries with N bytes, which is the size of the column offset into the header
                    //      3. Move the record content data by N bytes too.
                    //      4. Append the columns offsets meta data into the record header
                    //
                    /*******************************************************************/

                    foreach (var record in queue)
                    {
                        var block = blocks.GetBlock(record);
                        if (block > blockPrev)
                        {
                            // Ensure capacity for all column offsets for all block records

                            var opinfo = manager.EnsureBlockCapacity(block, blocks.EntryBlock * _columnsCountToExtend);
                            if (/*currentInTransation && */ opinfo.BlocksAffected > 0)
                            {
                                // double check for the actual free space
                                long actuallyFree = blocks.GetBlockSpaceFree(block);
                                System.Diagnostics.Debug.WriteLine("\t\t\t  WARN! Re-Ensuring capacity block {0}, currentRecords {1}, currentFree {2}  [{3}]", block, storage.RowCount, actuallyFree, opinfo);
                            }

                            blockPrev = block;
                        }

                        manager.RewriteBlockDescendant(block, record + 1, _columnsCountToExtend);

                        int length = 0, lengthHeader = 0, lengthData;

                        byte[] entryDataOrig = manager.GetEntryData(record, out length);
                        byte[] entryDataNew = new byte[length + _columnsCountToExtend];

                        manager.GetEntryMetaLengths(entryDataOrig, out lengthHeader, out lengthData, storage.ColumnCount - _columnsCountToExtend, length);

                        System.Diagnostics.Debug.Assert(length == lengthData + lengthHeader);

                        //  The lengthHeader here, contains the original header length, which will be used as starting point to rewrite the record content data
                        //  Move the record content data by one byte and append the new column offset into the header

                        Buffer.BlockCopy(entryDataOrig, 0, entryDataNew, 0, lengthHeader);   // Copy the header first
                        Buffer.BlockCopy(entryDataOrig, lengthHeader, entryDataNew, lengthHeader + _columnsCountToExtend, lengthData);  // Copy the record content next, shifting it by N bytes

                        //  Alter the header-length meta with the new value (HybridFixed format only)
                        fixed (byte* buffer = entryDataNew)
                            Native.Utils.NumberToBytes(buffer, lengthHeader + _columnsCountToExtend, 2);

                        manager.WriteEntry(record, entryDataNew, entryDataNew.Length, 0); // The -1 is for setting this record back to the original position
                    }


                    //  Finalize by registering the new data size in the blocks manager, one byte for each extended column for each record
                    //
                    blocks.RegisterEntry(storage.RowCount * _columnsCountToExtend);
                    storage.IncreaseDataSize(storage.RowCount * _columnsCountToExtend);
                }
            }

            /// <summary>
            /// Transaction for modifying data records
            /// </summary>
            internal sealed class ModifyTransation : ManagerTransaction<ModifyTransation.Cell>
            {
                readonly int _queueSize;

                public ModifyTransation(ContentManager manager)
                    : base(manager, manager.storage)
                {
                    // For the update transation, this number is size of 3 blocka and the number of columns - aka, Cells count
                    _queueSize = (manager.blocks.EntryBlock * 3) * manager.storage.ColumnCount;
                }

                public void Modify(long record, int column, int currentValueLength, byte[] valueNew)
                {
                    this.Queue(new Cell(record, column, currentValueLength, valueNew));
                }

                protected override int QueueSize
                {
                    get { return _queueSize; }
                }

                /// <summary>
                /// Gets the modification operation summary info
                /// </summary>
                public CapacityOperationInfo OperationInfo { get; private set; }

                protected override void ProcessAccumulation(IEnumerable<Cell> queue)
                {
                    // 1. Group the cells by records first. 
                    // 2. Group those records by row-blocks.
                    // 3. Ensure thre is enougth row-block buffer size of each of the blocks. 
                    // 4. Determine the records continuations and update the complete rows

                    var viewManager = manager.storage.MapViews;

                    // Group by blocks first
                    var blocks = queue.GroupBy(cell => manager.blocks.GetBlock(cell.Record), cell => cell,
                        (key, group) =>
                            new
                            {
                                Block = key,
                                // BlockUsedSize = manager.blocks.GetBlockSpaceUsed(key),
                                BlockCells = group,

                                DataSize = group.Sum(cell => (cell.Value.Length - cell.CurrentValueLength) + 0)  // << +2 is hard coded, for 2 bytes per cell header length ( [new calc cell len size] - [curr calc cell len size] )
                                                                                                                 // Data size is calculated by the new and old values lengths differences, 
                                                                                                                 // plus the differences in their data lengths for the record header. 
                                                                                                                 // If the record header is configured to use static lengths, this value is always 1 bytes (which limits the field value to be longer then 256 bytes)
                                                                                                                 // update: we set +2 bytes per cell, temporary. (for the header). The correct apporach is to calculate the actual header size, from the value, as mentioned bellow.

                                // Old version: 
                                // DataSize = group.Sum(cell => cell.Value.Length + 1)
                                //  Addition of 1 byte per cell for the record header. 
                                //  Note: The correct approach is to calculate the len of the fields from its value. it can be longer then 256 bytes
                            });

                    // Group by rows next
                    foreach (var block in blocks)
                    {
                        var info = manager.EnsureBlockCapacity(block.Block, block.DataSize);

                        //  Accumulate the operation capacity info
                        OperationInfo = new CapacityOperationInfo(OperationInfo.BlocksAffected + info.BlocksAffected, OperationInfo.SizeReorganized + info.SizeReorganized, OperationInfo.SizeUpdated + info.SizeUpdated);

                        if (LogTransaction && info.BlocksAffected > 0)
                            System.Diagnostics.Debug.WriteLine("UPDATE-TRANSACTION: Ensuring block capacity for block {0}, size {1}: {2}", block.Block, block.DataSize, info);

                        var blockRecords = block.BlockCells.GroupBy(cell => cell.Record, cell => cell,
                        (key, group) =>
                            new
                            {
                                Record = key,
                                RowCells = group
                            });


                        // Determine if the block records belongs to 2 different map views. If so, process them grouped by views they belongs to
                        bool withinSameMapView = viewManager.GetViewId(manager.indexer.GetPosition(blockRecords.First().Record)) == viewManager.GetViewId(manager.indexer.GetPosition(blockRecords.Last().Record));

                        if (!withinSameMapView)
                        {
                            //  Final group: Group by records within single mapped view (View Records Group). 
                            //  This prevents blocks re-writes of records which spans 2 mapped views

                            //  Used to determine blocks who are spaning between 2 map views
                            var blockViewRecords = blockRecords.GroupBy(record => viewManager.GetViewId(manager.indexer.GetPosition(record.Record)), record => record,
                                (key, group) =>
                                    new
                                    {
                                        MapViewId = key,
                                        Records = group
                                    });

#if DEBUG
                            if (blockViewRecords.Count() > 1)
                                if (!System.Diagnostics.Debugger.IsAttached)
                                    System.Diagnostics.Debugger.Launch();
#endif

                            foreach (var records in blockViewRecords)
                            {
                                foreach (var record in records.Records)
                                {
                                    var values = record.RowCells.ToDictionary(cell => cell.Column, cell => cell.Value);

                                    manager.UpdateBlockRecordValues(block.Block, record.Record, values);
                                }
                            }
                        }
                        else
                        {
                            //  Records are aligned within the same map view, so process them normally

                            //  Buffer containing the updated records entries
                            var entriesValues = new Dictionary<long, byte[]>();
                            foreach (var record in blockRecords)
                            {
                                var values = record.RowCells.ToDictionary(cell => cell.Column, cell => cell.Value);
                                manager.UpdateBlockRecordValues(record.Record, values, ref entriesValues);
                            }

                            manager.RewriteBlock(block.Block, entriesValues, ignoreReductionsRewrites: true);

                            //  Force block defragmentation
                            var opinfo = manager.DefragmentBlock(block.Block, true, true, returnFullInfo: true);
                            if (LogTransaction && opinfo.EntriesAffectedCount > 0)
                                System.Diagnostics.Debug.WriteLine("\t\t\t> Block {0} Defragmented, {1}", block.Block, opinfo);
                        }

                        // Sets the participating columns sort indexes to dirty
                        //manager.  records.SelectMany(e=>e.RowCells.Select(c=>c.Column)).Distinct().ToArray()                        
                    }

                }

                /// <summary>
                /// Splits the source by groups of continous values
                /// </summary>
                /// <typeparam name="T"></typeparam>
                /// <param name="headers"></param>
                /// <returns></returns>
                private IEnumerable<IEnumerable<InsertRecordHeader>> GetContinuations(IEnumerable<InsertRecordHeader> headers)
                {
                    var continuations = GroupAdjacentBy<InsertRecordHeader>(headers.OrderBy(header => header.Position), (rh1, rh2) => rh1.Position + 1 == rh2.Position);
                    return continuations;
                }

                internal struct Cell
                {
                    public readonly long Record;
                    public readonly int Column;
                    public readonly byte[] Value;
                    public readonly int CurrentValueLength;

                    internal Cell(long record, int column, int currentValueLength, byte[] value)
                    {
                        Record = record;
                        Column = column;
                        Value = value;
                        CurrentValueLength = currentValueLength;
                    }
                }
            }

        }
    }
}
