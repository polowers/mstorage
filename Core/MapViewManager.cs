﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Management;
using System.Runtime;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Win32.SafeHandles;
using DigitalToolworks.Common;
using DigitalToolworks.Common.Debug;
using System.Runtime.CompilerServices;
using System.Collections.Concurrent;

namespace DigitalToolworks.Data
{
    public class MapViewManager : IDisposable
    {
        private enum ReleaseMode { OldestAccessedView, LastView, FarestView }

        /// <summary>
        /// Partial views mode, is mode where the views are allocated to take the view size specified or multiplication of it. 
        /// Otherwise, full views mode is when each view is reserved for allocation size equal to maximum map capacity
        /// </summary>
        private bool PartialViewsMode = true;
        private static bool FilePerViewMode = true;
     
        private readonly MTableCore _table;

        private readonly Dictionary<string, FileStream> _fileStreams = new Dictionary<string, FileStream>();
        private readonly MemoryMappedFile _mapFile;
        private readonly string _mapName;
        private readonly string _mapFileName;

        /// <summary>
        /// Readonly variable for fast access, performance critical, same as the Configuration.ViewSize
        /// </summary>
        private readonly long _viewSize;
        private readonly long _mapCapacity;

        private readonly Dictionary<long, View> _activeViews = new Dictionary<long, View>();

        /// <summary>
        /// List of view id and associated physical filename
        /// </summary>
        private readonly Dictionary<long, string> _activeViewsFiles = new Dictionary<long, string>();
        private readonly List<long> _lockedViews = new List<long>();
        private readonly SortedList<long, DateTime> _passiveViews = new SortedList<long, DateTime>();

        private readonly ReleaseMode _releaseMode = ReleaseMode.LastView;
        private readonly FileStream _mapFileStream;
        private readonly ReaderWriterLockSlim _viewLocker = new ReaderWriterLockSlim();
        private readonly object _debugDumpLocker = new object();

        private long _currentViewId = long.MinValue;
        private long _lockedViewId = long.MinValue;
        private long _currentClosingViewId = long.MinValue;

        /// <summary>
        /// Callback to be called when map view is about to be modified
        /// </summary>
        private Action<View, FileInfo> _callbackOnBeginModification;

        public static readonly MemoryInfo Memory = new MemoryInfo();

        /// <summary>
        /// Global MViewManagers configuration. It applies to all instances of MViewManager running under the current process. <br />
        /// Must be configured before runtime.
        /// </summary>
        public static readonly ConfigInfo Configuration = new ConfigInfo();

        /// <summary>
        /// Whether the view manager will leave the map files on the disk, or it will delete them once the process terminates
        /// </summary>
        private readonly bool _persistMapFiles;

        public static bool MultipleViewReleaseMode = false;
      
        private unsafe View _currentView;
        private long _currentViewStart = long.MaxValue;
        private long _currentViewEnd = long.MaxValue;       

        private FileStream CreateMapFileStreamNative(string filename)
        {
            short INVALID_HANDLE_VALUE = -1;
            uint GENERIC_WRITE = 0x40000000;
            uint GENERIC_READ = 0x80000000;
            int CREATE_ALWAYS = 2;
            short FILE_ATTRIBUTE_NORMAL = 0x80;
            short FILE_ATTRIBUTE_TEMPORARY = 0x100;
            short FILE_SHARE_DELETE = 0x00000004;
            uint FILE_FLAG_DELETE_ON_CLOSE = 0x04000000;

            var handle = new SafeFileHandle(Native.CreateFile(filename, (int)GENERIC_WRITE | (int)GENERIC_READ, 0, IntPtr.Zero, CREATE_ALWAYS, (0x04000000) | (0x100), IntPtr.Zero), true);

            if (handle.IsInvalid)
                Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());

            return new FileStream(handle, FileAccess.ReadWrite);
        }

        /// <summary>
        /// Gets file stream if it is already created and opened, for the specified filename. 
        /// If the file stream is not found, it creates and opens file stream which will be associated with map view. 
        /// Note: Existing files will be overwriten!
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private FileStream GetOrCreateMapFStream(string filename)
        {
            FileStream fstream;

            if (!_fileStreams.TryGetValue(Path.GetFileName(filename), out fstream))
            {
                fstream = new FileStream(filename,
                    FileMode.Create,
                    FileSystemRights.FullControl, //   FileSystemRights.Read | FileSystemRights.Write | FileSystemRights.Delete,
                    FileShare.Delete,
                    1024 * 1024 * 4,
                    _persistMapFiles ? FileOptions.WriteThrough : FileOptions.WriteThrough | FileOptions.DeleteOnClose,
                    null);

                //  Change the attributes to temporary. Mark the file for deletion explicitly: https://msdn.microsoft.com/en-us/library/aa365539%28v=vs.85%29.aspx
                //  This is used to change or override the FileOpen FileOptions.DeleteOnClose option flag.
                //Native.SetFileAsTemporary(fstream.SafeFileHandle, isTemporary: true);

                _fileStreams.Add(Path.GetFileName(filename), fstream);
            }

            return fstream;
        }

        /// <summary>
        /// Provide delegate which will be called when content of map view is about to be modified for first time after its been opened.  <br />It occurs before the first Write() operation on a view.
        /// </summary>
        /// <param name="callback"></param>
        public void OnBeginViewModification(Action<View, FileInfo> callback)
        {
            _callbackOnBeginModification = callback;
        }

        /// <summary>
        /// Global MapViewManager configuration
        /// </summary>
        public sealed class ConfigInfo
        {
            private bool _initialized;
            private long _viewSize;
            private long _viewBufferSize;
            private long _memoryMargin;

            internal ConfigInfo()
            {
                /// Default values
                ViewSize = 1024L * 1024 * 256L;
                MemoryMargin = 1024L * 1024 * 128L;
                LockViews = true;
            }

            /// <summary>
            /// Whether the manager will lock the memory allocated for views (default: yes)
            /// </summary>
            public bool LockViews { get; set; }

            internal bool Initialized { get { return _initialized; } }

            /// <summary>
            /// Gets or sets the size of single memory mapped view. The size will be rounded to the nearest system granularity
            /// </summary>
            public long ViewSize 
            {
                get { return _viewSize; } 
                set
                {
                    if (value != ViewSize)
                    {
                        if (_initialized)
                            throw new InvalidOperationException("Unable to change the ViewSize, once the configuration has been initialized");

                        // Calculate the actial View size
                        _viewSize = Native.Utils.GetNearestSizeToAllocationGranularity(value);

                        // Calculates the buffer size
                        _viewBufferSize = Math.Max(1024L * 1024L * 0L, (ViewSize / 100) * 10);
                    }
                }
            }

            /// <summary>
            /// Gets the view buffer size used after the end of the each view data
            /// </summary>
            public long ViewBufferSize
            {
                get { return _viewBufferSize; }
            }

            /// <summary>
            /// Memory size which is used as upper bound or the system memory, avaiable for allocation by the view manager. 
            /// When the total size of allocated views reach this bound, the manager will start release allocated memory mapped views. 
            /// Amount of available memory, at which to start reclaiming views
            /// </summary>
            public long MemoryMargin
            {
                get { return _memoryMargin; }
                set
                {
                    if (value != MemoryMargin)
                    {
                        if (_initialized)
                            throw new InvalidOperationException("Unable to change the MemoryMargin, once the configuration has been initialized");

                        _memoryMargin = Math.Max(0, value > 0 ? value : ConfigInfo.CalculateMemoryMargin(64));
                    }
                }
            }

            internal void Initialize(bool reinitialize = false)
            {
                if (!_initialized || reinitialize)
                {
                    // Ensure the process memory working set amount

                    this.EnsureWorkingSetSize();

                    Utils.ConsoleWriteLine("{0}:: Initialized:  ViewSize={1}, MemoryMargin={2}", "MViewManager.Configuration", Utils.FormatData(this.ViewSize), Utils.FormatData(this.MemoryMargin));

                    _initialized = true;
                }
            }

            /// <summary>
            /// Ensures that the process memory working set size is set to the needed amount
            /// </summary>
            private void EnsureWorkingSetSize()
            {
                long worksetSize = uint.MaxValue;
                var sw = System.Diagnostics.Stopwatch.StartNew();
                do
                {
                    worksetSize += (this.ViewSize); // Environment.SystemPageSize * 1000;
                }
                while (Native.SetProcessWorkingSetSize(Process.GetCurrentProcess().Handle, worksetSize, worksetSize) && worksetSize < (AvailableViewMemory(false) + (this.ViewSize * 1))); // remove the * 4, it is temporary

                sw.Stop();
                Utils.ConsoleWriteLine("{0}:: Ensure Working Set:  MaxWorkingSet={1}, {2:n0}ms", "MViewManager.Configuration", Utils.FormatData(worksetSize), sw.ElapsedMilliseconds);
            }

            /// <summary>
            /// Determines the minimum amount of memory margin
            /// </summary>
            /// <param name="fixedMegabytes"></param>
            /// <returns></returns>
            private static long CalculateMemoryMargin(int fixedMegabytes)
            {
                Memory.Calculate();

                //MaxRamAvailable = MaxRamAvailable - (INT(TotalRAM / 32) + 65536) 
                long memoryMargin = (Memory.Total / 32) + (fixedMegabytes * 1024 * 1024);
                return memoryMargin;
            }

            /// <summary>
            /// Memory available for allocating mapped views, considering the memory margin setting
            /// </summary>
            internal long AvailableViewMemory(bool passiveViewsPresent)
            {
                /*
                    If Stream HAS NONE On-Disk Views AND Free RAM > (Margin + ViewSize) Then
                        View is In-RAM
                    If Stream DOES HAVE On-Disk Views AND Free RAM > (Margin + ViewSize + 128MB) Then
                        View is On-Disk
                     End If
                 */

                Memory.Calculate();

                var availableMemory = Memory.Free - MemoryMargin;

                if (passiveViewsPresent)
                    return availableMemory - (128L * 1024L * 1024L);

                return availableMemory;
            }
        }

        internal MapViewManager(MTableCore table, string backingMapFile, long mapCapacity, bool persistMapFiles)
        {
            _table = table;

            _persistMapFiles = persistMapFiles;
            _mapName = "mapping_" + Thread.CurrentThread.ManagedThreadId + "_" + Guid.NewGuid().ToString("N");
            _mapFileName = backingMapFile;
            _mapCapacity = Native.Utils.GetNearestSizeToAllocationGranularity(mapCapacity);

            MapViewManager.Configuration.Initialize();

            _viewSize = MapViewManager.Configuration.ViewSize;

            if (!FilePerViewMode)
            {
                if (Configuration.ViewSize + Configuration.ViewBufferSize > _mapCapacity)
                    throw new InvalidOperationException(string.Format(this + ": The map capacity ({0}) not large enougth. It should be at least twice larger then the map view size ({1})", _mapCapacity, Configuration.ViewSize));

                _mapFileStream = this.GetOrCreateMapFStream(backingMapFile);
                _mapFile = MemoryMappedFile.CreateFromFile(_mapFileStream, _mapName, _mapCapacity, MemoryMappedFileAccess.ReadWrite, null, HandleInheritability.None, false);
            }

            if (DebugForm.Instance.Enabled)
                Task.Run(() =>
                    {
                        long currentViewId = long.MinValue;

                        while (DebugForm.Instance.Enabled)
                        {
                            if (_currentViewId != currentViewId && _currentClosingViewId < 0)
                            {
                                DumpViewsToDebugWindow();

                                currentViewId = _currentViewId;
                            }

                            Thread.Sleep(100);
                        }
                    });

            System.Diagnostics.Trace.WriteLine(string.Format("{0}: Created:: Views={1}, MemoryLimitMargin={2}", this, _activeViews.Count, Utils.FormatData(Configuration.MemoryMargin)));
        }


        /// <summary>
        /// Reads data from the memory mapped file
        /// </summary>
        /// <param name="buffer">Buffer to read data into</param>
        /// <param name="position">Position within the memory mapped file, to start reading at</param>
        /// <param name="length">Lenght of data to read</param>
        /// <returns>Data actually readen</returns>
        public int Read(byte[] buffer, long position, int length)
        {
            unsafe
            {
                Marshal.Copy(new IntPtr(this.Read(position)), buffer, 0, length);

                return length;
            }
        }

        /// <summary>
        /// Gets pointer to the memory mapped file block at the position, to read from
        /// </summary>
        /// <param name="position"></param>
        /// <returns>Unamanged Memory pointer</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe byte* Read(long position, bool threadLock = false)
        {
            //if (!PartialViewsMode && position + length > _mapCapacity)
            //    throw new InvalidOperationException("No capacity to read " + length + " bytes, at position " + position + " in the memory mapped file");

            var mapView = this.GetView(position, threadLock);

            return (mapView.Pointer + mapView.Position);

            //var pointer = new IntPtr(mapView.Pointer + mapView.Position);            
            //IntPtr.Add(new IntPtr(mapView.Pointer), (int)mapView.Position);
            //return pointer;
        }

        /// <summary>
        /// Writes data to the memory mapped file
        /// </summary>
        /// <param name="buffer">Buffer containing the data to write</param>
        /// <param name="position">Position within the memory mapped file, to start writing at</param>
        /// <param name="offset">The offset in the buffer where to start getting data to be writen</param>
        /// <param name="length">Length of data to write</param>        
        public void Write(byte[] buffer, int offset, long position, int length)
        {
            unsafe
            {
                fixed (byte* bufferPtr = buffer)
                {
                    Write(bufferPtr + offset, position, length);
                }
            }
        }

        /// <summary>
        /// Writes data to the memory mapped file
        /// </summary>
        /// <param name="buffer">Pointer to the buffer containing the data to write</param>
        /// <param name="position">Position within the memory mapped file, to start writing at</param>
        /// <param name="length">Length of data to write</param>        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void Write(byte* buffer, long position, int length, bool threadLock = true)
        {
            if (!PartialViewsMode && position + length > _mapCapacity)
                throw new InvalidOperationException(this + ": No capacity to write " + length + " bytes, at position " + position + " in the memory mapped file");

            // Get map view for the position and length, for Writing (data modification). 

            var mapView = this.GetView(position, threadLock);

            if(mapView.Position + length > mapView.Length)
                throw new InvalidOperationException(this + ": Attempt to write large amount of data, which exceeds map view maximum size. Partitioning the data is needed.\r\nWriting " + length + " bytes, at position " + position + "\r\n" + mapView);

            this.BeginWrite(mapView);          

            Native.CopyMemory(mapView.Pointer + mapView.Position, buffer, length);

            //  Sets the last write timestamp and counter. Used when closing this view file to manually update the file times.
            mapView.SetLastWrite();
        }

        public void WriteManaged(byte[] buffer, long position, int length, bool threadLock = true)
        {
            if (!PartialViewsMode && position + length > _mapCapacity)
                throw new InvalidOperationException(this + ": No capacity to write " + length + " bytes, at position " + position + " in the memory mapped file");

            // Get map view for the position and length. 
            var mapView = this.GetView(position, threadLock);

            this.BeginWrite(mapView);

            mapView.InternalWrite(buffer, length);
        }

        /// <summary>
        /// Call this before performing write operation on view
        /// </summary>
        /// <param name="view"></param>
        internal void BeginWrite(View view)
        {
            //  Called when map view is about to be modified for first time since it was last opened.
            //  Notify any registered subscriber to the modification event callback.

            if (_callbackOnBeginModification != null && !view.IsModified)
            {
                //  Get the associated file and pass it to the callback

                _callbackOnBeginModification(view, new FileInfo(_activeViewsFiles[view.Id]));
            }
        }

        /// <summary>
        /// Calculates the view ID from the data position passed
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal long GetViewId(long position)
        {
            var viewIndex = position / _viewSize;
            return viewIndex;
        }

        /// <summary>
        /// Gets the map view for the specified data range
        /// </summary>
        /// <param name="position">Position within the memory mapped file, the start of the data range. The end of data range is determined by the ViewAllocation size</param>
        /// <returns>Memory mapped view stream</returns>
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public View GetView(long position, bool threadLock)
        {
            //y = X-(X/n)*n     long dataPosition = position - (position / ViewSize) * ViewSize;         

            // If the max capacity is less then the view size, it means that only one view will be used
            // In that case, create or return the current view, bypassing the further logic, for performances

            if (_mapCapacity < _viewSize && _currentView != null)
            {
                _currentView.Position = position;
                return _currentView;
            }

            long dataPosition = position % _viewSize;

            if (_currentView != null && position >= _currentView.ViewPosition && position < _currentView.ViewPosition + _viewSize)
            {
                _currentView.Position = dataPosition;
                return _currentView;
            }

            //_currentViewStart = position;
            //_currentViewEnd = position + ViewSize;

            var viewIndex = position / _viewSize;

            if (_currentViewId == viewIndex)
            {
                _currentView.Position = dataPosition;
                return _currentView;
            }

            var viewPosition = viewIndex * _viewSize;

            // Monitor.Enter(_viewLocker);

            try
            {
                if (threadLock)
                    _viewLocker.EnterUpgradeableReadLock();

                // Create live map view or return existing

                View mapView = null;

                if (!_activeViews.TryGetValue(viewIndex, out mapView))
                {
                    if (threadLock)
                        _viewLocker.EnterWriteLock();

                    var elapsed = DateTime.UtcNow;

                    // This will ensure that there is enougth memory to handle this new map view.
                    long available = 0;
                    while ((available = Configuration.AvailableViewMemory(_passiveViews.Count > 0)) < Configuration.ViewSize)
                    {
                        if (!this.ReleaseExcessiveView(viewIndex, available))
                            break;
                    }

                    GC.AddMemoryPressure(_viewSize);

                    MemoryMappedFile mapFile;
                    FileStream fileStream = null;
                    if (FilePerViewMode)
                    {
                        fileStream = GetOrCreateMapFStream(_mapFileName + "." + viewIndex + ".data");
                        mapFile = MemoryMappedFile.CreateFromFile(fileStream, null, _viewSize + Configuration.ViewBufferSize, MemoryMappedFileAccess.ReadWrite, null, HandleInheritability.None, true);
                    }
                    else
                        mapFile = MemoryMappedFile.OpenExisting(_mapName);

                    if (!PartialViewsMode)
                    {
                        if (viewPosition + _viewSize + Configuration.ViewBufferSize > _mapCapacity)
                            throw new InvalidOperationException(this + ": Not enougth capacity in the map file to map memory view at position " + position + ".");

                        mapView = new View((int)viewIndex, viewPosition, fileStream != null ? fileStream.SafeFileHandle : null, mapFile, mapFile.CreateViewStream(), 0, _viewSize);
                    }
                    else
                    {
                        mapView = new View((int)viewIndex, viewPosition, fileStream != null ? fileStream.SafeFileHandle : null, mapFile, mapFile.CreateViewStream(FilePerViewMode ? 0 : viewPosition, _viewSize + Configuration.ViewBufferSize, MemoryMappedFileAccess.ReadWrite), 0, _viewSize);
                    }

                    //  add the associate view physical filename to the lookup list
                    if (fileStream != null)
                        _activeViewsFiles.Add(mapView.Id, fileStream.Name);

                    _activeViews.Add(viewIndex, mapView);
                    // if (!_activeViews.Add(viewIndex, mapView))
                    //     throw new InvalidOperationException("Unable to add view " + viewIndex);


                    _currentViewId = mapView.Id;

                    LockView(mapView.Id);

                    DumpViewsToDebugWindow();

                    Utils.ConsoleWriteLine(ConsoleColor.Green, this + ":  VM > {5}{6} >>  View {0} Opened in {4:n0}ms     [position @ {1} -> {2}, size={3}]\r\n",
                        viewIndex + 1, Utils.FormatData(viewPosition), Utils.FormatData(dataPosition), Utils.FormatData(_viewSize), (DateTime.UtcNow - elapsed).TotalMilliseconds, Utils.FormatData(available), _passiveViews.Count > 0 ? " incl.T" : " excl.T");

                    if (threadLock)
                        _viewLocker.ExitWriteLock();
                }

                // calculate and set the position within the map view
                if (!PartialViewsMode)
                    mapView.Position = viewPosition + dataPosition;
                else
                    mapView.Position = dataPosition;

                _currentViewId = mapView.Id;

                _currentView = mapView;

                return mapView;
            }
            finally
            {
                if (threadLock)
                    _viewLocker.ExitUpgradeableReadLock();
                // Monitor.Exit(_viewLocker);
            }
        }

        private void LockView(long viewIndex)
        {
            // Lock the views memory
            if (Configuration.LockViews && viewIndex != _lockedViewId)
            {
               // lock (_lockedViews)
                    if (!_lockedViews.Contains(viewIndex))
                    {
                        unsafe
                        {
                            // unlock the first view
                            while (_lockedViews.Count > 3)
                            {
                                var lockedView = _lockedViews.First();

                                if (!Native.VirtualUnlock((IntPtr)_activeViews[lockedView].Pointer, (UIntPtr)_viewSize))
                                    throw new Win32Exception();

                                _lockedViews.Remove(lockedView);
                            }

                            if (!Native.VirtualLock((IntPtr)_activeViews[viewIndex].Pointer, (UIntPtr)_viewSize))
                                throw new Win32Exception();

                            _lockedViews.Add(viewIndex);
                        }
                    }
            }
        }

        [Obsolete]
        private void _RestoreView(long position, MemoryMappedViewStream targetView)
        {
            using (var backedView = _mapFile.CreateViewStream(position, _viewSize, MemoryMappedFileAccess.Read))
            {
                TransferBytes(backedView, targetView, _viewSize);

                /*
                byte[] buffer = new byte[1024 * 1024];
                
                unsafe
                {
                    byte* p = null;
                    targetView.SafeMemoryMappedViewHandle.AcquirePointer(ref p);
                    fixed (byte* src = buffer)
                    {
                        for (int i = 0; i < targetView.Capacity; i += buffer.Length)
                        {
                            Native.NativeMethods.CopyMemory(p + i, src, buffer.Length);
                        }
                    }
                    targetView.SafeMemoryMappedViewHandle.ReleasePointer();
                    targetView.Flush();
                }*/

                /*
                {
                    int readen = 0;
                    while ((readen = backedView.Read(buffer, 0, buffer.Length)) > 0)
                        targetView.Write(buffer, 0, readen);
                }
                */
            }
        }

        [Obsolete]
        private void _StoreView(long position, MemoryMappedViewStream sourceView)
        {
            using (var backedView = _mapFile.CreateViewStream(position, _viewSize, MemoryMappedFileAccess.ReadWrite))
            {
                sourceView.Position = 0;

                TransferBytes(sourceView, backedView, _viewSize);

                /*
                byte[] buffer = new byte[1024 * 1024 * 1];

                while (sourceView.Position < sourceView.Capacity)
                {                   
                    ReadBytes(sourceView, buffer, (int)sourceView.Position, (int)buffer.Length);

                    WriteBytes(backedView, (int) sourceView.Position, buffer);

                    sourceView.Position += buffer.Length;
                }
                */

                // int readen = 0;
                // while ((readen = sourceView.Read(buffer, 0, buffer.Length)) > 0)
                //     backedView.Write(buffer, 0, readen);
            }
        }

        [Obsolete]
        private static unsafe int ReadBytes(MemoryMappedViewStream view, byte[] data, int offset, int num)
        {
            byte* ptr = (byte*)0;
            view.SafeMemoryMappedViewHandle.AcquirePointer(ref ptr);
            Marshal.Copy(IntPtr.Add(new IntPtr(ptr), offset), data, 0, num);
            view.SafeMemoryMappedViewHandle.ReleasePointer();
            return num;
        }

        [Obsolete]
        internal static unsafe void TransferBytes(MemoryMappedViewStream source, MemoryMappedViewStream target, long length)
        {
            byte[] buffer = new byte[1024 * 1024 * 1];
            long size = buffer.Length;
            long position = 0;

            byte* pSource = (byte*)0;
            byte* pTarget = (byte*)0;
            source.SafeMemoryMappedViewHandle.AcquirePointer(ref pSource);
            target.SafeMemoryMappedViewHandle.AcquirePointer(ref pTarget);

            while (position < length)
            {
                Native.CopyMemory(pTarget + position, pSource + position, (int)size);
                position += size;
            }

            target.SafeMemoryMappedViewHandle.ReleasePointer();
            source.SafeMemoryMappedViewHandle.ReleasePointer();
        }

        [Obsolete]
        private static unsafe void WriteBytes(MemoryMappedViewStream view, int offset, byte[] data)
        {
            byte* ptr = (byte*)0;
            view.SafeMemoryMappedViewHandle.AcquirePointer(ref ptr);
            Marshal.Copy(data, 0, IntPtr.Add(new IntPtr(ptr), offset), data.Length);
            view.SafeMemoryMappedViewHandle.ReleasePointer();
        }

        /// <summary>
        /// Dispose and reclaim memory for the map view, which is farest relative to this map view
        /// </summary>
        /// <param name="viewIndex">This map view</param>
        /// <returns>True if there are more live map views. Otherwise, false if no more live views exists</returns>
        private bool ReleaseExcessiveView(long viewIndex, long availableMemory)
        {
            // find the usage map view index, which has lowest value of all, different them this view index

            var releaseView = GetViewToRelease(viewIndex);
            if (long.MinValue.Equals(releaseView))
                return false;

            if (releaseView != viewIndex)
            {
                var view = _activeViews[releaseView];

                Utils.ConsoleWrite(ConsoleColor.Cyan, this + ":  VM > {2}{3} >>  Closing View {0} for View {1} ... ", view.Id + 1, viewIndex + 1, Utils.FormatData(availableMemory), _passiveViews.Count > 0 ? " incl.T" : " excl.T");

                CloseView(releaseView, flushToDisk: true);

                return _activeViews.Count > 0;
            }
            else
                throw new InvalidOperationException(this + ": Invalid attempt to close view " + (viewIndex + 1) + " which is opening");

        }

        private void CloseView(long viewIndex, bool flushToDisk)
        {
            // var elapsed = DateTime.UtcNow;

            var mapView = _activeViews[viewIndex];

            if (mapView != null)
            {
                _currentClosingViewId = viewIndex;

                DumpViewsToDebugWindow();

                if (Configuration.LockViews && _lockedViews.Contains(mapView.Id))
                    unsafe
                    {
                        if (!Native.VirtualUnlock((IntPtr)mapView.Pointer, (UIntPtr)_viewSize))
                            throw new Win32Exception();

                        _lockedViews.Remove(mapView.Id);
                    }

                if (_currentClosingViewId == _lockedViewId)
                    _lockedViewId = long.MinValue;

                Trace.WriteLine(this + ": View " + viewIndex + ":: Reclaiming " + mapView.Position + "...");

                GC.RemoveMemoryPressure(_viewSize);

            
                var fileStream = _fileStreams[Path.GetFileName(_activeViewsFiles[viewIndex])];

                if (flushToDisk)
                {
                    //  Remove the temporary behaviour from the underlaying file. 
                    //  This will allow automatic flushing on map view pages.

                    Native.SetFileAsTemporary(fileStream.SafeFileHandle, isTemporary: false);
                }
                else
                {
                    //  Mark the underlaying file as temporary and close its underlaying handle.
                    //  This should prevent the unmapping and disposial of the view from flusing the modification to disk.

                    mapView.Unmap();

                    fileStream.Flush(flushToDisk: false);
                    fileStream.Dispose();
                }

                ((IDisposable)mapView).Dispose();

                _activeViews.Remove(viewIndex);

                _activeViewsFiles.Remove(viewIndex);

                GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true);

                _currentClosingViewId = long.MinValue;

                _passiveViews[viewIndex] = DateTime.UtcNow;

                Trace.WriteLine(this + ": View " + viewIndex + ":: Reclaimed !");
            }
            else
                throw new InvalidOperationException(this + ": Invalid attempt to close view " + viewIndex + " which no longer exist");
        }

        internal void DebugOpenView(int viewIndex)
        {
            var view = GetView(viewIndex * _viewSize, true);
            System.Diagnostics.Debug.WriteLine(this + ": DebugOpenView(" + viewIndex + ")");
        }

        /// <summary>
        /// Gets the index of the view to release, having the viewIndex passed and one with highter importance to keep
        /// </summary>
        /// <param name="newIndex"></param>
        private long GetViewToRelease(long viewIndex)
        {
            long releaseViewIndex = 0;

            if (_releaseMode == ReleaseMode.OldestAccessedView)
            {
                // strategy for returning the oldest view. this is the view which has been first accessed
                long minTimestamp = long.MaxValue;

                /*
                for (int index = 0; index < __mapViewUsage.Length; index++)
                    if (index != viewIndex)
                    {
                        long usage = __mapViewUsage[index];

                        if (usage < minTimestamp && usage > 0)
                        {
                            minTimestamp = usage;
                            releaseViewIndex = index;
                        }
                    }
                */

                return releaseViewIndex;
            }

            if (_releaseMode == ReleaseMode.FarestView)
            {
                // strategy for releasing the view, which is the farest relative to this view

                // 1. get the index of the first and last opened views.

                var viewLeft = _activeViews.Values.FirstOrDefault();
                var viewRight = _activeViews.Values.LastOrDefault();

                if (viewLeft != null && viewRight != null)
                {
                    var indexLeft = viewLeft.Id;
                    var indexRight = viewRight.Id;

                    // 2. use this index to determine the farest of both ends to take for closing.
                    if (viewIndex >= (indexRight - indexLeft) / 2)
                        return releaseViewIndex = indexLeft;

                    if (viewIndex <= (indexRight - indexLeft) / 2)
                        return releaseViewIndex = indexRight;

                    throw new InvalidOperationException(this + ": Failed to determine the view to close");
                }
                else
                    throw new InvalidOperationException(this + ": No more open view to close");
            }

            if (_releaseMode == ReleaseMode.LastView)
            {
                // close the last view located in the data tail
                lock (_activeViews)
                {
                    var viewRight = _activeViews.Values.Where(view => view.Id != viewIndex).OrderBy(view => view.Id).LastOrDefault();

                    releaseViewIndex = viewRight != null ? viewRight.Id : long.MinValue;

                    return releaseViewIndex;
                }
            }

            throw new InvalidOperationException(this + ": No close view mode implemented");
        }

        /// <summary>
        /// Total of views currently opened
        /// </summary>
        public int OpenViewCount
        {
            get
            {
                return _activeViews.Count;
            }
        }

        /// <summary>
        /// Closes all opened views reclaiming memory
        /// </summary>
        /// <param name="flushToDisk">Modifications has to be flushed to disk before closing the view. Otherwise the modifications will be rejected and the view closed directly.</param>
        public void ReleaseViews(bool flushToDisk = true)
        {
            _viewLocker.EnterWriteLock();

            try
            {
                foreach (var view in _activeViews.Keys.ToArray())
                    CloseView(view, flushToDisk);

                this._currentClosingViewId = long.MinValue;
                this._currentViewId = long.MinValue;
                this._lockedViewId = long.MinValue;
                this._currentViewEnd = long.MaxValue;
                this._currentViewStart = long.MaxValue;
                this._currentView = null;
            }
            finally
            {
                _viewLocker.ExitWriteLock();
            }            
        }

        void IDisposable.Dispose()
        {
            ReleaseViews();

            if (_mapFile != null)
                _mapFile.Dispose();

            if (_mapFileStream != null)
                _mapFileStream.Dispose();

            foreach (var stream in _fileStreams.Values)
            {
                try
                {
                    stream.Flush(true);
                    stream.Dispose();
                }
                catch (Exception)
                {
 
                }
            }

            _fileStreams.Clear();
        }

        public override string ToString()
        {
            return string.Format("MViewManager [{0}]", _table.Name);
        }

        public sealed class MemoryInfo
        {
            private readonly object _locker = new object();

            private long _total;
            private long _free;
            private long _used;
            private int _usage;

            internal MemoryInfo()
            {
                Task.Run(() =>
                    {
                        Calculate();
                        Thread.Sleep(3000);
                    }).ContinueWith(task => { });
            }

            public void Calculate()
            {
                lock (_locker)
                {
                    using (ManagementObject osInfo = new ManagementObject("Win32_OperatingSystem=@"))
                    {
                        _total = (long)((ulong)osInfo["TotalVisibleMemorySize"] * 1024);
                        _free = (long)((ulong)osInfo["FreePhysicalMemory"] * 1024);

                        // reduce the free by 2 percent.

                        var internallyUsed = (_total - _free) * 1.02d - (_total - _free);

                        _free = (long)(_free - internallyUsed);

                        _used = _total - _free;

                        _usage = (int)((_used / (double)_total) * 100);
                    }
                }
            }

            public long Total
            {
                get
                {
                    lock (_locker)
                        return _total;
                }
            }

            public long Free
            {
                get
                {
                    lock (_locker)
                        return _free;
                }
            }

            public long Used
            {
                get
                {
                    lock (_locker)
                        return _used;
                }
            }

            public long Usage
            {
                get
                {
                    lock (_locker)
                        return _usage;
                }
            }

            public override string ToString()
            {
                return string.Format("Total={0}, Free={1}, Used={2}", Utils.FormatData(this.Total), Utils.FormatData(this.Free), Utils.FormatData(this.Used));
            }
        }

        public sealed unsafe class View : IDisposable
        {
            public readonly long ViewPosition;
            public readonly int Id;
            public long Position;
            public readonly long Length;
            public readonly byte* Pointer;

            readonly MemoryMappedViewStream _mapView;
            readonly MemoryMappedFile _mapFile;
            readonly SafeFileHandle _fileHandle;
            readonly long _viewSize;
            long _lastWrite;
            /// <summary>
            /// Total number of writes
            /// </summary>
            long _writeCount;

            public View(int id, long viewPosition, SafeFileHandle fileHandle, MemoryMappedFile mapFile, MemoryMappedViewStream mapView, long position, long viewSize)
            {
                this.Id = id;
                this.ViewPosition = viewPosition;
                this.Length = mapView.Capacity;
                this._mapFile = mapFile;
                this._mapView = mapView;
                this._viewSize = viewSize;
                this._fileHandle = fileHandle;

                //GC.AddMemoryPressure((long)mapViewHandle.SafeMemoryMappedViewHandle.ByteLength);

                byte* pointer = null;
                mapView.SafeMemoryMappedViewHandle.AcquirePointer(ref pointer);
                this.Pointer = pointer;
                this.Position = position;

                //if (MapViewManager.LockViews)
                //    if (!Native.NativeMethods.VirtualLock((IntPtr)pointer, (UIntPtr)viewSize))
                //        throw new Win32Exception();

            }

            private void Flush()
            {
                int repeat = 100;
                while (repeat-- > 0)
                {
                    byte* p = null;
                    _mapView.SafeMemoryMappedViewHandle.AcquirePointer(ref p);
                    try
                    {
                        if (!Native.FlushViewOfFile(p, (IntPtr)(this.Length)))
                            throw new Win32Exception();

                        return;
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Trace.Write(this + ": " + e.Message);
                    }
                    finally
                    {
                        _mapView.SafeMemoryMappedViewHandle.ReleasePointer();
                    }


                    Thread.Sleep(2000);
                }
            }

            /// <summary>
            /// Force the active map view to flush pending changes to disk
            /// </summary>
            internal void InternalFlush()
            {
                byte* p = null;
                _mapView.SafeMemoryMappedViewHandle.AcquirePointer(ref p);
                try
                {
                    if (!Native.FlushViewOfFile(p, (IntPtr)0))
                        throw new Win32Exception();
                }
                catch (Win32Exception e)
                {
                    System.Diagnostics.Trace.Write(this + ": " + e.Message + "\r\n");
                    if (e.NativeErrorCode.Equals(33))
                    {
                       
                    }
                }
                finally
                {
                    _mapView.SafeMemoryMappedViewHandle.ReleasePointer();
                }

                _mapView.Flush();
                 
            }

            private void TryFlushAndDispose()
            {
                bool releaseHandle = false;
                byte* p = null;
                _mapView.SafeMemoryMappedViewHandle.AcquirePointer(ref p);
                try
                {
                    if (!Native.FlushViewOfFile(p, (IntPtr)0))
                        throw new Win32Exception();
                }
                catch (Win32Exception e)
                {
                    System.Diagnostics.Trace.Write(this + ": " + e.Message + "\r\n");
                    if (e.NativeErrorCode.Equals(33))
                        releaseHandle = true;
                }
                finally
                {
                    _mapView.SafeMemoryMappedViewHandle.ReleasePointer();
                }

                if (releaseHandle)
                {
                    _mapView.SafeMemoryMappedViewHandle.Close();

                    try
                    {
                        _mapView.Flush();
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Trace.Write(this + ": " + e.Message + "\r\n");
                    }

                    _mapView.Dispose();
                    GC.ReRegisterForFinalize(_mapView);
                    _mapFile.Dispose();
                    GC.ReRegisterForFinalize(_mapFile);

                    GC.Collect();
                }
                else
                {
                    _mapView.Dispose();
                    _mapFile.Dispose();
                }
            }

            void IDisposable.Dispose()
            {
                //if (MapViewManager.LockViews)
                //    if (!Native.NativeMethods.VirtualLock((IntPtr)Pointer, (UIntPtr)_viewSize))
                //        throw new Win32Exception();

                _mapView.SafeMemoryMappedViewHandle.ReleasePointer();

                if (!MapViewManager.FilePerViewMode)
                    TryFlushAndDispose();
                else
                {
                    try
                    {
                        _mapView.Dispose();
                    }
                    catch (IOException)
                    {
                        //  Caused if the map view was unmapped manually (using native function), 
                        //  Usually when purposely discarding the flush operation.
                    }

                    _mapFile.Dispose();
                }

                //  Only when this view has been modified, manually force the final flush (using file stream handle) and set the last write time.
                if (IsModified && !_fileHandle.IsClosed)
                {
                    if (!Native.FlushFileBuffers(_fileHandle))
                        throw new Win32Exception();
                    Native.SetFileTimes(_fileHandle.DangerousGetHandle(), DateTime.MinValue, DateTime.MinValue, DateTime.FromBinary(_lastWrite));
                }

                //this.Flush();

                //GC.RemoveMemoryPressure((long)_mapViewHandle.SafeMemoryMappedViewHandle.ByteLength);
            }

            internal void InternalWrite(byte[] buffer, int length)
            {
                _mapView.Position = this.Position;
                _mapView.Write(buffer, 0, length);
                this.Position += length;

                this.SetLastWrite();
            }

            public override string ToString()
            {
                return string.Format("view[id={4}   vp={0:n0}   dp={1:n0}   sz={2:n0}   ocp={3:n0}]", ViewPosition, Position, Length / 2, _mapView.Position, Id);
            }

            /// <summary>
            /// Set last write timestamp
            /// </summary>
            internal void SetLastWrite()
            {
                _writeCount++;
                _lastWrite = DateTime.UtcNow.ToBinary();
            }

            /// <summary>
            /// Indicates if the content of this map view has been ever modified since it has been last opened by the manager
            /// </summary>
            internal bool IsModified
            {
                get { return _writeCount > 0; }
            }

            /// <summary>
            /// Unmaps the memory mapped view
            /// </summary>
            internal void Unmap()
            {
                if (!Native.UnmapViewOfFile(Pointer))
                    throw new Win32Exception();
            }
        }

        internal void DumpViewsToDebugWindow()
        {
            if (!DebugForm.Instance.Enabled)
                return;

            lock (this._debugDumpLocker)
            {
                var sb = new StringBuilder();

                var mapViews = _activeViews.Values
                    .Select(view => Tuple.Create<long, long, bool>(view.Id, view.ViewPosition, true))
                    .Union(_passiveViews.Keys
                    .Where(view => !_activeViews.Keys.Contains(view))
                    .Select(viewId => Tuple.Create<long, long, bool>(viewId, viewId * _viewSize, false)))
                    .OrderBy(view => view.Item1).ToArray();

                sb.AppendLine(this + "");

                sb.AppendLine(string.Format("- System Memory: {0}, Margin: {1}", Memory, Utils.FormatData(Configuration.MemoryMargin)));

                sb.AppendLine(string.Format("\r\n- View Manager Views: {0}", mapViews.Length));
                sb.AppendLine(new string('-', 93));

                foreach (var view in mapViews)
                {
                    var viewText = string.Format("View {0,-8}   @ {1, -13}", view.Item1 + 1, Utils.FormatData(view.Item2));

                    sb.AppendLine(string.Format(" {0,-4} {1} {2, -15}",

                         _currentViewId == view.Item1 ? " >>" : "      ",
                         viewText,
                         _currentClosingViewId == view.Item1 ? " > closing ..." : (view.Item3 ? " -  open" : " -  closed")));
                }

                sb.AppendLine(new string('-', 93));

                sb.AppendLine(string.Format("       Opened: {0:n0}, Closed: {1:n0}, Total: {2:n0}, Size: {3}", mapViews.Count(view => view.Item3), mapViews.Count(view => !view.Item3), mapViews.Count(), Utils.FormatData(mapViews.Count() * _viewSize)));
                sb.AppendLine();
                sb.AppendLine(string.Format("       Available View Memory: {0}", Utils.FormatData(Configuration.AvailableViewMemory(_passiveViews.Count > 0))));

                DebugForm.Instance.SetDebugDisplayText(sb.ToString());
            }
        }

        public void DumpStatistics()
        {
            lock (_activeViews)
            {
                var openViews = this._activeViews.Values.ToArray();

                Utils.ConsoleWriteLine(ConsoleColor.Yellow, "\r\n- View Manager Open Views: {0}, Size {1}", openViews.Length, Utils.FormatData(openViews.Sum(v => v.Length / 2)));
                Utils.ConsoleWriteLine(ConsoleColor.Yellow, new string('-', 93));

                foreach (var view in openViews)
                    Utils.ConsoleWriteLine(null, "    {0}", view);
            }

            Utils.ConsoleWriteLine(ConsoleColor.Green, new string('-', 93));

            Console.WriteLine("Press any key to continue ...");
            Console.ReadLine();
        }

        /// <summary>
        /// Returns list of physical map view files, full paths. 
        /// It contains list of files which are already peristent on the disk, or map files which were used as memory page and are not yet persisted.
        /// The folder location of the file can be used to determine of the file is peristent or page file
        /// </summary>
        /// <returns></returns>
        internal string[] GetMapFiles()
        {
            // all persistent and unpersistent file names
            return _fileStreams.Values.Select(fstream => fstream.Name).ToArray();
        }

        /// <summary>
        /// Associates the map view manager with data from physical persisted map files
        /// </summary>
        /// <param name="dataFiles"></param>
        internal void AssignMapFiles(string[] dataFiles)
        {
            // Close any opened streams 
            foreach (var fstream in _fileStreams.Values)
            {
                fstream.Flush(flushToDisk: true);
                fstream.Dispose();
            }

            _fileStreams.Clear();

            foreach (var filename in dataFiles)
            {
                var fstream = new FileStream(filename,
                    FileMode.Open,
                    FileSystemRights.Read | FileSystemRights.Write | FileSystemRights.Delete,
                    FileShare.ReadWrite | FileShare.Delete,
                    1024 * 1024 * 4,
                    FileOptions.WriteThrough,
                    null);

                _fileStreams.Add(Path.GetFileName(filename), fstream);
            }

            this.ReleaseViews();

            _currentViewEnd = long.MaxValue;
            _currentViewStart = long.MaxValue;
            _currentViewId = long.MinValue;
            _currentView = null;
            _lockedViewId = long.MinValue;
            _currentClosingViewId = long.MinValue;

            Memory.Calculate();
        }

        /// <summary>
        /// Associates the map view manager with data from physical map file. The file consist of several map files, which are being mapped to its own file stream and map view
        /// </summary>
        /// <param name="mergedDataFile"></param>
        /// <param name="totalDataFiles"></param>
        protected void SetMapFile(string mergedDataFile, int totalDataFiles)
        {
            throw new NotImplementedException();

            // Close any opened streams 
            foreach (var fileStream in _fileStreams.Values)
                fileStream.Close();
            _fileStreams.Clear();

            string[] dataFiles = Enumerable.Range(0, totalDataFiles).Select(index => mergedDataFile.Replace("merged", string.Format("{0}.data", index))).ToArray();
            for (int fileIndex = 0; fileIndex < dataFiles.Length; fileIndex++)
            {
                FileStream fileStream;
                fileStream = new FileStream(mergedDataFile,
                    FileMode.Open,
                    FileSystemRights.Read | FileSystemRights.Write,
                    FileShare.ReadWrite,
                    1024 * 1024 * 4,
                    FileOptions.WriteThrough,
                    null);

                fileStream.Seek(fileIndex * (Configuration.ViewSize + Configuration.ViewBufferSize), SeekOrigin.Begin);
               // fileStream.SetLength(ViewSize + ViewBufferSize);

                _fileStreams.Add(Path.GetFileName(dataFiles[fileIndex]), fileStream);
            }

            this.ReleaseViews();

            _currentViewEnd = long.MaxValue;
            _currentViewStart = long.MaxValue;
            _currentViewId = long.MinValue;
            _lockedViewId = long.MinValue;
            _currentClosingViewId = long.MinValue;

            Memory.Calculate();
        }

        /// <summary>
        /// Flushes all views content
        /// </summary>
        internal void FlushAllViews()
        {
            _viewLocker.EnterWriteLock();

            try
            {
                foreach (var view in _activeViews.Keys.ToArray())
                {
                    var mapView = _activeViews[view];
                    if (mapView != null)
                        mapView.InternalFlush();
                }
            }
            finally
            {
                _viewLocker.ExitWriteLock();
            }            
        }

        /// <summary>
        /// Reposition region of data from one location to another within the same memory view
        /// </summary>
        /// <param name="position"></param>
        /// <param name="writeOffset"></param>
        /// <param name="regionLength"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void RepositionViewData(long position, int offset, int length, bool threadLock = false)
        {
            unsafe
            {
                //  Get the map view for the position and make sure the reposition will not exceed the bounds of the current map view

                var mapView = this.GetView(position, threadLock);

                if (mapView.Position + offset + length > mapView.Length)
                    throw new InvalidOperationException(this + ": Attempt to reposition large amount of data, which exceeds map view maximum size. Partitioning the data is needed.\r\nRepositioning " + length + " bytes, at position " + position + " to position " + position + offset + "\r\n" + mapView);

                this.BeginWrite(mapView);

                //  Memory pointer to the source location
                byte* pData = (mapView.Pointer + mapView.Position);

                //  Move the length of data by the offet specified
                Native.memmove(pData + offset, pData, length);

                //  Sets the last write timestamp and counter. Used when closing this view file to manually update the file times.
                mapView.SetLastWrite();                
            }
        }
    }
}