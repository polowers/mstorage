﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.IO.MemoryMappedFiles;
using System.ComponentModel;
using System.IO;

namespace DigitalToolworks.Data
{
    public static unsafe class Native
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct SYSTEM_INFO
        {
            public ushort processorArchitecture;
            // ReSharper disable once FieldCanBeMadeReadOnly.Local
            ushort reserved;
            public uint pageSize;
            public IntPtr minimumApplicationAddress;
            public IntPtr maximumApplicationAddress;
            public IntPtr activeProcessorMask;
            public uint numberOfProcessors;
            public uint processorType;
            public uint allocationGranularity;
            public ushort processorLevel;
            public ushort processorRevision;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MEMORY_BASIC_INFORMATION
        {
            public UIntPtr BaseAddress;
            public UIntPtr AllocationBase;
            public uint AllocationProtect;
            public IntPtr RegionSize;
            public uint State;
            public uint Protect;
            public uint Type;
        }

        /// <summary>
        /// Indicates whether a file should be deleted. Used for any handles. Use only when calling SetFileInformationByHandle. 
        /// https://msdn.microsoft.com/en-us/library/aa364221%28v=vs.85%29.aspx
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        internal struct FILE_DISPOSITION_INFO
        {
            /// <summary>
            /// Indicates whether the file should be deleted. Set to TRUE to delete the file. This member has no effect if the handle was opened with FILE_FLAG_DELETE_ON_CLOSE.
            /// </summary>
            public bool DeleteFile;
        }

        /// <summary>
        /// Contains the basic information for a file. Used for file handles. 
        /// https://msdn.microsoft.com/en-us/library/aa364217%28v=vs.85%29.aspx
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        internal struct FILE_BASIC_INFO
        {
            public long CreationTime;
            public long LastAccessTime;
            public long LastWriteTime;
            public long ChangeTime;
            public uint FileAttributes;
        }

        /// <summary>
        /// Contains information that the GetFileInformationByHandle function retrieves. 
        /// https://msdn.microsoft.com/en-us/library/aa363788%28v=vs.85%29.aspx
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        internal struct BY_HANDLE_FILE_INFORMATION
        {
            public uint FileAttributes;
            public FILETIME CreationTime;
            public FILETIME LastAccessTime;
            public FILETIME LastWriteTime;
            public uint VolumeSerialNumber;
            public uint FileSizeHigh;
            public uint FileSizeLow;
            public uint NumberOfLinks;
            public uint FileIndexHigh;
            public uint FileIndexLow;
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern byte* VirtualAlloc(byte* lpAddress, UIntPtr dwSize, AllocationType flAllocationType, MemoryProtection flProtect);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool VirtualFree(byte* lpAddress, UIntPtr dwSize, FreeType dwFreeType);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool VirtualUnlock(IntPtr lpAddress, UIntPtr dwSize);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool VirtualLock(IntPtr lpAddress, UIntPtr dwSize);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetProcessWorkingSetSize(IntPtr process, long minimumWorkingSetSize, long maximumWorkingSetSize);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetProcessWorkingSetSizeEx(IntPtr process, UIntPtr minimumWorkingSetSize, UIntPtr maximumWorkingSetSize, uint flags);

        [DllImport("kernel32.dll")]
        public static extern int VirtualQuery(byte* lpAddress, MEMORY_BASIC_INFORMATION* lpBuffer, IntPtr dwLength);

        [Flags]
        public enum FreeType : uint
        {
            MEM_DECOMMIT = 0x4000,
            MEM_RELEASE = 0x8000
        }

        /// <summary>
        /// Used by SetFileInformationByHandle(). 
        /// A FILE_INFO_BY_HANDLE_CLASS enumeration value that specifies the type of information to be changed. 
        /// https://msdn.microsoft.com/en-us/library/aa365539%28v=vs.85%29.aspx
        /// </summary>
        internal enum FileInformationClass : uint
        {
            FileBasicInfo = 0,
            FileRenameInfo = 3,
            FileDispositionInfo = 4,
            FileAllocationInfo = 5,
            FileEndOfFileInfo = 6,
            FileIoPriorityHintInfo = 12
        }

        /// <summary>
        /// File attributes
        /// </summary>
        [Flags]
        internal enum FileAttribute : uint
        {
            /// <summary>
            /// FILE_ATTRIBUTE_TEMPORARY: A file that is being used for temporary storage. File systems avoid writing data back to mass storage if sufficient cache memory is available, because typically, an application deletes a temporary file after the handle is closed. In that scenario, the system can entirely avoid writing the data. Otherwise, the data is written after the handle is closed.
            /// </summary>
            Temporary = 256
        }

        [Flags]
        public enum AllocationType : uint
        {
            COMMIT = 0x1000,
            RESERVE = 0x2000,
            RESET = 0x80000,
            LARGE_PAGES = 0x20000000,
            PHYSICAL = 0x400000,
            TOP_DOWN = 0x100000,
            WRITE_WATCH = 0x200000
        }

        [Flags]
        public enum MemoryProtection : uint
        {
            EXECUTE = 0x10,
            EXECUTE_READ = 0x20,
            EXECUTE_READWRITE = 0x40,
            EXECUTE_WRITECOPY = 0x80,
            NOACCESS = 0x01,
            READONLY = 0x02,
            READWRITE = 0x04,
            WRITECOPY = 0x08,
            GUARD_Modifierflag = 0x100,
            NOCACHE_Modifierflag = 0x200,
            WRITECOMBINE_Modifierflag = 0x400
        }

        [DllImport("kernel32.dll")]
        public static extern void GetSystemInfo(out SYSTEM_INFO lpSystemInfo);

        [DllImport("msvcrt.dll", EntryPoint = "memcpy", CallingConvention = CallingConvention.Cdecl, SetLastError = false)]
        public static extern IntPtr memcpy(byte* dest, byte* src, IntPtr count);

        [DllImport("msvcrt.dll", EntryPoint = "memcpy", CallingConvention = CallingConvention.Cdecl, SetLastError = false)]
        public static extern IntPtr memcpy(byte* dest, byte* src, int count);



        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl, SetLastError = false)]
        [SuppressUnmanagedCodeSecurity]
        [SecurityCritical]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
        public static extern int memcmp(byte* dest, byte* src, int count);

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl, SetLastError = false)]
        [SuppressUnmanagedCodeSecurity]
        [SecurityCritical]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
        public static extern int memmove(byte* dest, byte* src, int count);

        [DllImport("msvcrt.dll", EntryPoint = "memset", CallingConvention = CallingConvention.Cdecl, SetLastError = false)]
        public static extern IntPtr memset(byte* dest, int c, int count);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CloseHandle(IntPtr hObject);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool UnmapViewOfFile(byte* lpBaseAddress);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool FlushFileBuffers(SafeFileHandle hFile);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool SetFileTime(IntPtr hFile, ref long lpCreationTime, ref long lpLastAccessTime, ref long lpLastWriteTime);

        /// <summary>
        /// Sets the file information for the specified file. 
        /// https://msdn.microsoft.com/en-us/library/aa365539%28v=vs.85%29.aspx
        /// </summary>
        /// <param name="hFile"></param>
        /// <param name="FileInformationClass"></param>
        /// <param name="lpFileInformation"></param>
        /// <param name="dwBufferSize"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool SetFileInformationByHandle(SafeFileHandle hFile, FileInformationClass fileInformationClass, void*  lpFileInformation, int dwBufferSize);

        /// <summary>
        /// Retrieves file information for the specified file. 
        /// https://msdn.microsoft.com/en-us/library/aa364952%28v=vs.85%29.aspx
        /// </summary>
        /// <param name="hFile"></param>
        /// <param name="lpFileInformation"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetFileInformationByHandle(SafeFileHandle hFile, out BY_HANDLE_FILE_INFORMATION lpFileInformation);

        /// <summary>
        /// Sets the underlaying file attebutes to temporary and marks the file for deletion. 
        /// This will cause the OS to delete the file as soon as its handle is closed. 
        /// </summary>
        /// <param name="stream"></param>
        public static void SetFileAsTemporary(SafeFileHandle handle, bool isTemporary)
        {
            Native.FILE_BASIC_INFO fbi;
            Native.FILE_DISPOSITION_INFO fdi;
            Native.BY_HANDLE_FILE_INFORMATION fi;
            if (!GetFileInformationByHandle(handle, out fi))
                throw new Win32Exception();

            if (isTemporary)
            {
                //  Set the temporary attribute and deletion flag.

                fbi.FileAttributes = fi.FileAttributes | (uint)FileAttribute.Temporary;
                fdi.DeleteFile = true;
            }
            else
            {
                //  Remove the temporary attribute and deletion flag.

                fbi.FileAttributes = fi.FileAttributes & ~(uint)FileAttribute.Temporary;
                fdi.DeleteFile = false;      
            }

            if (!SetFileInformationByHandle(handle, FileInformationClass.FileBasicInfo, &fbi, sizeof(FILE_BASIC_INFO)))
                throw new Win32Exception();

            if (!SetFileInformationByHandle(handle, FileInformationClass.FileDispositionInfo, &fdi, sizeof(FILE_DISPOSITION_INFO)))
                throw new Win32Exception();
        }
 
        public static void SetFileTimes(IntPtr hFile, DateTime creationTime, DateTime accessTime, DateTime writeTime)
        {
            long lCreationTime = creationTime != DateTime.MinValue ? creationTime.ToFileTime() : 0;
            long lAccessTime = creationTime != DateTime.MinValue ? accessTime.ToFileTime() : 0;
            long lWriteTime = writeTime != DateTime.MinValue ? writeTime.ToFileTime() : 0;

            if (!SetFileTime(hFile, ref lCreationTime, ref lAccessTime, ref lWriteTime))
                throw new Win32Exception();
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool FlushViewOfFile(byte* lpBaseAddress, IntPtr dwNumberOfBytesToFlush);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern byte* MapViewOfFileEx(IntPtr hFileMappingObject, NativeFileMapAccessType dwDesiredAccess, uint dwFileOffsetHigh, uint dwFileOffsetLow, UIntPtr dwNumberOfBytesToMap, byte* lpBaseAddress);

        [DllImport("psapi.dll")]
        public static extern bool EmptyWorkingSet(IntPtr hProcess);

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern IntPtr CreateFile(String lpFileName, int dwDesiredAccess, int dwShareMode, IntPtr lpSecurityAttributes, int dwCreationDisposition, int dwFlagsAndAttributes, IntPtr hTemplateFile);


        /// <summary>
        /// Checks whether the specified address is within a memory-mapped file in the address space of the specified process. If so, the function returns the name of the memory-mapped file. 
        /// https://msdn.microsoft.com/en-us/library/windows/desktop/ms683195%28v=vs.85%29.aspx
        /// </summary>
        /// <param name="hProcess">process handle</param>
        /// <param name="lpv">pointer to the memory mapped view</param>
        /// <param name="lpFilename">resulting string</param>
        /// <param name="nSize">maximum length of the path (MAX_PATH)</param>
        /// <returns></returns>
        [DllImport("psapi.dll", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern int GetMappedFileName(IntPtr hProcess, IntPtr lpv, StringBuilder lpFilename, int nSize);

        /// <summary>
        /// Checks whether the specified address is within a memory-mapped file in the address space of the specified process. If so, the function returns the name of the memory-mapped file. 
        /// </summary>
        /// <param name="mmv"></param>
        /// <returns></returns>
        public static string GetMappedFileName(IntPtr mmv)
        {
            StringBuilder lpFilename = new StringBuilder();
            if (GetMappedFileName(System.Diagnostics.Process.GetCurrentProcess().Handle, mmv, lpFilename, 1 * 1024) == 0)
                throw new Win32Exception();
            return lpFilename.ToString();
        }

        [DllImport("kernel32.dll", EntryPoint = "RtlMoveMemory")]
        public static extern void CopyMemory(byte* dst, byte* src, int size);

        [Flags]
        public enum NativeFileMapAccessType : uint
        {
            Copy = 0x01,
            Write = 0x02,
            Read = 0x04,
            AllAccess = 0x08,
            Execute = 0x20,
        }

        public class Murmur3
        {
            public readonly static Murmur3 Instance = new Murmur3();

            // 128 bit output, 64 bit platform version

            public static ulong READ_SIZE = 16;
            private static ulong C1 = 0x87c37b91114253d5L;
            private static ulong C2 = 0x4cf5ad432745937fL;

            private ulong length;
            private uint seed; // if want to start with a seed, create a constructor
            ulong h1;
            ulong h2;

            private Murmur3()
            {
            }

            private void MixBody(ulong k1, ulong k2)
            {
                h1 ^= MixKey1(k1);

                h1 = IntHelpers.RotateLeft(h1, 27);
                h1 += h2;
                h1 = h1 * 5 + 0x52dce729;

                h2 ^= MixKey2(k2);

                h2 = IntHelpers.RotateLeft(h2, 31);
                h2 += h1;
                h2 = h2 * 5 + 0x38495ab5;
            }

            private static ulong MixKey1(ulong k1)
            {
                k1 *= C1;
                k1 = IntHelpers.RotateLeft(k1, 31);
                k1 *= C2;
                return k1;
            }

            private static ulong MixKey2(ulong k2)
            {
                k2 *= C2;
                k2 = IntHelpers.RotateLeft(k2, 33);
                k2 *= C1;
                return k2;
            }

            private static ulong MixFinal(ulong k)
            {
                // avalanche bits

                k ^= k >> 33;
                k *= 0xff51afd7ed558ccdL;
                k ^= k >> 33;
                k *= 0xc4ceb9fe1a85ec53L;
                k ^= k >> 33;
                return k;
            }

            public byte[] ComputeHash(byte[] bb)
            {
                h1 = 0;
                h2 = 0;

                ProcessBytes(bb);
                return Hash;
            }

            private void ProcessBytes(byte[] bb)
            {
                h1 = seed;
                this.length = 0L;

                int pos = 0;
                ulong remaining = (ulong)bb.Length;

                // read 128 bits, 16 bytes, 2 longs in eacy cycle
                while (remaining >= READ_SIZE)
                {
                    ulong k1 = IntHelpers.GetUInt64(bb, pos);
                    pos += 8;

                    ulong k2 = IntHelpers.GetUInt64(bb, pos);
                    pos += 8;

                    length += READ_SIZE;
                    remaining -= READ_SIZE;

                    MixBody(k1, k2);
                }

                // if the input MOD 16 != 0
                if (remaining > 0)
                    ProcessBytesRemaining(bb, remaining, pos);
            }

            private void ProcessBytesRemaining(byte[] bb, ulong remaining, int pos)
            {
                ulong k1 = 0;
                ulong k2 = 0;
                length += remaining;

                // little endian (x86) processing
                switch (remaining)
                {
                    case 15:
                        k2 ^= (ulong)bb[pos + 14] << 48; // fall through
                        goto case 14;
                    case 14:
                        k2 ^= (ulong)bb[pos + 13] << 40; // fall through
                        goto case 13;
                    case 13:
                        k2 ^= (ulong)bb[pos + 12] << 32; // fall through
                        goto case 12;
                    case 12:
                        k2 ^= (ulong)bb[pos + 11] << 24; // fall through
                        goto case 11;
                    case 11:
                        k2 ^= (ulong)bb[pos + 10] << 16; // fall through
                        goto case 10;
                    case 10:
                        k2 ^= (ulong)bb[pos + 9] << 8; // fall through
                        goto case 9;
                    case 9:
                        k2 ^= (ulong)bb[pos + 8]; // fall through
                        goto case 8;
                    case 8:
                        k1 ^= IntHelpers.GetUInt64(bb, pos);
                        break;
                    case 7:
                        k1 ^= (ulong)bb[pos + 6] << 48; // fall through
                        goto case 6;
                    case 6:
                        k1 ^= (ulong)bb[pos + 5] << 40; // fall through
                        goto case 5;
                    case 5:
                        k1 ^= (ulong)bb[pos + 4] << 32; // fall through
                        goto case 4;
                    case 4:
                        k1 ^= (ulong)bb[pos + 3] << 24; // fall through
                        goto case 3;
                    case 3:
                        k1 ^= (ulong)bb[pos + 2] << 16; // fall through
                        goto case 2;
                    case 2:
                        k1 ^= (ulong)bb[pos + 1] << 8; // fall through
                        goto case 1;
                    case 1:
                        k1 ^= (ulong)bb[pos]; // fall through
                        break;
                    default:
                        throw new Exception("Something went wrong with remaining bytes calculation.");
                }

                h1 ^= MixKey1(k1);
                h2 ^= MixKey2(k2);
            }

            public byte[] Hash
            {
                get
                {
                    h1 ^= length;
                    h2 ^= length;

                    h1 += h2;
                    h2 += h1;

                    h1 = Murmur3.MixFinal(h1);
                    h2 = Murmur3.MixFinal(h2);

                    h1 += h2;
                    h2 += h1;

                    var hash = new byte[Murmur3.READ_SIZE];

                    Array.Copy(BitConverter.GetBytes(h1), 0, hash, 0, 8);
                    Array.Copy(BitConverter.GetBytes(h2), 0, hash, 8, 8);

                    return hash;
                }
            }


            public static class IntHelpers
            {
                public static ulong RotateLeft(ulong original, int bits)
                {
                    return (original << bits) | (original >> (64 - bits));
                }

                public static ulong RotateRight(ulong original, int bits)
                {
                    return (original >> bits) | (original << (64 - bits));
                }

                unsafe public static ulong GetUInt64(byte[] bb, int pos)
                {
                    // we only read aligned longs, so a simple casting is enough
                    fixed (byte* pbyte = &bb[pos])
                    {
                        return *((ulong*)pbyte);
                    }
                }
            }

            public unsafe byte[] ComputeHash(byte* buffer, int len)
            {
                byte[] data = new byte[len];
                fixed (byte* pData = data)
                    CopyMemory(pData, buffer, len);
                var hash = ComputeHash(data);
                return hash;
            }
        }

        public static class Utils
        {
            /// <summary>
            /// Calculates the actual data size for the number (max 8 bytes can be described)
            /// </summary>
            /// <param name="number"></param>
            /// <returns></returns>
            public static int GetNumberSize(long number)
            {
                int length = 8;

                // process only positive numbers. for negative, use the original type size = 8 bytes
                if (number > 0)
                {
                    if (number < 256)
                        length = 1;
                    else if (number < 65536)
                        length = 2;
                    else if (number < 16777216)
                        length = 3;
                    else if (number < 4294967296)
                        length = 4;
                    else if (number < 1099511627776)
                        length = 5;
                    else if (number < 281474976710656)
                        length = 6;
                    else if (number < 72057594037927936)
                        length = 7;
                }

                return length;
            }


            [System.Security.SecurityCritical]  // auto-generated
            internal static unsafe void wstrcpy(char* dmem, char* smem, int charCount)
            {
                Memory.Memcpy((byte*)dmem, (byte*)smem, charCount * sizeof(char)); // 2 used everywhere instead of sizeof(char)
            }

            public static string PtrToString(char* p)
            {
                return PtrToString(new IntPtr(p));
            }

            public static string PtrToString(IntPtr p)
            {
                // TODO: deal with character set issues.  Will PtrToStringAnsi always
                // "Do The Right Thing"?
                if (p == IntPtr.Zero)
                    return null;
                return Marshal.PtrToStringAnsi(p);
            }

            private static uint GetSystemGranularity()
            {
                Native.SYSTEM_INFO systemInfo;
                Native.GetSystemInfo(out systemInfo);
                var allocationGranularity = systemInfo.allocationGranularity;
                return allocationGranularity;
            }

            public static long GetNearestSizeToAllocationGranularity(long allocationSize)
            {
                // http://msdn.microsoft.com/en-us/library/windows/desktop/aa366761%28v=vs.85%29.aspx
                // the offset must be a multiple of the allocation granularity. To obtain the memory allocation granularity of the system, use the GetSystemInfo function”
                // http://stackoverflow.com/questions/10465184/how-to-get-int-from-memory-mapped-file

                var granularity = Environment.SystemPageSize;
                var modulos = allocationSize % granularity;
                if (modulos == 0)
                    return Math.Max(allocationSize, granularity);

                return ((allocationSize / granularity) + 1) * granularity;
            }

            public static class Nybble
            {
                /// <summary>
                /// Returns a nibble of a number, like using a byte as an array
                /// </summary> name="num">The numerical data.</param>
                /// <param name="index">The zero-based index of the nibble you want.</param>
                public static byte getNibble(ulong num, int index)
                {
                    return getPortion<byte>(num, index, 4);
                }
                /// <summary>
                /// Returns a byte of a number, like using a number as an array
                /// </summary>
                /// <param name="num">The numerical data.</param>
                /// <param name="index">The zero-based index of the byte you want.</param>
                public static byte getByte(ulong num, int index)
                {
                    return getPortion<byte>(num, index);
                }
                /// <summary>
                /// Returns a portion of a number. Uses the size of T to infer size of desired data.
                /// </summary>
                /// <param name="num">The numerical data.</param>
                /// <param name="index">The zero-based index of the section you want (2 corresponds to an offset of 2*size)</param>
                public static T getPortion<T>(ulong num, int index) where T : struct
                {
                    return getPortion<T>(num, index, System.Runtime.InteropServices.Marshal.SizeOf(typeof(T)) * 8);
                }
                /// <summary>
                /// Returns a portion of a number, like using a number as an array
                /// </summary>
                /// <param name="num">The numerical data.</param>
                /// <param name="index">The zero-based index of the section you want (2 corresponds to an offset of 2*size)</param>
                /// <param name="size">The size of the section (in bits) that you want.</param>
                public static T getPortion<T>(ulong num, int index, int size) where T : struct
                {
                    int indexShift = (index * size);

                    //Do [num AND indexShiftedBitmask] to select the bits in that bit mask, then shift it down so it's just {size} long
                    //     [num & (ulong)( sizeBitmask    << indexShift)] >> indexShift;
                    ulong result = ((num & (ulong)((1 << size) - 1 << indexShift)) >> indexShift);

                    //Can't simply cast to T it's variable
                    try { return (T)Convert.ChangeType(result, typeof(T)); }
                    catch (InvalidCastException) { return new T(); }
                }
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                public static void setHigh(byte* buffer, byte val)
                {
                    buffer[0] = (byte)((buffer[0] & 0xf) | (val << 4));
                }
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                public static byte getHigh(byte* buffer)
                {
                    return (byte)((buffer[0] & 0xf0) >> 4);
                }
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                public static void setLow(byte* buffer, byte val)
                {
                    buffer[0] = (byte)((buffer[0] & 0xf0) | val);
                }
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                public static byte getLow(byte* buffer)
                {

                    return (byte)(buffer[0] & 0xf);
                }
            }

            public static class Memory
            {
                /// <summary>
                /// Acquire the fixed pointer to the start of the memory mapped view. It takes the internal view offset under consideration, unlike the native implementation
                /// The pointer will be released during <see cref="Dispose(bool)"/>
                /// </summary>
                /// <returns>The pointer to the start of the memory mapped view. The pointer is valid, and remains fixed for the lifetime of this object.</returns>
                public static byte* AcquirePointer(MemoryMappedViewStream accessor)
                {
                    byte* ptr = null;
                    accessor.SafeMemoryMappedViewHandle.AcquirePointer(ref ptr);
                    ptr += GetPrivateOffset(accessor);
                    return ptr;
                }

                // this is a copy from compiler's MemoryMappedFileBlock. see MemoryMappedFileBlock for more information on why this is needed.
                public static long GetPrivateOffset(MemoryMappedViewStream stream)
                {
                    System.Reflection.FieldInfo unmanagedMemoryStreamOffset = typeof(MemoryMappedViewStream).GetField("m_view", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.GetField);
                    object memoryMappedView = unmanagedMemoryStreamOffset.GetValue(stream);
                    System.Reflection.PropertyInfo memoryMappedViewPointerOffset = memoryMappedView.GetType().GetProperty("PointerOffset", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.GetProperty);
                    return (long)memoryMappedViewPointerOffset.GetValue(memoryMappedView);
                }

                [SecurityCritical]
                public static unsafe int IndexOfByte(byte* src, byte value, int index, int count)
                {
                    byte* numPtr = src + index;
                    while ((((int)numPtr) & 3) != 0)
                    {
                        if (count == 0)
                        {
                            return -1;
                        }
                        if (numPtr[0] == value)
                        {
                            return (int)((long)((numPtr - src) / 1));
                        }
                        count--;
                        numPtr++;
                    }
                    uint num = (uint)((value << 8) + value);
                    num = (num << 0x10) + num;
                    while (count > 3)
                    {
                        uint num2 = *((uint*)numPtr);
                        num2 ^= num;
                        uint num3 = 0x7efefeff + num2;
                        num2 ^= uint.MaxValue;
                        num2 ^= num3;
                        if ((num2 & 0x81010100) != 0)
                        {
                            int num4 = (int)((long)((numPtr - src) / 1));
                            if (numPtr[0] == value)
                            {
                                return num4;
                            }
                            if (numPtr[1] == value)
                            {
                                return (num4 + 1);
                            }
                            if (numPtr[2] == value)
                            {
                                return (num4 + 2);
                            }
                            if (numPtr[3] == value)
                            {
                                return (num4 + 3);
                            }
                        }
                        count -= 4;
                        numPtr += 4;
                    }
                    while (count > 0)
                    {
                        if (numPtr[0] == value)
                        {
                            return (int)((long)((numPtr - src) / 1));
                        }
                        count--;
                        numPtr++;
                    }
                    return -1;
                }

                //[MethodImpl(MethodImplOptions.InternalCall), SecuritySafeCritical]
                //internal static extern void InternalBlockCopy(Array src, int srcOffsetBytes, Array dst, int dstOffsetBytes, int byteCount);

                //[MethodImpl(MethodImplOptions.InternalCall), SecurityCritical]
                // private static extern bool IsPrimitiveTypeArray(Array array);

                [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success), SecurityCritical]
                public static unsafe void Memcpy(byte* dest, byte* src, int len)
                {
                    if (len >= 0x10)
                    {
                        do
                        {
                            *((long*)dest) = *((long*)src);
                            *((long*)(dest + 8)) = *((long*)(src + 8));
                            dest += 0x10;
                            src += 0x10;
                        }
                        while ((len -= 0x10) >= 0x10);
                    }
                    if (len > 0)
                    {
                        if ((len & 8) != 0)
                        {
                            *((long*)dest) = *((long*)src);
                            dest += 8;
                            src += 8;
                        }
                        if ((len & 4) != 0)
                        {
                            *((int*)dest) = *((int*)src);
                            dest += 4;
                            src += 4;
                        }
                        if ((len & 2) != 0)
                        {
                            *((short*)dest) = *((short*)src);
                            dest += 2;
                            src += 2;
                        }
                        if ((len & 1) != 0)
                        {
                            dest++;
                            src++;
                            dest[0] = src[0];
                        }
                    }
                }

                [SecurityCritical, ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
                public static unsafe void Memcpy(byte[] dest, int destIndex, byte* src, int srcIndex, int len)
                {
                    if (len != 0)
                    {
                        fixed (byte* numRef = dest)
                        {
                            Memcpy(numRef + destIndex, src + srcIndex, len);
                        }
                    }
                }

                [SecurityCritical, ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
                public static unsafe void Memcpy(byte* pDest, int destIndex, byte[] src, int srcIndex, int len)
                {
                    if (len != 0)
                    {
                        fixed (byte* numRef = src)
                        {
                            Memcpy(pDest + destIndex, numRef + srcIndex, len);
                        }
                    }
                }

                [SecurityCritical, ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
                public static unsafe void Memcpy(char* pDest, int destIndex, char* pSrc, int srcIndex, int len)
                {
                    if (len != 0)
                    {
                        Memcpy((byte*)(pDest + destIndex), (byte*)(pSrc + srcIndex), len * 2);
                    }
                }
            }
            // The unsafe keyword allows pointers to be used within the following method:
            [MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public static void Copy(byte[] src, int srcIndex, byte[] dst, int dstIndex, int count)
            {
                if (src == null || srcIndex < 0 ||
                    dst == null || dstIndex < 0 || count < 0)
                {
                    throw new System.ArgumentException();
                }

                int srcLen = src.Length;
                int dstLen = dst.Length;
                if (srcLen - srcIndex < count || dstLen - dstIndex < count)
                {
                    throw new System.ArgumentException();
                }

                // The following fixed statement pins the location of the src and dst objects
                // in memory so that they will not be moved by garbage collection.
                fixed (byte* pSrc = src, pDst = dst)
                {
                    byte* ps = pSrc;
                    byte* pd = pDst;

                    // Loop over the count in blocks of 4 bytes, copying an integer (4 bytes) at a time:
                    for (int i = 0; i < count / 8; i++)
                    {
                        *((long*)pd) = *((long*)ps);
                        pd += 8;
                        ps += 8;
                    }

                    // Complete the copy by moving any bytes that weren't moved in blocks of 4:
                    for (int i = 0; i < count % 8; i++)
                    {
                        *pd = *ps;
                        pd++;
                        ps++;
                    }
                }
            }

            /*
    ** Bitmasks used by sqlite3GetVarint().  These precomputed constants
    ** are defined here rather than simply putting the constant expressions
    ** inline in order to work around bugs in the RVT compiler.
    **
    ** SLOT_2_0     A mask for  (0x7f<<14) | 0x7f
    **
    ** SLOT_4_2_0   A mask for  (0x7f<<28) | SLOT_2_0
    */
            private const uint SLOT_2_0 = 0x001fc07f;
            private const uint SLOT_4_2_0 = 0xf01fc07f;

            private const long LARGEST_INT64 = long.MaxValue;
            private const long SMALLEST_INT64 = long.MinValue;
            private const uint SQLITE_MAX_U32 = uint.MaxValue;


            /*
    ** The string z[] is an text representation of a real number.
    ** Convert this string to a double and write it into *pResult.
    **
    ** The string z[] is length bytes in length (bytes, not characters) and
    ** uses the encoding enc.  The string is not necessarily zero-terminated.
    **
    ** Return TRUE if the result is a valid real number (or integer) and FALSE
    ** if the string is empty or contains extraneous text.  Valid numbers
    ** are in one of these formats:
    **
    **    [+-]digits[E[+-]digits]
    **    [+-]digits.[digits][E[+-]digits]
    **    [+-].digits[E[+-]digits]
    **
    ** Leading and trailing whitespace is ignored for the purpose of determining
    ** validity.
    **
    ** If some prefix of the input string is a valid number, this routine
    ** returns FALSE but it still converts the prefix and writes the result
    ** into *pResult.
    */

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public static bool sqlite3AtoF(byte* z, double* pResult, int length, byte enc)
            {
                int incr;
                byte* zEnd = z + length;
                /* sign * significand * (10 ^ (esign * exponent)) */
                int sign = 1;    /* sign of significand */
                long s = 0;       /* significand */
                int d = 0;       /* adjust exponent for shifting decimal point */
                int esign = 1;   /* sign of exponent */
                int e = 0;       /* exponent */
                int eValid = 1;  /* True exponent is either not used or is well-formed */
                double result;
                int nDigits = 0;
                int nonNum = 0;


                *pResult = 0.0;   /* Default return value, in case of an error */

                //if (enc == SQLITE_UTF8)
                {
                    incr = 1;
                }
                /*  else
                  {
                      int i;
                      incr = 2;
                      assert(SQLITE_UTF16LE == 2 && SQLITE_UTF16BE == 3);
                      for (i = 3 - enc; i < length && z[i] == 0; i += 2) { }
                      nonNum = i < length;
                      zEnd = z + i + enc - 3;
                      z += (enc & 1);
                  }*/

                /* skip leading spaces */
                while (z < zEnd && totypeIsspace(*z))
                {
                    z += incr;
                }

                if (z >= zEnd)
                    return false;

                /* get sign of significand */
                if (*z == '-')
                {
                    sign = -1;
                    z += incr;
                }
                else if (*z == '+')
                {
                    z += incr;
                }

                /* skip leading zeroes */
                while (z < zEnd && z[0] == '0')
                {
                    z += incr;
                    nDigits++;
                }

                /* copy max significant digits to significand */
                while (z < zEnd && totypeIsdigit(*z) && s < ((LARGEST_INT64 - 9) / 10))
                {
                    s = s * 10 + (*z - '0');
                    z += incr;
                    nDigits++;
                }

                /* skip non-significant significand digits
                ** (increase exponent by d to shift decimal left) */
                while (z < zEnd && totypeIsdigit(*z))
                {
                    z += incr;
                    nDigits++;
                    d++;
                }

                if (z >= zEnd)
                    goto do_atof_calc;

                /* if decimal point is present */
                if (*z == '.')
                {
                    z += incr;
                    /* copy digits from after decimal to significand
                    ** (decrease exponent by d to shift decimal right) */
                    while (z < zEnd && totypeIsdigit(*z) && s < ((LARGEST_INT64 - 9) / 10))
                    {
                        s = s * 10 + (*z - '0');
                        z += incr; nDigits++; d--;
                    }
                    /* skip non-significant digits */
                    while (z < zEnd && totypeIsdigit(*z)) { z += incr; nDigits++; }
                }
                if (z >= zEnd) goto do_atof_calc;

                /* if exponent is present */
                if (*z == 'e' || *z == 'E')
                {
                    z += incr;
                    eValid = 0;
                    if (z >= zEnd) goto do_atof_calc;
                    /* get sign of exponent */
                    if (*z == '-')
                    {
                        esign = -1;
                        z += incr;
                    }
                    else if (*z == '+')
                    {
                        z += incr;
                    }
                    /* copy digits to exponent */
                    while (z < zEnd && totypeIsdigit(*z))
                    {
                        e = e < 10000 ? (e * 10 + (*z - '0')) : 10000;
                        z += incr;
                        eValid = 1;
                    }
                }

                /* skip trailing spaces */
                if (nDigits > 0 && eValid > 0)
                {
                    while (z < zEnd && totypeIsspace(*z)) { z += incr; }
                }

            do_atof_calc:

                /* adjust exponent by d, and update sign */
                e = (e * esign) + d;

                if (e < 0)
                {
                    esign = -1;
                    e *= -1;
                }
                else
                {
                    esign = 1;
                }

                /* if 0 significand */
                if (s == 0)
                {
                    /* In the IEEE 754 standard, zero is signed.
                    ** Add the sign if we've seen at least one digit */
                    result = (sign < 0 && nDigits > 0) ? -(double)0 : (double)0;
                }
                else
                {
                    /* attempt to reduce exponent */
                    if (esign > 0)
                    {
                        while (s < (LARGEST_INT64 / 10) && e > 0)
                        {
                            e--;
                            s *= 10;
                        }
                    }
                    else
                    {
                        while ((s % 10) == 0 && e > 0)
                        {
                            e--;
                            s /= 10;
                        }
                    }

                    /* adjust the sign of significand */
                    s = sign < 0 ? -s : s;

                    /* if exponent, scale significand as appropriate
                    ** and store in result. */
                    if (e > 0)
                    {
                        double scale = 1.0;
                        /* attempt to handle extremely small/large numbers better */
                        if (e > 307 && e < 342)
                        {
                            while ((e % 308) > 0)
                            {
                                scale *= 1.0e+1; e -= 1;
                            }
                            if (esign < 0)
                            {
                                result = s / scale;
                                result /= 1.0e+308;
                            }
                            else
                            {
                                result = s * scale;
                                result *= 1.0e+308;
                            }
                        }
                        else if (e >= 342)
                        {
                            if (esign < 0)
                            {
                                result = 0.0 * s;
                            }
                            else
                            {
                                result = 1e308 * 1e308 * s;  /* Infinity */
                            }
                        }
                        else
                        {
                            /* 1.0e+22 is the largest power of 10 than can be 
                            ** represented exactly. */
                            while ((e % 22) > 0)
                            {
                                scale *= 1.0e+1;
                                e -= 1;
                            }
                            while (e > 0)
                            {
                                scale *= 1.0e+22;
                                e -= 22;
                            }
                            if (esign < 0)
                            {
                                result = s / scale;
                            }
                            else
                            {
                                result = s * scale;
                            }
                        }
                    }
                    else
                    {
                        result = (double)s;
                    }
                }

                /* store the result */
                *pResult = result;

                /* return true if number and no extra non-whitespace chracters after */
                return z >= zEnd && nDigits > 0 && eValid > 0 && nonNum == 0;
            }


            /*
            ** Return TRUE if character c is a whitespace character
            */
            static bool totypeIsspace(byte c)
            {
                return c == ' ' || c == '\t' || c == '\n' || c == '\v' || c == '\f' || c == '\r';
            }

            /*
            ** Return TRUE if character c is a digit
            */
            static bool totypeIsdigit(byte c)
            {
                return c >= '0' && c <= '9';
            }



            /*
    ** Convert zNum to a 64-bit signed integer.
    **
    ** If the zNum value is representable as a 64-bit twos-complement
    ** integer, then write that value into *pNum and return 0.
    **
    ** If zNum is exactly 9223372036854665808, return 2.  This special
    ** case is broken out because while 9223372036854665808 cannot be a
    ** signed 64-bit integer, its negative -9223372036854665808 can be.
    **
    ** If zNum is too big for a 64-bit integer and is not
    ** 9223372036854665808  or if zNum contains any non-numeric text,
    ** then return 1.
    **
    ** The string is not necessarily zero-terminated.
    */
            public static bool totypeAtoi64(byte* zNum, long* pNum, int length)
            {
                ulong u = 0;
                int neg = 0; /* assume positive */
                int i;
                int c = 0;
                int nonNum = 0;
                byte* zStart;
                byte* zEnd = zNum + length;

                while (zNum < zEnd && totypeIsspace(*zNum)) zNum++;
                if (zNum < zEnd)
                {
                    if (*zNum == '-')
                    {
                        neg = 1;
                        zNum++;
                    }
                    else if (*zNum == '+')
                    {
                        zNum++;
                    }
                }
                zStart = zNum;
                while (zNum < zEnd && zNum[0] == '0') { zNum++; } /* Skip leading zeros. */
                for (i = 0; &zNum[i] < zEnd && (c = zNum[i]) >= '0' && c <= '9'; i++)
                {
                    u = u * 10 + (ulong)c - '0';
                }
                if (u > LARGEST_INT64)
                {
                    *pNum = SMALLEST_INT64;
                }
                else if (neg != 0)
                {
                    *pNum = -(long)u;
                }
                else
                {
                    *pNum = (long)u;
                }
                if ((c != 0 && &zNum[i] < zEnd) || (i == 0 && zStart == zNum) || i > 19 || nonNum != 0)
                {
                    /* zNum is empty or contains non-numeric text or is longer
                    ** than 19 digits (thus guaranteeing that it is too large) */
                    return true;
                }
                else if (i < 19)
                {
                    /* Less than 19 digits, so we know that it fits in 64 bits */
                    ///System.Diagnostics.Debug.Assert(u <= LARGEST_INT64);
                    return true;
                }
                else
                {
                    /* zNum is a 19-digit numbers.  Compare it against 9223372036854775808. */
                    c = totypeCompare2pow63(zNum);
                    if (c < 0)
                    {
                        /* zNum is less than 9223372036854775808 so it fits */
                        ///System.Diagnostics.Debug.Assert(u <= LARGEST_INT64);
                        return false;
                    }
                    else if (c > 0)
                    {
                        /* zNum is greater than 9223372036854775808 so it overflows */
                        return true;
                    }
                    else
                    {
                        /* zNum is exactly 9223372036854775808.  Fits if negative.  The
                        ** special case 2 overflow if positive */
                        ///System.Diagnostics.Debug.Assert(u - 1 == LARGEST_INT64);
                        ///System.Diagnostics.Debug.Assert((*pNum) == SMALLEST_INT64);
                        return neg != 0 ? false : true;
                    }
                }
            }

            /*
    ** Compare the 19-character string zNum against the text representation
    ** value 2^63:  9223372036854775808.  Return negative, zero, or positive
    ** if zNum is less than, equal to, or greater than the string.
    ** Note that zNum must contain exactly 19 characters.
    **
    ** Unlike memcmp() this routine is guaranteed to return the difference
    ** in the values of the last digit if the only difference is in the
    ** last digit.  So, for example,
    **
    **      totypeCompare2pow63("9223372036854775800")
    **
    ** will return -8.
    */
            public static int totypeCompare2pow63(byte* zNum)
            {
                int c = 0;
                int i;
                /* 012345678901234567 */
                fixed (char* pow63 = "922337203685477580")
                    for (i = 0; c == 0 && i < 18; i++)
                    {
                        c = (zNum[i] - pow63[i]) * 10;
                    }
                if (c == 0)
                {
                    c = zNum[18] - '8';
                }
                return c;
            }

            /*
            ** The string z[] is an text representation of a real number.
            ** Convert this string to a double and write it into *pResult.
            **
            ** The string is not necessarily zero-terminated.
            **
            ** Return TRUE if the result is a valid real number (or integer) and FALSE
            ** if the string is empty or contains extraneous text.  Valid numbers
            ** are in one of these formats:
            **
            **    [+-]digits[E[+-]digits]
            **    [+-]digits.[digits][E[+-]digits]
            **    [+-].digits[E[+-]digits]
            **
            ** Leading and trailing whitespace is ignored for the purpose of determining
            ** validity.
            **
            ** If some prefix of the input string is a valid number, this routine
            ** returns FALSE but it still converts the prefix and writes the result
            ** into *pResult.
            */
            public static bool totypeAtoF(byte* z, double* pResult, int length)
            {
                byte* zEnd = z + length;
                /* sign * significand * (10 ^ (esign * exponent)) */
                int sign = 1;    /* sign of significand */
                long s = 0;       /* significand */
                int d = 0;       /* adjust exponent for shifting decimal point */
                int esign = 1;   /* sign of exponent */
                int e = 0;       /* exponent */
                int eValid = 1;  /* True exponent is either not used or is well-formed */
                double result;
                int nDigits = 0;
                int nonNum = 0;

                *pResult = 0.0;   /* Default return value, in case of an error */

                /* skip leading spaces */
                while (z < zEnd && totypeIsspace(*z))
                    z++;

                if (z >= zEnd)
                    return false;

                /* get sign of significand */
                if (*z == '-')
                {
                    sign = -1;
                    z++;
                }
                else if (*z == '+')
                {
                    z++;
                }

                /* skip leading zeroes */
                while (z < zEnd && z[0] == '0')
                {
                    z++;
                    nDigits++;
                }

                /* copy max significant digits to significand */
                while (z < zEnd && totypeIsdigit(*z) && s < ((LARGEST_INT64 - 9) / 10))
                {
                    s = s * 10 + (*z - '0');
                    z++; nDigits++;
                }

                /* skip non-significant significand digits
                ** (increase exponent by d to shift decimal left) */
                while (z < zEnd && totypeIsdigit(*z))
                {
                    z++;
                    nDigits++;
                    d++;
                }

                if (z >= zEnd)
                    goto totype_atof_calc;

                /* if decimal point is present */
                if (*z == '.')
                {
                    z++;
                    /* copy digits from after decimal to significand
                    ** (decrease exponent by d to shift decimal right) */
                    while (z < zEnd && totypeIsdigit(*z) && s < ((LARGEST_INT64 - 9) / 10))
                    {
                        s = s * 10 + (*z - '0');
                        z++;
                        nDigits++;
                        d--;
                    }
                    /* skip non-significant digits */
                    while (z < zEnd && totypeIsdigit(*z))
                    {
                        z++;
                        nDigits++;
                    }
                }

                if (z >= zEnd)
                    goto totype_atof_calc;

                /* if exponent is present */
                if (*z == 'e' || *z == 'E')
                {
                    z++;
                    eValid = 0;

                    if (z >= zEnd)
                        goto totype_atof_calc;

                    /* get sign of exponent */
                    if (*z == '-')
                    {
                        esign = -1;
                        z++;
                    }
                    else if (*z == '+')
                    {
                        z++;
                    }
                    /* copy digits to exponent */
                    while (z < zEnd && totypeIsdigit(*z))
                    {
                        e = e < 10000 ? (e * 10 + (*z - '0')) : 10000;
                        z++;
                        eValid = 1;
                    }
                }

                /* skip trailing spaces */
                if (nDigits != 0 && eValid != 0)
                {
                    while (z < zEnd && totypeIsspace(*z)) z++;
                }

            totype_atof_calc:
                /* adjust exponent by d, and update sign */
                e = (e * esign) + d;
                if (e < 0)
                {
                    esign = -1;
                    e *= -1;
                }
                else
                {
                    esign = 1;
                }

                /* if 0 significand */
                if (s == 0)
                {
                    /* In the IEEE 754 standard, zero is signed.
                    ** Add the sign if we've seen at least one digit */
                    result = (sign < 0 && nDigits != 0) ? -(double)0 : (double)0;
                }
                else
                {
                    /* attempt to reduce exponent */
                    if (esign > 0)
                    {
                        while (s < (LARGEST_INT64 / 10) && e > 0)
                        {
                            e--;
                            s *= 10;
                        }
                    }
                    else
                    {
                        while ((s % 10) == 0 && e > 0)
                        {
                            e--;
                            s /= 10;
                        }
                    }

                    /* adjust the sign of significand */
                    s = sign < 0 ? -s : s;

                    /* if exponent, scale significand as appropriate
                    ** and store in result. */
                    if (e != 0)
                    {
                        double scale = 1.0;
                        /* attempt to handle extremely small/large numbers better */
                        if (e > 307 && e < 342)
                        {
                            while ((e % 308) != 0)
                            {
                                scale *= 1.0e+1;
                                e -= 1;
                            }

                            if (esign < 0)
                            {
                                result = s / scale;
                                result /= 1.0e+308;
                            }
                            else
                            {
                                result = s * scale;
                                result *= 1.0e+308;
                            }

                        }
                        else if (e >= 342)
                        {
                            if (esign < 0)
                            {
                                result = 0.0 * s;
                            }
                            else
                            {
                                result = 1e308 * 1e308 * s;  /* Infinity */
                            }
                        }
                        else
                        {
                            /* 1.0e+22 is the largest power of 10 than can be
                            ** represented exactly. */
                            while ((e % 22) != 0)
                            {
                                scale *= 1.0e+1; e -= 1;
                            }

                            while (e > 0)
                            {
                                scale *= 1.0e+22; e -= 22;
                            }

                            if (esign < 0)
                            {
                                result = s / scale;
                            }
                            else
                            {
                                result = s * scale;
                            }
                        }
                    }
                    else
                    {
                        result = (double)s;
                    }
                }

                /* store the result */
                *pResult = result;

                /* return true if number and no extra non-whitespace chracters after */
                return z >= zEnd && nDigits > 0 && eValid != 0 && nonNum == 0;
            }


            /*
    ** The variable-length integer encoding is as follows:
    **
    ** KEY:
    **         A = 0xxxxxxx    7 bits of data and one flag bit
    **         B = 1xxxxxxx    7 bits of data and one flag bit
    **         C = xxxxxxxx    8 bits of data
    **
    **  7 bits - A
    ** 14 bits - BA
    ** 21 bits - BBA
    ** 28 bits - BBBA
    ** 35 bits - BBBBA
    ** 42 bits - BBBBBA
    ** 49 bits - BBBBBBA
    ** 56 bits - BBBBBBBA
    ** 64 bits - BBBBBBBBC
    */

            /*
            ** Write a 64-bit variable-length integer to memory starting at p[0].
            ** The length of data write will be between 1 and 9 bytes.  The number
            ** of bytes written is returned.
            **
            ** A variable-length integer consists of the lower 7 bits of each byte
            ** for all bytes that have the 8th bit set and one byte with the 8th
            ** bit clear.  Except, if we get to the 9th byte, it stores the full
            ** 8 bits and is the last byte.
            */
            public static int sqlite3PutVarint(byte* p, ulong v)
            {
                int i, j, n;
                byte[] buf = new byte[10];
                if ((v & (((ulong)0xff000000) << 32)) != 0)
                {
                    p[8] = (byte)v;

                    v >>= 8;

                    for (i = 7; i >= 0; i--)
                    {
                        p[i] = (byte)((v & 0x7f) | 0x80);

                        v >>= 7;
                    }

                    return 9;
                }

                n = 0;

                do
                {
                    buf[n++] = (byte)((v & 0x7f) | 0x80);
                    v >>= 7;
                }
                while (v != 0);

                buf[0] &= 0x7f;

                System.Diagnostics.Debug.Assert(n <= 9);

                for (i = 0, j = n - 1; j >= 0; j--, i++)
                {
                    p[i] = buf[j];
                }

                return n;
            }

            /*
            ** This routine is a faster version of sqlite3PutVarint() that only
            ** works for 32-bit positive integers and which is optimized for
            ** the common case of small integers.  A MACRO version, putVarint32,
            ** is provided which inlines the single-byte case.  All code should use
            ** the MACRO version as this function assumes the single-byte case has
            ** already been handled.
            */
            public static int sqlite3PutVarint32(byte* p, uint v)
            {

#if true //putVarint32

                if ((v & ~0x7f) == 0)
                {
                    p[0] = (byte)v;
                    return 1;
                }

#endif

                if ((v & ~0x3fff) == 0)
                {
                    p[0] = (byte)((v >> 7) | 0x80);

                    p[1] = (byte)(v & 0x7f);

                    return 2;
                }

                // 1FFFFF
                if ((v & ~0x1fffff) == 0)
                {
                    p[0] = (byte)((v >> 14) | 0x80);

                    p[1] = (byte)((v >> 7) | 0x80);

                    p[2] = (byte)(v & 0x7f);

                    return 3;
                }

                return sqlite3PutVarint(p, v);
            }




            /*
            ** Read a 64-bit variable-length integer from memory starting at p[0].
            ** Return the number of bytes read.  The value is stored in *v.
            */
            public static byte sqlite3GetVarint(byte* p, ulong* v)
            {

                uint a, b, s;

                a = *p;
                /* a: p0 (unmasked) */
                if ((a & 0x80) == 0)
                {
                    *v = a;
                    return 1;
                }

                p++;
                b = *p;
                /* b: p1 (unmasked) */
                if ((b & 0x80) == 0)
                {
                    a &= 0x7f;
                    a = a << 7;
                    a |= b;
                    *v = a;
                    return 2;
                }

                /* Verify that constants are precomputed correctly */
                System.Diagnostics.Debug.Assert(SLOT_2_0 == ((0x7f << 14) | (0x7f)));
                System.Diagnostics.Debug.Assert(SLOT_4_2_0 == ((0xfU << 28) | (0x7f << 14) | (0x7f)));

                p++;
                a = a << 14;
                a |= *p;
                /* a: p0<<14 | p2 (unmasked) */
                if ((a & 0x80) == 0)
                {
                    a &= SLOT_2_0;
                    b &= 0x7f;
                    b = b << 7;
                    a |= b;
                    *v = a;
                    return 3;
                }

                /* CSE1 from below */
                a &= SLOT_2_0;
                p++;
                b = b << 14;
                b |= *p;

                /* b: p1<<14 | p3 (unmasked) */
                if ((b & 0x80) == 0)
                {
                    b &= SLOT_2_0;
                    /* moved CSE1 up */
                    /* a &= (0x7f<<14)|(0x7f); */
                    a = a << 7;
                    a |= b;
                    *v = a;
                    return 4;
                }

                /* a: p0<<14 | p2 (masked) */
                /* b: p1<<14 | p3 (unmasked) */
                /* 1:save off p0<<21 | p1<<14 | p2<<7 | p3 (masked) */
                /* moved CSE1 up */
                /* a &= (0x7f<<14)|(0x7f); */
                b &= SLOT_2_0;
                s = a;
                /* s: p0<<14 | p2 (masked) */

                p++;
                a = a << 14;
                a |= *p;
                /* a: p0<<28 | p2<<14 | p4 (unmasked) */
                if ((a & 0x80) == 0)
                {
                    /* we can skip these cause they were (effectively) done above in calc'ing s */
                    /* a &= (0x7f<<28)|(0x7f<<14)|(0x7f); */
                    /* b &= (0x7f<<14)|(0x7f); */
                    b = b << 7;
                    a |= b;
                    s = s >> 18;
                    *v = ((ulong)s) << 32 | a;
                    return 5;
                }

                /* 2:save off p0<<21 | p1<<14 | p2<<7 | p3 (masked) */
                s = s << 7;
                s |= b;
                /* s: p0<<21 | p1<<14 | p2<<7 | p3 (masked) */

                p++;
                b = b << 14;
                b |= *p;

                /* b: p1<<28 | p3<<14 | p5 (unmasked) */
                if ((b & 0x80) == 0)
                {
                    /* we can skip this cause it was (effectively) done above in calc'ing s */
                    /* b &= (0x7f<<28)|(0x7f<<14)|(0x7f); */
                    a &= SLOT_2_0;
                    a = a << 7;
                    a |= b;
                    s = s >> 18;
                    *v = ((ulong)s) << 32 | a;
                    return 6;
                }

                p++;
                a = a << 14;
                a |= *p;

                /* a: p2<<28 | p4<<14 | p6 (unmasked) */
                if ((a & 0x80) == 0)
                {
                    a &= SLOT_4_2_0;
                    b &= SLOT_2_0;
                    b = b << 7;
                    a |= b;
                    s = s >> 11;
                    *v = ((ulong)s) << 32 | a;
                    return 7;
                }

                /* CSE2 from below */
                a &= SLOT_2_0;
                p++;
                b = b << 14;
                b |= *p;

                /* b: p3<<28 | p5<<14 | p7 (unmasked) */
                if ((b & 0x80) == 0)
                {
                    b &= SLOT_4_2_0;
                    /* moved CSE2 up */
                    /* a &= (0x7f<<14)|(0x7f); */
                    a = a << 7;
                    a |= b;
                    s = s >> 4;
                    *v = ((ulong)s) << 32 | a;
                    return 8;
                }

                p++;
                a = a << 15;
                a |= *p;
                /* a: p4<<29 | p6<<15 | p8 (unmasked) */

                /* moved CSE2 up */
                /* a &= (0x7f<<29)|(0x7f<<15)|(0xff); */
                b &= SLOT_2_0;
                b = b << 8;
                a |= b;

                s = s << 4;
                b = p[-4];
                b &= 0x7f;
                b = b >> 3;
                s |= b;

                *v = ((ulong)s) << 32 | a;

                return 9;
            }

            /*
    ** Read a 32-bit variable-length integer from memory starting at p[0].
    ** Return the number of bytes read.  The value is stored in *v.
    **
    ** If the varint stored in p[0] is larger than can fit in a 32-bit unsigned
    ** integer, then set *v to 0xffffffff.
    **
    ** A MACRO version, getVarint32, is provided which inlines the 
    ** single-byte case.  All code should use the MACRO version as 
    ** this function assumes the single-byte case has already been handled.
    */
            public static byte sqlite3GetVarint32(byte* p, uint* v)
            {
                uint a, b;

                /* The 1-byte case.  Overwhelmingly the most common.  Handled inline
                ** by the getVarin32() macro */
                a = *p;
                /* a: p0 (unmasked) */
#if true //getVarint32
                if ((a & 0x80) == 0)
                {
                    /* Values between 0 and 127 */
                    *v = a;
                    return 1;
                }
#endif

                /* The 2-byte case */
                p++;
                b = *p;
                /* b: p1 (unmasked) */
                if ((b & 0x80) == 0)
                {
                    /* Values between 128 and 16383 */
                    a &= 0x7f;
                    a = a << 7;
                    *v = a | b;
                    return 2;
                }

                /* The 3-byte case */
                p++;
                a = a << 14;
                a |= *p;
                /* a: p0<<14 | p2 (unmasked) */
                if ((a & 0x80) == 0)
                {
                    /* Values between 16384 and 2097151 */
                    a &= (0x7f << 14) | (0x7f);
                    b &= 0x7f;
                    b = b << 7;
                    *v = a | b;
                    return 3;
                }

                /* A 32-bit varint is used to store size information in btrees.
                ** Objects are rarely larger than 2MiB limit of a 3-byte varint.
                ** A 3-byte varint is sufficient, for example, to record the size
                ** of a 1048569-byte BLOB or string.
                **
                ** We only unroll the first 1-, 2-, and 3- byte cases.  The very
                ** rare larger cases can be handled by the slower 64-bit varint
                ** routine.
                */
#if true
                {
                    ulong v64;
                    byte n;

                    p -= 2;
                    n = sqlite3GetVarint(p, &v64);

                    System.Diagnostics.Debug.Assert(n > 3 && n <= 9);

                    if ((v64 & SQLITE_MAX_U32) != v64)
                    {
                        *v = 0xffffffff;

                    }
                    else
                    {
                        *v = (uint)v64;
                    }

                    return n;
                }

#else
              /* For following code (kept for historical record only) shows an
              ** unrolling for the 3- and 4-byte varint cases.  This code is
              ** slightly faster, but it is also larger and much harder to test.
              */
              p++;
              b = b<<14;
              b |= *p;
              /* b: p1<<14 | p3 (unmasked) */
              if (!(b&0x80))
              {
                /* Values between 2097152 and 268435455 */
                b &= (0x7f<<14)|(0x7f);
                a &= (0x7f<<14)|(0x7f);
                a = a<<7;
                *v = a | b;
                return 4;
              }

              p++;
              a = a<<14;
              a |= *p;
              /* a: p0<<28 | p2<<14 | p4 (unmasked) */
              if (!(a&0x80))
              {
                /* Values  between 268435456 and 34359738367 */
                a &= SLOT_4_2_0;
                b &= SLOT_4_2_0;
                b = b<<7;
                *v = a | b;
                return 5;
              }

              /* We can only reach this point when reading a corrupt database
              ** file.  In that case we are not in any hurry.  Use the (relatively
              ** slow) general-purpose sqlite3GetVarint() routine to extract the
              ** value. */
              {
                u64 v64;
                u8 n;

                p -= 4;
                n = sqlite3GetVarint(p, &v64);
                assert( n>5 && n<=9 );
                *v = (u32)v64;
                return n;
              }
#endif
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public static unsafe void Copy(byte* src, int srcIndex, byte* dst, int dstIndex, int count)
            {
                // The following fixed statement pins the location of the src and dst objects
                // in memory so that they will not be moved by garbage collection.

                // Loop over the count in blocks of 4 bytes, copying an integer (4 bytes) at a time:
                for (int i = 0; i < count / 8; i++)
                {
                    *((long*)dst) = *((long*)src);
                    dst += 8;
                    src += 8;
                }

                // Complete the copy by moving any bytes that weren't moved in blocks of 4:
                for (int i = 0; i < count % 8; i++)
                {
                    *dst = *src;
                    dst++;
                    src++;
                }
            }

            /// <summary>
            /// Returns a value built from the specified number of bytes from the given buffer,
            /// starting at index.
            /// </summary>
            /// <param name="buffer">The data in byte array format</param>
            /// <param name="startIndex">The first index to use</param>
            /// <param name="bytesToConvert">The number of bytes to use</param>
            /// <returns>The value built from the given bytes</returns>
            private static long NumberFromBytes(byte* buffer, int startIndex, int bytesToConvert)
            {
                long ret = 0;
                for (int i = 0; i < bytesToConvert; i++)
                {
                    ret = unchecked((ret << 8) | buffer[startIndex + i]);
                }
                return ret;
            }

            /// <summary>
            /// Returns a value built from the specified number of bytes from the given buffer,
            /// starting at index.
            /// </summary>
            /// <param name="buffer">The data in byte array format</param>
            /// <param name="startIndex">The first index to use</param>
            /// <param name="bytesToConvert">The number of bytes to use</param>
            /// <returns>The value built from the given bytes</returns>
            private static long FromBytes(byte[] buffer, int startIndex, int bytesToConvert)
            {
                long ret = 0;
                for (int i = 0; i < bytesToConvert; i++)
                {
                    ret = unchecked((ret << 8) | buffer[startIndex + i]);
                }
                return ret;
            }

            /// <summary>
            /// Returns a value built from the specified number of bytes from the given buffer,
            /// starting at index.
            /// </summary>
            /// <param name="buffer">The data in byte array format</param>
            /// <param name="length">The number of bytes to use</param>
            /// <returns>The value built from the given bytes</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public static long BytesToNumber(byte* buffer, int length)
            {
                long number = 0;
                for (int i = 0; i < length; i++)
                {
                    /// big endian
                    /// number = unchecked((number << 8) | buffer[i]);

                    // little endian
                    number = unchecked((number << 8) | buffer[length - 1 - i]);
                }
                return number;
            }


            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public unsafe static double BytesToDouble(byte* buffer, int length)
            {
                double db = 0; //  BitConverter.ToDouble(buffer)
                long lgn = (long)db;
                return db;
            }

            public static class Hashing
            {
                const int p = 16777619;

                /// <summary>
                /// Had no collisions also but it took 7 seconds to execute!
                /// </summary>
                /// <param name="buffer"></param>
                /// <param name="length"></param>
                /// <returns></returns>
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                public static long CalculateI(byte* buffer, ushort length)
                {
                    // 7.1 seconds
                    unchecked
                    {
                        long hash = (long)2166136261;

                        for (int i = 0; i < length; i++)
                            hash = (hash ^ buffer[i]) * p;

                        hash += hash << 13;
                        hash ^= hash >> 7;
                        hash += hash << 3;
                        hash ^= hash >> 17;
                        hash += hash << 5;
                        return hash;
                    }
                }

                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                public static long CalculateII(byte* buffer, int length)
                {
                    long hash = 0;
                    for (var i = 0; i < length; i++)
                        hash = (hash << 4) + buffer[i];
                    return hash;
                }


                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                public static long CalculateIII(byte* buffer, int length)
                {
                    // http://www.java2s.com/Code/CSharp/Data-Types/Gethashcodeforabytearray.htm
                    if (length == 0)
                        return 0;

                    int i = length;
                    long hc = i + 1;

                    while (--i >= 0)
                    {
                        hc *= 257;
                        hc ^= buffer[i];
                    }

                    return hc;
                }

                /// <summary>
                /// Using native hash calculation over byte array. Converts the pointer into bytes array (performance impact)
                /// </summary>
                /// <param name="buffer"></param>
                /// <param name="length"></param>
                /// <returns></returns>
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                public static long CalculateNative(byte* buffer, int length)
                {
                    byte[] data = new byte[length];
                    fixed (byte* pData = data)
                        CopyMemory(pData, buffer, length);
                    //  memcpy(pData, buffer, length);

                    var hash = RuntimeHelpers.GetHashCode(data);

                    return hash;
                }

                /// <summary>
                /// http://www.javamex.com/tutorials/collections/strong_hash_code_implementation_2.shtml
                /// </summary>
                public static class BSon
                {
                    // http://www.javamex.com/tutorials/collections/strong_hash_code_implementation_2.shtml

                    private static ulong[] createLookupTable()
                    {
                        var byteTable = new ulong[256];
                        ulong h = 0x544B2FBACAAF1684L;

                        for (int i = 0; i < 256; i++)
                        {
                            for (int j = 0; j < 31; j++)
                            {
                                h = (h >> 7) ^ h;
                                h = (h << 11) ^ h;
                                h = (h >> 10) ^ h;
                            }
                            byteTable[i] = h;
                        }

                        return byteTable;
                    }

                    private readonly static ulong[] byteTable = createLookupTable();
                    private const ulong HSTART = 0xBB40E64DA205B064L;
                    private const ulong HMULT = 7664345821815920749L;

                    /// <summary>
                    /// Almost perfect hash function. Extremely small collisions, almost none
                    /// </summary>
                    /// <param name="data"></param>
                    /// <returns></returns>
                    [MethodImpl(MethodImplOptions.AggressiveInlining)]
                    [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                    public static ulong Calculate(byte[] data)
                    {
                        fixed (byte* pData = data)
                            return Calculate(pData, data.Length);
                    }

                    /// <summary>
                    /// Almost perfect hash function. Extremely small collisions, almost none
                    /// </summary>
                    /// <param name="data"></param>
                    /// <param name="len"></param>
                    /// <returns></returns>
                    [MethodImpl(MethodImplOptions.AggressiveInlining)]
                    [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                    public static ulong Calculate(byte* data, int len)
                    {
                        ulong h = HSTART;
                        const ulong hmult = HMULT;

                        for (int i = 0; i < len; i++)
                        {
                            h = (h * hmult) ^ byteTable[data[i] & 0xff];
                        }

                        return h;
                    }
                }
            }

            /// <summary>
            /// Extract 4-bit number from bytes buffer
            /// </summary>
            /// <param name="buffer"></param>
            /// <param name="realLeft">The left 4-bits. Otherwise, the right 4-bits</param>
            /// <returns></returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public static byte BytesToSmallNumber(byte* buffer, bool realLeft)
            {
                byte number;

                if (realLeft)
                {
                    number = (byte)(buffer[0] << 1);
                    int s = (buffer[0] & 0xF);
                    return (byte)(number - (s + s));
                }
                else
                {
                    number = (byte)(buffer[0] << 2);
                    int s = (buffer[0] & 0xFF);
                    return (byte)(number - s);
                }
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public static void BytesFromSmallNumber(byte* buffer, byte number, bool storeLeft)
            {
                if (storeLeft)
                {
                    int b = (number >> 1);
                    int s = (buffer[0] & 0xF); // retain lower 4 bits 
                    buffer[0] = (byte)(s + b);
                }
                else
                {
                    byte b = (byte)(number >> 2);
                    int s = (buffer[0] & 0xFF); // retain higher 4 bits 
                    buffer[0] = (byte)(s + b);
                }
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public static long BytesToNumberOpt(byte* buffer, int length)
            {
                // uint nValue = array[0] + (array[1] << 8) + (array[2] << 16) + (array[3] << 24);

                switch (length)
                {
                    case 1: return (*buffer);

                    case 2: return (*buffer) + (buffer[1] << 8);

                    case 3: return (*buffer) + (buffer[1] << 8) + (buffer[2] << 16);

                    case 4: return (*buffer) + (buffer[1] << 8) + (buffer[2] << 16) + (buffer[3] << 24);

                    case 5: return (*buffer) + (buffer[1] << 8) + (buffer[2] << 16) + (buffer[3] << 24) + (buffer[4] << 32);

                    default: return BytesToNumber(buffer, length);
                }
            }

            /// <summary>
            /// Converts number to bytes, with specified length
            /// </summary>
            /// <param name="buffer"></param>
            /// <param name="value"></param>
            /// <param name="length"></param>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public static void NumberToBytes(byte* buffer, long value, int length)
            {
                switch (length)
                {
                    case 1:
                        buffer[0] = (byte)value;
                        break;
                    case 2:
                        buffer[0] = (byte)value;
                        buffer[1] = (byte)(value >> 8);
                        break;
                    case 3:
                        buffer[0] = (byte)value;
                        buffer[1] = (byte)(value >> 8);
                        buffer[2] = (byte)(value >> 16);
                        break;
                    case 4:
                        buffer[0] = (byte)value;
                        buffer[1] = (byte)(value >> 8);
                        buffer[2] = (byte)(value >> 16);
                        buffer[3] = (byte)(value >> 24);
                        break;
                    case 5:
                        buffer[0] = (byte)value;
                        buffer[1] = (byte)(value >> 8);
                        buffer[2] = (byte)(value >> 16);
                        buffer[3] = (byte)(value >> 24);
                        buffer[4] = (byte)(value >> 32);
                        break;
                    case 6:
                        buffer[0] = (byte)value;
                        buffer[1] = (byte)(value >> 8);
                        buffer[2] = (byte)(value >> 16);
                        buffer[3] = (byte)(value >> 24);
                        buffer[4] = (byte)(value >> 32);
                        buffer[5] = (byte)(value >> 40);
                        break;
                    case 7:
                        buffer[0] = (byte)value;
                        buffer[1] = (byte)(value >> 8);
                        buffer[2] = (byte)(value >> 16);
                        buffer[3] = (byte)(value >> 24);
                        buffer[4] = (byte)(value >> 32);
                        buffer[5] = (byte)(value >> 40);
                        buffer[6] = (byte)(value >> 48);
                        break;
                    case 8:
                        buffer[0] = (byte)value;
                        buffer[1] = (byte)(value >> 8);
                        buffer[2] = (byte)(value >> 16);
                        buffer[3] = (byte)(value >> 24);
                        buffer[4] = (byte)(value >> 32);
                        buffer[5] = (byte)(value >> 40);
                        buffer[6] = (byte)(value >> 48);
                        buffer[7] = (byte)(value >> 56);
                        break;
                    default: throw new Exception("Unsupported");
                }

            }
        }

        [DllImport("msvcrt.dll", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int strncmp(string lhs, string rhs, ushort count);

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl, SetLastError = false)]
        [SuppressUnmanagedCodeSecurity]
        [SecurityCritical]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
        public static extern int strncmp(byte* lhs, byte* rhs, ushort count);

        [DllImport("msvcrt.dll", SetLastError = true)]
        public static extern int strncpy(char* dest, char* src, uint n);

        [DllImport("msvcrt.dll", SetLastError = true)]
        public static extern int strncpy(char* dest, [MarshalAs(UnmanagedType.LPStr)] string src);

        [DllImport("msvcrt.dll", SetLastError = true)]
        public static extern int strlen(char* str);

        /// <summary>
        /// http://research.microsoft.com/en-us/um/redmond/projects/invisible/src/crt/memcmp.c.htm
        /// </summary>
        /// <param name="Ptr1"></param>
        /// <param name="Ptr2"></param>
        /// <param name="Count"></param>
        /// <returns></returns>
        public static int memoryCompare5(byte* Ptr1, byte* Ptr2, int Count)
        {
            int v = 0;
            byte* p1 = (byte*)Ptr1;
            byte* p2 = (byte*)Ptr2;

            while (Count-- > 0 && v == 0)
            {
                v = *(p1++) - *(p2++);
            }

            return v;
        }

       

        /// <summary>
        /// VORON
        /// </summary>
	public unsafe static class MemoryUtils
	{	 
        /// <summary>
        /// Voron opimized comparision
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <param name="n"></param>
        /// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int CompareVoron(byte* lhs, byte* rhs, int n)
		{
			if (n == 0)
				return 0;

            var sizeOfUInt = sizeof(uint);

			if (n > sizeOfUInt)
			{
				var lUintAlignment = (long)lhs % sizeOfUInt;
				var rUintAlignment = (long)rhs % sizeOfUInt;

				if (lUintAlignment != 0 && lUintAlignment == rUintAlignment)
				{
					var toAlign = sizeOfUInt - lUintAlignment;
					while (toAlign > 0)
					{
						var r = *lhs++ - *rhs++;
						if (r != 0)
							return r;
						n--;

						toAlign--;
					}
				}

				uint* lp = (uint*)lhs;
				uint* rp = (uint*)rhs;

				while (n > sizeOfUInt)
				{
					if (*lp != *rp)
						break;

					lp++;
					rp++;

					n -= sizeOfUInt;
				}

				lhs = (byte*)lp;
				rhs = (byte*)rp;
			}

			while (n > 0)
			{
				var r = *lhs++ - *rhs++;
				if (r != 0)
					return r;
				n--;
			}

			return 0;
		}
	}

        /// <summary>
        /// http://ayende.com/blog/170114/excerpts-from-the-ravendb-performance-team-report-optimizing-compare-the-circle-of-life-a-post-mortem
        /// </summary>
        /// <param name="bpx"></param>
        /// <param name="bpy"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int CompareRaven(byte* bpx, byte* bpy, int n)
        {
            switch (n)
            {
                case 0: return 0;
                case 1: return *bpx - *bpy;
                case 2:
                    {
                        int v = *bpx - *bpy;
                        if (v != 0)
                            return v;

                        bpx++;
                        bpy++;

                        return *bpx - *bpy;
                    }

                default: return memcmp(bpx, bpy, n);
            }
        }

        /* Compare, at most, the first Count chars in strings pointed by pSrt1, pSrt2.
 * Return 0 if same, < 0 if pStr1 less then pStr2, > 0 otherwise.
 */
        public static int _tcsncmp(byte* pStr1, byte* pStr2, int Count)
        {
            byte c1, c2;
            uint v;

            if (Count == 0)
                return 0;

            do
            {
                c1 = *pStr1++;
                c2 = *pStr2++;
                /* the casts are necessary when pStr1 is shorter & char is signed */
                v = (uint)c1 - (uint)c2;
            } while ((v == 0) && (--Count > 0));   // while ((v == 0) && (c1 != '\0') && (--Count > 0));

            return (int)v;
        }

        public static int memoryCompare1(byte* lhs, byte* rhs, int n)
        {
            uint* lp = (uint*)lhs;
            uint* rp = (uint*)rhs;
            uint  l;
            uint  r;
            int count = (n / sizeof(uint)) / 4;

            var rem = (n / sizeof(uint)) % 4;


            while (count-- > 0)
            {
                if ((l = *lp++) != (r = *rp++))
                {
                    return (l < r) ? -1 : 1;
                }
                if ((l = *lp++) != (r = *rp++))
                {
                    return (l < r) ? -1 : 1;
                }
                if ((l = *lp++) != (r = *rp++))
                {
                    return (l < r) ? -1 : 1;
                }
                if ((l = *lp++) != (r = *rp++))
                {
                    return (l < r) ? -1 : 1;
                }
            }

            // process the rest
            return 0;
        }

        public static int memoryCompare3(byte* lhs, byte* rhs, int n)
        {
            void* endLHS = lhs + n / sizeof(uint);
            while (lhs < endLHS)
            {
                int i = *lhs - *rhs;
                if (i != 0) return i > 0 ? 1 : -1;
                lhs++;
                rhs++;
            }
            //more code for the remaining bytes or call memoryCompare<char>(lhs, rhs, n%sizeof(T));
            return 0;
        }

        public static int memoryCompare4(byte* lhs, byte* rhs, int n) 
        {
            void* endLHS = &lhs[n];
            while (lhs < endLHS)
            {
                //A good optimiser will keep these values in register
                //and may even be clever enough to just retest the flags
                //before incrementing the pointers iff we loop again.
                //gcc -O3 did the optimisation very well.
                if (*lhs > *rhs) return 1;
                if (*lhs++ < *rhs++) return -1;
            }
            //more code for the remaining bytes or call memoryCompare<char>(lhs, rhs, n%sizeof(T));
            return 0;
        }

        public static int memoryCompare0(byte* lhs, byte* rhs, int count)
        {
            long diff = 0;

            // Test the first few bytes until we are 32-bit aligned.
            while ((count & 0x3) != 0 && diff != 0)
            {
                diff = (byte*)lhs - (byte*)rhs;
                count--;
                lhs++;
                rhs++;
            }

            // Test the next set of 32-bit integers using comparisons with
            // aligned data.

            uint* _lhs = (uint*)lhs;
            uint* _rhs = (uint*)rhs;
            while (count > sizeof(uint) && diff != 0)
            {
                diff = _lhs - _rhs;
                count -= sizeof(uint);
                _lhs++;
                _rhs++;
            }

            // now do final bytes.
            while (count > 0 && diff != 0)
            {
                diff = (byte*)lhs - (byte*)rhs;
                count--;
                lhs++;
                rhs++;
            }

            return (int) diff;
            //return (int)(diff / Math.Abs(diff));
        }
    }
}
