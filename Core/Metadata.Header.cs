﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DigitalToolworks.Data.MStorage.Metadata
{
    internal unsafe static class Header
    {
        /// <summary>
        /// Size of the meta data structure in bytes. The structure is aligned at the begin of the meta data stream. 
        /// The specific size for this version is 52 bytes, but we set much larger size (12 MB) - for future use
        /// </summary>
        public const int MetaBlockSize = 12 * 1024 * 1024;

        /// <summary>
        /// Marker value used for checking the header format validability
        /// </summary>
        public const ulong HeaderMarker = 0xB16BAADC0DEF0015;

        /// <summary>
        /// MTable metadata root header
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct Table
        {
            /// <summary>
            /// Identifier for checking of the header is really mtable header [ FMT-MTABLE ]
            /// </summary>
            public ulong FormatMarker;

            /// <summary>
            /// Version of the table state (for future use)
            /// </summary>
            public long RevisitonVersion;

            /// <summary>
            /// Unique identifier for the table
            /// </summary>
            public uint TableID;

            /// <summary>
            /// Table display name
            /// </summary>
            public fixed char TableName[1024];     // max table name (1024)

            /// <summary>
            /// Maximum capacity for the data indexer
            /// </summary>
            public ulong MaxIndexerCapacity;

            /// <summary>
            /// Maximum capacity for the table data storage
            /// </summary>
            public ulong MaxDataCapacity;

            /// <summary>
            /// The timestamp when the table was created (UTC)
            /// </summary>
            public ulong TimestampCreated;

            /// <summary>
            /// The timestamp when the table was modified (UTC)
            /// </summary>
            public ulong TimestampModified;

            /// <summary>
            /// For future use
            /// </summary>
            public int Flags;

            /// <summary>
            /// Number of Total columns in table (it could contain columns marked as removed)
            /// </summary>
            public uint ColumnsCount;
           
            /// <summary>
            /// Pointer to the columns collection
            /// </summary>
            public Column* Columns;

            /// <summary>
            /// MTable Column metadata header
            /// </summary>
            [StructLayout(LayoutKind.Sequential)]
            public struct Column
            {
                /// <summary>
                /// The id for the column, which is the unique ordinal index number in the core table
                /// </summary>
                public ushort ColumnID;

                /// <summary>
                /// The name of the table
                /// </summary>
                public fixed char ColumnName[512];

                /// <summary>
                /// The visibility index position of the column
                /// </summary>
                public ushort VisibleIndex;

                /// <summary>
                /// Type of data for the column
                /// </summary>
                public byte DataType;

                /// <summary>
                /// Size of the data type for the column
                /// </summary>
                public ushort DataTypeSize;

                /// <summary>
                /// Flag indicating if the column was marked as removed
                /// </summary>
                public bool Removed;

                /// <summary>
                /// For future use
                /// </summary>
                public uint Flags;
            }
        }
        

        /// <summary>
        /// Header for the underlaying core memory table
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct Core
        {
            /// <summary>
            /// Total number of records in table
            /// </summary>
            public long Records;

            /// <summary>
            /// Number of fields in table
            /// </summary>
            public int Fields;

            /// <summary>
            /// Total data size
            /// </summary>
            public long DataSize;

            /// <summary>
            /// The total size of the blocks buffers
            /// </summary>
            public long BlocksBufferSize;

            /// <summary>
            /// The total size of the data in the blocks
            /// </summary>
            public long BlocksDataSize;

            /// <summary>
            /// Data indexer
            /// </summary>
            public DataIndexer Indexer;

            /// <summary>
            /// Header for the data indexer
            /// </summary>
            [StructLayout(LayoutKind.Sequential)]
            public struct DataIndexer
            {
                /// <summary>
                /// Total amount of data indexed
                /// </summary>
                public long IndexedDataSize;

                /// <summary>
                /// Number of indexes
                /// </summary>
                public ulong Indexes;

                /// <summary>
                /// The index content data in memory
                /// </summary>
                public byte* Content;
            }
        }


        /// <summary>
        /// Serializes the header structure to stream
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="header"></param>
        public static void SerializeHeader<T>(System.IO.Stream stream, ref T header) where T : struct
        {            
            // Get size of struct
            int size = Marshal.SizeOf(typeof(T));
            byte[] data = new byte[size];

            // Initialize unmanaged memory.
            IntPtr p = Marshal.AllocHGlobal(size);

            try
            {
                // Copy struct to unmanaged memory.
                Marshal.StructureToPtr(header, p, false);

                // Copy from unmanaged memory to byte array.
                Marshal.Copy(p, data, 0, size);

                // Write to memory mapped file.
                stream.Write(data, 0, data.Length);
            }
            finally
            {
                // Free unmanaged memory.
                Marshal.FreeHGlobal(p);
                p = IntPtr.Zero;
            } 
        }


        /// <summary>
        /// Deserializes the header structure from stream
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="header"></param>
        public static void DeserializeHeader<T>(System.IO.Stream stream, ref T header) where T : struct
        {
            // Get size of struct
            int size = Marshal.SizeOf(typeof(T));
            byte[] data = new byte[size];

            // Initialize unmanaged memory.
            IntPtr p = Marshal.AllocHGlobal(size);

            try
            {
                // Read from stream
                stream.Read(data, 0, data.Length);

                // Copy from byte array to unmanaged memory.
                Marshal.Copy(data, 0, p, size);

                // Copy unmanaged memory to struct.
               header = (T) Marshal.PtrToStructure(p, typeof(T));
            }
            finally
            {
                // Free unmanaged memory.
                Marshal.FreeHGlobal(p);
                p = IntPtr.Zero;
            }
        }    
    }     
}
