﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DigitalToolworks.Data
{
    public unsafe abstract class IDataStorage : IDisposable
    {
        private long _rowCount;
        private long _dataSize;

        public readonly static Encoding Encoder = Encoding.Default; // Encoding.UTF8;

        public abstract void Setup();
        public abstract void Release();
        public abstract void Complete();

        public abstract IEnumerable<IRow> GetRows(long startRow, long endRow, params int[] fields);

        public virtual long RowCount { get { return _rowCount; } } // Interlocked.Read(ref _rowCount); } }
        public virtual long DataSize { get { return Interlocked.Read(ref _dataSize); } }
        public abstract int ColumnCount { get; }

        protected virtual void IncreaseDataSize(long increaseBy)
        {
            if (Interlocked.Add(ref _dataSize, increaseBy) < 0)
                throw new InvalidOperationException("Storage data size dropped bellow zero (" + this.DataSize + "). Implementation inspection suggested.");
        }

        protected virtual long IncreaseCount(int increaseBy = 1)
        {
            return Interlocked.Add(ref _rowCount, increaseBy);
        }

        protected virtual void SetRowCount(long rowCount)
        {
            _rowCount = rowCount;
        }

        void IDisposable.Dispose()
        {
            Release();
        }

        public abstract string Name { get; }

        public abstract class IRow
        {
            public abstract IEnumerator<string> GetEnumerator();
            public abstract string[] GetFields();
            private long _number;

            internal IRow(long number)
            {
                _number = number;
            }

            public long Number
            {
                get { return _number; }
                internal set { _number = value; }
            }
        }

        public abstract ITransaction BeginTransation();

        public abstract class ITransaction : IDisposable
        {
#if LOG_TRANSACT
            public const bool LogTransaction = true;
#else 
            public const bool LogTransaction = false;
#endif
            private bool _commited;
            private readonly IDataStorage _storage;

            public ITransaction(IDataStorage storage)
            {
                _storage = storage;
            }

            public virtual void Commit()
            {
                _commited = true;
            }

            public bool Commited { get { return _commited; } }

            public abstract void Rollback();

            void IDisposable.Dispose()
            {
                Commit();
            }
        }

        public abstract void AddRow(InsertRecordHeader header);
        public abstract void QueueRow(InsertRecordHeader header);
    }
}
