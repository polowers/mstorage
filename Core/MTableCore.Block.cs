﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DigitalToolworks.Data
{
    partial class MTableCore
    {
        public sealed unsafe class Block
        {
            private readonly MTableCore _storage;
            private readonly Indexer _indexer;
            private readonly AlignmentMode _mode;
            private readonly long _capacity;

            private int _registeredEntries;
            private long _registeredEntriesSize;


#if BLOCK_ZERO_CONFIG   // ############ Configuration for setting all parameters to zero, disabling the rowblocks buffer completely

            /// <summary>
            /// Percentage of the block size, to be used as buffered block space
            /// </summary>
            public const int BufferSizePercentage = 0;

            /// <summary>
            /// Minimum size for buffered block space
            /// </summary>
            public const int MinBufferSizeBytes = 0;



#elif BLOCK_MIN_CONFIG  // ############ Configuration for setting minimum parameters

            /// <summary>
            /// Percentage of the block size, to be used as buffered block space
            /// </summary>
            public const int BufferSizePercentage = 1;

            /// <summary>
            /// Minimum size for buffered block space
            /// </summary>
            public const int MinBufferSizeBytes = 0;


#else   // ############ Normal Default blocks parameter configuration

            /// <summary>
            /// Percentage of the block size, to be used as buffered block space
            /// </summary>
            public const int BufferSizePercentage = 10;

            /// <summary>
            /// Minimum size for buffered block space
            /// </summary>
            public const int MinBufferSizeBytes = 1024 * 2;
#endif

            /// <summary>
            /// Number of Rows in Entry Block
            /// </summary>
            public const int BlockRows = 1000 / 1;

            public Block(MTableCore storage, AlignmentMode mode)
            {
                _storage = storage;
                _mode = mode;
                _indexer = ((MTableCore)storage)._indexer;
                _capacity = ((MTableCore)storage).Capacity;
            }

            /// <summary>
            /// Registers block entry by adding its size. This will calculate the block buffer needed to be writen in the data stream, after this entry
            /// </summary>
            /// <param name="entrySize">The size of the entry in bytes</param>
            /// <param name="writeBlockBuffer">Delegate which will be called when block buffer is needed to be writen into the data stream, after this entry</param>
            public void RegisterEntry(long entrySize, Action<int> writeBlockBuffer)
            {
                var bufferSize = this.RegisterEntry(entrySize);
                if (bufferSize > 0)
                    writeBlockBuffer(bufferSize);
            }

            /// <summary>
            /// Registers block entry by adding its size. This will calculate the block buffer needed to be writen in the data stream, after this entry
            /// </summary>
            /// <param name="entrySize">The size of the entry in bytes</param>
            /// <returns>The size of the buffer, which has to be writen after writing this entry data in the data stream</returns>
            public int RegisterEntry(long entrySize)
            {
                long bufferSize = 0;

                // increase current block counter.
                // aggregate the current block entry size
                // sum the total data of all blocks entries

                _registeredEntriesSize += entrySize;

                _storage.HEADER->BlocksDataSize += entrySize;

                _registeredEntries++;

                if (_registeredEntries == Block.BlockRows * this.RowEntries)
                {
                    // calculate the buffer size, from the registered entries.

                    bufferSize = Math.Max((long)((BufferSizePercentage / 100d) * (double)_registeredEntriesSize), Block.MinBufferSizeBytes);

                    // reset the entries counter & size aggregate
                    _registeredEntries = 0;
                    _registeredEntriesSize = 0;

                    // sum the total allocated buffer size
                    _storage.HEADER->BlocksBufferSize += bufferSize;
                }

                return (int)bufferSize;
            }
            /// <summary>
            /// Causes any pending block entry registratoin to flish its content into the block buffer
            /// </summary>
            public void RegisterEntryComplete()
            {
                // sum the total allocated buffer size
                _storage.HEADER->BlocksBufferSize += Math.Max((long)((BufferSizePercentage / 100d) * (double)_registeredEntriesSize), Block.MinBufferSizeBytes);

                // reset the entries counter & size aggregate
                _registeredEntries = 0;
                _registeredEntriesSize = 0;
            }

            /// <summary>
            /// Total size of the allocated buffers for all entry blocks
            /// </summary>
            public long BlocksBufferSize { get { return _storage.HEADER->BlocksBufferSize; } }

            /// <summary>
            /// Total size of all entry blocks data, excluding the block bufferes
            /// </summary>
            public long BlocksDataSize { get { return _storage.HEADER->BlocksDataSize; } }

            /// <summary>
            /// Total size of all entry blocks data and the block buffers
            /// </summary>
            public long TotalDataSize { get { return BlocksBufferSize + BlocksDataSize; } }

            /*

                RowSize = Number of Columns in Row
                RowBlock (RowsInBlock) = Number of Rows in RowBlock
                RowBlockCount = Number of RowBlocks in table: Long((ColCount - 1) / ColBlock) + 1
                ColCount = Columns in Table
                ColBlock = RowBlock * RowSize
                ColumnNo = A specific Column
             
             */

            /// <summary>
            /// Number of Entries in Row. In Fields mode, this is equal to number of columns (each entry represent row column). In Row mode, this is equal to one (each entry represent a row)
            /// </summary>
            public int RowEntries { get { return _mode == AlignmentMode.RowMode ? 1 : _storage.ColumnCount; } }

            /// <summary>
            /// Total number of entries in data storage. In Fields mode, this is equal to number of cells. In Row mode, this is equal to number of Rows
            /// </summary>
            public long DataEntries { get { return _mode == AlignmentMode.RowMode ? _storage.RowCount : _storage.RowCount * _storage.ColumnCount; } }

            /// <summary>
            /// The size of an Entry Block 
            /// </summary>
            public int EntryBlock { get { return RowEntries * Block.BlockRows; } }

            /// <summary>
            ///  Number of Entry Blocks in the data storage
            /// </summary>
            public long Blocks { get { return (DataEntries / EntryBlock) + (DataEntries % EntryBlock > 0 ? 1 : 0); } }

            /*
                RowBlock(ColumnNo) - returns Long(  (ColumnNo - 1) / ColBlock) + 1

                RowBlockList(List:ColumnsAffected) - returns a grouped (aka unique) RowBlock list

                RowBlockModified(List:ColumnsAffected).Count - returns a count of all *unique* RowBlocks affected (eg 500 cols across 2 rowblocks, is only 2)

                RowBlockModified(List:ColumnsAffected).Req -   loop all columns modified in each rowblock, determining new size (MINUS) RowBlockUsed()

                RowBlockSpace(RowBlock) - Get total rowblock space: pointer for first col in NEXT-ROWBLOCK (MINUS) pointer for first col in THIS ROWBLOCK

                RowBlockUsed(RowBlock) - Get total used space: (pointer for last col in this row-block + pLen for last col) (MINUS) pointer for first col in this row-block

                RowBlockFree = RowBlockSpace - RowBlockUsed
              
             */

            /// <summary>
            /// Gets the Entry Block number where this entry belong
            /// </summary>
            /// <param name="column"></param>
            /// <returns></returns>
            public long GetBlock(long entry)
            {
                return entry / EntryBlock;
            }

            /// <summary>
            ///  Gets the block data info, including the data location and data size
            /// </summary>
            /// <param name="block">The block number to get the data for</param>
            /// <param name="location">The location of the data inside the data stream</param>
            /// <param name="length">The total length of the block data</param>
            public void GetBlockDataInfo(long block, ref long location, ref long length)
            {
                // 1. Find the start and end block entries.
                // 2. Use the start entry data location.
                // 3. Use the end entry location + length.
                // 4. Calculate the block location and length.

                var startIndex = GetBlockIndex(block);
                var endIndex = GetBlockLastIndex(block);

                location = startIndex.Position;
                length = (endIndex.Position - startIndex.Position) + endIndex.Length;
            }

            /// <summary>
            /// Gets the first Data Index for an Entry Block
            /// </summary>
            /// <param name="block">The entry block to get the first index for</param>
            /// <returns>First Data Index for the block</returns>
            public Indexer.Index GetBlockIndex(long block)
            {
                var entry = EntryBlock * block;

                return GetEntryIndex(entry);
            }

            /// <summary>
            /// Get the last Data Index for an Entry Block. It could be located anywhere within the block, in case the block is partially occupied
            /// </summary>
            /// <param name="block">The entry block to get the last index for</param>
            /// <returns>Last Data Index for the block</returns>
            public Indexer.Index GetBlockLastIndex(long block)
            {
                // Get the first entry
                var entryHead = (EntryBlock * block);

                // Get the last entry which has data (real entry)
                var entryTail = Math.Min(entryHead + EntryBlock, this.DataEntries) - 1;

                return GetEntryIndex(entryTail);
            }

            /// <summary>
            /// Gets the entry which is last in the block
            /// </summary>
            /// <param name="block"></param>
            /// <returns></returns>
            public long GetBlockLastEntry(long block)
            {
                // Get the first entry
                var entryHead = GetBlockFirstEntry(block);

                // Get the last entry which has data (real entry)
                var entryTail = Math.Min(entryHead + EntryBlock, this.DataEntries) - 1;
                return entryTail;
            }

            /// <summary>
            /// Gets the start head and end tail entries for the block wrapped in tupple
            /// </summary>
            /// <param name="block">The block to get the entries for</param>
            /// <param name="reversed">Whether the tail and head are reversed</param>
            /// <returns></returns>
            public Tuple<long, long> GetBlockEntries(long block, bool reversed)
            {
                // Get the first entry
                var entryHead = (EntryBlock * block);

                // Get the last entry which has data (real entry)
                var entryTail = Math.Min(entryHead + EntryBlock, this.DataEntries) - 1;

                return Tuple.Create<long, long>(entryHead, entryTail);
            }

            /// <summary>
            /// Gets he entry which is first in this block
            /// </summary>
            /// <param name="block"></param>
            /// <returns></returns>
            public long GetBlockFirstEntry(long block)
            {
                // Get the first entry
                var entryHead = (EntryBlock * block);
                return entryHead;
            }

            /// <summary>
            /// Gets the Data Index associated for an Entry
            /// </summary>
            /// <param name="entry"></param>
            /// <returns></returns>
            public Indexer.Index GetEntryIndex(long entry)
            {
                if (_mode == AlignmentMode.RowMode)
                    return this._indexer.Get(entry, 0, 1);

                long row = 0;
                int column = 0;
                GetEntry(entry, ref row, ref column);

                var index = this._indexer.Get(row, column, this.RowEntries);

                return index;
            }

            /// <summary>
            /// Gets the entry data source Row and Column numbers
            /// </summary>
            /// <param name="entry"></param>
            /// <param name="row"></param>
            /// <param name="column"></param>
            public void GetEntry(long entry, ref long row, ref int column)
            {
                var rowEntries = (this.DataEntries / _storage.RowCount);
                row = entry / rowEntries;
                column = (int)(entry % rowEntries);
            }

            /// <summary>
            /// Returns a grouped (aka unique) Entry Block list (blocks numbers)
            /// </summary>
            /// <param name="entries">Entries Affected</param>
            /// <returns>List of block numbers for the entries</returns>
            public long[] GetBlocks(long[] entries)
            {
                var query = entries.Select(entry => GetBlock(entry));
                var blocks = query.Distinct().OrderBy(entry => entry).ToArray();
                return blocks;
            }

            /// <summary>
            /// Gets all real entries for entry block
            /// </summary>
            /// <param name="block">The block number to get entries for</param>
            /// <returns>List of entries numbers of the block</returns>
            public long[] GetBlockEntries(long block)
            {
                if (block < Blocks)
                {
                    // Get the first entry
                    var entryHead = (EntryBlock * block);

                    // Get the last entry which has data (real entry)
                    var entryTail = Math.Min(entryHead + EntryBlock, this.DataEntries) - 1;

                    var entries = Enumerable.Range((int)entryHead, (int)(entryTail - entryHead) + 1).Select(entry => (long)entry).ToArray();

                    return entries;
                }

                throw new Exception("Invalid block index number " + block + ". Out of blocks bounds of " + Blocks + " blocks.");
            }

            /// <summary>
            /// returns a count of all *unique* Entry Blocks affected
            /// </summary>
            /// <param name="entries">Entries Affected</param>
            public int GetBlockModifiedCount(long[] entries)
            {
                return GetBlocks(entries).Length;
            }

            /// <summary>
            /// Calculate the entry block size required to store the modified entries
            /// </summary>
            /// <param name="entriesValues">List of modified entries and their values</param>
            /// <returns>The size required to store the modification</returns>
            public long GetBlockRequiredSize(Dictionary<long, byte[]> entriesValues)
            {
                //  loop all columns modified in each rowblock, determining new size (MINUS) RowBlockUsed()

                var entries = entriesValues.Keys.ToArray();

                // var blocks = GetBlocks(entries);

                // var blocksAvailableSize = blocks.Sum(block => GetBlockSpaceFree(block));

                // Get the data size of all values associated with entries
                var modificationSize = entriesValues.Sum(item => item.Value.Length);

                // Get existing entries data size and use it to calculate the requred modification size
                var existingSize = entries.Sum(entry => this.GetEntryIndex(entry).Length);

                return modificationSize - existingSize;
            }

            /// <summary>
            /// Gets the Total entry block space size
            /// </summary>
            /// <param name="block">The entry block to take the total space for</param>
            public long GetBlockSpaceTotal(long block)
            {
                if (this.DataEntries > 0)
                {
                    // 1. Pointer for first col in NEXT-ROWBLOCK (MINUS) pointer for first col in THIS ROWBLOCK
                    // 2. If this is the last block, take the remaining of the stream for it's size

                    var index = GetBlockIndex(block);

                    if (block + 1 < this.Blocks)
                    {
                        var next = GetBlockIndex(block + 1);
                        if (next.Position > index.Position)
                            return next.Position - index.Position;

                        //  The final last block, has total size of the remaining data storage capacity
                        return this._capacity - this._indexer.IndexedDataSize;
                    }
                    else
                    {
                        //  Get the end of all blocks
                        //  The final last block, has total size of the remaining data storage capacity
                        return this._capacity - index.Position;
                    }

                    throw new Exception("No more next entry blocks");
                }

                // No entries yet, return the complete data storage capacity
                return this._capacity - this._indexer.IndexedDataSize;
            }

            /// <summary>
            ///  Gets the Used entry block space size
            /// </summary>
            /// <param name="block">The entry block to take the used space for</param>
            public long GetBlockSpaceUsed(long block)
            {
                // 1. pointer for last col in this row-block + pLen for last col) (MINUS) pointer for first col in this row-block.
                // 2. if theer is no next entry (due to partially block fill), determine the last valid block entry

                if (DataEntries > 0)
                {
                    var head = GetBlockIndex(block);
                    var tail = GetBlockLastIndex(block);

                    long sizeUsed = (tail.Position + tail.Length) - head.Position;

                    return sizeUsed;
                }

                // No entries yet, so no space is used
                return 0;
            }

            /// <summary>
            /// Gets the Free entry block space size
            /// </summary>
            /// <param name="block">The entry block to take the used space for</param>
            public long GetBlockSpaceFree(long block)
            {
                return GetBlockSpaceTotal(block) - GetBlockSpaceUsed(block);
            }

            public string PrintStats(long row, bool blocksOnly, bool thisRowBlockOnly, int? topN = null, bool returnOnly = false)
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("- BLOCKS STATISTICS -----");

                if (!blocksOnly)
                {
                    sb.AppendLine(string.Format("- EntryBlock:      {0}", this.EntryBlock));
                    sb.AppendLine(string.Format("- RowEntries:      {0}", this.RowEntries));
                    sb.AppendLine(string.Format("- DataEntries:     {0}", this.DataEntries));
                    sb.AppendLine(string.Format("- Blocks:          {0}", this.Blocks));
                    sb.AppendLine(string.Format("- DataSize:        {0}", this.BlocksDataSize));
                    sb.AppendLine(string.Format("- BufferSize:      {0}", this.BlocksBufferSize));
                    sb.AppendLine(string.Format("- TotalDataSize:   {0}", this.TotalDataSize));
                }

                sb.AppendFormat("- ROW {0} ", row);
                sb.AppendLine(new string('-', 100));

                if (topN == null)
                    topN = (int)this.Blocks;
                else
                    topN = Math.Min((int)topN, (int)this.Blocks);

                if (!thisRowBlockOnly)
                    for (int block = 0; block < topN; block++)
                        sb.AppendLine(string.Format("- B {0}: {1}", block, FormatUsage(block)));
                else
                    sb.AppendLine(string.Format("- B {0}: {1}", GetBlock(row), FormatUsage((int)GetBlock(row))));

                sb.AppendLine(new string('-', 100));

                if (!returnOnly)
                    System.Diagnostics.Debug.WriteLine(sb.ToString());

                return sb.ToString();
            }

            public string FormatUsage(int block)
            {
                return string.Format("Total={2}, Used={1}, Free={0}", this.GetBlockSpaceFree(block), this.GetBlockSpaceUsed(block), this.GetBlockSpaceTotal(block));
            }


            /// <summary>
            /// Sets or updates the index information for this entry
            /// </summary>
            /// <param name="entry">Entry number</param>
            /// <param name="position">Data location to index</param>
            /// <param name="length">Data length to index</param>
            /// <returns>Reference to the updated index</returns>
            public Indexer.Index SetEntryIndex(long entry, long position, int length)
            {
                if (_mode == AlignmentMode.RowMode)
                    return this._indexer.Set(entry, 0, 1, position, length);

                long row = 0;
                int column = 0;
                GetEntry(entry, ref row, ref column);

                var index = this._indexer.Set(row, column, this.RowEntries, position, length);

#if VERBOSE_BLOCKS
                System.Diagnostics.Debug.WriteLine("Block SetEntryIndex: entry={0}  index={1}   [{2} x {3}]", entry, index, row, column);
#endif

                return index;
            }

            /// <summary>
            /// Gets the amount of data space occupied by the entries, within the containing block. Entries which does not belong to the block are ignored
            /// </summary>
            /// <param name="block">Block number containing the entries</param>
            /// <param name="entries">Enumerator of entries to get the space for. Only entries which belongs to the block are considered</param>
            /// <returns>Amount of data space occupied by the entries</returns>
            public long GetBlockEntriesSpace(long block, IEnumerator<long> entries)
            {
                // Get the first and entry which has data (real entry)
                var entryHead = (EntryBlock * block);
                var entryTail = Math.Min(entryHead + EntryBlock, this.DataEntries) - 1;

                long occupiedSpace = 0;

                while (entries.MoveNext())
                {
                    var entry = entries.Current;

                    // ignore the entry if does not belong to this block
                    if (entry < entryHead && entry >= entryTail)
                        continue;

                    var index = GetEntryIndex(entry);

                    occupiedSpace += index.Length;
                }

                return occupiedSpace;
            }

            internal void BackupData(BinaryWriter writer)
            {
                writer.Write(this.BlocksBufferSize);
                writer.Write(this.BlocksDataSize);

            }

            internal void RestoreData(BinaryReader reader)
            {
                _storage.HEADER->BlocksBufferSize = reader.ReadInt64();
                _storage.HEADER->BlocksDataSize = reader.ReadInt64();
            }

            /// <summary>
            /// Removes entry size from the blocks manager
            /// </summary>
            /// <param name="entrySize"></param>
            internal void UnregisterEntry(long entrySize)
            {
                // Calculates the size released by removing an entry
                if ((_storage.HEADER->BlocksDataSize = _storage.HEADER->BlocksDataSize - entrySize) < 0)
                    throw new InvalidOperationException("The blocks data size droped bellow zero after unregistering an entry of size " + entrySize + ". Implementation inspection suggested");
            }

            /// <summary>
            /// Indicates whether the block contains real entries
            /// </summary>
            /// <param name="p"></param>
            /// <returns></returns>
            internal bool HasEntries(long block)
            {
                return this.DataEntries > GetBlockFirstEntry(block);
            }
        }

    }
}
