﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DigitalToolworks.Data.MStorage
{
    /// <summary>
    /// Contains operations which can be run by queries
    /// </summary>
    public partial class Query
    {
        /// <summary>
        /// Takes top number of records from the query.
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public Query Top(int count)
        {
            return new Query(Table, _recordsAreCached ? this.Take(count).ToArray() : this.Take(count))
            {
                _querySortIndex = _querySortIndex
            };
        }

        /// <summary>
        /// Skips top number of records from the query.
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public Query Skip(int count)
        {
            return new Query(Table, _recordsAreCached ? ((IEnumerable<long>)this).Skip(count).ToArray() : ((IEnumerable<long>)this).Skip(count))
            {
                _querySortIndex = _querySortIndex
            };
        }

        /// <summary>
        /// Filters the query by retuning records matching the specified predicate
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public Query Where(Func<long, bool> predicate)
        {
            return new Query(Table, _recordsAreCached ? ((IEnumerable<long>)this).Where(predicate).ToArray() : ((IEnumerable<long>)this).Where(predicate))
            {
                _querySortIndex = _querySortIndex
            };
        }

        /// <summary>
        /// Returns Query which contains records having distinct values for the columns specified
        /// </summary>
        /// <param name="columnNames">Comma-separate list of column names</param>
        /// <returns></returns>
        public Query Distinct(string columnNames)
        {
            // Determine the unique records using their calulated hash keys
            //
            using (var iterator = Iterator2(columnNames))
            {
                var distinctIterator = ViewIterator.DistinctIterator(this, record => iterator.CalculateKey(record), EqualityComparer<long>.Default);

                return new Query(this.Table, _recordsAreCached ? distinctIterator.ToArray() : distinctIterator)
                {
                    _querySortIndex = _querySortIndex
                };
            }
        }
    }
}
