﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace DigitalToolworks.Data.MStorage
{
    using fieldset = MTableCore.Records.FieldSet;

    /// <summary>
    /// Represent query records views iterator and bindable component
    /// </summary>
    public sealed partial class Query
    {
        public abstract class IteratorBase
        {

        }

        /// <summary>
        /// Represent query records views iterator
        /// </summary>
        public sealed class ViewIterator : IteratorBase, IEnumerator<View>, IEnumerable<View>, ITypedList, IDisposable  // Provide metadata info for the underlaying iterator columns schema 
        {
            View _currentView;
            long _currentKey = -1;
            dynamic _currentSingleValue;

            /// <summary>
            /// Enumerator used in the current iteration
            /// </summary>
            IEnumerator<long> _currentEnumerator = null;

            readonly Func<long, long> _callbackSort;
            internal readonly fieldset fieldset;
            readonly MTable.Column[] _columns;
            readonly bool _cloneCurrent;
            readonly int _recordsCount;
            readonly ITable _table;
            readonly Query _query;

            /// <summary>
            /// Gets list of columns associated with this view iterator
            /// </summary>
            /// <returns></returns>
            public MTable.Column[] Columns { get { return _columns; } }

            /// <summary>
            /// Gets access to the query iterator owner table
            /// </summary>
            public ITable Table { get { return _table; } }

            internal ViewIterator(Query query, MTable.Column[] columns, Func<long, long> callbackSort, bool cloneCurrent)
            {
                ITable table = query._table;
                _query = query;
                _table = table;
                _recordsCount = (int)query._table.Count;
                _columns = columns;
                _cloneCurrent = cloneCurrent;
                _callbackSort = callbackSort != null ? callbackSort : unsorted => unsorted; // just returns the same record, if no sorting callback is specified   


                //          Prepare the view columns
                //          Prepare the values dictionary
                //          Prepare the expression columns callbacks 
                //###################################################################          

                //###################################################################                

                fieldset = table.CreateFieldSet(columns);

                //  Validate the number of columns with number of fields. 
                //      Note: this should not fail normally unless inconsistency happened at the higher level table
                if (columns.Length > fieldset.totalFields)
                    throw new InvalidOperationException($"Number of columns {columns.Length} is larger then total of {fieldset.totalFields} native columns stored. Possible unsynchronized columns structure with native structure.");

                //  Single instance of view iterator object
                _currentView = new View(columns, fieldset.totalFields);


                //  Note: Prepare calculated columns. 
                //      The old logic is as the following but thats not working properly. 
                //      The new logic should rely on calculated columns always positioned at certain place in the native structure.
                //
                //  Former Logic:
                //      Check if there are calculated columns in the list
                //      Reorder the inner columns list, so the calculated will be last in the list
                //
                if (false)
                    if (_columns.Count(column => column.ExpressionFunc != null) > 0)
                        Array.Sort(_columns, new Comparison<MTable.Column>((c1, c2) =>
                        {
                            if (c1.ExpressionFunc != null && c2.ExpressionFunc == null)
                                return 1;
                            if (c1.ExpressionFunc == null && c2.ExpressionFunc != null)
                                return -1;
                            return 0;
                        }));
            }

            /// <summary>
            /// Returns enumerable for iterating over records and generated hash value keys only
            /// </summary>
            /// <returns></returns>
            public IEnumerable<HashKey> ToHashKeyIterator()
            {
                var currentKey = new HashKey(-1, 0);

                foreach (var record in _query)
                {
                    currentKey.Key = this.CalculateKey(record);
                    currentKey.Record = record;
                    yield return currentKey;
                }

                yield break;
            }

            /// <summary>
            /// Enumerates the records underlaying fieldsets only
            /// </summary>
            /// <returns></returns>
            [Obsolete("Unused at this point. Possibly used in the past for testing and benchmarking")]
            public IEnumerable<fieldset> ToFieldsetIterator()
            {
                foreach (var record in _query)
                {
                    var sorted = _callbackSort(record);
                    fieldset.ReadEntry(sorted);
                    yield return fieldset;
                }

                yield break;
            }

            /// <summary>
            /// Returns data reader associated with this iterator
            /// </summary>
            /// <returns></returns>
            public DataReader ToDataReader()
            {
                return new DataReader(this, _table);
            }

            /// <summary>
            /// Returns enumerable for iterating over records keys only
            /// </summary>
            /// <returns></returns>
            public IEnumerable<long> ToRecordsIterator()
            {
                ///var sorted = _iterator._callbackSort(x.record);

                return _query.AsEnumerable();
            }

            /// <summary>
            /// Calculates hash value key for the fields contained in this query record (hightly optimized, use with care)
            /// </summary>
            /// <param name="record"></param>
            /// <returns></returns>
            public long CalculateKey(long record)
            {
                var sorted = _callbackSort(record);

                fieldset.ReadEntry(sorted);

                // Access the fields in most performant way and calcualte the hash values

                if (fieldset.Count > 1)
                {
                    unchecked
                    {
                        long hash = 17;

                        foreach (var ordinal in fieldset.ordinals)
                        {
                            var field = fieldset[ordinal];

                            // get hash code for all items in array

                            hash = hash * 23 + field.CalculateHashKey();
                        }

                        return hash;
                    }
                }
                else
                {
                    var field = fieldset[fieldset.ordinals[0]];

                    return field.CalculateHashKey();
                }
            }

            /// <summary>
            /// Reads single value from the iterator. The method assumes only one column is specified when the iterator was created
            /// </summary>
            /// <param name="record"></param>
            /// <param name="checkError">Performs check if error is present for the cell value</param>
            /// <returns></returns>
            public dynamic GetValue(long record, bool checkError = false, bool threadSafe = false)
            {
                //if (_currentSingleKey != record)
                {
                    // Fast reads single value (assumes only one field has been selected)

                    var sorted = _callbackSort(record);

                    fieldset.ReadEntry(sorted, threadLock: threadSafe);

                    var column = _columns[0];
                    var field = fieldset[column.ordinal];
                    if (checkError && column.HasError(record))
                        _currentSingleValue = field.GetText();
                    else
                        if (column.ExpressionFunc != null)
                        // passing the currentView, with all none calculated values. the calculated values are not included at this point
                        _currentSingleValue = column.ExpressionFunc(GetView(record));
                    else
                        _currentSingleValue = column.GetValue(field);

                    //_currentSingleKey = record;
                }

                return _currentSingleValue;
            }

            /// <summary>
            /// Returns record view for the record key provided, using this query iterator configuration. Any currently cached record will be returned
            /// </summary>
            /// <param name="checkError">Performs check if error is present for the cell value</param>
            /// <param name="record"></param>
            /// <returns></returns>
            public View GetView(long record, bool checkError = false, bool cloneCurrent = false)
            {
                // create copy (less performant)

                if (_currentKey != record)
                {
                    if (!(record < _recordsCount))
                        throw new InvalidOperationException("Record " + record + " out of bounds of table count " + _recordsCount + "");

                    // create new instance with clean underlaying values or reuse the current view instance

                    View view = _currentView;
                    if (cloneCurrent)
                        view = view.New();

                    var sorted = _callbackSort(record);

                    fieldset.ReadEntry(sorted);

                    // Set the current record
                    view.record = sorted;

                    // Populate the current view row with values                 
                    foreach (var column in _columns)
                    {
                        var field = fieldset[column.ordinal];

                        object value = null;

                        if (checkError && column.HasError(record))
                            value = field.GetText();

                        else if (column.ExpressionFunc != null) // passing the currentView, with all none calculated values. the calculated values are not included at this point
                        {
                            // Evaluate the expression function, or catch runtime evaluation exception. 
                            // In case of exception, ignore the value and log exception details.
                            try
                            {
                                value = column.ExpressionFunc(view);
                            }
                            catch (Exception)
                            {
                                // TODO: Ignoring the runtime exception. Determine what to do with it in future.
                            }
                        }
                        else
                            value = column.GetValue(field);

                        view.values[column.ordinal] = value;
                    }

                    _currentKey = record;

                    return view;
                }

                // Just returned the current record view
                return _currentView;
            }

            /// <summary>
            /// Reads the underlaying data for the record into the fieldset and returns the fieldset object
            /// </summary>
            /// <param name="record"></param>
            /// <returns></returns>
            public fieldset GetFields(long record)
            {
                var sorted = _callbackSort(record);

                fieldset.ReadEntry(sorted);

                return fieldset;
            }

            public View Current
            {
                get
                { // Populate the current view if the current record differs from the records iterator state
                    return _currentView = this.GetView(_currentEnumerator.Current, checkError: false, cloneCurrent: _cloneCurrent);
                }
            }           

            object IEnumerator.Current
            {
                get { return this.Current; }
            }

            public bool MoveNext()
            {
                return _currentEnumerator.MoveNext();
            }

            public void Reset()
            {
                _currentEnumerator.Reset();
                _currentEnumerator = null;

                // _records.Reset();
                _currentView.record = -1;
                _currentKey = -1;

                // perhaps, clear the cached values
            }

            IEnumerator<View> IEnumerable<View>.GetEnumerator()
            {
                // Allocate the current enumerator
                _currentEnumerator = _query.AsEnumerable().GetEnumerator();
                return this;
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                // Allocate the current enumerator
                _currentEnumerator = _query.AsEnumerable().GetEnumerator();
                return this;
            }

            /// <summary>
            /// Returns records views who has distinct values for this iterator
            /// </summary>
            /// <returns></returns>
            public IEnumerable<View> DistinctViews()
            {
                // Determine the unique records using their calulated hash keys
                //              
                foreach (var record in this.DistinctKeys())
                    yield return this.GetView(record);
                yield break;
            }

            /// <summary>
            /// Returns record keys who has distinct values for this iterator
            /// </summary>
            /// <returns></returns>
            public IEnumerable<long> DistinctKeys()
            {
                // Determine the unique records using their calulated hash keys
                //
                return DistinctIterator(this.ToRecordsIterator(), record => this.CalculateKey(record), EqualityComparer<long>.Default);
            }

            /// <summary>
            /// https://code.google.com/p/morelinq/source/browse/MoreLinq/DistinctBy.cs
            /// </summary>
            /// <typeparam name="TSource"></typeparam>
            /// <typeparam name="TKey"></typeparam>
            /// <param name="source"></param>
            /// <param name="keySelector"></param>
            /// <param name="comparer"></param>
            /// <returns></returns>
            internal static IEnumerable<TSource> DistinctIterator<TSource, TKey>(
                IEnumerable<TSource> source,
                Func<TSource, TKey> keySelector,
                IEqualityComparer<TKey> comparer)
            {
#if !NO_HASHSET

                var knownKeys = new HashSet<TKey>(comparer);

                foreach (var element in source)
                {
                    if (knownKeys.Add(keySelector(element)))
                    {
                        yield return element;
                    }
                }
#else
            //
            // On platforms where LINQ is available but no HashSet<T>
            // (like on Silverlight), implement this operator using 
            // existing LINQ operators. Using GroupBy is slightly less
            // efficient since it has do all the grouping work before
            // it can start to yield any one element from the source.
            //

            return source.GroupBy(keySelector, comparer).Select(g => g.First());
#endif
            }

            string ITypedList.GetListName(PropertyDescriptor[] listAccessors)
            {
                return this.Table.Name;
            }

            /// <summary>
            /// Property descriptors for the underlaying interator columns
            /// </summary>
            /// <param name="listAccessors"></param>
            /// <returns></returns>
            PropertyDescriptorCollection ITypedList.GetItemProperties(PropertyDescriptor[] listAccessors)
            {
                return this.Columns.GetDescriptors();
            }

            void IDisposable.Dispose()
            {
                //  Release any resources here ...

                if (_currentEnumerator != null)
                    _currentEnumerator.Dispose();
            }
        }


        /// <summary>
        /// Create records view iterator for this query
        /// </summary>
        /// <param name="columns"></param>
        /// <param name="callbackSort"></param>
        /// <param name="cloneCurrent"></param>
        /// <returns></returns>
        public ViewIterator Iterator2(MTable.Column[] columns, Func<long, long> callbackSort = null, bool cloneCurrent = false)
        {
            return new ViewIterator(this, columns, callbackSort, cloneCurrent);
        }
    }
}
