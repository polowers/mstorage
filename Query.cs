﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DigitalToolworks.Data.MStorage
{
    public sealed partial class Query : IReadOnlyList<long>
    {
        readonly IEnumerable<long> _records;
        readonly ITable _table;
        MTable.ISortIndex _querySortIndex;
        /// <summary>
        /// Indicates whether the internal enumerator contains cached in-memory list of keys or it is loaded deferred
        /// </summary>
        readonly bool _recordsAreCached;

        internal Query(ITable table, IEnumerable<long> records)
        {
            _records = records;
            _table = table;

            // Determine if the records enumerable is in-memory collection
            _recordsAreCached = records is IList || records is Array || records is ICollection || records is ArrayList;
        }

        private static void ParseMatchModeAndValue(ref MTable.MatchMode mode, ref string value)
        {
            bool approximate = mode == MTable.MatchMode.Approximates;

            if (value.EndsWith("*") && value.StartsWith("*"))
                mode = approximate ? MTable.MatchMode.DataContainsMatchApproximately : MTable.MatchMode.DataContainsMatch;
            else
                if (value.EndsWith("*"))
                    mode = approximate ? MTable.MatchMode.DataStartsWithMatch : MTable.MatchMode.DataStartsWithMatch;
                else
                    if (value.StartsWith("*"))
                        mode = approximate ? MTable.MatchMode.DataEndsWithMatch : MTable.MatchMode.DataEndsWithMatch;
                    else
                        if (value.EndsWith("|") && value.StartsWith("|"))
                            mode = approximate ? MTable.MatchMode.MatchContainsDataApproximately : MTable.MatchMode.MatchContainsData;
                        else
                            if (value.EndsWith("|"))
                                mode = approximate ? MTable.MatchMode.MatchStartsWithData : MTable.MatchMode.MatchStartsWithData;
                            else
                                if (value.StartsWith("|"))
                                    mode = approximate ? MTable.MatchMode.MatchEndsWithData : MTable.MatchMode.MatchEndsWithData;
                                else if (!approximate)
                                    mode = MTable.MatchMode.IsEqual;

            value = value.Trim('*').Trim('|');
        }

        /// <summary>
        /// Add sort index to this query. The records returned will be sorted using the index information
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Query WithSortIndex(MTable.ISortIndex sortIndex)
        {
            _querySortIndex = sortIndex;
            return this;
        }

        /// <summary>
        /// Adds options to this query and return configured query object.
        /// </summary>
        /// <param name="cacheRecords">Whether the records iterator is loaded in memory. Allows calling the same interator multiple times. This option applies immideately.</param>
        /// <returns></returns>
        public Query WithOptions(bool cacheRecords)
        {
            if (cacheRecords)
                return new Query(_table, this._records.ToArray())    // Loads the records enumerator in memory. 
                {
                    _querySortIndex = _querySortIndex               // Inherit other variables from the old to the new query object
                };
            else
                return this;
        }

        public static Query Build(ITable table, string queryLanguage)
        {
            Query query = null;

            var operandsAND = queryLanguage.Split(new string[] { "AND" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var operand in operandsAND)
                if (operand != queryLanguage)
                {
                    string columnName = null;
                    string columnValue = null;
                    MTable.MatchMode mode = MTable.MatchMode.IsEqual;

                    if (operand.Contains('~') && !operand.Contains('='))
                    {
                        columnName = operand.Split('~')[0].Trim();
                        columnValue = operand.Split('~')[1].Trim();
                        mode = MTable.MatchMode.Approximates;
                    }
                    else
                        if (operand.Contains('=') && !operand.Contains('~'))
                        {
                            columnName = operand.Split('=')[0].Trim();
                            columnValue = operand.Split('=')[1].Trim();
                            mode = MTable.MatchMode.IsEqual;
                        }
                        else
                            throw new InvalidOperationException("Invalid query (" + queryLanguage + "). Operator missing");

                    ParseMatchModeAndValue(ref mode, ref columnValue);

                    if (query == null)
                        query = table.Query(columnName, mode, columnValue);
                    else
                        query = query.AND(columnName, mode, columnValue);
                }

            var operandsOR = queryLanguage.Split(new string[] { "OR" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var operand in operandsOR)
                if (operand != queryLanguage)
                {
                    string columnName = null;
                    string columnValue = null;
                    MTable.MatchMode mode = MTable.MatchMode.IsEqual;

                    if (operand.Contains('~') && !operand.Contains('='))
                    {
                        columnName = operand.Split('~')[0].Trim();
                        columnValue = operand.Split('~')[1].Trim();
                        mode = MTable.MatchMode.Approximates;
                    }
                    else
                        if (operand.Contains('=') && !operand.Contains('~'))
                        {
                            columnName = operand.Split('=')[0].Trim();
                            columnValue = operand.Split('=')[1].Trim();
                            mode = MTable.MatchMode.IsEqual;
                        }
                        else
                            throw new InvalidOperationException("Invalid query (" + queryLanguage + "). Operator missing");

                    ParseMatchModeAndValue(ref mode, ref columnValue);

                    if (query == null)
                        query = table.Query(columnName, mode, columnValue);
                    else
                        query = query.OR(columnName, mode, columnValue);
                }

            // For queries who does not contains AND or OR
            if (query == null)
            {
                string columnName = null;
                string columnValue = null;
                MTable.MatchMode mode = MTable.MatchMode.IsEqual;

                if (queryLanguage.Contains('~') && !queryLanguage.Contains('='))
                {
                    columnName = queryLanguage.Split('~')[0].Trim();
                    columnValue = queryLanguage.Split('~')[1].Trim();
                    mode = MTable.MatchMode.Approximates;
                }
                else
                    if (queryLanguage.Contains('=') && !queryLanguage.Contains('~'))
                    {
                        columnName = queryLanguage.Split('=')[0].Trim();
                        columnValue = queryLanguage.Split('=')[1].Trim();
                        mode = MTable.MatchMode.IsEqual;
                    }
                    else
                        throw new InvalidOperationException("Invalid query (" + queryLanguage + "). Operator missing");

                ParseMatchModeAndValue(ref mode, ref columnValue);

                query = table.Query(columnName, mode, columnValue);
            }

            return query;
        }

        public Query AND(string columnName, MTable.MatchMode matchMode, Object matchValue)
        {
            MTable.Column column = _table.Columns[columnName].Single();
            // Execute the second query , limiting the records to this query

            bool ignoreSortIndex = false;
            Tuple<long, long> recordRange = null;

#if PREPARATION_COLUMN_MODE
                if (column.NativeSorted)
                {
                    recordRange = Tuple.Create<long, long>(Records.First(), Records.Last());

                }
                else if (column.SortIndex != null)
                {
                    // Find the maximum and minimum sorted record

                    var first = Records.Min(record => column.SortIndex.GetRecordNumber(record));
                    var last = Records.Max(record => column.SortIndex.GetRecordNumber(record));
                    recordRange = Tuple.Create<long, long>(first, last);
                    ignoreSortIndex = true;
                }
#endif

              // Records.ToArray() Use to limit the records
            return AND(new Query(_table, MTable.FindRecords(column, _table is MView ? ((MView)_table).Table : (MTable)_table, matchValue, matchMode, recordRange, null, ignoreSortIndex)));
        }

        private Query AND(Query query)
        {
            // Gets the intersected records only, distinct them and sort them
            var records = this._records.Intersect(query._records).Distinct().OrderBy(record => record);

            return new Query(_table, records.ToArray());
        }

        public Query OR(string columnName, MTable.MatchMode matchMode, Object matchValue)
        {
            MTable.Column column = _table.Columns[columnName].Single();

            return OR(new Query(_table, MTable.FindRecords(column, _table is MView ? ((MView)_table).Table : (MTable)_table, matchValue, matchMode)));
        }

        private Query OR(Query query)
        {
            // Append the records from the passed query to this query 
            return new Query(_table, Enumerable.Concat(_records, query._records).Distinct().OrderBy(record => record));
        }

        /// <summary>
        /// Indicates whether this query is configured to return sorted records, using information by sort index
        /// </summary>
        public bool IsSorted
        {
            get { return _querySortIndex != null; }
        }

        private IEnumerable<long> __Records
        {
            get
            {
                return _records;
            }
        }

        /// <summary>
        /// Gets the owner table of this query
        /// </summary>
        /// <returns></returns>
        public ITable Table { get { return _table; } }

        int IReadOnlyCollection<long>.Count
        {
            get
            {
                return _records.Count();
            }
        }

        long IReadOnlyList<long>.this[int index]
        {
            get
            {
                return _records.ElementAt(index);
            }
        }

        /// <summary>
        /// Creates records views iterator, selecting the columns listed and applying sort order to the iterator if the query is configured WithSortIndex
        /// </summary>
        /// <param name="columnsNames"></param>
        /// <param name="sortIndex"></param>
        /// <param name="cloneCurrent"></param>
        /// <returns></returns>
        public ViewIterator Iterator2(string columnsNames = null, bool cloneCurrent = false)
        {
            var columns = string.IsNullOrWhiteSpace(columnsNames)
                ? _table.Columns.OrderBy(c => c.Ordinal).ToArray()
                : _table.Columns[columnsNames].OrderBy(c => c.ordinal).ToArray();

            // Assign sorting callback deledate, using the sort index passed
            Func<long, long> callbackSort = null;
            if (_querySortIndex != null)
                callbackSort = unsortedRecord => _querySortIndex.GetSortedRecord(unsortedRecord);

            return Iterator2(columns, callbackSort, cloneCurrent);
        }

        /// <summary>
        /// Creates records views iterator applying sort order using the specified sorting callback delegate, optionally selecting data for the columns listed only
        /// </summary>
        /// <param name="columnsNames"></param>
        /// <param name="callbackSort"></param>
        /// <param name="cloneCurrent"></param>
        /// <returns></returns>
        public ViewIterator Iterator2Sortable(Func<long, long> callbackSort, string columnsNames = null, bool cloneCurrent = false)
        {
            if (_querySortIndex != null)
                throw new InvalidOperationException("This query has sort index configured. An attempt to create iterator for explicit sorting is not allowed.");

            var columns = string.IsNullOrWhiteSpace(columnsNames) ? _table.Columns.ToArray() : _table.Columns[columnsNames];

            return Iterator2(columns, callbackSort, cloneCurrent);
        }
 
 
        IEnumerator<long> IEnumerable<long>.GetEnumerator()
        {
            return _records.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _records.GetEnumerator();
        }

        /// <summary>
        /// Contains record key and values hash key
        /// </summary>
        public struct HashKey : IEquatable<HashKey>, IEquatable<long>
        {
            internal HashKey(long record, int key)
            {
                Record = record;
                Key = key;
            }

            public long Record;
            public long Key;

            bool IEquatable<HashKey>.Equals(HashKey other)
            {
                return Key == other.Key;
            }

            bool IEquatable<long>.Equals(long other)
            {
                return Key == other;
            }

            public override int GetHashCode()
            {
                return (int)Key;
            }
        }

        /// <summary>
        /// Represents single record info including record key and fields values. The component supports IDataRecord interface.
        /// </summary>
        public struct View : IDataRecord, IEquatable<View>
        {
            readonly MTable.Column[] columns;

            internal long record;

            /// <summary>
            /// Array for storing view values which is sized to fit all fields in table regardless the actual number of columns
            /// </summary>
            internal readonly object[] values;

            /// <summary>
            ///  List of selected fields ordinals
            /// </summary>
            readonly int[] ordinals;

            /// <summary>
            /// Fast lookup of column ordinals by column names
            /// </summary>
            readonly SortedDictionary<string, int> nameOrdinal;
                      

            private View(MTable.Column[] columns, int[] ordinals, SortedDictionary<string, int> nameOrdinal, long record, object[] values)
            {
                this.columns = columns;
                this.ordinals = ordinals;
                this.record = record;
                this.nameOrdinal = nameOrdinal;
                this.values = values;
            }

            internal View(MTable.Column[] columns, int totalFields)
            {
                this.columns = columns;
                          
                //  The ordinals contains only the list of columns provided     
                this.ordinals = columns.Select(column => column.Ordinal).ToArray();

                //  The values has to be allocated for all fields in the table. 
                //  This array is used for ultra light and fast indexed value access.
                this.values = new object[totalFields];

                this.record = -1;

                //  Fast lookup of column ordinals by column names.
                //  Use case-insensitive sorted dictionary (possible performance hit)
                nameOrdinal = new SortedDictionary<string, int>(StringComparer.OrdinalIgnoreCase);
                foreach (var column in columns)
                    nameOrdinal.Add(column.Name, column.Ordinal);
            }

            public long Record
            {
                get
                {
                    return record;
                }
            }

            /// <summary>
            /// Gets cell value by index
            /// </summary>
            /// <param name="idx"></param>
            /// <returns></returns>
            public dynamic this[int idx]
            {
                get
                {
                    return values[ordinals[idx]];
                }
            }


            /// <summary>
            /// Get cell value for the specified column instance
            /// </summary>
            /// <param name="column"></param>
            /// <returns></returns>
            public dynamic this[MTable.Column column]
            {
                get
                {
                    return values[column.ordinal];
                }
            }

            /// <summary>
            /// Get cell value for the specified column name
            /// </summary>
            /// <param name="column"></param>
            /// <returns></returns>
            public dynamic this[string column]
            {
                get
                {
                    return values[nameOrdinal[column]];
                }
            }

            /// <summary>
            /// Collection of columns in this record view
            /// </summary>
            public MTable.Column[] Columns
            {
                get
                {
                    return this.columns;
                }
            }           

            /// <summary>
            /// Enumerates all values in this view
            /// </summary>
            public IEnumerable<dynamic> Values
            {
                get
                {
                    for (int idx = 0; idx < ordinals.Length; idx++)
                        yield return values[ordinals[idx]]; 
                    yield break;
                }
            }

            /// <summary>
            /// Creates new view from the existing instance coping the internal state and creating new underlaying values structure.
            /// </summary>
            /// <returns></returns>
            internal View New()
            {
                return new View(columns, ordinals, nameOrdinal, record, new object[this.values.Length]);
            }

            #region - IDataRecord Implicit Implementation -

            int IDataRecord.FieldCount
            {
                get
                {
                    return this.columns.Length;
                }
            }

            object IDataRecord.this[string name]
            {
                get
                {
                    return this[column: name];
                }
            }

            object IDataRecord.this[int i]
            {
                get
                {
                    return this[idx: i];
                }
            }

            string IDataRecord.GetName(int i)
            {
                return this.columns[i].Name;
            }

            string IDataRecord.GetDataTypeName(int i)
            {
                return this.columns[i].DataType.ToString();
            }

            Type IDataRecord.GetFieldType(int i)
            {
                // Here we have to return native type from column type:  this.columns[i].DataType.ToString();
                switch (columns[i].DataType)
                {
                    case MTable.Column.DataTypes.Text:
                        return typeof(string);
                    case MTable.Column.DataTypes.Number:
                        return typeof(decimal);
                    case MTable.Column.DataTypes.Long:
                        return typeof(long);
                    case MTable.Column.DataTypes.Date:
                        return typeof(DateTime);
                    case MTable.Column.DataTypes.Binary:
                        return typeof(byte[]);
                    default:
                        throw new NotImplementedException();
                }
            }

            object IDataRecord.GetValue(int i)
            {
                return this[idx: i];
            }

            int IDataRecord.GetValues(object[] target)
            {
                if (target.Length < ordinals.Length)
                    throw new InvalidOperationException("Target array not large enougth");

                for (int idx = 0; idx < ordinals.Length; idx++)
                    target[idx] = this.values[ordinals[idx]];
                return ordinals.Length;
            }

            int IDataRecord.GetOrdinal(string name)
            {
                for (int idx = 0; idx < columns.Length; idx++)
                    if (name.Equals(columns[idx].Name, StringComparison.InvariantCultureIgnoreCase))
                        return idx;
                throw new InvalidOperationException($"Field {name} not found");
            }

            bool IDataRecord.GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            byte IDataRecord.GetByte(int i)
            {
                throw new NotImplementedException();
            }

            long IDataRecord.GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            char IDataRecord.GetChar(int i)
            {
                throw new NotImplementedException();
            }

            long IDataRecord.GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            Guid IDataRecord.GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            short IDataRecord.GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            int IDataRecord.GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            long IDataRecord.GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            float IDataRecord.GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            double IDataRecord.GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            string IDataRecord.GetString(int i)
            {
                return this[idx: i].ToString();
            }

            decimal IDataRecord.GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            DateTime IDataRecord.GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            IDataReader IDataRecord.GetData(int i)
            {
                throw new NotImplementedException();
            }

            bool IDataRecord.IsDBNull(int i)
            {
                return this[idx: i] == null;
            }

            /// <summary>
            /// Determines if the underlaying values of this view are equals to values of another view
            /// </summary>
            /// <param name="other"></param>
            /// <returns></returns>
            bool IEquatable<View>.Equals(View other)
            {
                // Compare the underlaying values arrays

                if (ReferenceEquals(values, other.values))
                    throw new InvalidOperationException("Checking for equity not allowed when referencing own view. The current and the other view points to the same reference. Make sure you run the iterator with copyView parameter.");

                if (values.Length != other.values.Length)
                    return false;

                if (ordinals.Length != other.ordinals.Length)
                    return false;

                // Compare the column keys values 
                for (var idx = 0; idx < ordinals.Length; idx++)
                    if (!object.Equals(values[ordinals[idx]], other.values[other.ordinals[idx]]))
                        return false;
                return true;
            }

            #endregion
        }

        [Obsolete("This should be replaced by something else in future. It represents record values wrapper. Its only used in the  undo framework. ")]
        public class ViewRow
        {
            private readonly long _record;
            private readonly Dictionary<MTable.Column, Object> _columnValues;
            internal ViewRow(long record, Dictionary<MTable.Column, Object> columnValues)
            {
                _record = record;
                _columnValues = new Dictionary<MTable.Column, object>(columnValues);
            }

            public long Record
            {
                get
                {
                    return _record;
                }
            }

            public object this[MTable.Column column]
            {
                get
                {
                    return ColumnValues[column];
                }
            }

            public Dictionary<MTable.Column, Object> ColumnValues
            {
                get
                {
                    return _columnValues;
                }
            }

            public IEnumerable<object> Values
            {
                get
                {
                    foreach (var value in _columnValues.Values)
                        yield return value;
                    yield break;
                }
            }
        }

      
    }
}
