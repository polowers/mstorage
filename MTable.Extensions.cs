﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt = DigitalToolworks.Data.MStorage.MTable;

namespace DigitalToolworks.Data.MStorage
{
    /// <summary>
    /// Various extensions methods related to the MTable
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Create where comparable lambda for this match mode , passing the comparable value delegate
        /// </summary>
        /// <param name="matchMode"></param>
        /// <param name="comparableValue"></param>
        /// <returns></returns>
        public static Func<dynamic, Query.View, bool> GetWhereComparer(this mt.MatchMode matchMode, Func<Query.View, dynamic> comparableValue)
        {
            var comparer = Comparer<dynamic>.Default;

            // Creates the appropriate comparision lembda expression, executed by the Where clause

            switch (matchMode)
            {
                case mt.MatchMode.IsEqual:
                    return (cellValue, record) =>
                    {
                        int diff = comparer.Compare(cellValue, comparableValue(record));
                        return diff == 0;
                    };

                case mt.MatchMode.NotEqual:
                    return (cellValue, record) =>
                    {
                        int diff = comparer.Compare(cellValue, comparableValue(record));
                        return diff != 0;
                    };

                case mt.MatchMode.IsGraterThen:
                    return (cellValue, record) =>
                    {
                        int diff = comparer.Compare(cellValue, comparableValue(record));
                        return diff > 0;
                    };

                case mt.MatchMode.IsGraterOrEqual:
                    return (cellValue, record) =>
                    {
                        int diff = comparer.Compare(cellValue, comparableValue(record));
                        return diff >= 0;
                    };

                case mt.MatchMode.IsLessThen:
                    return (cellValue, record) =>
                    {
                        int diff = comparer.Compare(cellValue, comparableValue(record));
                        return diff < 0;
                    };

                case mt.MatchMode.DataStartsWithMatch:
                    return (cellValue, record) =>
                    {
                        dynamic value = comparableValue(record);

                        if (!(value is string))
                            throw new InvalidOperationException("Invalid operation 'startsWith' on non-text value");

                        // StartsWith 
                        // skip if the target length is smaller then the comparable length
                        if (cellValue != null && cellValue.Length < value.Length)
                            return false;

                        int diff = string.Compare(strA: cellValue, indexA: 0, strB: value, indexB: 0, length: value.Length, ignoreCase: true);
                        return diff == 0;
                    };

                case mt.MatchMode.DataEndsWithMatch:
                    return (cellValue, record) =>
                    {
                        dynamic value = comparableValue(record);

                        if (!(value is string))
                            throw new InvalidOperationException("Invalid operation 'endsWith' on non-text value");

                        // EndsWith 
                        // skip if the target length is smaller then the comparable length
                        if (cellValue != null && cellValue.Length < value.Length)
                            return false;

                        int diff = string.Compare(strA: cellValue, indexA: value.Length - cellValue.Length, strB: value, indexB: 0, length: value.Length, ignoreCase: true);
                        return diff == 0;
                    };

                case mt.MatchMode.DataContainsMatch:
                    return (cellValue, record) =>
                    {
                        dynamic value = comparableValue(record);

                        if (!(value is string))
                            throw new InvalidOperationException("Invalid operation 'contains' on non-text value");

                        // Contains 
                        // skip if the target length is smaller then the comparable length
                        if (cellValue != null && cellValue.Length < value.Length)
                            return false;

                        return ((string)cellValue).Contains(value);
                    };

                case mt.MatchMode.IsLessOrEqual:
                    return (cellValue, record) =>
                    {
                        int diff = comparer.Compare(cellValue, comparableValue(record));
                        return diff <= 0;
                    };

                case mt.MatchMode.AnyOfList:
                    return (cellValue, record) =>
                    {
                        // Extract the list of values from the comparableValue. 
                        // Its enumerable containing the list of one or more values to compare.
                        // If any of the values in the list matches the cell value, return
                        foreach (var value in comparableValue(record))
                        {
                            int diff = comparer.Compare(cellValue, value);
                            if (diff == 0)
                                return true;
                        }
                        return false;
                    };

                case mt.MatchMode.NoneOfList:
                    return (cellValue, record) =>
                    {
                        // Extract the list of values from the comparableValue. 
                        // Its enumerable containing the list of one or more values to compare.
                        // If any of the values in the list matches the cell value, return
                        foreach (var value in comparableValue(record))
                        {
                            int diff = comparer.Compare(cellValue, value);
                            if (diff == 0)
                                return false;
                        }
                        return true;
                    };

                default:
                    throw new NotImplementedException("Match mode comparer not implemented. Mode=" + matchMode);
            }
        }

        /// <summary>
        /// Determines whether the match mode is comparable (can be used by comparators)
        /// </summary>
        /// <param name="matchMode"></param>
        /// <returns></returns>
        internal static bool IsComparableMode(this mt.MatchMode matchMode)
        {
            switch (matchMode)
            {
                case mt.MatchMode.IsEqual: return true;
                case mt.MatchMode.IsGraterOrEqual: return true;
                case mt.MatchMode.IsLessOrEqual: return true;
                case mt.MatchMode.IsLessThen: return true;
                case mt.MatchMode.IsGraterThen: return true;
            }

            return false;
        }

        /// <summary>
        /// Get column property descriptors for the columns in this collection 
        /// </summary>
        /// <param name="columns"></param>
        /// <param name="readOnly"></param>
        /// <returns></returns>
        public static PropertyDescriptorCollection GetDescriptors(this IEnumerable<MTable.Column> columns, bool readOnly = false)
        {
            var descriptors = new PropertyDescriptorCollection(columns.Select(column => new MTable.Column.Descriptor(column)).ToArray(), readOnly);
            return descriptors;
        } 
    }

}
