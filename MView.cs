﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalToolworks.Data.MStorage
{
    public sealed class MView : ITable
    {
        private readonly string _name;
        private readonly MTable _table;
        private readonly MTable.ColumnCollection _columns;

        private readonly long _countSkip;
        private readonly long _countTop;

        internal MView(string name, MTable table, long countSkip, long countTop)
        {
            _name = name;
            _table = table;
            _countSkip = countSkip;
            _countTop = countTop;
            _columns = new MTable.ColumnCollection(this);
        }

        public MTable Table
        {
            get
            {
                return _table;
            }
        }

        public Data.MTableCore.Records.FieldSet CreateFieldSet(MTable.Column[] columns)
        {
            var ordinals = columns.Select(column => column.Ordinal).Distinct().OrderBy(ordinal => ordinal).ToArray();
            return MTableCore.Records.FieldSet.Allocate((Data.MTableCore)_table.Core, ordinals, _table.Columns.Count);
        }

        internal void AddColumn(string name, string columnKeyName, MTable sourceTable, string sourceColumnName)
        {
            MTable.Column sourceColumn = sourceTable.Columns[sourceColumnName].Single();

            var columnKey = _table.Columns[columnKeyName].Single();
            var column = new MTable.Column(_table, name, columnKey.Ordinal, sourceColumn.DataType, sourceColumn.Size);

            column.association = new MTable.Association(null, _table, columnKey, name, sourceTable, sourceColumn);

            column.SortIndex = columnKey.SortIndex;
            column.TextIndex = columnKey.TextIndex;
            column.NativeSorted = columnKey.NativeSorted;

            _columns.Add(column);
        }

        internal void AddColumn(string name)
        {
            var column = _table.Columns[name].Single();

            _columns.Add(column);
        }

        public long Count { get { return _countTop > 0 ? Math.Min(_countTop, _table.Count) : _table.Count; } }

        public string Name { get { return _name; } }

        public override string ToString()
        {
            return Name;
        }

        public long StartAt
        {
            get
            {
                return _table.StartAt + _countSkip;
            }
        }

        public Query Query(string columnName, MTable.MatchMode matchMode, object matchValue)
        {
            var recordRange = Tuple.Create<long, long>(StartAt, StartAt + Count);

            var records = MTable.FindRecords(this.Columns[columnName].Single(), _table, matchValue, matchMode, null);
            return new Query(this, records);
        }

        public Query Query(params long[] recordKey)
        {
            return new Query(this, recordKey);
        }

        public Query Query()
        {
            return Query(0, (int)this.Count);
        }

        public Query Query(long start, int count)
        {
            return new Query(this, Enumerable.Range((int)(this.StartAt + start), count).Select(record => (long)record));
        }


        public IReadOnlyDictionary<string, MTable.ColumnIndex> Indexes
        {
            get { return _indexes; }
        }


        private readonly Dictionary<string, MTable.ColumnIndex> _indexes = new Dictionary<string, MTable.ColumnIndex>();

        public MTable.SortIndex AddSortIndex(string columnNames)
        {
            var index = new MTable.SortIndex(this, string.Format("[AIndex: {0} -> {1}]", this.Name, columnNames), columnNames);
            _indexes.Add(string.Format("SIDX:{0}", columnNames), index);
            return index;
        }

        public void RemoveRecord(params long[] recordKey)
        {
            _table.RemoveRecord(recordKey);
        }

        /// <summary>
        /// Gets the collection of columns for the view
        /// </summary>
        public MTable.ColumnCollection Columns
        {
            get { return _columns; }
        }


        public MTable.ColumnIndex AddHashIndex(string keyColumns)
        {
            throw new NotImplementedException();
        }


        public Query Query(string filterCriteriaExpression, Query.IFunctionResolver resolver, bool cacheRecords)
        {
            throw new NotImplementedException();
        }
    }
}
