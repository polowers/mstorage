﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using fieldset = DigitalToolworks.Data.MTableCore.Records.FieldSet;

namespace DigitalToolworks.Data.MStorage
{
    public sealed partial class MTable : ITable, IDisposable, INotifyPropertyChanged
    {
        public event ListChangedEventHandler ListChanged;
        string _name;
        MTableCore _tableCore;
        readonly MStorage storage;
       
        readonly List<Association> _associations = new List<Association>();
        readonly Dictionary<string, MTable.ColumnIndex> _indexes = new Dictionary<string, ColumnIndex>();
        readonly SortedDictionary<string, Column> _cacheColumns = new SortedDictionary<string, Column>();

        ColumnCollection _columns;

        readonly object _locker = new object();

        /// <summary>
        /// Unique table object identifier
        /// </summary>
        readonly int Uid;

        long _count = long.MinValue;
        bool _imported = false;
        long _startAt = long.MinValue;

        MTable.ConfigInfo config;

        public delegate Func<DigitalToolworks.Data.MStorage.Query.View, object> CalculationExpressionRequestedDelegate(MTable table, Column column, string expression);
        /// <summary>
        /// Event is called when table needs calculation expression callback method
        /// </summary>
        public static event CalculationExpressionRequestedDelegate CalculationExpresionRequested;

        /// <summary>
        /// Causes all expression calculations to be reset and request compilations
        /// </summary>
        public static void NotifyForExpressionsContextUpdate()
        {
            if (OnExpressionsContextUpdated != null)
                OnExpressionsContextUpdated(null, EventArgs.Empty);
        }

        static event EventHandler OnExpressionsContextUpdated;

        /// <summary>
        /// Indicates whether structure metadata was changed and needed to by flushed
        /// </summary>
        bool _isModified;

        /// <summary>
        /// Contains the table object unique identifier
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Uid;
        }

        public enum MatchMode
        {
            /// <summary>
            /// The cell value starts with the match value
            /// </summary>
            [BinaryOperatorAlias("startsWith|sw")]
            DataStartsWithMatch,
            /// <summary>
            /// The cell value ends with the match value
            /// </summary>
            [BinaryOperatorAlias("endsWith|ew")]
            DataEndsWithMatch,
            /// <summary>
            /// The cell value contains the match value
            /// </summary>
            [BinaryOperatorAlias("contains|ctn|like")]
            DataContainsMatch,
            /// <summary>
            /// The match value starts with the data value
            /// </summary>
            MatchStartsWithData,
            /// <summary>
            /// The match value ends with the data value
            /// </summary>
            MatchEndsWithData,
            /// <summary>
            /// The match value contains the data value
            /// </summary>
            MatchContainsData,
            /// <summary>
            /// The cell and match values are equal
            /// </summary>
            [BinaryOperatorAlias("eq|equal|=")]
            IsEqual,
            /// <summary>
            /// The cell and match values are not equal
            /// </summary>
            [BinaryOperatorAlias("ne|notEqual|not|<>")]
            NotEqual,
            /// <summary>
            /// The cell and match values are equal approximately
            /// </summary>
            [BinaryOperatorAlias("approximates|apx|~")]
            Approximates,
            /// <summary>
            /// The cell value contains the match value approximately
            /// </summary>
            [BinaryOperatorAlias("containsApprox|capx|\\^~")]
            DataContainsMatchApproximately,
            /// <summary>
            /// The match value contains the data value approximately
            /// </summary>
            MatchContainsDataApproximately,
            /// <summary>
            /// The match value is greater then the data value
            /// </summary>
            [BinaryOperatorAlias("greater|gt|>")]
            IsGraterThen,
            /// <summary>
            /// The match value is less then the data value
            /// </summary>
            [BinaryOperatorAlias("less|ls|<")]
            IsLessThen,
            /// <summary>
            /// The match value is greater or equal to the data value
            /// </summary>
            [BinaryOperatorAlias("greaterOrEqual|gteq|>=")]
            IsGraterOrEqual,
            /// <summary>
            /// The match value is less or equal to the data value
            /// </summary>
            [BinaryOperatorAlias("lessOrEqual|lseq|<=")]
            IsLessOrEqual,

            /// <summary>
            /// The cell value matches any of the values in the list
            /// </summary>
            [BinaryOperatorAlias("anyOf|in")]
            AnyOfList,
            /// <summary>
            /// The cell value does not match any of the values in the list
            /// </summary>
            [BinaryOperatorAlias("noneOf|notIn")]
            NoneOfList
        }

        /// <summary>
        /// Provides functionality to managing table state versioning
        /// </summary>
        public VersionManager Version { get; private set; }

        /// <summary>
        /// Create table for single-table mode storage
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="name"></param>
        /// <param name="uid"></param>
        internal MTable(MStorage storage, string name, int uid)
        {
            this.Uid = uid;
            _tableCore = null;
            _name = name;

            _createdOn = DateTime.UtcNow;
            _modifiedOn = _createdOn;

            this.storage = storage;

            this.Version = new VersionManager(this);
        }

        /// <summary>
        /// Create table for multi-table mode storage
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="name"></param>
        /// <param name="uid"></param>
        /// <param name="staticColumnLength"></param>

        internal MTable(MStorage storage, string name, int uid, ConfigInfo config)
            : this(storage, name, uid)
        {
            this.Initialize(config);
        }

        /// <summary>
        /// Initializes this MTable instance and creates the table core
        /// </summary>
        /// <param name="config"></param>
        private void Initialize(ConfigInfo config)
        {
            //  Validate initialization state. 
            if (_initialized)
                throw new InvalidOperationException("Attempt to initialize already initialized table was made");

            /// If the storage is persistent, configure the code table to use the table storage path as the paging path and configure the view manager for peristency
            /// In that case, also ensure the table folder is created

            this.config = config;

            string pagingMapFile = Uid.ToString();

            if (Config.IsPersistent)
            {
                //  The persistent table folder has to be stored in version subfolder, in form [000]

                while (!Directory.Exists(Config.DataFolder))
                    Directory.CreateDirectory(Config.DataFolder);
                pagingMapFile = Path.Combine(Config.DataFolder, "table");
            }

            //  Create the underlayoung mtable core, Use the table.Uid for underlaying storage name and file.
            _tableCore = new MTableCore(
                name: Uid.ToString(),
                filename: pagingMapFile,
                capacity: 1024L * 1024L * Config.MaxCapacityMb,
                indexCapacityMb: Config.IndexCapacityMb,
                persistMapFiles: Config.IsPersistent,
                metadataMapFile: Config.GetMetadataMap());

            _tableCore.RecordHeaderFormat = Config.UseStaticColumnLength ? MTableCore.RecordHeaderFormats.HybridFixed : MTableCore.RecordHeaderFormats.HybridFixed;

            //  Handle the view and content modification events
            _tableCore.Manager.OnBeginContentModification(Version.OnBeginContentModification);
            _tableCore.MapViews.OnBeginViewModification(Version.OnBeginViewModification);

            _initialized = true;   // This flag must be set here so following routines won't crash

            if (!Config.NewlyCreated)
                this.LoadPersistentData();

            _tableCore.Setup();

            _loadedOn = DateTime.UtcNow;

            _loaded = true;
        }

        /// <summary>
        /// Marks the table structure as modified
        /// </summary>
        private void SetModified()
        {
            lock (_locker)
            {
                if (_isModified) return;

                //  Prevent modification non recent table version
                this.Version.CheckAllowVersionModify();

                _modifiedOn = DateTime.UtcNow;
                _isModified = true;
            }
        }

        /// <summary>
        /// Ensures that the data are loaded from persistency storage (if the table is in persistent mode)
        /// </summary>
        internal void LoadPersistentData()
        {
            var sw = System.Diagnostics.Stopwatch.StartNew();

            lock (_locker)
            {
                if (_isModified)
                    throw new InvalidOperationException("Table modified upon loading persistent data. This is not expected table state.");

                _isModified = true;  // << Purposely set the modification flag, so it will be ignored by subsequent calls durring loading persistent data.

                this.BeginImport();

                // If there is metadata mapped file, load the meta data, which is located at the start of the stream

                using (var view = Config.GetMetadataMap().CreateViewStream(0, Metadata.Header.MetaBlockSize))
                {

                    unsafe
                    {
                        //  Deserialize the MTable Header, including the columns.
                        //  Attach the columns block to the proper header pointer location
                        Metadata.Header.Table* tableHdr = (Metadata.Header.Table*)Native.Utils.Memory.AcquirePointer(view);
                        tableHdr->Columns = (Metadata.Header.Table.Column*)((byte*)tableHdr + sizeof(Metadata.Header.Table));

                        //  Validate the header format
                        if (tableHdr->FormatMarker != Metadata.Header.HeaderMarker)
                            throw new InvalidDataException("Failed to load peristent storage data. Invalid table format");

                        this.Name = new string(tableHdr->TableName);

                        this.RevisitonVersion = tableHdr->RevisitonVersion;

                        //  Deserialize the MTable Columns
                        this.Columns.Clear();
                        this._cacheColumns.Clear();

                        for (var idx = 0; idx < tableHdr->ColumnsCount; idx++)
                        {
                            Metadata.Header.Table.Column* columnHdr = &tableHdr->Columns[idx];
                            var column = new Column(this, new string(columnHdr->ColumnName), (int)columnHdr->ColumnID, (Column.DataTypes)columnHdr->DataType, (int)columnHdr->DataTypeSize);

                            //  Add the column in the collection. And if the column is marked as removed, detach the same instance from the collection
                            this.Columns.Add(column);
                            if (columnHdr->Removed)
                                this.Columns.RemoveInternal(column);
                        }

                        //  Read the timestamps. Note: this has to be done as last operation (here), becuase the modification timestamp is already altered by the earlier columns adding.
                        this._modifiedOn = DateTime.FromBinary((long)tableHdr->TimestampModified);
                        this._createdOn = DateTime.FromBinary((long)tableHdr->TimestampCreated);

                        view.SafeMemoryMappedViewHandle.ReleasePointer();
                    }

                    //  Load Extended information to separate persistent file extended.meta  
                    if (File.Exists(Path.Combine(Config.DataFolder, "extended.meta")))
                        using (var reader = new BinaryReader(File.Open(Path.Combine(Config.DataFolder, "extended.meta"), FileMode.Open)))
                            if (reader.BaseStream.Length > 0)
                            {
                                //  Deserialize the Expressions for columns
                                foreach (var column in Columns.GetAllColumns().Keys)
                                {
                                    var expression = reader.ReadString();
                                    column.Expression = string.IsNullOrWhiteSpace(expression) ? null : expression;
                                }
                            }

                }

                //  Attach the persistent data files.
                //  If the table header contains list of associated view IDs, use them to create the file list.
                //  Another way to determine the associated files is, by calculating using the total table data and the view size.
                //  Otherwise, use the Config.DataFolder as root for the data files.

                int viewsCount = (int)Math.Round((double)Core.Blocks.TotalDataSize / (double)MapViewManager.Configuration.ViewSize);
                if (viewsCount > 0)
                {

                }

                Core.Manager.RestoreDataFiles(Config.DataFolder);

                this.EndImport();

                //  Load the ActionManager 
                //  if (this.Config.ActionManagerEnabled)
                if (Config.ActionManager != null)
                    Config.ActionManager.LoadActions(this, Path.Combine(Config.DataFolder, "table.actions"));

                _loadedOn = DateTime.UtcNow;
                _isModified = false;
                _loaded = true;
            }

            sw.Stop();
            System.Diagnostics.Debug.WriteLine("MTable {0}: LoadPersistentData, {1}ms", this.Name, sw.ElapsedMilliseconds);
        }

        /// <summary>
        /// <remarks>
        /// Ensures that the data are saved into the persistency storage (if the table is in persistent mode). <para/>
        /// </remarks>
        /// Takes place only if the table is in modified state, unless the force parameter is set, in which case the data will be saved no matter the modification state of the table.
        /// <param name="force">True to force saving persistent data, no matter the modification state of the table</param>
        /// </summary>
        internal void SavePersistentData(bool force = false)
        {
            lock (_locker)
            {
                //  If there is metadata mapped file, store the meta data, which is located at the start of the stream.
                if (Config.IsPersistent && (_isModified || force))
                {
                    // Validate: Allow saving only if the data has been already loaded by checking the _loaded flag to determine the case. 
                    // This prevents saving potencially corrupted table state or data.
                    if (!_loaded)
                        throw new InvalidOperationException("Attempt to save table persistent data is not allowed. Cause: the table content has not been loaded yet.");

                    var sw = System.Diagnostics.Stopwatch.StartNew();

                    using (var view = Config.GetMetadataMap().CreateViewStream(0, Metadata.Header.MetaBlockSize))
                    {
                        unsafe
                        {
                            // Serialize the MTable Header, including the columns.
                            // Attach the columns pointer to the proper position, right after the TableHeader.
                            Metadata.Header.Table* tableHdr = (Metadata.Header.Table*)Native.Utils.Memory.AcquirePointer(view);
                            tableHdr->Columns = (Metadata.Header.Table.Column*)((byte*)tableHdr + sizeof(Metadata.Header.Table));

                            tableHdr->FormatMarker = Metadata.Header.HeaderMarker;

                            // Set the timestamps
                            tableHdr->TimestampCreated = (ulong)this._createdOn.ToBinary();
                            tableHdr->TimestampModified = (ulong)this._modifiedOn.ToBinary();

                            tableHdr->RevisitonVersion = ++this.RevisitonVersion; // increase the version on each Save

                            tableHdr->TableID = (uint)Uid;

                            fixed (char* pName = this.Name)
                            {
                                Native.memset((byte*)tableHdr->TableName, 0, (Name.Length + 1) * sizeof(char));
                                Native.Utils.wstrcpy(tableHdr->TableName, pName, Name.Length);
                            }

                            var columns = this.Columns.GetAllColumns();

                            tableHdr->ColumnsCount = (uint)columns.Count;

                            // Process the columns
                            var idx = 0;
                            foreach (var entry in columns)
                            {
                                var column = entry.Key;
                                var removed = entry.Value;

                                Metadata.Header.Table.Column* columnHdr = &tableHdr->Columns[idx];

                                columnHdr->ColumnID = (ushort)column.Ordinal;

                                fixed (char* pName = column.Name)
                                {
                                    Native.memset((byte*)columnHdr->ColumnName, 0, (column.Name.Length + 1) * sizeof(char));
                                    Native.Utils.wstrcpy(columnHdr->ColumnName, pName, column.Name.Length);
                                }

                                columnHdr->DataType = (byte)column.DataType;
                                columnHdr->DataTypeSize = (ushort)column.Size;
                                columnHdr->VisibleIndex = (ushort)idx;
                                columnHdr->Removed = removed;

                                idx++;
                            }

                            view.SafeMemoryMappedViewHandle.ReleasePointer();
                        }
                    }

                    // Store Extended information to separate persistent file extended.meta                     
                    using (var writer = new BinaryWriter(File.Open(Path.Combine(Config.DataFolder, "extended.meta"), FileMode.Create)))
                    {
                        // Serialize the expressions for columns
                        foreach (var column in Columns.GetAllColumns().Keys)
                            writer.Write(column.Expression ?? string.Empty);
                    }

                    //if (Config.ActionManagerEnabled)
                    //    Config.ActionManager.SaveActions(this, Path.Combine(Storage.StorageFolder, Path.Combine(Uid.ToString(), "table.actions")));

                    // Manually modify the last edit date of the file
                    File.SetLastWriteTime(Path.Combine(Config.DataFolder, "table.meta"), _modifiedOn);

                    _isModified = false;

                    sw.Stop();
                    System.Diagnostics.Debug.WriteLine("MTable {0}: SavePersistentData, {1}ms", this.Name, sw.ElapsedMilliseconds);
                }
            }
        }

        public ConfigInfo Config
        {
            get { return config; }
        }

        private object[] DefaultValues()
        {
            var defaults = Enumerable.Range(0, this.Columns.Count).Select(idx => (object)null).ToArray();
            return defaults;
        }

        public void AppendRecord()
        {
            AppendRecord(DefaultValues());
        }

        public void AppendRecord(object[] values)
        {
            if (!Core.Manager.InTransation)
                InsertRecord(values, this.Count);
            else
                InsertRecord(values, this.Count + Core.Manager.TransactionQueueCount);
        }

        public void InsertRecord(params long[] positions)
        {
            InsertRecord(DefaultValues(), positions);
        }

        public void InsertRecord(object[] values, params long[] positions)
        {
            // Store the undo-redo action if the action manager is enabled
            if (config.ActionManagerEnabled)
                config.ActionManager.RecordAction(new TableAction.InsertRecords(this, positions, values));
            else
                foreach (var position in positions)
                    this.InsertRecordInternal(values, position);
        }

        /// <summary>
        /// Insert group of records in single call. The positions contains the records keys and the values collection contains the values for each record
        /// </summary>
        /// <param name="values"></param>
        /// <param name="positions"></param>
        public void InsertRecord(object[][] values, long[] positions)
        {
            // Store the undo-redo action if the action manager is enabled
            if (config.ActionManagerEnabled)
                config.ActionManager.RecordAction(new TableAction.InsertRecords(this, positions, values));
            else
                foreach (var position in positions)
                    this.InsertRecordInternal(values, position);
        }

        private void InsertRecordInternal(object[] values, long position)
        {
            if (storage.isMultiTable && values.Length != this.Columns.Count)
                throw new InvalidOperationException("Source and target column counts mismatch. Count = " + values.Length + ", Table = " + this.Name);

            bool insertAsTyped = Config.InsertTypedValues;

            var header = InsertRecordHeader.GetFree();

            header.Position = position;
            header.Length = 0;
            header.Count = values.Length;

            int idx = 0;

            foreach (var column in Columns)
            {
                int length = 0;

                // Prepare the insert value
                object value = values[idx];

                // Ensure the value is nulled correctly
                if (insertAsTyped)
                    if (value is DBNull)
                        value = null;

                switch (column.DataType)
                {
                    case Column.DataTypes.Binary:
                        {
                            byte[] binary = (byte[])value;
                            if (binary != null)
                            {
                                length = binary.Length;
                                Buffer.BlockCopy(binary, 0, header.Data, header.Length, length);
                            }

                            header.Lengths[idx] = length;
                            header.Offsets[idx] = header.Length;
                            header.Length += length;
                            break;
                        }
                    case Column.DataTypes.Text:
                        {
                            string text = (value as string) ?? string.Empty;

                            if (insertAsTyped)
                            {
                                // when inserting as typed, ensure the non-string values are converted to text
                                if (value != null && !(value is string))
                                {
                                    // if value is Byte[], convert it to Hex String
                                    if (value is byte[])
                                        text = GetHexText((byte[])value);
                                    else
                                        text = Convert.ToString(value);
                                }
                            }

                            length = IDataStorage.Encoder.GetBytes(text, 0, text.Length, header.Data, header.Length);

                            header.Lengths[idx] = length;
                            header.Offsets[idx] = header.Length;

                            header.Length += length;

                            if (config.AutoDataInfoEnabled && insertAsTyped)
                                column.DataInfo.Register(text, length);
                            break;
                        }
                    case Column.DataTypes.Long:
                        {
                            length = 0;

                            if ((value != null && (!(value is string)) || !string.IsNullOrWhiteSpace((string)value)))
                            {
                                // when inserting as typed, use the native type if it is acceptable, or convert it to its text representation

                                long number;

                                if (insertAsTyped)
                                {
                                    if (value is long)
                                        number = (long)value;

                                    else if (value is int)
                                        number = (int)value;

                                    else if (value is short)
                                        number = (short)value;

                                    else if (value is byte)
                                        number = (byte)value;

                                    else
                                        number = Convert.ToInt64(value); // fallback to normal convertion
                                }
                                else
                                    number = Convert.ToInt64(value);

                                if (column.Size > 0)
                                    length = column.Size;
                                else
                                    length = Native.Utils.GetNumberSize(number);

                                var binary = BitConverter.GetBytes(number);
                                Buffer.BlockCopy(binary, 0, header.Data, header.Length, length);

                                if (config.AutoDataInfoEnabled && insertAsTyped)
                                    column.DataInfo.Register(number, length);
                            }
                            else
                                if (config.AutoDataInfoEnabled && insertAsTyped)
                                column.DataInfo.Register(0, length);

                            header.Lengths[idx] = length;
                            header.Offsets[idx] = header.Length;
                            header.Length += length;

                            break;
                        }

                    case Column.DataTypes.Number:
                        {
                            length = 0;

                            if ((value != null && (!(value is string)) || !string.IsNullOrWhiteSpace((string)value)))
                            {
                                double real;

                                // when inserting as typed, use the native type if it is acceptable, or convert it to its text representation
                                if (insertAsTyped)
                                {
                                    if (value is double)
                                        real = (double)value;

                                    else if (value is decimal)
                                        real = Convert.ToDouble(value);

                                    else if (value is int)
                                        real = Convert.ToDouble(value);

                                    else if (value is long)
                                        real = Convert.ToDouble(value);

                                    else throw new NotSupportedException("Type not supported. Cant convert to Double: " + value.GetType());
                                }
                                else
                                    real = Convert.ToDouble(value);

                                // convert the object to double and store it as number
                                var number = BitConverter.DoubleToInt64Bits(real);

                                if (column.Size > 0)
                                    length = column.Size;
                                else
                                    length = Native.Utils.GetNumberSize(number);

                                var binary = BitConverter.GetBytes(number);
                                Buffer.BlockCopy(binary, 0, header.Data, header.Length, length);

                                if (config.AutoDataInfoEnabled && insertAsTyped)
                                    column.DataInfo.Register(real, length);
                            }
                            else
                                if (config.AutoDataInfoEnabled && insertAsTyped)
                                column.DataInfo.Register(0, length);

                            header.Lengths[idx] = length;
                            header.Offsets[idx] = header.Length;
                            header.Length += length;

                            break;
                        }
                    case Column.DataTypes.Date:
                        {
                            length = 0;

                            if ((value != null && (!(value is string)) || !string.IsNullOrWhiteSpace((string)value)))
                            {
                                // Store the Dates as .NET Ticks
                                // convert the Text into date

                                DateTime date;

                                // when inserting as typed, use the native type if it is acceptable, or convert it to its text representation
                                if (insertAsTyped && (value is DateTime))
                                    date = (DateTime)value;
                                else
                                    date = Convert.ToDateTime(value);

                                var dateDate = date.ToBinary();

                                if (column.Size > 0)
                                    length = column.Size;
                                else
                                    length = Native.Utils.GetNumberSize(dateDate);

                                var binary = BitConverter.GetBytes(dateDate);
                                Buffer.BlockCopy(binary, 0, header.Data, header.Length, length);

                                if (config.AutoDataInfoEnabled && insertAsTyped)
                                    column.DataInfo.Register(dateDate, length);
                            }
                            else
                                   if (config.AutoDataInfoEnabled && insertAsTyped)
                                column.DataInfo.Register(0, length);

                            header.Lengths[idx] = length;
                            header.Offsets[idx] = header.Length;
                            header.Length += length;

                            break;
                        }

                    default:
                        throw new NotImplementedException("Data type not implemented");
                }

                // Updates the column data info statistics
                if (config.AutoDataInfoEnabled && !insertAsTyped)
                    column.DataInfo.Register(values[idx], length);

                idx++;
            }

            if (storage.isMultiTable)
            {
                Core.Manager.InsertRecord(header);

                _count = long.MinValue; // reset the count, so it will be lazy obtained from the core on demand
            }
            else
            {
                storage.table.Manager.InsertRecord(header);

                _count++;
            }

            // Mark the indexes as dirty
            foreach (IBuildableIndex index in this.Indexes.Values)
                index.SetDirty();

            if (false)
                if (ListChanged != null)
                    ListChanged(this, new ListChangedEventArgs(ListChangedType.ItemAdded, (int)position));
        }

        /// <summary>
        /// Notifies any clients which has been attached to this table for changes withing the data or structure
        /// </summary>
        /// <param name="args"></param>
        internal void NotifyListChanged(ListChangedEventArgs args)
        {
            // NOTE: Handle the UI threading issue
            return;
            if (ListChanged != null)
                ListChanged(this, args);
        }

        /// <summary>
        /// Notifies any clients which has been attached to this table for changes
        /// </summary>
        public void NotifyChanged()
        {
            if (ListChanged != null)
                ListChanged(this, new ListChangedEventArgs(ListChangedType.Reset, 0));
        }

        /// <summary>
        /// Converts the data to hex-string representation of the binary (0xAE0FF0ECC3CD0FF... )
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private string GetHexText(byte[] binary)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Converts the object content into numerical bytes with exact length based on the numeric value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="columnSize"></param>
        /// <returns></returns>
        private static byte[] ObjectToNumeric(object value, int columnSize, bool realNumber = false)
        {
            var text = value != null ? value.ToString() : null;

            var length = 0;
            byte[] numeric;

            if (!string.IsNullOrEmpty(text))
            {
                long number;

                if (realNumber) // Get the double number and conver it to long
                    number = BitConverter.DoubleToInt64Bits(Convert.ToDouble(text));
                else
                    number = Convert.ToInt64(text);

                if (columnSize > 0)
                    length = columnSize;
                else
                    length = Native.Utils.GetNumberSize(number);

                var binary = BitConverter.GetBytes(number);
                numeric = new byte[length];
                Buffer.BlockCopy(binary, 0, numeric, 0, length);
            }
            else numeric = new byte[0];

            return numeric;
        }


        /// <summary>
        /// Modify value of record field
        /// </summary>
        /// <param name="recordKey"></param>
        /// <param name="column"></param>
        /// <param name="value"></param>
        public void ModifyRecord(long recordKey, Column column, object value, int currentValueLength)
        {
            // Store the undo-redo action if the action manager is enabled
            if (config.ActionManagerEnabled)
            {
                // Optimization: Make this as simple as possible, no need for passing complex ViewRow structures of dictionaries
                var columnsValues = new Dictionary<Column, object>();
                columnsValues.Add(column, value);

                config.ActionManager.RecordAction(new TableAction.ModifyRecords(this, new Query.ViewRow(recordKey, columnsValues)));
            }
            else
                this.ModifyRecordInternal(recordKey, column, value, currentValueLength);
        }

        private void ModifyRecordInternal(long recordKey, Column column, object value, int currentValueLength)
        {
            if (StartAt > recordKey || StartAt + Count <= recordKey)
                throw new InvalidOperationException("The record key " + recordKey + " was not found in the table " + Name);

            // Modify the underlaying record value
            byte[] valueData = new byte[0];

            if (column.DataType == Column.DataTypes.Text)
                valueData = System.Text.Encoding.Default.GetBytes((string)value ?? string.Empty);
            else
                if (column.DataType == Column.DataTypes.Long)
                valueData = ObjectToNumeric(value, column.Size, false);
            else
                    if (column.DataType == Column.DataTypes.Number)
                valueData = ObjectToNumeric(value, column.Size, true);
            else
                        if (column.DataType == Column.DataTypes.Date)
                valueData = ObjectToNumeric(value != null ? (object)Convert.ToDateTime(value).ToBinary() : null, column.Size, false);
            else
                throw new NotSupportedException("Modified record value data type (" + column.DataType + ") not supported yet");

            // If the storage is in single table mode, alter the column count of the core table to the current table
            var result = this.Core.Manager.ModifyRecord(recordKey, column.Ordinal, currentValueLength, valueData, this.Columns.Count);

#if DEBUG_
            System.Diagnostics.Debug.WriteLine(result);
            this.Core.Blocks.PrintStats(recordKey, false, false);

            var modifiedValue = this.Query(recordKey).RecordView(column.Name).Single().Values.Single();

            if (!Object.Equals(modifiedValue, value))
                throw new InvalidOperationException("The modified value was not properly commited. Implementation inspection needed");
#endif

            if (column.SortIndex != null)
                column.SortIndex.SetDirty();

            if (false)
                if (ListChanged != null)
                    ListChanged(this, new ListChangedEventArgs(ListChangedType.ItemChanged, (int)recordKey));
        }

        internal MStorage Storage { get { return storage; } }

        public string Name
        {
            get
            {
                return _name ?? _tableCore.Name;
            }

            set
            {
                if (_name != value)
                {
                    _name = value;

                    if (PropertyChanged != null)
                        PropertyChanged(this, new PropertyChangedEventArgs("Name"));

                    this.SetModified();
                }
            }
        }

        public MTableCore Core
        {
            get { return storage.isMultiTable ? _tableCore : storage.table; }
        }

        /// <summary>
        /// Gets the collection of columns for the table
        /// </summary>
        public ColumnCollection Columns
        {
            get
            {
                VerifyInitialized();

                return _columns != null ? _columns : _columns = new ColumnCollection(this);
            }
        }

        public IReadOnlyList<Association> Associations
        {
            get
            {
                return _associations;
            }
        }

        public Association AddAssociation(string name, string columnName, string displayAs, MTable childTable, string childColumnName)
        {
            var parentColumn = this.Columns.Single(column => column.Name == columnName);
            return AddAssociation(name, parentColumn, displayAs, childTable, childTable.Columns.Single(column => column.Name == childColumnName));
        }

        public TextIndex AddTextIndex(string columnName)
        {
            var column = this.Columns.Single(col => col.Name == columnName);
            var index = new TextIndex(this, column, string.Format("[CIndex: {0} -> {1}]", this.Name, column.Name));
            _indexes.Add(string.Format("TIDX:{0}", column.Name), index);
            return index;
        }



        public Association AddAssociation(string name, Column column, string displayAs, MTable childTable, Column childColumn)
        {
            var association = new Association(name, this, column, displayAs, childTable, childColumn);
            _associations.Add(association);
            return association;
        }

        public static int StringCompare(string text1, string text2, int compareCount)
        {
            if (compareCount > 0)
                return StringComparer.Ordinal.Compare(text1.Length > compareCount ? text1.Substring(0, compareCount) : text1, text2.Length > compareCount ? text2.Substring(0, compareCount) : text2);

            return StringComparer.Ordinal.Compare(text1, text2);
        }

        private static IEnumerable<long> BinaryTextMatch(ITable table, Column column, fieldset fieldSet, long startAtRecord, long endAtRecord, string value, MatchMode matchMode, int compareCount = 0)
        {
            var iterationCalcCount = Math.Log((double)endAtRecord - (double)startAtRecord, 2);
            int iterationCount = 0;
            int foundCount = 0;

            try
            {
                if (compareCount == 0)
                    switch (matchMode)
                    {
                        case MatchMode.DataStartsWithMatch:
                            {
                                compareCount = value.Length;
                                break;
                            }
                    }

                var associationIndex = column.SortIndex;
                var nativeSorted = associationIndex != null ? false : column.NativeSorted;

                long mid,
                    lowBound = startAtRecord,
                    highBound = endAtRecord;

                while (lowBound <= highBound)
                {
                    mid = (lowBound + highBound) / 2;

                    var sortedNumber = nativeSorted ? mid : associationIndex.GetSortedRecord(mid);

                    var columnValue = (string)column.GetValue(fieldSet.GetFields(sortedNumber)[0], false);

                    var cmp = StringCompare(columnValue, value, compareCount);

                    if (cmp < 0)//the element we search is located to the right from the mid point
                    {
                        lowBound = mid + 1;

                        iterationCount++;

                        continue;
                    }
                    else if (cmp > 0)//the element we search is located to the left from the mid point
                    {
                        highBound = mid - 1;

                        iterationCount++;
                        continue;
                    }
                    //at this point low and high bound are equal and we have found the element or
                    //arr[mid] is just equal to the value => we have found the searched element
                    else
                    {
                        iterationCount++;

                        // Determine the range of same values. 
                        // Check the next and previous values. If any is same to this, start moving forward and/or backwards untill different value is found or end/begin.

                        string neighbourValue;
                        long position = mid;
                        lowBound = mid;
                        highBound = mid;

                        while (position > startAtRecord && StringCompare(neighbourValue = (string)column.GetValue(fieldSet.GetFields(nativeSorted ? --position : associationIndex.GetSortedRecord(--position))[0], false), value, compareCount) == 0)
                        {
                            lowBound = position;
                            iterationCount++;
                        }

                        position = mid;

                        while (position < endAtRecord && StringCompare(neighbourValue = (string)column.GetValue(fieldSet.GetFields(nativeSorted ? ++position : associationIndex.GetSortedRecord(++position))[0], false), value, compareCount) == 0)
                        {
                            highBound = position;
                            iterationCount++;
                        }

                        if (lowBound != highBound)
                        {
                            foundCount = ((int)highBound - (int)lowBound) + 1;
                            return Enumerable.Range((int)lowBound, foundCount).Select(number => nativeSorted ? number : associationIndex.GetSortedRecord(number));
                        }
                        else
                        {
                            foundCount = 1;
                            return new long[1] { nativeSorted ? mid : associationIndex.GetSortedRecord(mid) };
                        }
                    }
                }

                return Enumerable.Empty<long>(); //value not found
            }
            finally
            {

#if SHOW_QUERY_INFO
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    string columnName = string.Format("{0}.{1}", table.Name, column.Name);
                    if (column.Association != null)
                        columnName = column.Association.ToString(); // string.Format("{0}.{1}", column.Association.ChildTable.Name, column.Association.ChildColumn.Name);

                    System.Diagnostics.Debug.WriteLine("find mode: sort, column: {0}, value: {1}, found: {2}, range: {3} - {4} ({5}), iterations: {6} / {7}",
                        columnName,
                        value,
                        foundCount,
                        startAtRecord,
                        endAtRecord,
                        endAtRecord - startAtRecord,
                        iterationCount,
                        iterationCalcCount);
                }
#endif
            }
        }

        private static IEnumerable<long> BinaryNumericMatch(ITable table, Column column, fieldset fieldSet, long startAtRecord, long endAtRecord, long value)
        {
            var iterationCalcCount = Math.Log((double)endAtRecord - (double)startAtRecord, 2);
            int iterationCount = 0;
            int foundCount = 0;

            try
            {
                var associationIndex = column.SortIndex;
                var nativeSorted = column.NativeSorted;

                long mid,
                    lowBound = startAtRecord,
                    highBound = endAtRecord;

                while (lowBound <= highBound)
                {
                    mid = (lowBound + highBound) / 2;

                    var sortedNumber = nativeSorted ? mid : associationIndex.GetSortedRecord(mid);

                    var columnValue = (long)column.GetValue(fieldSet.GetFields(sortedNumber)[0], false);

                    var cmp = columnValue - value;

                    if (cmp < 0)//the element we search is located to the right from the mid point
                    {
                        lowBound = mid + 1;

                        iterationCount++;

                        continue;
                    }
                    else if (cmp > 0)//the element we search is located to the left from the mid point
                    {
                        highBound = mid - 1;

                        iterationCount++;
                        continue;
                    }
                    //at this point low and high bound are equal and we have found the element or
                    //arr[mid] is just equal to the value => we have found the searched element
                    else
                    {
                        iterationCount++;

                        // Determine the range of same values. 
                        // Check the next and previous values. If any is same to this, start moving forward and/or backwards untill different value is found or end/begin.

                        long neighbourValue;
                        long position = mid;
                        lowBound = mid;
                        highBound = mid;

                        while (position > startAtRecord && (neighbourValue = (long)column.GetValue(fieldSet.GetFields(nativeSorted ? --position : associationIndex.GetSortedRecord(--position))[0], false)) == value)
                        {
                            lowBound = position;
                            iterationCount++;
                        }

                        position = mid;

                        while (position < endAtRecord && (neighbourValue = (long)column.GetValue(fieldSet.GetFields(nativeSorted ? ++position : associationIndex.GetSortedRecord(++position))[0], false)) == value)
                        {
                            highBound = position;
                            iterationCount++;
                        }

                        if (lowBound != highBound)
                        {
                            foundCount = ((int)highBound - (int)lowBound) + 1;
                            return Enumerable.Range((int)lowBound, foundCount).Select(number => nativeSorted ? number : associationIndex.GetSortedRecord(number));
                        }
                        else
                        {
                            foundCount = 1;
                            return new long[1] { nativeSorted ? mid : associationIndex.GetSortedRecord(mid) };
                        }
                    }
                }

                return Enumerable.Empty<long>(); //value not found
            }
            finally
            {

#if SHOW_QUERY_INFO
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    string columnName = string.Format("{0}.{1}", table.Name, column.Name);
                    if (column.Association != null)
                        columnName = column.Association.ToString(); // string.Format("{0}.{1}", column.Association.ChildTable.Name, column.Association.ChildColumn.Name);

                    System.Diagnostics.Debug.WriteLine("find mode: sort, column: {0}, value: {1}, found: {2}, range: {3} - {4} ({5}), iterations: {6} / {7}",
                        columnName,
                        value,
                        foundCount,
                        startAtRecord,
                        endAtRecord,
                        endAtRecord - startAtRecord,
                        iterationCount,
                        iterationCalcCount);
                }
#endif
            }
        }

        public static IEnumerable<long> FindRecords(Column column, MTable table, object matchValue, MTable.MatchMode matchMode, Tuple<long, long> recordRange = null, IEnumerable<long> matchInRecords = null, bool ignoreSortIndex = false)
        {
            string matchValueText = matchValue != null ? matchValue.ToString() : string.Empty;
            var rows = Enumerable.Empty<long>();
            var sortRecords = true;


            // Prepare the native match mode
            /********************************************************/
            bool directMatch = true;
            if (matchMode == MatchMode.MatchContainsData || matchMode == MatchMode.MatchEndsWithData || matchMode == MatchMode.MatchStartsWithData || matchMode == MatchMode.MatchContainsDataApproximately)
                directMatch = false;

            bool performApproximateMatch = false;
            MTableCore.ContentManager.MatchMode nativeMatchMode = MTableCore.ContentManager.MatchMode.Equals;
            if (matchMode == MatchMode.DataContainsMatch || matchMode == MatchMode.MatchContainsData)
                nativeMatchMode = MTableCore.ContentManager.MatchMode.Contains;
            if (matchMode == MatchMode.DataEndsWithMatch || matchMode == MatchMode.MatchEndsWithData)
                nativeMatchMode = MTableCore.ContentManager.MatchMode.EndsWith;
            if (matchMode == MatchMode.DataStartsWithMatch || matchMode == MatchMode.MatchStartsWithData)
                nativeMatchMode = MTableCore.ContentManager.MatchMode.StartsWith;

            if (matchMode == MatchMode.Approximates || matchMode == MatchMode.DataContainsMatchApproximately || matchMode == MatchMode.MatchContainsDataApproximately)
                performApproximateMatch = true;
            /********************************************************/


            // Create list of all associated columns and tables and do search on them first, keeping the results in associated column dictionary. 
            // Then, process this call columns normally and if the current column processed has associated results in the dictionary, use those results as match values

            long[] associatedRecordKeys = null;
            if (column.Association != null)
            {
                var childTable = column.Association.ChildTable;

                associatedRecordKeys = childTable
                    .Query(column.Association.ChildColumn.Name, matchMode, matchValue)
                    .Select(record => (record + 1) - childTable.StartAt)       // Adjust the 0-based key to 1-based and add the table start offset to get the native key value
                    .ToArray();
            }

            var fieldSet = table.CreateFieldSet(new Column[] { column });

            long startAt = table.StartAt;
            long endAt = table.StartAt + table.Count;

            // Narrow the rows to the records range and validate the range to belong to the table range
            if (recordRange != null)
            {
                startAt = Math.Max(recordRange.Item1, startAt);     // Math.Max(startAt + recordRange.Item1, startAt);
                endAt = Math.Min(recordRange.Item2, endAt);         // endAt = Math.Min(startAt + (recordRange.Item2 - recordRange.Item1), endAt);
            }
            else
                if (matchInRecords != null)
            {
                try
                {
                    startAt = Math.Max(matchInRecords.Min(), startAt);
                    endAt = Math.Min(matchInRecords.Max(), endAt);
                }
                catch (InvalidOperationException)
                {
                    // The matchInRecords is empty, which causes the Min or Max to crash. 
                    // In that case, return empty results iterator

                    return rows;
                }
            }

            // If this column has associated results, convert the associated match result into numbers and get their associated values rows back
            if (column.Association != null)
            {
                // If there are more then two associated keys, determine the limitation records range, by getting the first key, then last key
                // then process all the remaining keys using the range calculated using the first and the last keys
                bool rangeDetermined = false;
                if (associatedRecordKeys.Length > 2)
                {
                    var upperKeys = FindRecords(column.Association.ParentColumn, table, associatedRecordKeys.First(), MatchMode.IsEqual, recordRange, null, ignoreSortIndex);
                    rows = rows.Concat(upperKeys);

                    long upperKey = upperKeys.First();
                    // Narrow the search using the upper key
                    var lowerKeys = FindRecords(column.Association.ParentColumn, table, associatedRecordKeys.Last(), MatchMode.IsEqual, Tuple.Create<long, long>(upperKey, endAt), null, ignoreSortIndex);

                    rows = rows.Concat(lowerKeys);

                    recordRange = Tuple.Create<long, long>(upperKey, rows.Last());

                    rangeDetermined = true;
                }

                // Skip to process and first and last key, if the range limitation was deterined
                for (int i = rangeDetermined ? 1 : 0; i < associatedRecordKeys.Length - (rangeDetermined ? 1 : 0); i++)
                {
                    var associatedRecordKey = associatedRecordKeys[i];

                    IEnumerable<long> records = FindRecords(column.Association.ParentColumn, table, associatedRecordKey, MatchMode.IsEqual, recordRange, null, ignoreSortIndex);

                    rows = rows.Concat(records);
                }
            }
            else // Processing of non-associated column match
            {
                switch (column.DataType)
                {
                    case Column.DataTypes.Number:
                        {
                            // convert the object to double and store it as number
                            double? number = null;
                            double value;
                            if (!string.IsNullOrWhiteSpace(matchValueText) && double.TryParse(matchValueText, out value))
                                number = value;

                            // lembda executed by the Where clause
                            var whereComparer = matchMode.GetWhereComparer(record => number);

                            // Prepare Iterator
                            var iterator = table.Query(start: startAt, count: (int)(endAt - startAt)).Iterator2(new[] { column });

                            var results = iterator.Where(record => whereComparer(record[column], record)).Select(record => record.Record);
                            rows = rows.Concat(results);


                            /*
                               // convert the object to double and store it as number
                             double real = Convert.ToDouble(matchValueText);
                             long number = BitConverter.DoubleToInt64Bits(real);
                              
                            var length = 0;
                            if (column.Size > 0)
                                length = column.Size;
                            else
                                length = GetNumberSize(number);
                            var matchBinary = BitConverter.GetBytes(number);
                            // Use native fast equity
                            if (matchMode == MatchMode.IsEqual)
                            {
                                var results = table.Core.Manager.MatchRows(matchBinary, null, length, nativeMatchMode, true, fieldSet, directMatch, startAt, endAt, matchInRecords, false, false);

                                rows = rows.Concat(results.Keys);
                                break;
                            }*/


                            break;
                        }
                    case Column.DataTypes.Long:
                        {
                            var number = Convert.ToInt64(matchValueText);

                            if ((column.SortIndex != null && !ignoreSortIndex && matchInRecords == null) || column.NativeSorted)
                            {
                                // Use the binary search with middle-point to find the value
                                IEnumerable<long> records = BinaryNumericMatch(table, column, fieldSet, startAt, endAt, number);
                                rows = rows.Concat(records);
                            }
                            else
                            {
#if NATIVE_MATCH
                                var length = 0;
                                if (column.Size > 0)
                                    length = column.Size;
                                else
                                    length = GetNumberSize(number);
                                var matchBinary = BitConverter.GetBytes(number);
                                var results = table.Core.Manager.MatchRows(matchBinary, null, length, nativeMatchMode, true, fieldSet, directMatch, startAt, endAt, matchInRecords, false, false);
                                rows = rows.Concat(results.Keys);
#else

                                // lembda executed by the Where clause
                                var whereComparer = matchMode.GetWhereComparer(record => number);

                                // Prepare Iterator
                                var iterator = table.Query(start: startAt, count: (int)(endAt - startAt)).Iterator2(new[] { column });

                                var results = iterator.Where(record => whereComparer(record[column], record)).Select(record => record.Record);
                                rows = rows.Concat(results);
#endif

#if SHOW_QUERY_INFO
                                if (System.Diagnostics.Debugger.IsAttached)
                                {
                                    string columnName = string.Format("{0}.{1}", table.Name, column.Name);
                                    if (column.Association != null)
                                        columnName = column.Association.ToString(); // string.Format("{0}.{1}", column.Association.ChildTable.Name, column.Association.ChildColumn.Name);

                                    System.Diagnostics.Debug.WriteLine("find mode: direct, column: {0}, value: {1}, found: {2}, range: {3} - {4} ({5}), iterations: {6}",
                                        columnName,
                                        number,
                                        0, //results.Keys.Count,
                                        startAt,
                                        endAt,
                                        endAt - startAt,
                                        matchInRecords != null ? matchInRecords.Count() : endAt - startAt);
                                }
#endif
                            }

                            break;
                        }
                    case Column.DataTypes.Text:
                        {
                            if (matchMode.IsComparableMode())
                            {
                                // lembda executed by the Where clause
                                var whereComparer = matchMode.GetWhereComparer(record => matchValueText);

                                // Prepare Iterator
                                var iterator = table.Query(start: startAt, count: (int)(endAt - startAt)).Iterator2(new[] { column });

                                var results = iterator.Where(record => whereComparer(record[column], record)).Select(record => record.Record);

                                rows = rows.Concat(results);
                            }
                            else
                            {
                                bool textIndexUsed = false;

                                // Process the search based on the index configuration

                                if ((column.SortIndex != null && !ignoreSortIndex && matchInRecords == null) || column.NativeSorted)
                                {
                                    // Limit the sorted data to range from the text range index

                                    if (column.TextIndex != null && (matchMode == MatchMode.DataStartsWithMatch || matchMode == MatchMode.IsEqual || matchMode == MatchMode.Approximates))
                                    {
                                        var range = column.TextIndex.GetRange((matchValueText as string)[0]);
                                        startAt = range.Item1;
                                        endAt = range.Item2;

                                        textIndexUsed = true;
                                    }

                                    // For equals, run the binary sorted search
                                    if (matchMode == MatchMode.DataStartsWithMatch || matchMode == MatchMode.IsEqual)
                                    {
                                        // Use the binary search with middle-point to find the value
                                        IEnumerable<long> records = BinaryTextMatch(table, column, fieldSet, startAt, endAt, matchValueText, matchMode);

                                        rows = rows.Concat(records);
                                        break;
                                    }

                                }
                                else if (column.TextIndex != null && (matchMode == MatchMode.DataStartsWithMatch || matchMode == MatchMode.IsEqual))
                                {
                                    var range = column.TextIndex.GetRange((matchValueText as string)[0]);
                                    startAt = range.Item1;
                                    endAt = range.Item2;

                                    textIndexUsed = true;
                                }

                                if (matchMode == MatchMode.Approximates || matchMode == MatchMode.DataContainsMatchApproximately || matchMode == MatchMode.MatchContainsDataApproximately)
                                {
                                    // Run approximate search
                                    var results = ApproximateTextMatch(table, column, fieldSet, directMatch, startAt, endAt, matchInRecords, matchMode, matchValue as string);

                                    var resultsQuery = results.OrderByDescending(result => result.Item2)
                                        .Take(5)
                                        .Select(result => result.Item1);

                                    System.Diagnostics.Debug.WriteLine("");
                                    foreach (var result in results.OrderByDescending(result => result.Item2).Take(5))
                                        System.Diagnostics.Debug.WriteLine("{0}, {1}, {2}", result.Item1, result.Item2, result.Item3);
                                    System.Diagnostics.Debug.WriteLine("");

                                    sortRecords = false;

                                    rows = rows.Concat(resultsQuery);
#if SHOW_QUERY_INFO
                                    if (System.Diagnostics.Debugger.IsAttached)
                                    {
                                        string columnName = string.Format("{0}.{1}", table.Name, column.Name);
                                        if (column.Association != null)
                                            columnName = column.Association.ToString(); // string.Format("{0}.{1}", column.Association.ChildTable.Name, column.Association.ChildColumn.Name);

                                        System.Diagnostics.Debug.WriteLine("find mode: {7}, column: {0}, value: {1}, found: {2}, range: {3} - {4} ({5}), iterations: {6}",
                                            columnName,
                                            matchValueText,
                                            resultsQuery.Count(),
                                            startAt,
                                            endAt,
                                            endAt - startAt,
                                            matchInRecords != null ? matchInRecords.Count() : endAt - startAt, textIndexUsed ? "approxi-Tindex-limited" : "approxi");
                                    }
#endif
                                }
                                else
                                {
                                    // Process the search normal

                                    var results = table.Core.Manager.MatchRows(matchValueText, false, nativeMatchMode, true, fieldSet, directMatch, startAt, endAt, matchInRecords, false, false);
                                    rows = rows.Concat(results.Keys);
#if SHOW_QUERY_INFO
                                    if (System.Diagnostics.Debugger.IsAttached)
                                    {
                                        string columnName = string.Format("{0}.{1}", table.Name, column.Name);
                                        if (column.Association != null)
                                            columnName = column.Association.ToString(); // string.Format("{0}.{1}", column.Association.ChildTable.Name, column.Association.ChildColumn.Name);

                                        System.Diagnostics.Debug.WriteLine("find mode: {7}, column: {0}, value: {1}, found: {2}, range: {3} - {4} ({5}), iterations: {6}",
                                            columnName,
                                            matchValueText,
                                            results.Keys.Count,
                                            startAt,
                                            endAt,
                                            endAt - startAt,
                                            matchInRecords != null ? matchInRecords.Count() : endAt - startAt, textIndexUsed ? "direct-Tindex-limited" : "direct");
                                    }
#endif
                                }
                            }

                            break;
                        }
                    case Column.DataTypes.Binary:
                        {
                            throw new NotImplementedException();
                        }
                }
            }

            if (sortRecords)
                return rows.Distinct().OrderBy(record => record);
            else
                return rows.Distinct();
        }

        private static IEnumerable<Tuple<long, double, string>> ApproximateTextMatch(MTable table, Column column, fieldset fieldSet, bool directMatch, long startAt, long endAt, IEnumerable<long> matchInRecords, MatchMode matchMode, string matchValue, double scoreThreshold = 0.5, bool ignoreCase = true)
        {
            if (matchInRecords != null)
            {
                foreach (var record in matchInRecords)
                {
                    var field = fieldSet.GetFields(record)[0];

                    // pre-process with skip logic
                    if (matchMode == MatchMode.Approximates && field.Size != matchValue.Length)
                        continue;
                    //else if (matchMode == MatchMode.DataContainsMatchApproximately && field.Size < matchValue.Length)
                    //    continue;
                    //else if (matchMode == MatchMode.MatchContainsDataApproximately && matchValue.Length < field.Size)
                    //    continue;

                    string dataValue = (string)column.GetValue(field);

                    double score = TextMatcher.Similarity(
                        directMatch ? (ignoreCase ? dataValue.ToLower() : dataValue) : (ignoreCase ? matchValue.ToLower() : matchValue),
                        directMatch ? (ignoreCase ? matchValue.ToLower() : matchValue) : (ignoreCase ? dataValue.ToLower() : dataValue));

                    if (score > scoreThreshold)
                        yield return Tuple.Create<long, double, string>(record, score, dataValue);
                }
            }
            else
            {
                for (long record = startAt; record < endAt; record++)
                {
                    var field = fieldSet.GetFields(record)[0];

                    // pre-process with skip logic
                    if (matchMode == MatchMode.Approximates && field.Size != matchValue.Length)
                        continue;
                    //else if (matchMode == MatchMode.DataContainsMatchApproximately && field.Size < matchValue.Length)
                    //    continue;
                    //else if (matchMode == MatchMode.MatchContainsDataApproximately && matchValue.Length < field.Size)
                    //    continue;

                    string dataValue = (string)column.GetValue(field);

                    double score = TextMatcher.Similarity(
                       directMatch ? (ignoreCase ? dataValue.ToLower() : dataValue) : (ignoreCase ? matchValue.ToLower() : matchValue),
                       directMatch ? (ignoreCase ? matchValue.ToLower() : matchValue) : (ignoreCase ? dataValue.ToLower() : dataValue));

                    if (score > scoreThreshold)
                        yield return Tuple.Create<long, double, string>(record, score, dataValue);
                }
            }

            yield break;
        }


        /// <summary>
        /// Columns sort index which defines the pre-sorted table columns in native order
        /// </summary>
        public sealed class NativeSortIndex : ColumnIndex, ISortIndex
        {
            internal NativeSortIndex(MTable table, string name, string columnNames)
                : base(table, name, columnNames)
            {
                this.Columns[0].NativeSorted = true;
            }

            public long GetSortedRecord(long unsortedRecord)
            {
                return unsortedRecord;
            }
        }

        /// <summary>
        /// Indexer based on hash values generated from column key values
        /// </summary>
        public unsafe sealed class HashIndex : ColumnIndex, IBuildableIndex, ISortIndex
        {
            private readonly MemoryMappedFile _indexMap;
            private long _startAt = 0;
            private int _records;
            private ushort _indexEntrySize = 4;
            private byte* _indexPointer;

            private Microsoft.Win32.SafeHandles.SafeMemoryMappedViewHandle _mappedView;
            private bool _isDirty = false;

            public bool RebuildNeeded
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            internal HashIndex(ITable table, string name, string columnNames)
                : base(table, name, columnNames)
            {
                _indexMap = MemoryMappedFile.CreateNew(null, 1024 * 1024 * 500);
                _indexPointer = (byte*)IntPtr.Zero;
            }

            /// <summary>
            /// Build the index for the table column data
            /// </summary>
            public void BuildIndex()
            {
                System.Diagnostics.Debug.WriteLine("Building Column Index: {0}", this);

                DeallocateMemoryMap();

                // Get the sorted records positions and write them into the index
                // Use the KeyIterator and sort the results by the key, returning the record number

                var sortedRecords = Table.Query().Iterator2(this.Columns).ToHashKeyIterator()
                    .AsParallel()
                    .WithDegreeOfParallelism(4)
                    .OrderBy(hashKey => hashKey.Key);

                _records = (int)Table.Count;

                using (var stream = new BinaryWriter(_indexMap.CreateViewStream(0, _records * _indexEntrySize)))
                {
                    foreach (var hashKey in sortedRecords)
                        stream.Write((uint)hashKey.Record);
                }

                _isDirty = false;
            }

            /// <summary>
            /// Ensure memory mapped view for the index data is released, if any is already allocated
            /// </summary>
            private void DeallocateMemoryMap()
            {
                // Dispose existing memory view handle
                if (_mappedView != null)
                {
                    _mappedView.ReleasePointer();
                    _mappedView.Dispose();
                    _mappedView = null;
                    _indexPointer = (byte*)IntPtr.Zero;
                }
            }

            /// <summary>
            /// Allocates memory mapped view for the index data, if not already allocated
            /// </summary>
            private void AllocateMemoryMap()
            {
                if (_indexPointer != (byte*)IntPtr.Zero)
                    return;

                _mappedView = _indexMap.CreateViewStream(0, _records * _indexEntrySize).SafeMemoryMappedViewHandle;
                _mappedView.AcquirePointer(ref _indexPointer);
                _startAt = Table.StartAt;
            }

            /// <summary>
            /// Marks this index as dirty which will cause it to be rebuilt on the next request
            /// </summary>
            public void SetDirty()
            {
                _isDirty = true;
            }

            public void Load(string indexFilename)
            {
                using (var stream = File.Open(indexFilename, FileMode.Open))
                    this.Load(stream);
            }

            public void Store(string indexFilename)
            {
                using (var stream = File.Open(indexFilename, FileMode.Create))
                    this.Store(stream);
            }

            public long GetSortedRecord(long unsortedRecord)
            {
                if (_isDirty)
                    this.BuildIndex();  //  build index on demand
                else if (_records == 0)
                    if (RebuildNeeded)     // for performance, just check the records count before checking the NeedsBuilt flag
                        throw new InvalidOperationException($"Hash index \"{this.Name}\" needs to be built");

                this.AllocateMemoryMap();

                var recordIndex = (unsortedRecord - _startAt);

                if (recordIndex < _records)
                {
                    var number = Native.Utils.BytesToNumberOpt(_indexPointer + (recordIndex * _indexEntrySize), _indexEntrySize);

                    return number;
                }

                return unsortedRecord;
            }


            public void Load(Stream stream)
            {
                // The index header, containing the total number of rows and the index size

                using (var reader = new BinaryReader(stream, Encoding.Default, true))
                {
                    _indexEntrySize = reader.ReadByte();    // 1-byte
                    _records = reader.ReadInt32();          // 4-bytes

                    // Read the content of the index
                    byte[] buffer = new byte[1024 * 8];
                    using (var view = _indexMap.CreateViewStream(0, _records * _indexEntrySize))
                        while (view.Position < view.Length)
                        {
                            var readen = reader.Read(buffer, 0, buffer.Length);
                            view.Write(buffer, 0, readen);
                        }

                    _isDirty = false;
                }
            }

            public void Store(Stream stream)
            {
                using (var writer = new BinaryWriter(stream, Encoding.Default, true))
                {
                    // Index header, containng the number of entries and the index entry size

                    writer.Write((byte)_indexEntrySize);    // 1-byte
                    writer.Write(_records);                 // 4-bytes

                    // Write the content of the index
                    byte[] buffer = new byte[1024 * 8];
                    using (var view = _indexMap.CreateViewStream(0, _records * _indexEntrySize))
                        while (view.Position < view.Length)
                        {
                            var readen = view.Read(buffer, 0, buffer.Length);
                            writer.Write(buffer, 0, readen);
                        }
                }
            }
        }


        public unsafe sealed class SortIndex : ColumnIndex, IBuildableIndex, ISortIndex
        {
            private readonly MemoryMappedFile _indexMap;
            private long _startAt = 0;
            private int _records;
            private ushort _indexEntrySize = 4;
            private byte* _indexPointer;
            private readonly List<Column> _descendingSortedColumns = new List<Column>();
            private Microsoft.Win32.SafeHandles.SafeMemoryMappedViewHandle _mappedView;
            private bool _isDirty = false;

            internal SortIndex(ITable table, string name, string columnNames)
                : base(table, name, StripColumnNames(columnNames))
            {
                _indexMap = MemoryMappedFile.CreateNew(null, 1024 * 1024 * 500);
                _indexPointer = (byte*)IntPtr.Zero;

                PopulateDescendingSortedColumns(columnNames);

                _isDirty = true;  // Set as dirty, to automaticly built index on demand, even without calling Built
            }

            private static string StripColumnNames(string columnNames)
            {
                var cleanColumnNames = new List<string>();

                foreach (var columnEntry in columnNames.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                {
                    int dirterm;
                    if ((dirterm = columnEntry.Trim().ToUpper().LastIndexOf(" DESC")) > -1 || (dirterm = columnEntry.Trim().ToUpper().LastIndexOf(" ASC")) > -1)
                        cleanColumnNames.Add(columnEntry.Trim().Substring(0, dirterm).Trim());
                    else
                        cleanColumnNames.Add(columnEntry.Trim());
                }

                return string.Join(",", cleanColumnNames);
            }

            private void PopulateDescendingSortedColumns(string columnNames)
            {
                // Get the sort directions from the column names

                foreach (var columnEntry in columnNames.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                    if (columnEntry.ToUpper().EndsWith(" DESC"))
                    {
                        var columnName = columnEntry.Substring(0, columnEntry.ToUpperInvariant().LastIndexOf(" DESC")).Trim();
                        this._descendingSortedColumns.AddRange(Table.Columns.GetColumns(columnName));
                    }
            }

            private class SortEntry
            {
                public readonly long SortedRowNumber;
                public readonly object[] CellValues;

                public SortEntry(long sortedRowNumber, object[] cellValues)
                {
                    this.SortedRowNumber = sortedRowNumber;

                    this.CellValues = new object[cellValues.Length];
                    Array.Copy(cellValues, CellValues, cellValues.Length);
                }
            }

            /// <summary>
            /// Indicates that the index needs to be built or rebuilt
            /// </summary>
            public bool RebuildNeeded
            {
                get
                {
                    // if the records of the table and the index records match, or the index is not dirty
                    if (_isDirty || Table.Count != _records)
                        return true;
                    return false;
                }
            }

            /// <summary>
            /// Build the index for the table column data
            /// </summary>
            public void BuildIndex()
            {
                System.Diagnostics.Debug.WriteLine("Building Column Index: {0}", this);

                DeallocateMemoryMap();


                //  NOTE:
                //      Normally the most performant routine works when there are columns in the sorting which are not calculated.
                //      However if there are calculated columns, we have to perform the more heavier sorting routine which reads values right from the view iterator.
                bool containsCalulations = Columns.Count(column => column.ExpressionFunc != null) > 0;

                // Get the sorted records positions and write them into the index

                // ### SORTING ROUTINE ###
                if (!containsCalulations)
                {
                    // prepare the ordinals involved in the sorting
                    int[] ordinals = Columns.Select(column => column.Ordinal).ToArray();
                    int keycount = ordinals.Length;

                    var valuesComparer = Comparer<object[]>.Create((cells1, cells2) =>
                    {
                        object value1 = cells1[0]; // cells1[ordinals[0]];
                        object value2 = cells2[0]; // cells2[ordinals[0]];

                        // Determine comparer type. Use string comparer if any of the values are strings
                        int difference;
                        if (value1 is string || value2 is string)
                            difference = StringComparer.Ordinal.Compare(value1, value2);
                        else
                            difference = Comparer.Default.Compare(value1, value2);

                        return difference;
                    });


                    // ################### Prepare Comparable ############
                    var descendingIndexes = new bool[keycount];
                    var singleDescending = false;
                    {
                        if (keycount > 1)
                            for (int idx = 0; idx < keycount; idx++)
                            {
                                if (_descendingSortedColumns.Contains(this.Columns[idx]))
                                    descendingIndexes[idx] = true;
                                else
                                    descendingIndexes[idx] = false;
                            }
                        else
                            singleDescending = _descendingSortedColumns.Contains(this.Columns[0]);
                    }

                    /// List of comparers associated with each column in the key list.
                    /// Create the comparer based on the type of the column
                    Comparer<fieldset.Field>[] ordinalComparers = new Comparer<fieldset.Field>[keycount];
                    for (var idx = 0; idx < keycount; idx++)
                        ordinalComparers[idx] =

                            (Columns[idx].DataType == Column.DataTypes.Number
                            || Columns[idx].DataType == Column.DataTypes.Long
                            || Columns[idx].DataType == Column.DataTypes.Date)

                            ? Comparer<fieldset.Field>.Create((first, second) =>
                                {
                                    // Numeric comparer

                                    long n1, n2;

                                    first.TryGetNumber(out n1);
                                    second.TryGetNumber(out n2);

                                    var diff = (n1 - n2);

                                    if (diff < 0)
                                        return -1;
                                    else if (diff > 0)
                                        return 1;
                                    else
                                        return 0;

                                    // return (int)(n1 - n2);
                                    //long numbe2 = 0;
                                    //if (second.Size > 0)
                                    //    numbe2 = Native.Utils.BytesToNumberOpt(second.Pointer, second.Size);

                                    //return first.CompareTo(numbe2);
                                })
                            : Comparer<fieldset.Field>.Create((first, second) =>
                                {
                                    return first.CompareTo(second);  // Default memory comparer
                                });

                    // Comparer used with array of cells values objects
                    Comparer<object[]> cellsComparer = Comparer<object[]>.Create((cells1, cells2) =>
                    {
                        // Compare the values here
                        // Determine comparer type. Use string comparer if any of the values are strings

                        // Optimization: Skip the iterator logic if there is only one sort column
                        if (keycount > 1)
                        {
                            for (int idx = 0; idx < keycount; idx++)
                            {
                                object value1 = cells1[idx];
                                object value2 = cells2[idx];

                                // Determine comparer type. Use string comparer if any of the values are strings
                                int difference;
                                if (value1 is string || value2 is string)
                                    difference = StringComparer.Ordinal.Compare(value1, value2);
                                else
                                    difference = Comparer.Default.Compare(value1, value2);

                                if (difference != 0)
                                    return descendingIndexes[idx] ? -difference : difference;
                            }
                            return 0;
                        }
                        else
                        {
                            object value1 = cells1[0];
                            object value2 = cells2[0];

                            int difference;
                            if (value1 is string || value2 is string)
                                difference = StringComparer.Ordinal.Compare(value1, value2);
                            else
                                difference = Comparer.Default.Compare(value1, value2);

                            if (difference != 0)
                                return singleDescending ? -difference : difference;
                            return 0;
                        }

                    });

                    // ## Split the input to parallel partitions
                    int perallelDegree = 1; // Environment.ProcessorCount;
                    var rangeSize = (int)Table.Count / perallelDegree;
                    var rangeRemainder = (int)Table.Count % perallelDegree;
                    var unsortedEntries = ParallelEnumerable.Range(0, perallelDegree)
                        .WithDegreeOfParallelism(perallelDegree)
                        // allocate iterator for range of records
                        .Select(rangeIndex => Table.Query(start: rangeSize * rangeIndex, count: (rangeIndex == perallelDegree ? rangeSize + rangeRemainder : rangeSize)).Iterator2(Columns, cloneCurrent: false))

                        // extract the sort column values into sort entry
                        .SelectMany(it =>
                        {
                            return it.ToRecordsIterator().Select(record =>
                            {

                                var fields = it.fieldset.GetFields(record);

                                fieldset.Field[] cells = new fieldset.Field[keycount];
                                for (var idx = 0; idx < keycount; idx++)
                                    cells[idx] = fields[idx];

                                return new { record = record, cells = cells };


                                /*
                                object[] cells;

                                it.fieldset.GetFields(record); //.GetView.GetView(record);

                                // Read the values to compare here.
                                // Optmiization: check if single or multiple keys are involved
                                if (keycount > 0)
                                {
                                   // value = column.GetValue(field);

                                    cells = new object[keycount];
                                    for (int idx = 0; idx < keycount; idx++)
                                    {
                                        cells[idx] = Columns[idx].GetValue(it.fieldset[ordinals[idx]], false);

                                        //cells[idx] = it.fieldset[ordinals[idx]].GetText();
                                    }
                                }
                                else
                                {
                                    // only one key column
                                    cells = new object[1];

                                    cells[0] = Columns[0].GetValue(it.fieldset[ordinals[0]], false);

                                    //cells[0] = it.fieldset[ordinals[0]].GetText();
                                }

                                return new { record = record, cells };
                                */


                            });
                        });

                    // Contains in-memory unsorted sort-entries
                    var sortables = unsortedEntries.ToArray();



                    // ################  Run sorting algorithm here (LINQ or Array.Sort)

                    var sortedEntries = sortables
                      .AsParallel()
                        //  .OrderBy(a => a.cells, cellsComparer);


                        .OrderBy(a => a.cells, Comparer<fieldset.Field[]>.Create((cells1, cells2) =>
                         {
                             // Compare the values here
                             // Determine comparer type. Use string comparer if any of the values are strings

                             // Optimization: Skip the iterator logic if there is only one sort column
                             if (keycount > 1)
                             {
                                 for (int idx = 0; idx < keycount; idx++)
                                 {
                                     int difference = ordinalComparers[idx].Compare(cells1[idx], cells2[idx]);

                                     //fieldset.Field first = cells1[idx];
                                     //fieldset.Field second = cells2[idx];

                                     //int difference = first.CompareTo(second);

                                     if (difference != 0)
                                         return descendingIndexes[idx] ? -difference : difference;
                                 }

                                 return 0;
                             }
                             else
                             {
                                 int difference = ordinalComparers[0].Compare(cells1[0], cells2[0]);

                                 //fieldset.Field first = cells1[0];
                                 //fieldset.Field second = cells2[0];

                                 //int difference = first.CompareTo(second);

                                 if (difference != 0)
                                     return singleDescending ? -difference : difference;

                                 return 0;
                             }

                         }));


                    /*
                    Array.Sort(sortables, (entry1, entry2) =>
                    {
                        // Compare the values here
                        // Determine comparer type. Use string comparer if any of the values are strings
                        int difference;
                        if (entry1.value is string || entry2.value is string)
                            difference = StringComparer.Ordinal.Compare(entry1.value, entry2.value);
                        else
                            difference = Comparer.Default.Compare(entry1.value, entry2.value);

                        return difference;
                    });
                    */

                    _records = (int)Table.Count;

                    using (var stream = new BinaryWriter(_indexMap.CreateViewStream(0, _records * _indexEntrySize)))
                    {
                        foreach (var entry in sortedEntries)
                            stream.Write((uint)entry.record);
                    }
                }
                else

                    // Use the iterator based routine
                    BuildIndexFromIterator();

                _isDirty = false;
            }


            /// <summary>
            /// Builds index using table query records iterator
            /// </summary>
            private void BuildIndexFromIterator()
            {
                // Get the sorted records positions and write them into the index

                // ### SORTING ROUTINE ###
                
                    // prepare the ordinals involved in the sorting
                    int[] ordinals = Columns.Select(column => column.Ordinal).ToArray();
                    int keycount = ordinals.Length;

                    var valuesComparer = Comparer<object[]>.Create((cells1, cells2) =>
                    {
                        object value1 = cells1[0]; // cells1[ordinals[0]];
                        object value2 = cells2[0]; // cells2[ordinals[0]];

                        // Determine comparer type. Use string comparer if any of the values are strings
                        int difference;
                        if (value1 is string || value2 is string)
                            difference = StringComparer.Ordinal.Compare(value1, value2);
                        else
                            difference = Comparer.Default.Compare(value1, value2);

                        return difference;
                    });


                    // ################### Prepare Comparable ############
                    var descendingIndexes = new bool[keycount];
                    var singleDescending = false;
                    {
                        if (keycount > 1)
                            for (int idx = 0; idx < keycount; idx++)
                            {
                                if (_descendingSortedColumns.Contains(this.Columns[idx]))
                                    descendingIndexes[idx] = true;
                                else
                                    descendingIndexes[idx] = false;
                            }
                        else
                            singleDescending = _descendingSortedColumns.Contains(this.Columns[0]);
                    }

                    /// List of comparers associated with each column in the key list.
                    /// Create the comparer based on the type of the column
                    Comparer<fieldset.Field>[] ordinalComparers = new Comparer<fieldset.Field>[keycount];
                    for (var idx = 0; idx < keycount; idx++)
                        ordinalComparers[idx] =

                            (Columns[idx].DataType == Column.DataTypes.Number
                            || Columns[idx].DataType == Column.DataTypes.Long
                            || Columns[idx].DataType == Column.DataTypes.Date)

                            ? Comparer<fieldset.Field>.Create((first, second) =>
                            {
                                // Numeric comparer

                                long n1, n2;

                                first.TryGetNumber(out n1);
                                second.TryGetNumber(out n2);

                                var diff = (n1 - n2);

                                if (diff < 0)
                                    return -1;
                                else if (diff > 0)
                                    return 1;
                                else
                                    return 0;

                                // return (int)(n1 - n2);
                                //long numbe2 = 0;
                                //if (second.Size > 0)
                                //    numbe2 = Native.Utils.BytesToNumberOpt(second.Pointer, second.Size);

                                //return first.CompareTo(numbe2);
                            })
                            : Comparer<fieldset.Field>.Create((first, second) =>
                            {
                                return first.CompareTo(second);  // Default memory comparer
                            });

                    // Comparer used with array of cells values objects
                    Comparer<object[]> cellsComparer = Comparer<object[]>.Create((cells1, cells2) =>
                    {
                        // Compare the values here
                        // Determine comparer type. Use string comparer if any of the values are strings

                        // Optimization: Skip the iterator logic if there is only one sort column
                        if (keycount > 1)
                        {
                            for (int idx = 0; idx < keycount; idx++)
                            {
                                object value1 = cells1[idx];
                                object value2 = cells2[idx];

                                // Determine comparer type. Use string comparer if any of the values are strings
                                int difference;
                                if (value1 is string || value2 is string)
                                    difference = StringComparer.Ordinal.Compare(value1, value2);
                                else
                                    difference = Comparer.Default.Compare(value1, value2);

                                if (difference != 0)
                                    return descendingIndexes[idx] ? -difference : difference;
                            }
                            return 0;
                        }
                        else
                        {
                            object value1 = cells1[0];
                            object value2 = cells2[0];

                            int difference;
                            if (value1 is string || value2 is string)
                                difference = StringComparer.Ordinal.Compare(value1, value2);
                            else
                                difference = Comparer.Default.Compare(value1, value2);

                            if (difference != 0)
                                return singleDescending ? -difference : difference;
                            return 0;
                        }

                    });

                    // ## Split the input to parallel partitions
                    int perallelDegree = 1; // Environment.ProcessorCount;
                    var rangeSize = (int)Table.Count / perallelDegree;
                    var rangeRemainder = (int)Table.Count % perallelDegree;
                    var unsortedEntries = ParallelEnumerable.Range(0, perallelDegree)
                        .WithDegreeOfParallelism(perallelDegree)
                        // allocate iterator for range of records
                        .Select(rangeIndex => Table.Query(start: rangeSize * rangeIndex, count: (rangeIndex == perallelDegree ? rangeSize + rangeRemainder : rangeSize)).Iterator2(Columns, cloneCurrent: true))

                        // extract the sort column values into sort entry
                        .SelectMany(it =>
                        {
                            return it.Select(record => record);
                        });

                    // Contains in-memory unsorted sort-entries
                    var sortables = unsortedEntries.ToArray();



                // ################  Run sorting algorithm here (LINQ or Array.Sort)

                var sortedEntries = sortables
                    .AsParallel()
                    .OrderBy(a =>
                    {
                        var cells =  a.Values.Cast<object>().ToArray();
                        return cells;

                    }, cellsComparer);


                    /*
                    Array.Sort(sortables, (entry1, entry2) =>
                    {
                        // Compare the values here
                        // Determine comparer type. Use string comparer if any of the values are strings
                        int difference;
                        if (entry1.value is string || entry2.value is string)
                            difference = StringComparer.Ordinal.Compare(entry1.value, entry2.value);
                        else
                            difference = Comparer.Default.Compare(entry1.value, entry2.value);

                        return difference;
                    });
                    */

                    _records = (int)Table.Count;

                    using (var stream = new BinaryWriter(_indexMap.CreateViewStream(0, _records * _indexEntrySize)))
                    {
                        foreach (var entry in sortedEntries)
                            stream.Write((uint)entry.record);
                    }
                }

            /// <summary>
            /// Object comparer class: https://www.informit.com/guides/content.aspx?g=dotnet&seqNum=782
            /// </summary>
            private class ObjectComparer : IComparer
            {
                public int Compare(object x, object y)
                {
                    // Do the right thing for null objects
                    if (x == null)
                    {
                        if (y == null)
                            return 0;
                        else
                            return -1;
                    }
                    else if (y == null)
                    {
                        return 1;
                    }
                    Type xType = x.GetType();
                    Type yType = y.GetType();
                    // If they're both the same type and implement IComparable,
                    // then call the IComparable.CompareTo method.
                    if (xType == yType)
                    {
                        if (x is IComparable)
                        {
                            return (x as IComparable).CompareTo(y);
                        }
                    }
                    else if (xType == typeof(String) && yType == typeof(Hashtable))
                    {
                        return (x as String).CompareTo((y as Hashtable));
                    }
                    else if (xType == typeof(Hashtable) && yType == typeof(String))
                    {
                        return (x as Hashtable).ToString().CompareTo((y as String));
                    }
                    // If we get here, then there is no known way to compare x and y
                    throw new ArgumentException(string.Format("Unable to compare objects of type {0} and {1}", xType.ToString(), yType.ToString()));
                }
            }

            /// <summary>
            /// Build the index for the table column data
            /// </summary>
            [Obsolete]
            private void BuildIndex1()
            {
                System.Diagnostics.Debug.WriteLine("Building Column Index: {0}", this);

                DeallocateMemoryMap();

                // Get the sorted records positions and write them into the index

                // ### SORTING ROUTINE ###
                {
                    var fieldSet = (Table is MView ? ((MView)Table) : Table).CreateFieldSet(Columns);

                    object[] values = new object[fieldSet.Count];

                    var queryRecords = Enumerable.Range((int)Table.StartAt, (int)Table.Count)
                        .Select(record =>
                        {
                            var fields = fieldSet.GetFields(record);

                            // populate the temporary values array
                            for (int i = 0; i < fieldSet.Count; i++)
                                values[i] = Columns[i].GetValue(fields[i], false);

                            return new SortEntry
                            (
                                 record,
                                 values
                            );
                        })
                        .AsParallel()
                        .WithDegreeOfParallelism(4)
                        .OrderBy(entry => entry, new SortEntryComparer(this));

                    _records = (int)Table.Count;

                    using (var stream = new BinaryWriter(_indexMap.CreateViewStream(0, _records * _indexEntrySize)))
                    {
                        foreach (var record in queryRecords)
                            stream.Write((uint)record.SortedRowNumber);
                    }
                }

                _isDirty = false;
            }

            /// <summary>
            /// Ensure memory mapped view for the index data is released, if any is already allocated
            /// </summary>
            private void DeallocateMemoryMap()
            {
                // Dispose existing memory view handle
                if (_mappedView != null)
                {
                    _mappedView.ReleasePointer();
                    _mappedView.Dispose();
                    _mappedView = null;
                    _indexPointer = (byte*)IntPtr.Zero;
                }
            }

            /// <summary>
            /// Allocates memory mapped view for the index data, if not already allocated
            /// </summary>
            private void AllocateMemoryMap()
            {
                if (_indexPointer != (byte*)IntPtr.Zero)
                    return;

                _mappedView = _indexMap.CreateViewStream(0, _records * _indexEntrySize).SafeMemoryMappedViewHandle;
                _mappedView.AcquirePointer(ref _indexPointer);
                _startAt = Table.StartAt;
            }

            /// <summary>
            /// Marks this index as dirty which will cause it to be rebuilt on the next request
            /// </summary>
            public void SetDirty()
            {
                _isDirty = true;
            }

            private class SortEntryComparer : IComparer<SortEntry>
            {
                private readonly SortIndex _index;
                private readonly bool[] _descendingIndexes;
                private readonly bool _singleDescending;
                private readonly int _columnCount;
                public SortEntryComparer(SortIndex index)
                {
                    _index = index;

                    _columnCount = index.Columns.Length;
                    _descendingIndexes = new bool[_columnCount];

                    if (_columnCount > 1)
                        for (int idx = 0; idx < _columnCount; idx++)
                        {
                            if (index._descendingSortedColumns.Contains(index.Columns[idx]))
                                _descendingIndexes[idx] = true;
                            else
                                _descendingIndexes[idx] = false;
                        }
                    else
                        _singleDescending = index._descendingSortedColumns.Contains(index.Columns[0]);
                }

                int IComparer<SortEntry>.Compare(SortEntry entry1, SortEntry entry2)
                {
                    // Optimization: Skip the iterator logic if there is only one sort column
                    if (_columnCount > 1)
                    {
                        for (int idx = 0; idx < _columnCount; idx++)
                        {
                            object value1 = entry1.CellValues[idx];
                            object value2 = entry2.CellValues[idx];

                            // Determine comparer type. Use string comparer if any of the values are strings
                            int sign;
                            if (value1 is string || value2 is string)
                                sign = StringComparer.Ordinal.Compare(value1, value2);
                            else
                                sign = Comparer.Default.Compare(value1, value2);

                            if (sign != 0)
                                return _descendingIndexes[idx] ? -sign : sign;
                        }
                        return 0;
                    }
                    else
                    {
                        object value1 = entry1.CellValues[0];
                        object value2 = entry2.CellValues[0];

                        int sign;
                        if (value1 is string || value2 is string)
                            sign = StringComparer.Ordinal.Compare(value1, value2);
                        else
                            sign = Comparer.Default.Compare(value1, value2);

                        if (sign != 0)
                            return _singleDescending ? -sign : sign;
                        return 0;
                    }
                }
            }

            public void Load(string indexFilename)
            {
                using (var stream = File.Open(indexFilename, FileMode.Open))
                    this.Load(stream);
            }

            public void Store(string indexFilename)
            {
                using (var stream = File.Open(indexFilename, FileMode.Create))
                    this.Store(stream);
            }

            public long GetSortedRecord(long unsortedRecord)
            {
                if (_isDirty)
                    this.BuildIndex();  //  build index on demand
                else if (_records == 0)
                    if (RebuildNeeded)     // for performance, just check the records count before checking the NeedsBuilt flag
                        throw new InvalidOperationException($"Sort index \"{this.Name}\" needs to be built");

                this.AllocateMemoryMap();

                var recordIndex = (unsortedRecord - _startAt);

                if (recordIndex < _records)
                {
                    var number = Native.Utils.BytesToNumberOpt(_indexPointer + (recordIndex * _indexEntrySize), _indexEntrySize);

                    return number;
                }

                return unsortedRecord;
            }

            public void Load(Stream stream)
            {
                // The index header, containing the total number of rows and the index size

                using (var reader = new BinaryReader(stream, Encoding.Default, true))
                {
                    _indexEntrySize = reader.ReadByte();    // 1-byte
                    _records = reader.ReadInt32();          // 4-bytes

                    // Read the content of the index
                    byte[] buffer = new byte[1024 * 8];
                    using (var view = _indexMap.CreateViewStream(0, _records * _indexEntrySize))
                        while (view.Position < view.Length)
                        {
                            var readen = reader.Read(buffer, 0, buffer.Length);
                            view.Write(buffer, 0, readen);
                        }

                    _isDirty = false;
                }
            }

            public void Store(Stream stream)
            {
                using (var writer = new BinaryWriter(stream, Encoding.Default, true))
                {
                    // Index header, containng the number of entries and the index entry size

                    writer.Write((byte)_indexEntrySize);    // 1-byte
                    writer.Write(_records);                 // 4-bytes

                    // Write the content of the index
                    byte[] buffer = new byte[1024 * 8];
                    using (var view = _indexMap.CreateViewStream(0, _records * _indexEntrySize))
                        while (view.Position < view.Length)
                        {
                            var readen = view.Read(buffer, 0, buffer.Length);
                            writer.Write(buffer, 0, readen);
                        }
                }
            }
        }

        public abstract class ColumnIndex
        {
            private readonly Column[] _columns;
            private readonly ITable _table;
            private readonly string _name;

            internal ColumnIndex(ITable table, string name, string columnNames)
            {
                _table = table;
                _columns = table.Columns[columnNames];
                _name = string.IsNullOrWhiteSpace(name) ? string.Format("IDX::{0}", String.Join(".", this.Columns.Select(column => column.Name))) : name;
                if (_columns.Length == 0)
                    throw new InvalidOperationException("No columns were found in the table " + table.Name + " upon creating index [" + name + "]");
            }

            public Column[] Columns { get { return _columns; } }

            public string Name { get { return _name; } }
            internal ITable Table { get { return _table; } }

            public override string ToString()
            {
                return Name;
            }
        }

        public interface IBuildableIndex
        {
            void BuildIndex();
            bool RebuildNeeded { get; }
            void Load(string indexFilename);
            void Store(string indexFilename);
            void Load(Stream stream);
            void Store(Stream stream);
            void SetDirty();
        }

        public interface ISortIndex
        {
            /// <summary>
            /// Get the sorted record number from unsorted record
            /// </summary>
            /// <param name="unsortedRecord"></param>
            /// <returns></returns>
            long GetSortedRecord(long unsortedRecord);
        }

        public sealed class TextIndex : ColumnIndex, IBuildableIndex
        {
            private SortedDictionary<char, long> _indexData;
            private char[] _indexKeys;
            private bool _isDirty;

            public bool RebuildNeeded
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            internal TextIndex(MTable table, Column column, string name)
                : base(table, name, column.Name)
            {
                _indexData = new SortedDictionary<char, long>();
            }

            /// <summary>
            /// Build the index for the table column data
            /// </summary>
            public void BuildIndex()
            {
                var column = Columns.Single();

                if (!column.NativeSorted && column.SortIndex == null)
                    throw new InvalidOperationException("Inable to build text index on column [" + column.Name + "]. It does not have native index, nor sort index.");

                if (column.DataType != Column.DataTypes.Text)
                    throw new InvalidOperationException("Inable to build text index on column [" + column.Name + "]. It is not of Text type");


                var fieldSet = Table.CreateFieldSet(Table.Columns[column.Name]);
                for (long record = Table.StartAt; record < Table.StartAt + Table.Count; record++)
                {
                    long sortedRecord = record;
                    if (column.SortIndex != null)
                        sortedRecord = column.SortIndex.GetSortedRecord(record);

                    string value = (string)column.GetValue(fieldSet.GetFields(sortedRecord)[0], true);

                    if (!string.IsNullOrEmpty(value))
                    {
                        var key = value[0];
                        if (!_indexData.ContainsKey(key))
                            _indexData.Add(char.ToUpper(key), record);
                    }
                }

                //foreach (var row in ((MTable)Table).GetRows(column))
                //{

                //    string value = (string)column.GetValue(row);
                //    if (!string.IsNullOrEmpty(value))
                //    {
                //        var key = value[0];
                //        if (!_indexData.ContainsKey(key))
                //            _indexData.Add(key, row.Number);
                //    }
                //}

                _indexKeys = new char[_indexData.Count];
                _indexData.Keys.CopyTo(_indexKeys, 0);

                _isDirty = false;
            }

            /// <summary>
            /// Returns the range of data rows where the particular index key belongs. If the key was not found, null is returned
            /// </summary>
            /// <param name="indexKey">Index key to get the range for</param>
            /// <returns>A range Tupple containing the start and end row positions</returns>
            public Tuple<long, long> GetRange(char indexKey)
            {
                indexKey = char.ToUpper(indexKey);

                long rangeStart = 0;
                long rangeEnd = 0;

                if (_indexData.TryGetValue(indexKey, out rangeStart))
                {
                    var position = Array.IndexOf(_indexKeys, indexKey);
                    if (position < _indexKeys.Length - 1)
                        rangeEnd = _indexData[_indexKeys[position + 1]];
                    else
                        rangeEnd = Table.Count;
                }
                else
                    return null;

                return Tuple.Create<long, long>(rangeStart, rangeEnd);
            }

            /// <summary>
            /// Returns the range of data rows where the particular index key belongs. If the key was not found, null is returned
            /// </summary>
            /// <param name="indexKey"></param>
            /// <returns></returns>
            public Tuple<long, long> this[char indexKey]
            {
                get
                {
                    return GetRange(indexKey);
                }
            }

            public void Load(string indexFilename)
            {
                using (var stream = File.Open(indexFilename, FileMode.Open))
                    this.Load(stream);
            }

            public void Store(string indexFilename)
            {
                using (var stream = File.Open(indexFilename, FileMode.Create))
                    this.Store(stream);
            }

            public void Load(Stream stream)
            {
                var formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                _indexData = (SortedDictionary<char, long>)formatter.Deserialize(stream);
                _indexKeys = (char[])formatter.Deserialize(stream);
                _isDirty = false;
            }

            public void Store(Stream stream)
            {
                var formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, _indexData);
                formatter.Serialize(stream, _indexKeys);
            }


            public void SetDirty()
            {
                _isDirty = true;
            }
        }

        [Serializable]
        public abstract partial class TableAction : GuiLabs.Undo.AbstractAction, ISerializable
        {
            private readonly MTable _table;

            /// <summary>
            /// Called durring deserialization by the formatter
            /// </summary>
            /// <param name="info"></param>
            /// <param name="context"></param>
            protected TableAction(SerializationInfo info, StreamingContext context)
                : base(info, context)
            {
                // Get the table object: 
                // 1) From storage streaming contenxt, by name
                // 2) From table reference directly from streaming context 

                if (context.Context is MTable)
                    _table = (MTable)context.Context;
                else if (context.Context is MStorage)
                    _table = ((MStorage)context.Context).Tables.Single(table => table.Name == info.GetString("TableName"));
                else
                    throw new InvalidDataException("Failed to deserialize TableAction. No appropriate streaming context object was found");
            }

            internal TableAction(MTable table)
            {
                _table = table;
            }

            public MTable Table { get { return _table; } }

            /// <summary>
            /// Action for removing table
            /// </summary>
            [Serializable]
            public class Remove : TableAction
            {
                private readonly string _tableName;

                /// <summary>
                /// Called durring deserialization by the formatter
                /// </summary>
                /// <param name="info"></param>
                /// <param name="context"></param>
                private Remove(SerializationInfo info, StreamingContext context) : base(info, context)
                {
                    // Deserialize 
                    _tableName = info.GetString("TableNameRemoved");
                }

                internal Remove(MTable table)
                    : base(table)
                {
                    _tableName = table.Name;
                }

                protected override void ExecuteCore()
                {
                    Table.Storage.RemoveTableInternal(Table, completely: false);
                }

                protected override void UnExecuteCore()
                {
                    Table.storage.AddTable(Table);
                }

                public override string ToString()
                {
                    return string.Format("Table \"{0}\": Removed", _tableName);
                }


                public override void GetObjectData(SerializationInfo info, StreamingContext context)
                {
                    base.GetObjectData(info, context);

                    // Serialize the Table Name (and possible the UID)
                    info.AddValue("TableNameRemoved", _tableName);
                }
            }

            /// <summary>
            /// Action for creating table
            /// </summary>
            [Serializable]
            public class Create : TableAction
            {
                private readonly string _tableName;

                private Create(SerializationInfo info, StreamingContext context) : base(info, context)
                {
                    _tableName = info.GetString("TableNameCreated");
                }

                internal Create(MTable table)
                    : base(table)
                {
                    _tableName = table.Name;
                }

                protected override void ExecuteCore()
                {
                    Table.Storage.AddTable(Table);
                }

                protected override void UnExecuteCore()
                {
                    Table.storage.RemoveTableInternal(Table, completely: false);
                }

                public override string ToString()
                {
                    return string.Format("Table \"{0}\": Created", _tableName);
                }

                public override void GetObjectData(SerializationInfo info, StreamingContext context)
                {
                    base.GetObjectData(info, context);

                    // Serialize the Table Name (and possible the UID)
                    info.AddValue("TableNameCreated", _tableName);
                }
            }

            /// <summary>
            /// Action for removing column
            /// </summary>
            [Serializable]
            public class RemoveColumn : TableAction
            {
                private readonly Column _column;
                private readonly string _columnName;

                private RemoveColumn(SerializationInfo info, StreamingContext context) : base(info, context)
                {
                    // Extract the refernce column from the table.
                    // The column is located in the removed list. 
                    // Use the orginal to get the reference

                    _columnName = info.GetString("ColumnName");
                    int ordinal = info.GetInt32("ColumnOrdinal");
                    _column = Table.Columns.GetAllColumns().Single(entry => entry.Key.Ordinal == ordinal).Key;
                }

                internal RemoveColumn(MTable table, Column column)
                    : base(table)
                {
                    _column = column;
                    _columnName = column.Name;
                }

                protected override void ExecuteCore()
                {
                    Table.Columns.RemoveInternal(_column);
                }

                protected override void UnExecuteCore()
                {
                    Table.Columns.Add(_column);
                }

                public override string ToString()
                {
                    return string.Format("Column \"{0}\" Removed", _columnName);
                }

                public override void GetObjectData(SerializationInfo info, StreamingContext context)
                {
                    base.GetObjectData(info, context);

                    // Serialize the Column Name and Ordinal (key)
                    // On deserialization, the orginal will be used, since the column will be present in the removed columns list

                    info.AddValue("ColumnName", _columnName);
                    info.AddValue("ColumnOrdinal", _column.Ordinal);
                }
            }

            /// <summary>
            /// Action for creating column
            /// </summary>
            [Serializable]
            public class CreateColumn : TableAction
            {
                private readonly Column _column;
                private readonly string _columnName;

                private CreateColumn(SerializationInfo info, StreamingContext context) : base(info, context)
                {
                    // Extract the refernce column from the table.
                    // The column might be located in the removed list. 
                    // Use the orginal to get the reference

                    _columnName = info.GetString("ColumnName");
                    int ordinal = info.GetInt32("ColumnOrdinal");
                    _column = Table.Columns.GetAllColumns().Single(entry => entry.Key.Ordinal == ordinal).Key;
                }

                internal CreateColumn(MTable table, Column column) : base(table)
                {
                    _column = column;
                    _columnName = column.Name;
                }

                protected override void ExecuteCore()
                {
                    _table.Columns.Add(_column);
                }

                protected override void UnExecuteCore()
                {
                    _table.Columns.RemoveInternal(_column);
                }

                public override string ToString()
                {
                    return string.Format("Column \"{0}\" Created", _columnName);
                }

                public override void GetObjectData(SerializationInfo info, StreamingContext context)
                {
                    base.GetObjectData(info, context);

                    // Serialize the Original Column Name and the Ordinal (key)
                    info.AddValue("ColumnName", _columnName);
                    info.AddValue("ColumnOrdinal", _column.Ordinal);
                }
            }

            /// <summary>
            /// Action for inserting records
            /// </summary>
            [Serializable]
            public class InsertRecords : TableAction
            {
                private readonly long[] _records;
                private object[][] _recordsValues;

                private InsertRecords(SerializationInfo info, StreamingContext context) : base(info, context)
                {
                    // Deserialization

                    _records = (long[])info.GetValue("Records", typeof(long[]));
                    _recordsValues = (object[][])info.GetValue("RecordsValues", typeof(object[][]));
                }

                internal InsertRecords(MTable table, long[] records, params object[][] recordValues) : base(table)
                {
                    _records = records;
                    _recordsValues = recordValues;
                }

                public long[] Records
                {
                    get { return _records; }
                }

                protected override void ExecuteCore()
                {
                    // Do nothing here, since the record is already inserted into the table and the record contained here is the one related to it.

                    using (var transaction = this.Table.Core.Manager.BeginInsertTransaction())
                    {
                        for (int idx = 0; idx < _records.Length; idx++)
                            this.Table.InsertRecordInternal(idx < _recordsValues.Length ? _recordsValues[idx] : _recordsValues[0], _records[idx]);
                    }

                    this.Table.Core.Complete();
                }

                protected override void UnExecuteCore()
                {
                    using (var transaction = this.Table.Core.Manager.BeginRemoveTransaction())
                    {
                        // Perform row deletion of the inserted record, using the record key from this action
                        foreach (var recordKey in _records)
                            this.Table.RemoveRecordInternal(recordKey);
                    }
                }

                public override string ToString()
                {
                    return string.Format("{0} Records Inserted", _records.Length);
                    //return string.Format("Records Inserted: key={0}, values={1}", string.Join(",", _records), _recordsValues.Length);
                }

                public override void GetObjectData(SerializationInfo info, StreamingContext context)
                {
                    base.GetObjectData(info, context);

                    // Serializarion

                    info.AddValue("Records", this.Records);
                    info.AddValue("RecordsValues", _recordsValues);
                }
            }

            /// <summary>
            /// Action for removing records
            /// </summary>
            [Serializable]
            public class RemoveRecords : TableAction
            {
                private readonly long[] _records;
                private object[][] _recordValues;

                private RemoveRecords(SerializationInfo info, StreamingContext context) : base(info, context)
                {
                    // Deserialization

                    _records = (long[])info.GetValue("Records", typeof(long[]));
                    _recordValues = (object[][])info.GetValue("RecordsValues", typeof(object[][]));
                }

                internal RemoveRecords(MTable table, long[] records)
                    : base(table)
                {
                    _records = records;
                }

                public long[] Records
                {
                    get { return _records; }
                }

                protected override void ExecuteCore()
                {
                    // Approaches: 
                    // 1) Copy read data from the record being deleted into the undo table
                    // 2) Copy the identifier record number only into the undo table and hide the deleted record from the main table

                    // Create copy of the record data being deleted and store it.

                    _recordValues = this.Table
                        .Query(_records)
                        .Iterator2(string.Empty)
                        .Select(view => view.Values.ToArray())
                        .ToArray();

                    using (var transation = this.Table.Core.Manager.BeginRemoveTransaction())
                    {
                        foreach (var recordKey in _records)
                            this.Table.RemoveRecordInternal(recordKey);
                    }
                }

                protected override void UnExecuteCore()
                {
                    using (var transation = this.Table.Core.Manager.BeginInsertTransaction())
                    {
                        // Get the copied backup data of the deleted records and perform insert of record with that data
                        for (int idx = 0; idx < _records.Length; idx++)
                            this.Table.InsertRecordInternal(_recordValues[idx], _records[idx]);
                    }

                    this.Table.Core.Complete();
                }

                public override string ToString()
                {
                    return string.Format("{0} Records Removed", _records.Length); //: key={0}", string.Join(",", _records));
                }

                public override void GetObjectData(SerializationInfo info, StreamingContext context)
                {
                    base.GetObjectData(info, context);

                    // Serialization
                    info.AddValue("Records", Records);
                    info.AddValue("RecordsValues", _recordValues);
                }
            }

            /// <summary>
            /// Action for modifying records
            /// </summary>
            [Serializable]
            public class ModifyRecords : TableAction
            {
                private readonly Query.ViewRow[] _recordsChanged;
                private readonly string _affectedColumns;
                private readonly long[] _records;

                private Query.View[] _recordsOriginal;

                private ModifyRecords(SerializationInfo info, StreamingContext context) : base(info, context)
                {
                    // Deserialization

                    _records = (long[])info.GetValue("Records", typeof(long[]));
                    _affectedColumns = info.GetString("AffectedColumns");
                    _recordsChanged = null;
                }

                internal ModifyRecords(MTable table, params Query.ViewRow[] recordsChanged) : base(table)
                {
                    _recordsChanged = recordsChanged;
                    _affectedColumns = string.Join(",", recordsChanged.SelectMany(record => record.ColumnValues.Keys.Select(column => column.Name)));
                    _records = recordsChanged.Select(record => record.Record).ToArray();
                }

                public long[] Records
                {
                    get { return _records; }
                }

                protected override void ExecuteCore()
                {
                    // Read the original record values and backup, then update the record with the new values

                    _recordsOriginal = Table.Query(_records).Iterator2(_affectedColumns).ToArray();

                    using (var transation = this.Table.Core.Manager.BeginModifyTransaction())
                    {
                        // Change the records here
                        foreach (var recordKey in _records)
                            foreach (var field in _recordsChanged.Single(record => record.Record == recordKey).ColumnValues)
                                this.Table.ModifyRecordInternal(recordKey, field.Key, field.Value, 0);
                    }
                }

                protected override void UnExecuteCore()
                {
                    // Change the records fields to the original values

                    using (var transation = this.Table.Core.Manager.BeginModifyTransaction())
                    {
                        foreach (var recordKey in _records)
                        {
                            var view = _recordsOriginal.Single(record => record.Record == recordKey);
                            foreach (var column in view.Columns)
                                this.Table.ModifyRecordInternal(recordKey, column, view[column], 0);
                        }
                    }
                }

                public override string ToString()
                {
                    return string.Format("{0} Records Modified", _records.Length);
                    //return string.Format("Records Changed: key={0}", string.Join(",", _records));
                }

                public override void GetObjectData(SerializationInfo info, StreamingContext context)
                {
                    base.GetObjectData(info, context);

                    // Serialization

                    info.AddValue("Records", this.Records);
                    info.AddValue("AffectedColumns", this._affectedColumns);
                }
            }

            public override void GetObjectData(SerializationInfo info, StreamingContext context)
            {
                base.GetObjectData(info, context);

                info.AddValue("TableName", Table.Name);
            }
        }

        public sealed class Association
        {
            private readonly string _name;
            private readonly string _displayAs;
            private readonly MTable _parentTable;
            private readonly MTable _childTable;
            private readonly Column _parentColumn;
            private readonly Column _childColumn;
            internal readonly fieldset childFieldSet;

            private readonly Dictionary<long, object> _associationCache = new Dictionary<long, object>();

            internal Association(string name, MTable parentTable, Column parentColumn, string displayAs, MTable childTable, Column childColumn)
            {
                _name = name;
                _displayAs = displayAs;
                _parentTable = parentTable;
                _parentColumn = parentColumn;
                _childTable = childTable;
                _childColumn = childColumn;

                // _parentColumn.association = this;

                this.childFieldSet = fieldset.Allocate(ChildTable.Core, new int[] { this.ChildColumn.Ordinal }, this.ChildTable.Columns.Count);
            }

            public string DisplayAs { get { return _displayAs; } }
            public string Name { get { return _name; } }
            public MTable ParentTable { get { return _parentTable; } }
            public MTable ChildTable { get { return _childTable; } }
            public Column ParentColumn { get { return _parentColumn; } }
            public Column ChildColumn { get { return _childColumn; } }

            internal void Cache(long associatedRowNumber, object value)
            {
                if (_associationCache.Count > 200000)
                {
                    var evict = _associationCache.Take(10000).Select(key => key.Key).ToArray();
                    for (int i = 0; i < evict.Length; i++)
                        _associationCache.Remove(evict[i]);
                }

                _associationCache.Add(associatedRowNumber, value);
            }

            internal bool TryGetCached(ref object value, long associatedRowNumber)
            {
                return _associationCache.TryGetValue(associatedRowNumber, out value);
            }

            public override string ToString()
            {
                if (string.IsNullOrWhiteSpace(Name))
                    return string.Format("[{0}.{1} -> {2}.{3}]", ParentTable.Name, ParentColumn.Name, ChildTable.Name, ChildColumn.Name);

                return string.Format("{0} - [{1}.{2} -> {3}.{4}]", Name, ParentTable.Name, ParentColumn.Name, ChildTable.Name, ChildColumn.Name);
            }
        }

        public sealed class ColumnCollection : IReadOnlyCollection<Column>
        {
            private readonly ITable _table;
            private readonly List<Column> _columnsVisible = new List<Column>();
            private readonly HashSet<Column> _columnsRemoved = new HashSet<Column>();
            private RestructureTransation _transation;

            internal ColumnCollection(ITable table)
            {
                _table = table;
            }

            /// <summary>
            /// Gets the number of columns in table
            /// </summary>
            public int Count
            {
                get { return _columnsVisible.Count; }
            }

            /// <summary>
            /// Begins structure modification transation. Allows executing all operations at commit stage.
            /// </summary>
            /// <returns></returns>
            public IDataStorage.ITransaction Begin()
            {
                if (_transation != null)
                    throw new InvalidOperationException("Unable to begin columns restructure transation. There is already one which has not been commited.");
                _transation = new RestructureTransation(this);
                return _transation;
            }

            /// <summary>
            /// Create new column and append it to the end of this columns list
            /// </summary>
            /// <param name="name"></param>
            /// <param name="type"></param>
            /// <param name="size"></param>
            /// <returns></returns>
            public Column CreateColumn(string name, Column.DataTypes type, int size)
            {
                var core = ((MTable)_table).Core;

                Column column;

                if (_table.Count > 0)
                {
                    // If the table is not empty the operation must be executed thru internal columns transation.
                    if (_transation == null)
                        throw new InvalidOperationException("Unable to add column in non empty table. There must be columns restructure transation present at this point. Make sure Columns.Begin transation was callen before this operation.");
                    if (_transation.Commited)
                        throw new InvalidOperationException("Transation already commited. Invalid call to create columns in restructured transation.");


                    //      Determine the column ordinal (aka key): 
                    //      Take the current core count and add it to the current transaction queued count
                    column = new Column((MTable)_table,
                        name,
                        core.ColumnCount + _transation.QueueColumnExtendion(),
                        type, size);
                }
                else
                {
                    column = new Column((MTable)_table, name, core.ColumnCount, type, size);

                    var totalColumnCount = core.ColumnCount + 1;
                    core.SetColumnCount(totalColumnCount);
                }

                if (((MTable)_table).Config.ActionManagerEnabled)
                    ((MTable)_table).Config.ActionManager.RecordAction(new TableAction.CreateColumn((MTable)_table, column));
                else
                    this.Add(column);

                return column;
            }

            public IEnumerator<Column> GetEnumerator()
            {
                return _columnsVisible.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }

            /// <summary>
            /// Get column property descriptors for the columns in this collection
            /// </summary>
            /// <returns></returns>
            public PropertyDescriptorCollection GetDescriptors(bool readOnly = false)
            {
                var descriptors = new PropertyDescriptorCollection(this.Select(column => new Column.Descriptor(column)).ToArray(), readOnly);
                return descriptors;
            }

            /// <summary>
            /// Builds columns data statistics information for all columns in the table. The result is contained in the Column.DataInfo object
            /// </summary>
            public void BuildDataInfo()
            {
                /// Reset the columns data information
                foreach (var column in this)
                    column.DataInfo.Clear();

                foreach (var record in _table.Query().Iterator2(string.Empty))
                {
                    foreach (var column in record.Columns)
                    {
                        column.DataInfo.Register(record[column]);
                    }
                }
            }

            /// <summary>
            /// Gets one or more columns by comma-separated names. Cause exception if column name was not found
            /// </summary>
            /// <param name="columnNames"></param>
            /// <returns></returns>
            public Column[] GetColumns(string columnNames)
            {
                var names = columnNames.Split(',').Select(name => name.Trim());
                //  Get the columns by names. Find the column or cause exception if the column does not exist
                return names
                    .Select(name =>
                        {
                            var found = _columnsVisible.SingleOrDefault(column => name.Equals(column.Name, StringComparison.InvariantCultureIgnoreCase) || name.Equals(column.DisplayName, StringComparison.InvariantCultureIgnoreCase));
                            if (found != null)
                                return found;
                            throw new KeyNotFoundException(string.Format("Column '{0}' not found in '{1}'", name, _table.Name));
                        })
                    .ToArray();
            }

            /// <summary>
            /// Gets single column by orginal
            /// </summary>
            /// <param name="ordinal"></param>
            /// <returns></returns>
            public Column this[int ordinal]
            {
                get { return _columnsVisible.Single(column => column.Ordinal == ordinal); }
            }

            /// <summary>
            /// Gets one or more columns by their ordinals
            /// </summary>
            /// <param name="columnNames"></param>
            /// <returns></returns>
            public Column[] this[params int[] ordinals]
            {
                get { return _columnsVisible.Where(column => ordinals.Contains(column.Ordinal)).ToArray(); }
            }

            /// <summary>
            /// Gets one or more columns by comma-separated names
            /// </summary>
            /// <param name="columnNames"></param>
            /// <returns></returns>
            public Column[] this[string columnNames]
            {
                get { return this.GetColumns(columnNames); }
            }

            /// <summary>
            /// Checks if the column with this name already exist in the table
            /// </summary>
            /// <param name="columnName"></param>
            /// <returns></returns>
            public bool Contains(string columnName)
            {
                return _columnsVisible.FirstOrDefault(column => String.Equals(column.Name, columnName, StringComparison.OrdinalIgnoreCase)) != null;
            }

            /// <summary>
            /// Adds column into table
            /// </summary>
            /// <param name="column"></param>
            public void Add(Column column)
            {
                //  Approach: Adds the new column always at the end. 
                //  There will be visible position property used to display the column at the desired location

                _columnsRemoved.Remove(column);

                if (column.Ordinal < _columnsVisible.Count)
                    _columnsVisible.Insert(column.Ordinal, column);
                else
                    _columnsVisible.Add(column);

                //  Set modified only if there is no restructure transation 
                if (_transation == null)
                    ((MTable)_table).SetModified();
            }

            /// <summary>
            /// Removes column from the table
            /// </summary>
            /// <param name="column"></param>
            public void Remove(Column column)
            {
                if (((MTable)_table).Config.ActionManagerEnabled)
                    ((MTable)_table).Config.ActionManager.RecordAction(new TableAction.RemoveColumn((MTable)_table, column));
                else
                    this.RemoveInternal(column);
            }

            internal void RemoveInternal(Column column)
            {
                //  Approach: Put the column into hiden list. Do not use this list when enumerating the columns

                _columnsRemoved.Add(column);
                _columnsVisible.Remove(column);

                //  Set modified only if there is no restructure transation 
                if (_transation == null)
                    ((MTable)_table).SetModified();
            }


            /// <summary>
            /// Gets all of the columns, the visible and removed, ordered by ordinal, wrapped in dictionary, where the boolean value indicates the remove flag
            /// </summary>
            internal IDictionary<Column, bool> GetAllColumns()
            {
                Dictionary<Column, bool> columns = new Dictionary<Column, bool>();
                foreach (var column in Enumerable.Concat(_columnsVisible, _columnsRemoved).Distinct().OrderBy(column => column.Ordinal))
                    columns.Add(column, _columnsRemoved.Contains(column));
                return columns;
            }

            /// <summary>
            /// Clears the columns collection to initial state
            /// </summary>
            internal void Clear()
            {
                _columnsVisible.Clear();
                _columnsRemoved.Clear();
            }


            /// <summary>
            /// Represents columns modification transation used internally to execute operations performed on column collection in single commit call
            /// </summary>
            private class RestructureTransation : IDataStorage.ITransaction
            {
                readonly ColumnCollection _columns;
                readonly MTable _table;

                /// <summary>
                /// Number of columns which has to be used for table extension
                /// </summary>
                int _columnsToExtend;

                public RestructureTransation(ColumnCollection columns) : base(((MTable)columns._table).Core)
                {
                    _columns = columns;
                    _table = (MTable)columns._table;
                }

                public override void Commit()
                {
                    //  Lock the table locker here, we dont want interation with the table state while the transation is running.
                    //
                    lock (_table._locker)
                    {
                        try
                        {
                            if (_columns._transation == null)
                                throw new InvalidOperationException("Invalid state of object. The columns restructure transation has not begun but it was attempted to commit");

                            //  Perform all columns restructure operations here in single call
                            if (_columnsToExtend > 0)
                            {
                                ////  If there is data in the table, we have to modify all records headers to add the column there too
                                //for (long record = StartAt; record < StartAt + Count; record++)
                                //    Core.Manager.ExtendRecord(record, totalColumnCount);

                                //  Begin core internal extend transation
                                //using (_table.Core.Manager.BeginExtendTransaction(_columnsToExtend))
                                {
                                    //  This kind of action does not requires body. This may be changed in future.
                                }

                                _table.Core.Manager.ExtendRecords(_table.StartAt, _table.Count, _columnsToExtend);
                            }

                            base.Commit();


                            //  Force the strucure changes to be immideately stored without setting the modification flag
                            _table.SavePersistentData(force: true);


                            // Notify clients about structure change
                            _table.NotifyListChanged(new ListChangedEventArgs(ListChangedType.PropertyDescriptorAdded, null));
                        }
                        finally
                        {
                            // Remove the parent transation                   
                            _columns._transation = null;
                        }
                    }
                }

                public override void Rollback()
                {
                    throw new NotImplementedException();
                }

                /// <summary>
                /// Indicate that the transation should extend the current table columns by one new column. 
                /// Returns previous pending count of columns to extend in this transaction.
                /// </summary>
                internal int QueueColumnExtendion()
                {
                    var count = _columnsToExtend;
                    _columnsToExtend++;
                    return count;
                }
            }
        }

        /// <summary>
        /// Represents table column definition
        /// </summary>
        public sealed partial class Column
        {
            [Serializable]
            public enum DataTypes
            {
                Text,
                Long,
                Number,
                Binary,
                Date
            }

            private readonly MTable _table;
            internal readonly int ordinal;
            private readonly DataSummaryInfo _dataInfo;
            private int _size;
            private string _name;
            private DataTypes _type;

            internal Association association;
            private TextIndex _textIndex;
            private SortIndex _sortIndex;
            private bool _nativeSorted;
            private bool _calculated;

            internal Column(MTable table, string name, int ordinal, DataTypes type, int size, string expression = null)
            {
                _table = table;
                _name = name;
                _type = type;
                _size = size;
                _dataInfo = new DataSummaryInfo();

                this.ordinal = ordinal;
                this.Expression = expression;

                MTable.OnExpressionsContextUpdated += (sender, args) =>
                    {
                        // Reset calculated expression callback and send recompilation request
                        if (_calculated)
                        {
                            // Compile the expression string into runnable code
                            if (MTable.CalculationExpresionRequested != null)
                                this.ExpressionFunc = MTable.CalculationExpresionRequested(_table, this, this.Expression);
                            else
                                throw new InvalidOperationException("Expression compilation has to be handled. Use the MTable.CalculationExpresionRequested event to handle it");
                        }
                    };
            }

            /// <summary>
            /// Expression used to calculate column value. Current language supported is C#. The expression has to be in form (record) => { return [ function body here ]; } 
            /// </summary>
            public string Expression
            {
                get { return _expression; }
                set
                {
                    if (Expression != value)
                    {
                        if (!string.IsNullOrWhiteSpace(value))
                        {
                            // Compile the expression string into runnable code
                            if (MTable.CalculationExpresionRequested != null)
                                this.ExpressionFunc = MTable.CalculationExpresionRequested(_table, this, value);
                            else
                                throw new InvalidOperationException("Expression compilation has to be handled. Use the MTable.CalculationExpresionRequested event to handle it");

                            _calculated = true;
                        }
                        else
                        {
                            this.ExpressionFunc = null; // Reset the function instance
                            _calculated = false;
                        }

                        _expression = value;

                        //  Set modified only if there is no restructure transation 
                        //if (_table.Columns. _transation != null)
                        //    ((MTable)_table).SetModified();
                        _table.SetModified();
                    }
                }
            }

            /// <summary>
            /// Function called to provide calculated value for this column
            /// </summary>
            internal Func<Query.View, object> ExpressionFunc;

            /// <summary>
            /// Name of the column
            /// </summary>
            public string Name
            {
                get { return _name; }
                set
                {
                    if (_name != value)
                    {
                        _name = value;
                        _table.SetModified();
                    }
                }
            }

            /// <summary>
            /// Name used for displaying purposes
            /// </summary>
            public string DisplayName
            {
                get
                {
                    return Association != null && !string.IsNullOrWhiteSpace(Association.DisplayAs)
                        ? Association.DisplayAs
                        : Name;
                }
            }

            /// <summary>
            /// Unique column identifier
            /// </summary>
            public int Ordinal
            {
                get { return ordinal; }
            }

            /// <summary>
            /// Size of the data values stored in this column. 0 means Auto
            /// </summary>
            public int Size
            {
                get { return _size; }
            }

            /// <summary>
            /// Type of the data values stored in this column
            /// </summary>
            public DataTypes DataType
            {
                get { return _type; }

                internal set
                {
                    if (_type != value)
                    {
                        _type = value;
                        _table.SetModified();
                    }
                }
            }

            public bool NativeSorted
            {
                get { return _nativeSorted; }
                internal set { _nativeSorted = value; }
            }

            public Association Association
            {
                get { return association; }
            }

            public SortIndex SortIndex
            {
                get
                {
                    if (_sortIndex != null)
                        return _sortIndex;

                    return
                        _sortIndex =
                            _table.Indexes.Values.FirstOrDefault(
                                index => (index is SortIndex && index.Columns.Contains(this))) as SortIndex;
                }
                internal set { _sortIndex = value; }
            }

            public TextIndex TextIndex
            {
                get
                {
                    if (_textIndex != null)
                        return _textIndex;

                    return
                        _textIndex =
                            _table.Indexes.Values.FirstOrDefault(
                                index => (index is TextIndex && index.Columns.Contains(this))) as TextIndex;
                }
                internal set { _textIndex = value; }
            }

            /// <summary>
            /// Contains statistic information about the data contained in this column
            /// </summary>
            public DataSummaryInfo DataInfo
            {
                get { return _dataInfo; }
            }

            public Association AssociateWith(MTable table, string columnName, string displayAs, string associationName)
            {
                return AssociateWith(table, table.Columns.Single(column => column.Name == columnName), displayAs, associationName);
            }

            public Association AssociateWith(MTable table, Column column, string displayAs, string associationName)
            {
                return association = table.AddAssociation(associationName, this, displayAs, table, column);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            private Object GetFieldValue(fieldset.Field field, DataTypes dataType)
            {
                switch (dataType)
                {
                    case DataTypes.Text:
                        {
                            return field.GetText();
                        }

                    case DataTypes.Long:
                        {
                            {
                                long value;
                                if (field.TryGetNumber(out value))
                                    return value;
                            }

                            //if (field.Size > 0)
                            //{
                            //    double value;
                            //    if (field.TryGetTextNumber(out value))
                            //        return value;

                            //    // Fallback: read the text representation of the field
                            //    return field.GetText();
                            //}

                            return null;
                        }

                    case DataTypes.Number:
                        {
                            double value;
                            if (field.TryGetNumber(out value))
                                return value;

                            return null;
                        }

                    case DataTypes.Date:
                        {
                            // Convert the native stored ticks to date object
                            long dateData;
                            if (field.TryGetNumber(out dateData))
                            {
                                return DateTime.FromBinary(dateData);
                            }

                            return null;
                        }

                    case DataTypes.Binary:
                        unsafe
                        {
                            byte[] value = new byte[(int)field.Size];
                            Marshal.Copy(new IntPtr(field.Pointer), value, 0, value.Length);
                            return value;
                        }
                }

                throw new Exception("Not implemented");
            }

            private static bool cacheAssociation = true;
            private bool _error;
            private SortedSet<long> _errorRecords = new SortedSet<long>();
            private string _expression;

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public Object GetValue(MTableCore.Records.Record row, bool checkAssociation = true)
            {
                fieldset.Field field;

                if (row.Fields.Length > 1)
                {
                    field = row.Fields.First(f => f.Ordinal == this.Ordinal);
                }
                else
                    field = row.Fields[0];


                //MTableCore.Records.FieldSet.Field field = row.FieldSet.Length > 1
                //    ? row.Fields[Array.IndexOf(row.FieldSet, (ushort) this.Ordinal)]
                //    : row.Fields[0];

                return GetValue(field, checkAssociation);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            public object GetValue(fieldset.Field field, bool checkAssociation = true)
            {
                // Based on the column type, convert the bytes into the appropriate data type.
                // If the column is associated with another table, the value is the row number in that table. 
                // In that case, get the external table map view , entry index and read it

                if (checkAssociation && this.association != null)
                {
                    Object value = null;
                    long associatedIndex;

                    if (field.TryGetNumber(out associatedIndex) && associatedIndex > 0 && (!cacheAssociation || !this.association.TryGetCached(ref value, associatedIndex)))
                    {
                        var childField = this.association.childFieldSet.GetFields((associatedIndex - 1) + this.association.ChildTable.StartAt)[0];

                        value = GetFieldValue(childField, this.association.ChildColumn.DataType);

                        if (cacheAssociation)
                            this.association.Cache(associatedIndex, value);
                    }

                    return value;
                }

                return GetFieldValue(field, this.DataType);
            }

            public override string ToString()
            {
                return string.Format("{0} - [{1}]", Ordinal, Name);
            }

            internal void ClearErrors()
            {
                _errorRecords.Clear();
                _error = false;
            }

            internal void SetError(long record)
            {
                _errorRecords.Add(record);
                _error = true;
            }

            internal bool HasError(long record)
            {
                if (_error && _errorRecords.Contains(record))
                    return true;
                return false;
            }

            internal void ClearError(long record)
            {
                if (_error && _errorRecords.Contains(record))
                {
                    _errorRecords.Remove(record);
                    if (_errorRecords.Count == 0)
                        _error = false;
                }
            }

            public sealed class DataSummaryInfo
            {
                private double _numberMin = double.MaxValue;
                private double _numberMax = double.MinValue;
                private double _numberSum;

                private int _lengthMin = int.MaxValue;
                private int _lengthMax = int.MinValue;
                private int _lengthSum;

                private int _countNull;
                private int _countEmpty;
                private int _countNumber;
                private int _countText;

                internal DataSummaryInfo()
                {
                    Clear();
                }

                /// <summary>
                /// Updates the data info by aggregating the value passed
                /// </summary>
                /// <param name="value"></param>
                internal void Register(string value, int? length = null)
                {
                    if (value == null)
                    {
                        _countNull++;
                        if (length == null)
                            length = 0;
                    }
                    else if (value == string.Empty)
                    {
                        _countEmpty++;
                        if (length == null)
                            length = 0;
                    }
                    else
                    {
                        if (length == null)
                            length = value.Length;

                        _countText++;
                    }

                    if (length < _lengthMin)
                        _lengthMin = (int)length;
                    if (length > _lengthMax)
                        _lengthMax = (int)length;
                    _lengthSum += (int)length;
                }

                /// <summary>
                /// Updates the data info by aggregating the value passed
                /// </summary>
                /// <param name="value"></param>
                internal void Register(double value, int length)
                {
                    if (length == 0)
                    {
                        _countNull++;
                    }
                    else
                    {
                        _countNumber++;

                        if (value < _numberMin)
                            _numberMin = value;
                        if (value > _numberMax)
                            _numberMax = value;
                        _numberSum += value;
                    }

                    if (length < _lengthMin)
                        _lengthMin = (int)length;
                    if (length > _lengthMax)
                        _lengthMax = (int)length;
                    _lengthSum += (int)length;
                }

                /// <summary>
                /// Updates the data info by aggregating the value passed
                /// </summary>
                /// <param name="value"></param>
                internal void Register(long value, int length)
                {
                    if (length == 0)
                    {
                        _countNull++;
                    }
                    else
                    {
                        _countNumber++;

                        if (value < _numberMin)
                            _numberMin = value;
                        if (value > _numberMax)
                            _numberMax = value;
                        _numberSum += value;
                    }

                    if (length < _lengthMin)
                        _lengthMin = (int)length;
                    if (length > _lengthMax)
                        _lengthMax = (int)length;
                    _lengthSum += (int)length;
                }

                /// <summary>
                /// Updates the data info by aggregating the value passed
                /// </summary>
                /// <param name="value"></param>
                internal void Register(object value, int? length = null)
                {
                    if (value == null)
                    {
                        _countNull++;
                        if (length == null)
                            length = 0;
                    }
                    else if (value == string.Empty)
                    {
                        _countEmpty++;
                        if (length == null)
                            length = 0;
                    }
                    else
                    {
                        string text = value.ToString();

                        if (length == null)
                            length = text.Length;

                        long numeric = 0;
                        if (long.TryParse(text, out numeric))
                        {
                            _countNumber++;

                            if (numeric < _numberMin)
                                _numberMin = numeric;
                            if (numeric > _numberMax)
                                _numberMax = numeric;
                            _numberSum += numeric;
                        }
                        else
                        {
                            _countText++;
                        }
                    }

                    if (length < _lengthMin)
                        _lengthMin = (int)length;
                    if (length > _lengthMax)
                        _lengthMax = (int)length;
                    _lengthSum += (int)length;
                }

                /// <summary>
                /// Percentage of column content occupancy
                /// </summary>
                public int Occupancy
                {
                    get
                    {
                        double countContent = _countNumber + _countText;
                        double countTotal = _countNull + _countEmpty + countContent;

                        return (int)((countContent / countTotal) * 100);
                    }
                }

                /// <summary>
                /// Gets the suggested data type determined by using the column data content summary info
                /// </summary>
                public Column.DataTypes ProposedDataType
                {
                    get
                    {
                        double countContent = _countNumber + _countText;
                        double countTotal = _countNull + _countEmpty + countContent;

                        // Get numbers percentage and text percentages. 

                        if (_countNumber / countContent > 0.8d)
                            return DataTypes.Number;
                        else
                            return DataTypes.Text;
                    }
                }

                /// <summary>
                /// Gets the solid data type determined by using the column data content summary info
                /// </summary>
                public Column.DataTypes DataType
                {
                    get
                    {
                        double countContent = _countNumber + _countText;
                        double countTotal = _countNull + _countEmpty + countContent;

                        // All of the content represent numbers 

                        if (_countNumber == countContent)
                            return DataTypes.Number;
                        else
                            return DataTypes.Text;
                    }
                }

                public double? NumberMin
                {
                    get { return _numberMin < double.MaxValue ? _numberMin : (double?)null; }
                }

                public double? NumberMax
                {
                    get { return _numberMax > double.MinValue ? _numberMax : (double?)null; }
                }

                public double NumberSum
                {
                    get { return _numberSum; }
                }

                public double? NumberMean
                {
                    get
                    {
                        double countContent = _countNumber + _countText;
                        return NumberSum / countContent;
                    }
                }

                public int? LengthMin
                {
                    get { return _lengthMin < double.MaxValue ? _lengthMin : (int?)null; }
                }

                public int? LengthMax
                {
                    get { return _lengthMax > double.MinValue ? _lengthMax : (int?)null; }
                }

                public int LengthSum
                {
                    get { return _lengthSum; }
                }

                public int LengthMean
                {
                    get
                    {
                        int countContent = _countNumber + _countText;
                        return LengthSum / countContent;
                    }
                }

                /// <summary>
                /// Clears the data info and sets it to its default states
                /// </summary>
                internal void Clear()
                {
                    _numberMin = double.MaxValue;
                    _numberMax = double.MinValue;
                    _numberSum = 0;
                    _lengthMin = int.MaxValue;
                    _lengthMax = int.MinValue;
                    _lengthSum = 0;
                    _countNull = 0;
                    _countEmpty = 0;
                    _countNumber = 0;
                    _countText = 0;
                }

                public override string ToString()
                {
                    return
                        string.Format(
                            "nulls={0}, empty={3}, texts={1}, numbers={2}, numMin={4}, numMax={5}, lenMin={6}, lenMax={7}, occp={8}, type={9}",
                            _countNull, _countText, _countNumber, _countEmpty, _numberMin, _numberMax, _lengthMin,
                            _lengthMax, Occupancy, Enum.GetName(typeof(DataTypes), ProposedDataType));
                }
            }

        }

        public long Count
        {
            get
            {
                if (_count > long.MinValue)
                    return _count;

                VerifyInitialized();

                // If not in multi-table mode, get the row count from the table layout entries for this table and the next one, or the total row count
                if (storage.isMultiTable)
                    return _count = _tableCore.RowCount;

                long startIndex = storage.tablesLayout[this];

                if (this.Equals(storage.Tables.Last()))
                    return _count = storage.table.RowCount - startIndex;

                long endIndex = storage.tablesLayout[storage.Tables[Array.IndexOf(storage.Tables.ToArray(), this) + 1]];

                return _count = endIndex - startIndex;
            }
        }

        /// <summary>
        /// Checks if the table instance is initialized and can be queried
        /// </summary>
        private void VerifyInitialized()
        {
            if (!_initialized)
                throw new InvalidOperationException("Table not initialized or disposed");
        }

        /// <summary>
        /// Returns all records in the table, containing the data for the specified columns only
        /// </summary>
        /// <param name="columns"></param>
        /// <returns></returns>
        public MTableCore.Records this[params Column[] columns]
        {
            get
            {
                return this.GetRecords(columns);
            }
        }

        /// <summary>
        /// Returns all records in the table
        /// </summary>
        /// <returns></returns>
        public MTableCore.Records GetRecords()
        {
            return GetRecords(null);
        }

        public static IEnumerable<Object> GetValues(ITable table, string columnName)
        {
            return GetValues(table, columnName, null);
        }

        public static IEnumerable<Object> GetValues(ITable table, string columnName, params long[] records)
        { 
            var column = table.Columns.Single(col => col.Name == columnName);
            return MTable.GetValues(table, column, records);
        }

        public static IEnumerable<Object> GetValues(ITable table, Column column, params long[] records)
        { 
            var fieldSet = table.CreateFieldSet(new Column[] { column });
            var associationIndex = column.SortIndex;

            if (records != null)
            {
                for (int i = 0; i < records.Length; i++)
                {
                    var record = records[i];

                    var sortedRecord = associationIndex != null ? associationIndex.GetSortedRecord(record) : record;

                    var value = column.GetValue(fieldSet.GetFields(sortedRecord)[0]);

                    yield return value;
                }

                yield break;
            }
            else
            {
                for (long record = table.StartAt, end = (table.StartAt + table.Count); record < end; record++)
                {
                    var sortedRecord = associationIndex != null ? associationIndex.GetSortedRecord(record) : record;

                    var value = column.GetValue(fieldSet.GetFields(sortedRecord)[0]);

                    yield return value;
                }

                yield break;
            }
        }

        /// <summary>
        /// Returns all records in the table, containing the data for the specified columns only
        /// </summary>
        /// <param name="columns"></param>
        /// <returns></returns>
        public MTableCore.Records GetRecords(params Column[] columns)
        {
            VerifyInitialized();

            if (columns == null || columns.Length == 0)
                columns = this.Columns.ToArray();

            // In non multi-table mode, get the starting index from the tables layout

            var fieldSet = this.CreateFieldSet(columns);

            return Core.QueryRows(StartAt, StartAt + this.Count, fieldSet);
        }

        public override string ToString()
        {
            return Name;
        }

        public void Dispose()
        {
            lock (_locker)
            {
                Uninitialize();
            }
        }

        /// <summary>
        /// Causes this MTable instance to uninitialize, which includes saving persistent data, actions, releasing the table code and configuration
        /// </summary>
        private void Uninitialize()
        {
            //  Validate initialization state. 
            if (!_initialized)
                throw new InvalidOperationException("Attempt to uninitialize not initialized table was made");

            this.SavePersistentData();

            if (storage.isMultiTable)
                _tableCore.Release();

            if (Config.ActionManagerEnabled)
                Config.ActionManager.SaveActions(this, Path.Combine(Config.DataFolder, "table.actions"));

            ((IDisposable)this.Config).Dispose();

            //  reset the records count and the cache to initial state
            _count = long.MinValue;

            _cacheColumns.Clear();

            _columns.Clear();
            _columns = null;

            _tableCore = null;
            _initialized = false;
        }

        /// <summary>
        /// Gets single column by name from column cache.
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public Column GetColumn(string columnName)
        {
            VerifyInitialized();

            Column column;
            if (!_cacheColumns.TryGetValue(columnName, out column))
                _cacheColumns.Add(columnName, column = this.Columns[columnName].Single());

            return column;
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        /// <param name="columnNames"></param>
        /// <returns></returns>
        private Column[] __GetColumns(string columnNames)
        {
            return this.Columns
                .Where(column =>
                {
                    var names = columnNames.Split(',').Select(name => name.Trim());
                    if (names.Contains(column.Name) || names.Contains(column.DisplayName))
                        return true;
                    throw new KeyNotFoundException(string.Format("Column '{0}' not found in '{1}'", column.Name, this.Name));

                }).ToArray();
        }

        public void BeginImport()
        {
            if (!storage.isMultiTable)
            {
                // Set this table layout entry

                if (!storage.tablesLayout.ContainsKey(this))
                    storage.tablesLayout.Add(this, storage.table.RowCount);
                else
                    storage.tablesLayout[this] = storage.table.RowCount;
                if (this.Columns.Count > storage.table.ColumnCount)
                    storage.table.SetColumnCount(this.Columns.Count);

                _count = 0;
            }
        }

        public void EndImport()
        {
            if (storage.isMultiTable)
            {
                Core.Complete();
                _count = long.MinValue;
            }
            else
            {
                _imported = true;

                // If all imported, complete the storage
                if (storage.Tables.Count(table => !table._imported) == 0)
                    storage.table.Complete();
            }
        }

        /// <summary>
        /// Get the starting row number for the table. It can be more then zero in storage-table mode
        /// </summary>
        public long StartAt
        {
            get
            {
                if (_startAt > long.MinValue)
                    return _startAt;

                if (storage.isMultiTable)
                    return _startAt = 0;
                else
                    return _startAt = storage.tablesLayout[this];
            }
        }

        public fieldset CreateFieldSet(Column[] columns)
        {
            //  NOTE:
            //      Get the ordered list of column ordinals for all native columns and skip the others calculated columns. 
            //      The calculated columns does not have data stored in the native table so their ordinals should not be used in the fieldset.

            // Must be ordered by Ordinals
            var ordinals = columns
                .OrderBy(column => column.Ordinal)
                .Select(column => column.Ordinal).ToArray();

            var fieldset = Data.MTableCore.Records.FieldSet.Allocate(Core, ordinals);

            // Sets the native numerical flag for fields which are asociated with numerical column data types
            //foreach (var column in columns.Where(column => column.DataType == Column.DataTypes.Long || column.DataType == Column.DataTypes.Number))
            //    fieldset[column.Ordinal].SetNativeNumeric();

            return fieldset;
        }

        public void BuildIndices(string storageFolder)
        {
            if (!Directory.Exists(storageFolder))
                Directory.CreateDirectory(storageFolder);

            foreach (IBuildableIndex index in _indexes.Values.Where(i => i is IBuildableIndex).OrderBy(i => !(i is SortIndex)))
            {
                // Load the index if exist. Otherwise, build it and store it

                var indexFilename = Path.Combine(storageFolder, string.Format("{0}.{1}{2}.index", this.Name, string.Join("-", ((ColumnIndex)index).Columns.Select(column => column.Name)), index is TextIndex ? ".t" : index is SortIndex ? ".s" : ""));
                if (!File.Exists(indexFilename))
                {
                    index.BuildIndex();
                    index.Store(indexFilename);
                }
                else
                {
                    index.Load(indexFilename);
                }
            }
        }

        /// <summary>
        /// Query the table data, passing the filter criteria expression. <para>
        /// If the criteria is null or empty, all records are returned. </para> 
        /// </summary>
        /// <param name="filterCriteriaExpression"></param>
        /// <param name="resolver">Function resolver service</param>
        /// <param name="cacheRecords">Whether to cache the internal records keys in memory, to allow multiple iterations</param>
        /// <returns></returns>
        public Query Query(string filterCriteriaExpression, Query.IFunctionResolver resolver = null, bool cacheRecords = false)
        {
            VerifyInitialized();

            if (string.IsNullOrWhiteSpace(filterCriteriaExpression))
                return new Query(this, cacheRecords ? this.Query().ToArray() : this.Query().AsEnumerable());
            //return this.Query();

            // built AST tree
            var filterTree = new DigitalToolworks.Data.MStorage.Query.FilterClauseTree(this, filterCriteriaExpression, resolver);

            // get the columns involved
            var queryColumns = filterTree.Columns.Distinct().ToArray();

            // compile the tree to executable
            var whereLamda = filterTree.ToCriteriaLambda();

            // Contains the records indentifiers only, pass this to Query object
            var records = this.Query().Iterator2(columns: queryColumns)
                .Where(record => whereLamda(record))
                .Select(record => record.record);

            // cacheRecords: Loads the record keys in memory. Allows calling the same interator multiple times

            return new Query(this, cacheRecords ? records.ToArray() : records);
        }

        public Query Query(string columnName, MTable.MatchMode matchMode, object matchValue)
        {
            VerifyInitialized();

            var records = MTable.FindRecords(this.Columns[columnName].Single(), this, matchValue, matchMode);
            return new Query(this, records);
        }

        public void AddSortIndex(ColumnIndex sortIndex)
        {
            _indexes.Add(string.Format("SIDX:{0}", string.Join(",", sortIndex.Columns.Select(column => column.Name))), sortIndex);
        }

        public Query Query(params long[] recordKey)
        {
            return new Query(this, recordKey);
        }

        public Query Query(IEnumerable<long> recordKey)
        {
            return new Query(this, recordKey);
        }

        public Query Query()
        {
            return Query(0, (int)this.Count);
        }

        public Query Query(long start, int count)
        {
            VerifyInitialized();

            start = (int)(this.StartAt + start); // adjust to the internal start position

            // limit to the bounds of the table count
            if (start + count > this.Count)
                count -= ((int)start + count) - (int)this.Count;

            return new Query(this, Enumerable.Range((int)start, count)
                .Select(record => (long)record));
        }

        public IReadOnlyDictionary<string, MTable.ColumnIndex> Indexes
        {
            get
            {
                return _indexes;
            }
        }

        /// <summary>
        /// Creates or gets existing sort index sorted by calculated key columns hash values
        /// </summary>
        /// <param name="keyColumnNames"></param>
        /// <returns></returns>
        public MTable.ColumnIndex AddHashIndex(string keyColumns)
        {
            string indexName = string.Format("HIDX:{0}", keyColumns);
            MTable.ColumnIndex index;
            // Get existing index or create new if not found

            if (!_indexes.TryGetValue(indexName, out index))
            {
                index = new HashIndex(this, string.Format("[HIndex: {0} -> {1}]", this.Name, keyColumns), keyColumns);
                _indexes.Add(indexName, index);
            }
            return index;
        }

        /// <summary>
        /// Creates or gets existing sort index for the specified columns and directions (usage: columnName ASC|DESC, columnName2 ASC|DESC)
        /// </summary>
        /// <param name="columnNames"></param>
        /// <returns></returns>
        public MTable.SortIndex AddSortIndex(string columnNames)
        {
            string indexName = string.Format("SIDX:{0}", columnNames);
            MTable.ColumnIndex index;
            // Get existing index or create new if not found

            if (!_indexes.TryGetValue(indexName, out index))
            {
                index = new SortIndex(this, string.Format("[AIndex: {0} -> {1}]", this.Name, columnNames), columnNames);
                _indexes.Add(indexName, index);
            }
            return (SortIndex)index;
        }

        public void RemoveRecord(params long[] recordKey)
        {
            if (config.ActionManagerEnabled)
                config.ActionManager.RecordAction(new TableAction.RemoveRecords(this, recordKey));
            else
            {
                //  Sort the keys larger first
                if (!Core.Manager.InTransation)
                    Array.Sort(recordKey, new Comparison<long>((key1, key2) =>
                    {
                        if (key1 < key2)
                            return 1;
                        if (key1 > key2)
                            return -1;
                        return 0;
                    }));

                foreach (var record in recordKey)
                    this.RemoveRecordInternal(record);
            }
        }

        private void RemoveRecordInternal(long recordKey)
        {
            if (StartAt > recordKey || StartAt + Count <= recordKey)
                throw new InvalidOperationException("The record key " + recordKey + " was not found in the table " + Name);

            var recordData = Core.Manager.RemoveRecord(recordKey);

            // reset the cached count
            _count = long.MinValue;

            // Mark the indexes as dirty
            foreach (IBuildableIndex index in this.Indexes.Values)
                index.SetDirty();

            if (false)
                if (ListChanged != null)
                    ListChanged(this, new ListChangedEventArgs(ListChangedType.ItemDeleted, (int)recordKey));
        }

        /// <summary>
        /// Rules for type conflicts
        /// </summary>
        public enum TypeConflictResolveRule
        {
            KeepOriginalValue,
            SetValueToNull,
            ThrowException
        }

        /// <summary>
        /// Change the data type for column, converting the existing data into the new type. Returns list of records whose values has been set to null due to conversion conflicts
        /// </summary>
        /// <param name="column"></param>
        /// <param name="dataType"></param>
        /// <param name="conflictRule"></param>
        /// <param name="performConvertionCheck"></param>
        public Dictionary<long, object> ChangeColumnType(Column column, Column.DataTypes dataType, TypeConflictResolveRule conflictRule, Dictionary<long, object> truncatedValues = null, bool performConvertionCheck = false)
        {

            /// The trucated values list contains the original cell values for column,
            /// to be used instead of the original mtable value, which should be null due to conversion conflicts
            if (truncatedValues == null)
                truncatedValues = new Dictionary<long, object>();

            if (column.DataType != dataType)
            {
                // If the source or target types are Binary, ignore the actual data convertion
                if (dataType != Column.DataTypes.Binary && column.DataType != Column.DataTypes.Binary)
                {
                    /// If the target type is numeric, perform check before processing the conversion
                    if (performConvertionCheck && dataType == Column.DataTypes.Long && column.DataType == Column.DataTypes.Text)
                    {
                        foreach (var record in this.Query().Iterator2(column.Name))
                        {
                            string originalValue = (string)record[column];
                            long number;
                            if (originalValue != null && originalValue != string.Empty && !long.TryParse(originalValue, out number))
                                throw new InvalidOperationException("Value '" + originalValue + "' cant be converted to number");
                            //  var number = Convert.ToInt64(originalValue);
                        }
                    }

                    using (var transation = Core.Manager.BeginModifyTransaction())
                    {
                        // Loop thru all column values and convert them to the new data type
                        foreach (var record in this.Query().Iterator2(column.Name))
                        {
                            object originalValue = record[column];

                            // Gets the original value for the truncated cell, only if the mtable value is null and remove it from the list
                            if (truncatedValues != null && (originalValue == null || string.Equals(originalValue, string.Empty)))
                                if (truncatedValues.TryGetValue(record.Record, out originalValue))
                                    truncatedValues.Remove(record.Record);

                            // Modify the underlaying record value
                            byte[] valueData = new byte[0];

                            if (dataType == Column.DataTypes.Text && originalValue != null)
                                valueData = System.Text.Encoding.Default.GetBytes(originalValue.ToString());

                            bool convertionFailed = false;
                            if (dataType == Column.DataTypes.Long || dataType == Column.DataTypes.Number)
                            {
                                try
                                {
                                    valueData = ObjectToNumeric(originalValue, column.Size,
                                        dataType == Column.DataTypes.Number);

                                    // clear record error on successfull conversion
                                    if (conflictRule == TypeConflictResolveRule.KeepOriginalValue)
                                        column.ClearError(record.Record);
                                }
                                catch (Exception)
                                {
                                    if (conflictRule == TypeConflictResolveRule.ThrowException)
                                        throw;

                                    //  Unable to convert the text to numer. Write this as warning and leave the original value in.
                                    convertionFailed = true;

                                    System.Diagnostics.Debug.WriteLine(
                                        "Convertion failed, field unmodified: key={0}, column={1}, value={2}",
                                        record.Record, column.Name, originalValue);

                                    if (conflictRule == TypeConflictResolveRule.KeepOriginalValue)
                                        column.SetError(record.Record);
                                    else
                                        truncatedValues.Add(record.Record, originalValue);
                                }
                            }

                            if (!convertionFailed || conflictRule == TypeConflictResolveRule.SetValueToNull)
                            {
                                // If the storage is in single table mode, alter the column count of the core table to the current table
                                var result = this.Core.Manager.ModifyRecord(record.Record, column.Ordinal, 0, valueData, this.Columns.Count);
                                result.ToString();
                            }
                        }

                        // Marks columns sort index as dirty so it will be rebuilt
                        if (column.SortIndex != null)
                            column.SortIndex.SetDirty();
                    }
                }

                column.DataType = dataType;
            }

            this.SetModified();

            return truncatedValues;
        }

        /// <summary>
        /// Removes all records from the table
        /// </summary>
        public void RemoveRecord()
        {
            using (Core.Manager.BeginRemoveTransaction())
            {
                this.RemoveRecord(Enumerable.Range((int)StartAt, (int)(StartAt + Count)).Select(key => (long)key).ToArray());

                //  // Force the transation to commit. This will ensure the final record count
                //  transation.Commit();
            }

            // reset the count
            _count = long.MinValue;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Contains the revision version. Its the identifier of the table structure and data state (or version)
        /// </summary>
        private long RevisitonVersion;

        /// <summary>
        /// The timestamp when the table was modified (UTC)
        /// </summary>
        private DateTime _modifiedOn;

        /// <summary>
        ///  The timestamp when the table was created (UTC)
        /// </summary>
        private DateTime _createdOn;

        /// <summary>
        /// The timestamp when the table persistent content was loaded (UTC)
        /// </summary>
        private DateTime _loadedOn;

        /// <summary>
        /// Flag indicating successfully loading of persistent state and content
        /// </summary>
        private bool _loaded;

        /// <summary>
        /// Flag indicating the initialization state of this table instance.
        /// </summary>
        private bool _initialized;
    }
}