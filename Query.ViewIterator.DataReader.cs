﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalToolworks.Data.MStorage
{
    public partial class Query
    {
        public sealed class DataReader : IDataReader, ITypedList, IListSource, System.Collections.IList, IBindingList, IEnumerable<IDataRecord>
        {
            ViewIterator _iterator;
            View _current;
            string _name;
            MTable.Column[] _columns;
            bool _isClosed;

            internal DataReader(ViewIterator iterator, ITable table)
            {
                _name = table.Name;
                _iterator = iterator;
                _columns = new MTable.Column[iterator.Columns.Length];

                // Allocate the inner columns array, with columns instances ordered by their orginals
                int idx = 0;
                foreach (var column in iterator.Columns.OrderBy(c => c.Ordinal))
                    _columns[idx++] = column;
            }

            public void Close()
            {
                _isClosed = true;
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                return null;
            }

            public bool IsClosed
            {
                get { return _isClosed; }
            }

            public bool NextResult()
            {
                return false;
            }

            public bool Read()
            {
                var move = _iterator.MoveNext();
                if (move)
                    _current = _iterator.Current;
                return move;
            }

            public int RecordsAffected
            {
                get { return -1; }
            }

            public void Dispose()
            {
                _isClosed = true;
            }

            public int FieldCount
            {
                get { return _iterator.fieldset.Count; }
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                string datatype = GetFieldType(i).ToString();
                return datatype;
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                switch (_columns[i].DataType)
                {
                    case MTable.Column.DataTypes.Text:
                        return typeof(string);
                    case MTable.Column.DataTypes.Number:
                        return typeof(decimal);
                    case MTable.Column.DataTypes.Long:
                        return typeof(long);
                    case MTable.Column.DataTypes.Date:
                        return typeof(DateTime);
                    case MTable.Column.DataTypes.Binary:
                        return typeof(byte[]);
                    default:
                        throw new NotImplementedException();
                }
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                return _columns[i].Name;
            }

            public int GetOrdinal(string name)
            {
                for (int idx = 0; idx < _columns.Length; idx++)
                    if (_columns[idx].Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                        return idx;

                throw new InvalidOperationException();
            }

            public string GetString(int i)
            {
                return _current[idx: i] as string;
            }

            public object GetValue(int i)
            {
                return _current[i];
            }

            public int GetValues(object[] values)
            {
                for (int i = 0; i < _iterator.fieldset.Count; i++)
                    values[i] = _current[idx: i];
                return _iterator.fieldset.Count;
            }

            public bool IsDBNull(int i)
            {
                return _current[idx: i] == null;
            }

            public object this[string name]
            {
                get { return _current[column: name]; }
            }

            public object this[int i]
            {
                get { return _current[idx: i]; }
            }

            PropertyDescriptorCollection ITypedList.GetItemProperties(PropertyDescriptor[] listAccessors)
            {
                PropertyDescriptorCollection descriptors = new PropertyDescriptorCollection(listAccessors, false);
                foreach (var column in _iterator.Columns)
                    descriptors.Add(new RecordViewPropertyDescriptor(column));

                return descriptors;
            }

            private class RecordViewPropertyDescriptor : PropertyDescriptor
            {
                readonly MTable.Column _column;

                public RecordViewPropertyDescriptor(MTable.Column column)
                    : base(column.DisplayName ?? "<Empty>", null)
                {
                    _column = column;

                    /*if (column.DataType == MTable.Column.DataTypes.Text)
                        _dataType = typeof (string);

                    else if (column.DataType == MTable.Column.DataTypes.Numeric)
                        _dataType = typeof(long);

                    else if (column.DataType == MTable.Column.DataTypes.Real)
                        _dataType = typeof (double);

                    else
                        _dataType = typeof (object);*/
                }

                public override object GetValue(object component)
                {
                    return ((Query.View)component)[_column];
                }

                public override void SetValue(object component, object value)
                {
                    var record = ((Query.View)component);

                    // Not supported, read only 
                }

                public override void ResetValue(object component)
                {
                    // ((Query.View)component)[Name] = null;
                }

                public override bool CanResetValue(object component)
                {
                    return false;
                }

                public override bool ShouldSerializeValue(object component)
                {
                    return ((Query.View)component)[_column] != null;
                }

                public override Type PropertyType
                {
                    get { return typeof(string); }
                }

                public override bool IsReadOnly
                {
                    get { return true; }
                }

                public override Type ComponentType
                {
                    get { return typeof(Query.View); }
                }
            }

            string ITypedList.GetListName(PropertyDescriptor[] listAccessors)
            {
                return this._name;
            }

            bool IListSource.ContainsListCollection
            {
                get { return false; }
            }

            System.Collections.IList IListSource.GetList()
            {
                return this;
            }

            int System.Collections.IList.Add(object value)
            {
                throw new NotImplementedException();
            }

            void System.Collections.IList.Clear()
            {
                throw new NotImplementedException();
            }

            bool System.Collections.IList.Contains(object value)
            {
                throw new NotImplementedException();
            }

            int System.Collections.IList.IndexOf(object value)
            {
                throw new NotImplementedException();
            }

            void System.Collections.IList.Insert(int index, object value)
            {
                throw new NotImplementedException();
            }

            bool System.Collections.IList.IsFixedSize
            {
                get { throw new NotImplementedException(); }
            }

            bool System.Collections.IList.IsReadOnly
            {
                get { return true; }
            }

            void System.Collections.IList.Remove(object value)
            {
                throw new NotImplementedException();
            }

            void System.Collections.IList.RemoveAt(int index)
            {
                throw new NotImplementedException();
            }

            object System.Collections.IList.this[int index]
            {
                get
                {
                    return _iterator.GetView(index);
                }
                set
                {
                    throw new NotImplementedException();
                }
            }

            void System.Collections.ICollection.CopyTo(Array array, int index)
            {
                throw new NotImplementedException();
            }

            int System.Collections.ICollection.Count
            {
                get { return _iterator.AsEnumerable().Count(); }
            }

            bool System.Collections.ICollection.IsSynchronized
            {
                get { throw new NotImplementedException(); }
            }

            object System.Collections.ICollection.SyncRoot
            {
                get { throw new NotImplementedException(); }
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return _iterator.AsEnumerable().GetEnumerator();
            }

            void IBindingList.AddIndex(PropertyDescriptor property)
            {
                throw new NotImplementedException();
            }

            object IBindingList.AddNew()
            {
                throw new NotImplementedException();
            }

            bool IBindingList.AllowEdit
            {
                get { throw new NotImplementedException(); }
            }

            bool IBindingList.AllowNew
            {
                get { throw new NotImplementedException(); }
            }

            bool IBindingList.AllowRemove
            {
                get { throw new NotImplementedException(); }
            }

            void IBindingList.ApplySort(PropertyDescriptor property, ListSortDirection direction)
            {
                throw new NotImplementedException();
            }

            int IBindingList.Find(PropertyDescriptor property, object key)
            {
                throw new NotImplementedException();
            }

            bool IBindingList.IsSorted
            {
                get { throw new NotImplementedException(); }
            }

            event ListChangedEventHandler IBindingList.ListChanged
            {
                add { throw new NotImplementedException(); }
                remove { throw new NotImplementedException(); }
            }

            void IBindingList.RemoveIndex(PropertyDescriptor property)
            {
                throw new NotImplementedException();
            }

            void IBindingList.RemoveSort()
            {
                throw new NotImplementedException();
            }

            IEnumerator<IDataRecord> IEnumerable<IDataRecord>.GetEnumerator()
            {
                // Keep return the current DataRecord until there are no more records in the iterator
                foreach (var record in _iterator)
                {
                    _current = record;
                    yield return ((IDataRecord)this);
                }

                yield break;
            }

            ListSortDirection IBindingList.SortDirection
            {
                get { throw new NotImplementedException(); }
            }

            PropertyDescriptor IBindingList.SortProperty
            {
                get { throw new NotImplementedException(); }
            }

            bool IBindingList.SupportsChangeNotification
            {
                get { return false; }
            }

            bool IBindingList.SupportsSearching
            {
                get { throw new NotImplementedException(); }
            }

            bool IBindingList.SupportsSorting
            {
                get { throw new NotImplementedException(); }
            }
        }
    }
}
