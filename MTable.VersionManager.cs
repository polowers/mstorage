﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalToolworks.Data.MStorage
{
    public partial class MTable
    {
        /// <summary>
        /// Provides operations for managing table state versions
        /// </summary>
        public sealed class VersionManager
        {
            readonly MTable _table;

            internal VersionManager(MTable table)
            {
                _table = table;
            }

            /// <summary>
            /// Gets list of restore point version numbers
            /// </summary>
            private int[] RestorePoints
            {
                get
                {
                    if (!_table.Config.IsPersistent)
                        throw new InvalidOperationException("The operation is supported in persistent storage mode only");

                    //  Extract the versions numbers from the data paths (stored in path form [000]). 
                    //  Sort versions ascending and return the last version number

                    var versions = new List<int>();

                    var regex = new System.Text.RegularExpressions.Regex(pattern: @"^\d{3}$");

                    foreach (var folder in Directory.GetDirectories(Path.GetDirectoryName(_table.Config.DataFolder)))
                    {
                        var match = regex.Match(input: Path.GetFileName(folder));
                        if (match.Success)
                            versions.Add(int.Parse(match.Value));
                    }

                    if (versions.Count > 0)
                    {
                        versions.Sort();
                        return versions.ToArray();
                    }

                    throw new InvalidOperationException("No restore points in the version queue. Use CreateRestorePoint() to create restore point.");
                }
            }

            /// <summary>
            /// The last restore point version number, present in the version queue. <para/>
            /// The versions are starting at one (base version) and incrementing by one.
            /// </summary>
            public int LastVersion
            {
                get { return RestorePoints.Last(); }
            }

            /// <summary>
            /// Gets the current restore point version. The version 1 is the base version.
            /// </summary>
            public int CurrentVersion
            {
                get
                {
                    if (!_table.Config.IsPersistent)
                        throw new InvalidOperationException("The operation is supported in persistent storage mode only");

                    //   Extract the version number from the data path (stored in path form [000]) and increase it.                
                    var regex = System.Text.RegularExpressions.Regex.Match(input: Path.GetFileName(_table.Config.DataFolder), pattern: @"^\d{3}$");
                    if (regex.Success)
                        return int.Parse(regex.Value);

                    throw new InvalidOperationException("Version folder not found for this table");
                }
            }

            /// <summary>
            /// Removes the last restore point version (permanently erasing) and sets table's state to precedent restore point version. <para/>
            /// <para>&#160;</para>
            /// <remarks>
            /// <para> 
            /// This operation consumes the restore point from the queue and destroys the current version. Use LoadRestorePoint(version) to preserve the current version state. <para/>
            /// Fails if no restore points are currently present in the queue (use CreateRestorePoint() to create restore point). 
            /// </para>
            /// </remarks>
            /// </summary>
            public int RemoveRestorePoint()
            {
                if (!_table.Config.IsPersistent)
                    throw new InvalidOperationException("The operation is supported in persistent storage mode only");

                lock (_table)
                {
                    if (this.CurrentVersion > 1)
                    {
                        var sw = System.Diagnostics.Stopwatch.StartNew();

                        //  Uninitialize the table and dissasociate it from the current version folders.
                        //  Move the current version files into the previous version folder if not present already.
                        //  Reinitialize the mtable with the previous version folder. 
                        //  Delete the current version folder with its content completely.

                        var versionFolder = this.GetVersionFolder(CurrentVersion - 1);
                        if (!versionFolder.Exists)
                            throw new InvalidOperationException("Previous version folder not found (" + versionFolder + ")");

                        //  Removes current table associations with the current version folder. 
                        //  This way, the files will not be held and can be moved. Also this ensures all modifications are stored.
                        //
                        _table.Uninitialize();

                        //  Move the files from the current version into the previous version folder.
                        //
                        foreach (var file in Directory.GetFiles(_table.Config.DataFolder).Select(path => new FileInfo(path)))
                        {
                            var target = Path.Combine(versionFolder.FullName, file.Name);

                            if (!File.Exists(target))
                            {
                                //  Don't move files which has been created after the last load. 
                                //  Even better, which has been created after its parent folder (current version folder).
                                //  Those are files which were created as part of this version - they have not been part of the prior version.

                                if (file.CreationTimeUtc >= file.Directory.CreationTimeUtc)
                                {
                                    System.Diagnostics.Debug.WriteLine("MTable {0}: RemoveRestorePoint:: Moving file {1} skipped. It was not part of the target version.", _table.Name, file);
                                    continue;
                                }

                                file.MoveTo(target);
                            }
                            else
                                System.Diagnostics.Debug.WriteLine("MTable {0}: RemoveRestorePoint:: Moving file {1} skipped. File already exist in target version.", _table.Name, file);
                        }

                        //  Remove the current version folder completely
                        Directory.Delete(_table.Config.DataFolder, recursive: true);

                        //  Reinitialize it using the previous version configuration.
                        //  Reassociate the table, using the persistent configuration, pointing to previous version folder.
                        //
                        _table.Initialize(MTable.ConfigInfo.CreatePersistent(versionFolder.FullName, _table.Config.ReadOnly, _table.Config.IndexCapacityMb, _table.Config.MaxCapacityMb));

                        sw.Stop();
                        System.Diagnostics.Debug.WriteLine("MTable {0}: LoadRestorePoint ({2}), {1}ms", _table.Name, sw.ElapsedMilliseconds, versionFolder.Name);

                        return this.CurrentVersion;
                    }
                    else
                        throw new InvalidOperationException("No restore points in the version queue. Use CreateRestorePoint() to create restore point.");
                }
            }

            /// <summary>
            /// Restores the table state from restore point version specified. <para/>
            /// Fails if no restore points are currently present in the queue (use CreateRestorePoint() to create restore point). <para/>
            /// This operation does not consumes the restore point from the queue.
            /// </summary>
            public void LoadRestorePoint(int restoreVersion)
            {
                if (!_table.Config.IsPersistent)
                    throw new InvalidOperationException("The operation is supported in persistent storage mode only");

                lock (_table)
                {
                    if (restoreVersion > this.RestorePoints.Last() || restoreVersion <= 0)
                        throw new InvalidOperationException("Invalid restore point version " + restoreVersion + ". Maximum version allowed " + this.CurrentVersion + ".");

                    var sw = System.Diagnostics.Stopwatch.StartNew();

                    //  Uninitialize the table and dissasociate it from the current version folders.
                    //  Merge files from all versions, starting from the restoreVersion. Pick the oldest of found duplicates.
                    //  Reinitialize the mtable with the restoreVersion version folder.                    

                    var versionFolder = this.GetVersionFolder(restoreVersion);
                    if (!versionFolder.Exists)
                        throw new InvalidOperationException("Version folder not found (" + versionFolder + ")");

                    //  Removes current table associations with the current version folder. 
                    //  This way, the files will not be held and can be moved. Also this ensures all modifications are stored.
                    //
                    _table.Uninitialize();

                    //  Merge all versions files starting at the requested restoreVersion number, picking the oldest files from duplicates.
                    //  Interate over all subsequent versions and find all files which were created before this restore point. Those files are considered unmodified and have to be used for version source.
                    //  Do backwards iteration, so when the last file is found, its considered to be the oldest.

                    Dictionary<string, FileInfo> dataFiles = new Dictionary<string, FileInfo>();

                    int current = this.RestorePoints.Last();

                    while (current >= restoreVersion)
                    {
                        var currentFolder = this.GetVersionFolder(current);
                        if (!currentFolder.Exists)
                            throw new InvalidOperationException("Version folder not found (" + currentFolder + ")");

                        //  Meta files
                        foreach (var file in currentFolder.EnumerateFiles("*.meta"))
                        {
                            //  Temp!!! For now, we consider that the metadata files are always in the root of the version. Ignore the actions files.
                            versionFolder = file.Directory;
                        }

                        //  Data map files
                        foreach (var file in currentFolder.EnumerateFiles("*.data"))
                        {
                            //  Collect the data files into single dictionary, with fileName as key.

                            if (dataFiles.ContainsKey(file.Name))
                                dataFiles[file.Name] = file;
                            else
                                dataFiles.Add(file.Name, file);
                        }

                        current--;
                    }

                    //  Reinitialize it using the previous version configuration.
                    //  Reassociate the table, using the persistent configuration, pointing to previous version folder.
                    //
                    _table.Initialize(MTable.ConfigInfo.CreatePersistent(versionFolder.FullName, _table.Config.ReadOnly, _table.Config.IndexCapacityMb, _table.Config.MaxCapacityMb));

                    //  Attach the persistent data map files.
                    //
                    //  If the table header contains list of associated view IDs, use them to create the file list.
                    //  Another way to determine the associated files is, by calculating using the total table data and the view size.
                    //
                    int viewsCount = (int)Math.Round((double)_table.Core.Blocks.TotalDataSize / (double)MapViewManager.Configuration.ViewSize);
                    if (viewsCount > 0)
                    {
                        /// TODO: use the view count to limit the number of data files assigned to the correct number
                    }

                    _table.Core.MapViews.AssignMapFiles(dataFiles.Select(file => file.Value.FullName).ToArray());

                    sw.Stop();
                    System.Diagnostics.Debug.WriteLine("MTable {0}: LoadRestorePoint ({2}), {1}ms", _table.Name, sw.ElapsedMilliseconds, versionFolder.Name);
                }
            }

            /// <summary>
            /// Gets the persistent folder for the specified version
            /// </summary>
            /// <param name="version"></param>
            /// <returns></returns>
            private DirectoryInfo GetVersionFolder(int version)
            {
                if (!_table.Config.IsPersistent)
                    throw new InvalidOperationException("The operation is supported in persistent storage mode only");
                return new DirectoryInfo(path: Path.Combine(Path.GetDirectoryName(_table.Config.DataFolder), string.Format("{0:000}", version)));
            }

            /// <summary>
            /// Stores version of the table current state into restore point queue. <para/>
            /// Use RemoveRestorePoint() to restore the table state from the previous restore point, or use LoadRestorePoint(version) to load any version state specified.
            /// </summary>
            public int CreateRestorePoint()
            {
                //   Logic: the idea is to have version subfolders, which contains copy or modified files only. the unmodified files will be moved from old version to new version subfolder. 
                //   This way, no need to have redundant copies of the same data.
                //   Version base folder: ../tableDataFolder/000/
                //
                //   1) Create version subfolder, next version sequence number (001/)
                //   2) Move table data files (data, meta, etc) from the current version, into the new version folder (from 000/ to 001/):
                //   3) Reinitialize the table with the new version folder. 
                // 

                var sw = System.Diagnostics.Stopwatch.StartNew();

                if (!_table.Config.IsPersistent)
                    throw new InvalidOperationException("The operation is supported in persistent storage mode only");

                var versionFolder = this.GetVersionFolder(CurrentVersion + 1);
                if (versionFolder.Exists)
                    throw new InvalidOperationException("Version folder already exist (" + versionFolder + ").\r\nCreating restore points are allowed only by appending to the end of the version queue.\r\nMake sure the current state version of the table is the last state version.");

                if (!versionFolder.Exists)
                    versionFolder.Create();

                if (string.Equals(_table.Config.DataFolder, versionFolder.FullName))
                    throw new InvalidOperationException("Failed to create restore point. The target verson folder is the same as the table data folder. The version folder has to be at different location.");


                //  Removes current table associations with the current version folder. 
                //  This way the files will not be locked and can be moved. Also this ensures all modifications are stored.
                //
                _table.Uninitialize();

                //  Move the files from the current version into the new version folder.
                //
                foreach (var file in Directory.GetFiles(_table.Config.DataFolder).Select(path => new FileInfo(path)))
                {
                    var target = Path.Combine(versionFolder.FullName, file.Name);
                    file.MoveTo(target);
                }

                //  Reinitialize it using the new version configuration.
                //  Re-associate the table, using the peristent configuration, pointing to new version folder.
                //
                _table.Initialize(MTable.ConfigInfo.CreatePersistent(versionFolder.FullName, _table.Config.ReadOnly, _table.Config.IndexCapacityMb, _table.Config.MaxCapacityMb));

                sw.Stop();
                System.Diagnostics.Debug.WriteLine("MTable {0}: CreateRestorePoint ({2}), {1}ms", _table.Name, sw.ElapsedMilliseconds, Path.GetFileName(versionFolder.Name));

                return this.CurrentVersion - 1;
            }

            /// <summary>
            /// Checks if modification is allowed. Throws exception is not permited.
            /// </summary>
            internal void CheckAllowVersionModify()
            {
                //  Base version is allowed to be modified
                //  Prevent modification of version which is not the last.
                if (this.CurrentVersion > 1 && this.CurrentVersion < this.LastVersion)
                    throw new InvalidOperationException("Attempt to modify the current table version is not permited. The current version is not the last version. Modifying any but the last restore point version is not permited.");
            }

            internal void OnBeginContentModification()
            {
                this.CheckAllowVersionModify();

                if (this.CurrentVersion > 1)
                {
                    System.Diagnostics.Debug.WriteLine(format: "MTable {0}: OnBeginContentModification()", args: _table.Name);

                    //  Ensure there is copy of the persistent metadata, extended metadata and actions, in the versioning folders.
                    //  A copy of the map files has to be present if the previous restore point version.
                    //  Copy it to the previous version folder if does not exist.

                    var versionFolder = this.GetVersionFolder(CurrentVersion - 1);
                    if (!versionFolder.Exists)
                        throw new InvalidOperationException("Previous version folder not found (" + versionFolder + ")");

                    _table.Config.FlushMetadata();

                    //  Copy only if the *.meta files does not exist in the previous version folder.
                    foreach (var metaFile in Directory.EnumerateFiles(_table.Config.DataFolder, "*.meta").Select(path => new FileInfo(path)))
                        if (!File.Exists(Path.Combine(versionFolder.FullName, metaFile.Name)))
                            metaFile.CopyTo(Path.Combine(versionFolder.FullName, metaFile.Name), overwrite: false);
                }
            }

            internal void OnBeginViewModification(MapViewManager.View mapView, FileInfo viewFile)
            {
                //  This map view is about to be modified for first time. 

                this.CheckAllowVersionModify();

                if (this.CurrentVersion > 1)
                {
                    System.Diagnostics.Debug.WriteLine("MTable {0}: OnBeginViewModification:: viewId={1}, file={2}", _table.Name, mapView.Id, viewFile);

                    //  Ensure there is copy of the persistent data in the versioning folders.
                    //  A copy of the map file has to be present in the previous restore point version.
                    //  Copy it to the previous version folder if does not exist.                    

                    var versionFolder = this.GetVersionFolder(CurrentVersion - 1);

                    if (!versionFolder.Exists)
                        throw new InvalidOperationException("Previous version folder not found (" + versionFolder + ")");

                    //  Only copy data view file if it has not been created. 
                    //  Use the creation dates of the file and the current version folder (parent folder), to determine if it was new file or existing old file.
                    if (viewFile.CreationTimeUtc >= viewFile.Directory.CreationTimeUtc)
                    {
                        System.Diagnostics.Debug.WriteLine("MTable {0}: OnBeginViewModification:: Skiping backing map file {1}. It is new map file, created in recent version.", _table.Name, viewFile);
                        return;
                    }

                    if (viewFile.Exists && !File.Exists(Path.Combine(versionFolder.FullName, viewFile.Name)))
                        viewFile.CopyTo(Path.Combine(versionFolder.FullName, viewFile.Name), false);
                    else
                        throw new InvalidOperationException("Inspection required");
                }
            }
        }
    }
}
