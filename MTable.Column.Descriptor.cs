﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalToolworks.Data.MStorage
{
    partial class MTable
    {
        public partial class Column
        {
            /// <summary>
            /// Property descriptor for table column
            /// </summary>
            public class Descriptor : PropertyDescriptor
            {
                MTable.Column _column;
                Type _dataType;

                internal Descriptor(MTable.Column column) : base(column.Name, null)
                {
                    _dataType = typeof(string);

                    if (column.DataType == MTable.Column.DataTypes.Text)
                        _dataType = typeof(string);
                    else if (column.DataType == MTable.Column.DataTypes.Long)
                        _dataType = typeof(long);
                    else if (column.DataType == MTable.Column.DataTypes.Number)
                        _dataType = typeof(double);
                    else if (column.DataType == MTable.Column.DataTypes.Date)
                        _dataType = typeof(DateTime);
                    else if (column.DataType == MTable.Column.DataTypes.Binary)
                        _dataType = typeof(byte[]);
                    else
                        _dataType = typeof(object);
                    _column = column;
                }


                public override Type ComponentType
                {
                    get
                    {
                        return typeof(Query.View);
                    }
                }

                public override bool IsReadOnly
                {
                    get
                    {
                        return true;
                    }
                }

                public override Type PropertyType
                {
                    get
                    {
                        return _dataType;
                    }
                }

                public override bool CanResetValue(object component)
                {
                    return false;
                }

                public override object GetValue(object component)
                {
                    return ((Query.View)component)[_column];
                }

                public override void ResetValue(object component)
                {
                    throw new NotImplementedException();
                }

                public override void SetValue(object component, object value)
                {
                    throw new NotImplementedException();
                }

                public override bool ShouldSerializeValue(object component)
                {
                    throw new NotImplementedException();
                }
            }
        }
    }
}
