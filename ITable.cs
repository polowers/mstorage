﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalToolworks.Data.MStorage
{
    public interface ITable
    {
        string Name { get; }
        long Count { get; }
        long StartAt { get; }
        Data.MTableCore.Records.FieldSet CreateFieldSet(MTable.Column[] columns);
        /// <summary>
        /// Gets the collection of columns for the table
        /// </summary>
        MTable.ColumnCollection Columns { get; }

        /// <summary>
        /// Get the indexes associated with the table, identifying them by column list
        /// </summary>
        IReadOnlyDictionary<string, MTable.ColumnIndex> Indexes { get; }

        /// <summary>
        /// The fastest approach is based on the data. If there is a large number of contiguous identical values, the Median-Split approach will be the fastest. When there the number of contiguous values is smaller (typically when less than 100 contiguous numbers, due to speed increases when reading contiguous memory), then the enumeration forward/backward is best. Then give the example of Zip Codes in the ZipCity data. There is an average of 2 Zipcodes per city. With only 2 it does not make sense to search for the beginning or end of the contiguous data with a Median-Split. 
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="matchMode"></param>
        /// <param name="matchValue"></param>
        /// <returns></returns>
        Query Query(string columnName, MTable.MatchMode matchMode, Object matchValue);

        Query Query(params long[] recordKey);

        Query Query();

        Query Query(long start, int count);


        /// <summary>
        /// Query the table data, passing the filter criteria expression
        /// </summary>
        /// <param name="filterCriteriaExpression"></param>
        /// <returns></returns>
        Query Query(string filterCriteriaExpression, Query.IFunctionResolver resolver, bool cacheRecords = false);

        /// <summary>
        /// Creates or gets existing sort index for the specified columns and directions (usage: columnName ASC|DESC, columnName2 ASC|DESC)
        /// </summary>
        /// <param name="columnNames"></param>
        /// <returns></returns>
        MTable.SortIndex AddSortIndex(string columnNames);

        /// <summary>
        /// Creates or gets existing sort index sorted by calculated key columns hash values
        /// </summary>
        /// <param name="keyColumns"></param>
        /// <returns></returns>
        MTable.ColumnIndex AddHashIndex(string keyColumns);

        /// <summary>
        /// Removes record from the table
        /// </summary>
        /// <param name="recordKey"></param>
        void RemoveRecord(params long[] recordKey);
    }
}
