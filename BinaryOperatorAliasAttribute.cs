﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalToolworks.Data.MStorage
{
    public partial class MTable
    {
        /// <summary>
        /// Represent binary logical operator keyword (alias). More then one alias can be specified, separated by pipe character | 
        /// </summary>
        [AttributeUsage(AttributeTargets.Field)]
        public class BinaryOperatorAliasAttribute : Attribute
        {
            public BinaryOperatorAliasAttribute(string alias)
            {
                this.Alias = alias;
            }

            /// <summary>
            /// Gets or sets alias (keyword) for binary logical operator. It can be more then one, listed by comma
            /// </summary>
            public string Alias { get; set; }
        }
    }
}
