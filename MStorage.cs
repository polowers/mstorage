﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace DigitalToolworks.Data.MStorage
{
    public sealed class MStorage : IDisposable
    {
        readonly string _name;
        internal readonly bool isMultiTable;
        internal readonly Data.MTableCore table;
        internal readonly Dictionary<MTable, long> tablesLayout;

        readonly object _locker = new object();

        List<MTable> _tables = new List<MTable>();
        List<MTable> _tablesRemoved = new List<MTable>();
        List<MView> _views = new List<MView>();

        private readonly System.Threading.Thread _threadFlusher;

        /// <summary>
        /// Gets the storage folder where the persistent data is saved - the root
        /// </summary>
        private string _folderStorageRoot = string.Empty;

        /// <summary>
        /// Flag indicating if the schema is modified and needs to be flushed, in peristency mode
        /// </summary>
        private bool _schemaModified = false;

        /// <summary>
        /// Indicates whether the storage metadata has been changed and needs to be flushed to disk
        /// </summary>
        private bool _storageMetaIsDirty = false;
        /// <summary>
        /// In persistent mode, indicates whether load of persistent content was performed for the storage
        /// </summary>
        private bool _loadingPerformed;

        /// <summary>
        /// Create storage using multi-table mode. Each of the tables, has its own underlaying table core
        /// </summary>
        /// <param name="name"></param>
        internal MStorage(string name)
        {
            _name = name;
            isMultiTable = true;
            _threadFlusher = new System.Threading.Thread(new System.Threading.ThreadStart(FlushPendingModifications));
            tablesLayout = new Dictionary<MTable, long>();

            // ############### Global MViewManager Configuration #####################
            if (!MapViewManager.Configuration.Initialized)
            {
                MapViewManager.Configuration.ViewSize = (256L * 1024L * 1024L);  // 256Mb
                MapViewManager.Configuration.MemoryMargin = 0; // Auto
                MapViewManager.Configuration.LockViews = true;
            }
            //########################################################################

        }

        /// <summary>
        /// Create storage using single-table mode. All tables are alighed using single underlaying table core
        /// </summary>
        /// <param name="name"></param>
        /// <param name="useStaticColumnLength"></param>
        /// <param name="indexCapacityMb"></param>
        internal MStorage(string name, long indexCapacityMb)
            : this(name)
        {
            isMultiTable = false;
            table = new Data.MTableCore(
                name: name,
                filename: name,
                capacity: 1024L * 1024L * 256L,
                indexCapacityMb: indexCapacityMb);

            table.RecordHeaderFormat = MTableCore.RecordHeaderFormats.HybridFixed;

            table.Setup();
        }

        /// <summary>
        /// Creates persistent storage having the data stored at the specified storage folder.
        /// </summary>
        /// <param name="folderStorageRoot"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static MStorage CreatePersistentStorage(string folderStorageRoot, string name)
        {
            var storage = new MStorage(name);
            storage._folderStorageRoot = !Path.IsPathRooted(folderStorageRoot) ? Path.GetFullPath(folderStorageRoot) : folderStorageRoot;

            // Ensure the folder exist
            while (!Directory.Exists(storage.StorageFolder))
                Directory.CreateDirectory(storage.StorageFolder);

            return storage;
        }

        /// <summary>
        /// Creates non-persistent storage, having the data stored in pure-memory
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static MStorage CreateMemoryStorage(string name)
        {
            var storage = new MStorage(name);
            return storage;
        }

        /// <summary>
        /// Folder for this storage, containing subfolter named same as the storage
        /// </summary>
        internal string StorageFolder
        {
            get { return Path.Combine(_folderStorageRoot, this.Name); }
        }

        /// <summary>
        /// Indicates if this storage is persistent
        /// </summary>
        private bool IsPersistent
        {
            get { return !string.IsNullOrWhiteSpace(_folderStorageRoot); }
        }

        public string Name { get { return _name; } }

        public override string ToString()
        {
            return string.Format("{1} [{0}]", Name, IsPersistent ? this.StorageFolder : "memory");
        }

        /// <summary>
        /// Commits all changes made to table indexes
        /// </summary>
        private void FlushPendingModifications()
        {
            try
            {
                while (IsPersistent)
                {
                    System.Threading.Thread.Sleep(2000);

                    lock (_locker)
                    {
                        if (_schemaModified)
                            StoreSchema();

                        if (_storageMetaIsDirty)
                            SaveStorageMetadata();

                        if (isMultiTable)
                        {
                            foreach (var table in this.Tables)
                                table.SavePersistentData();

                            //  foreach (var table in this.Tables)
                            //if (table.Core.Manager.Modified)
                            // table.SavePersistentData();
                            //table.Core.Manager.BackupMeta(Path.Combine(_folderStorageRoot, _name));
                        }
                        else
                        {
                            if (table.Manager.Modified)
                                table.Manager.BackupMeta(_folderStorageRoot);

                            /*
                            // write the table name and the starting row index number
                            using (var stream = new StreamWriter(File.Open(Path.Combine(Path.Combine(_storageFolder, _name), "table.layout"), FileMode.Create)))
                                foreach (var mtable in tablesLayout.Keys)
                                    stream.WriteLine(string.Format("{0}\t{1}", mtable.Name, tablesLayout[mtable]));
                            */
                        }
                    }
                }

            }
            catch (System.Threading.ThreadAbortException)
            {
                // ignored exception
            }
        }

        private void StoreSchema()
        {

#if STORAGE_XML_SCHEMA 
             if (!IsPersistent)
                throw new InvalidOperationException("The storage is not persistent. Failed to save schema");

             var schema = new FileInfo(Path.Combine(StorageFolder, "schema"));

             using (var stream = schema.Create())
                 this.GetSchemaXml().Save(stream);

             
#endif
            _schemaModified = false;
        }

        public void ImportData(string importFolder)
        {
        }

        /// <summary>
        /// Loads the complete persistent data associated with this storage. This includes the storage metadata and tables metadata and content 
        /// </summary>
        public void LoadPersistentData()
        {
            if (!IsPersistent)
                throw new InvalidOperationException("The storage is not persistent. Failed to load persistent data");

            this.LoadStorageMetadata();

       
#if STORAGE_XML_SCHEMA  // This block enables loading the structure from XML schema file
            
            var schema = new FileInfo(Path.Combine(StorageFolder, "schema"));
            if (schema.Exists)
            {
                // load the storage schema
                using (var reader = schema.OpenText())
                    this.LoadSchema(reader.ReadToEnd());

                // loads the storage data
                this.LoadData(_folderStorageRoot);
            }

            _schemaModified = false;

#endif
            _threadFlusher.Start();
        }

        /// <summary>
        /// Loads the storage metadata, which contains list of object identifiers associated with persistent tables
        /// </summary>
        private void LoadStorageMetadata()
        {
            var sw = System.Diagnostics.Stopwatch.StartNew();

            lock (_locker)
            {
                // 1) Get or create the storage.meta file, which contains list of table object identifiers and flag value (containing the deletion state for object)
                // 2) Use the object identifiers to create MTable objects. 
                // 2) The tables marked for deletion should be then move to the removed list

                using (var reader = new BinaryReader(new FileStream(Path.Combine(this.StorageFolder, "storage.meta"), FileMode.OpenOrCreate)))
                {
                    //  ++ HEADER  ++++++++
                    //  - HeaderMarker (for verifying the file validation)
                    //  - Count (number of entries)
                    //  [ENTRY]
                    //      - UID
                    //      - MaxCapacityMb
                    //      - Flags (removed)

                    int flgREM = 1; // flag for removed object

                    if (reader.BaseStream.Length > 0) // Existing storage.meta
                    {
                        // Process HEADER
                        if (Metadata.Header.HeaderMarker != reader.ReadUInt64())
                            throw new InvalidDataException("Failed to load peristent storage data. Invalid storage meta format");

                        var count = reader.ReadInt32();

                        // Process Tables ENTRIES
                        for (int idx = 0; idx < count; idx++)
                        {
                            var uid = reader.ReadInt32();
                            var capacityMb = reader.ReadInt64();
                            var flags = reader.ReadInt32();

                            // Reserved, for future use -------
                            reader.ReadInt64();
                            reader.ReadInt64();
                            reader.ReadInt64();
                            reader.ReadInt64();
                            reader.ReadInt64();
                            //----------------------------------

                            var isRemoved = (flags & flgREM) == flgREM;

                            string tableFolder = Path.Combine(StorageFolder, uid.ToString());

                            // There might be listed table IDs which does not exist. Skip those, becuase otherwise they will be created
                            if (Directory.Exists(tableFolder))
                            {
                                //   Resolve the table version subfolder. This folder is named with versoins of the table, in form of [000] (which means, max 999 of versions supported).
                                //   Enumerate the table folder subfolders (version folders) and get the version which is the last in the list in ascending order.
                                //   If no version folders are found, use the table folder as own version number (the versioning will be disabled in this case) and log warning information for the event.

                                var versionFolder = System.IO.Directory.EnumerateDirectories(tableFolder)

                                    //  match the version subfolder in form [000] (3 digit number, range 0 to 999)
                                    .Where(path => System.Text.RegularExpressions.Regex.IsMatch(input: Path.GetFileName(path), pattern: @"^\d{3}$"))
                                    .OrderByDescending(path => Path.GetFileName(path)).FirstOrDefault();

                                if (versionFolder != null)
                                {
                                    //  last version folder at to the table folder
                                    tableFolder = Path.Combine(tableFolder, versionFolder);
                                }

                                MTable.ConfigInfo config = null;
                                try
                                {
                                    config =
                                        MTable.ConfigInfo.CreatePersistent(
                                        persistencyRootFolder: tableFolder,
                                        readOnly: false,
                                        indexCapacityMb: 0, // <- 0 means, the capacity is determined by the file
                                        maxCapacityMb: capacityMb);

                                    var table = new MTable(this, null, uid, config);

                                    // Add the table into the active or removed tables list
                                    if (!isRemoved)
                                        _tables.Add(table);
                                    else
                                        _tablesRemoved.Add(table);
                                }
                                catch (Exception e)
                                {
                                    if (config != null)
                                        ((IDisposable)config).Dispose(); // make sure the resources are released

                                    // Force to rethrow
                                    throw e;

                                    // Unable to load this table. Ignore the exception , write it to the debug logger and move on with the rest of the tables
                                    System.Diagnostics.Debug.Write(e);
                                }
                            }
                            else
                                // just skip loading table
                                System.Diagnostics.Debug.WriteLine("Table folder [" + tableFolder + "] not found. Skip loading persistent data for this table");
                        }
                    }
                }

                _storageMetaIsDirty = false;
                _loadingPerformed = true;
            }

            Common.Utils.ConsoleWriteLine("MStorage::LoadStorageMetadata: {0:n0}ms", sw.ElapsedMilliseconds);            
            sw.Stop();
        }

        /// <summary>
        /// Saves the storage content including metadata and tables related persistent data at the persistent location (in storage persistent mode)
        /// </summary>
        public void SavePersistentData()
        {
            if (!IsPersistent)
                throw new InvalidOperationException("The storage is not persistent. Failed to save persistent data");

            lock (_locker)
            {
                this.SaveStorageMetadata();

                if (isMultiTable)
                    foreach (var table in this.Tables)
                        table.SavePersistentData();
                else
                    throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Saves the storage metadata, which contains list of object identifiers, associated with persistent tables
        /// </summary>
        private void SaveStorageMetadata()
        {
            lock (_locker)
            {
                if (!_storageMetaIsDirty)
                    return;

                // Validate operation: Do not allow saving if there was no successfull load performed first. 
                // This prevents saving potencially inconsistent storage state or content.
                //
                if (!_loadingPerformed)
                    throw new InvalidOperationException("Attempt to save persistent data is not allowed: Data load has not been performed for the storage yet.");

                var sw = System.Diagnostics.Stopwatch.StartNew();

                // 1) Get or create the storage.meta file, which contains list of table object identifiers and flag value (containing the deletion state for object)
                // 2) Use the object identifiers to create MTable objects. 
                // 2) The tables marked for deletion should be then move to the removed list

                using (var writer = new BinaryWriter(new FileStream(Path.Combine(this.StorageFolder, "storage.meta"), FileMode.OpenOrCreate)))
                {
                    //  ++ HEADER  ++++++++
                    //  - HeaderMarker (for verifying the file validation)
                    //  - Count (number of entries)
                    //  [ENTRY]
                    //      - UID
                    //      - MaxCapacityMb
                    //      - Flags (removed)

                    int flgREM = 1; // flag for removed object


                    // Process HEADER
                    writer.Write(Metadata.Header.HeaderMarker);

                    // Get all tables, including the removed
                    var tablesAll = Enumerable.Concat(_tables, _tablesRemoved).Distinct().ToArray();

                    writer.Write(tablesAll.Length); // Count of entries

                    // Process Tables ENTRIES
                    for (int idx = 0; idx < tablesAll.Length; idx++)
                    {
                        Int32 uid = tablesAll[idx].GetHashCode();
                        long capacityMb = tablesAll[idx].Config.MaxCapacityMb;
                        bool isRemoved = _tablesRemoved.Contains(tablesAll[idx]);
                        Int32 flags = (isRemoved ? flgREM : 0); // Flags

                        writer.Write(uid); // UID
                        writer.Write(capacityMb); // MaxCapacityMb
                        writer.Write(flags); // Flags    

                        // Reserved, for future use -------
                        writer.Write(long.MinValue);
                        writer.Write(long.MinValue);
                        writer.Write(long.MinValue);
                        writer.Write(long.MinValue);
                        writer.Write(long.MinValue);
                        // ---------------------------------
                    }
                }

                _storageMetaIsDirty = false;

                Common.Utils.ConsoleWriteLine("MStorage::SaveStorageMetadata: {0:n0}ms", sw.ElapsedMilliseconds);
                sw.Stop();
            }
        }

        [Obsolete]
        internal void LoadData(string storageFolder)
        {
            _folderStorageRoot = storageFolder;
            // In multitable mode, restore each of the table data. 
            // Otherwise, restore only the storage table data and the tables layout metadata

            if (isMultiTable)
            {
                foreach (var table in this.Tables)
                {
                    //table.LoadPersistentData();
                }
            }
            else
            {
                table.Manager.RestoreData(storageFolder);
                table.Complete();

                // Read the tables data layout entries
                tablesLayout.Clear();

                foreach (var entry in File.ReadLines(Path.Combine(Path.Combine(storageFolder, _name), "table.layout")))
                {
                    var startIndex = long.Parse(entry.Split('\t')[1]);
                    tablesLayout.Add(GetTable(entry.Split('\t')[0]), startIndex);
                }
            }
        }

        /// <summary>
        /// Generates the storage schema into xml
        /// </summary>
        /// <param name="xmlSchema"></param>
        private XmlDocument GetSchemaXml()
        {
            var parseType = new Func<MTable.Column.DataTypes, string>(dataType =>
                {
                    switch (dataType)
                    {
                        case MTable.Column.DataTypes.Binary: return "binary";
                        case MTable.Column.DataTypes.Long: return "long";
                        case MTable.Column.DataTypes.Number: return "numeric";
                        case MTable.Column.DataTypes.Text: return "text";
                        default: return "text";
                    }
                });

            var buider = new StringBuilder();

            /// Write the xml root header
            buider.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            buider.AppendLine("<storage name=\"" + _name + "\" staticColumnLength=\"true\" maxCapacity=\"10240\">");

            /// Write the storage tables and columns
            foreach (var table in this.Tables)
            {
                buider.AppendLine("<table name=\"" + table.Name + "\"  indexCapacity=\"" + table.Config.IndexCapacityMb + "\" staticColumnLength=\"true\" maxCapacity=\"" + table.Config.MaxCapacityMb + "\">");

                // Temporary Disabled
                if (false)
                    foreach (var column in table.Columns)
                        buider.AppendLine("<column name=\"" + column.Name + "\" type=\"" + parseType(column.DataType) + "\" />");

                buider.AppendLine("</table>");
            }

            buider.AppendLine("</storage>");
            var xml = buider.ToString();
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);
            xmlDoc.Normalize();
            return xmlDoc;
        }

        [Obsolete]
        internal void StoreData(string storageFolder)
        {
            // In multi-table mode, store / backup each of the tables data. 
            // Otherwise, store / backup only the storage table data and the table layout metadata

            _folderStorageRoot = storageFolder;

            if (isMultiTable)
            {
                foreach (var table in this.Tables)
                {
                    table.SavePersistentData();
                    //table.Core.Manager.BackupData(Path.Combine(storageFolder, _name));
                }
            }
            else
            {
                table.Manager.BackupData(storageFolder);

                // write the table name and the starting row index number
                using (var stream = new StreamWriter(File.Open(Path.Combine(Path.Combine(storageFolder, _name), "table.layout"), FileMode.Create)))
                    foreach (var mtable in tablesLayout.Keys)
                        stream.WriteLine(string.Format("{0}\t{1}", mtable.Name, tablesLayout[mtable]));
            }
        }

        internal void LoadSchema(string xmlSchema)
        {
            var xml = new XmlDocument();
            xml.LoadXml(xmlSchema);

            var name = xml.DocumentElement.GetAttribute("name");
            var staticColumnLength = xml.DocumentElement.HasAttribute("staticColumnLength")
                ? bool.Parse(xml.DocumentElement.GetAttribute("staticColumnLength"))
                : false;

            var parseType = new Func<string, MTable.Column.DataTypes>(dataType =>
            {
                switch (dataType)
                {
                    case "binary": return MTable.Column.DataTypes.Binary;
                    case "long": return MTable.Column.DataTypes.Long;
                    case "numeric": return MTable.Column.DataTypes.Number;
                    case "text" : return MTable.Column.DataTypes.Text;
                    default: return MTable.Column.DataTypes.Text;
                }
            });

            // Load the tables meta data
            foreach (XmlElement tableNode in xml.DocumentElement.SelectNodes("table"))
            {
                var uid = tableNode.HasAttribute("uid") ?  MTable.UID.RegisterUid(int.Parse(tableNode.GetAttribute("uid"))) : MTable.UID.NewUid();

                MTable.ConfigInfo config;

                if (IsPersistent)
                    config = MTable.ConfigInfo.CreatePersistent(Path.Combine(this.StorageFolder, uid.ToString()), false, long.Parse(tableNode.GetAttribute("indexCapacity")), long.Parse(tableNode.GetAttribute("maxCapacity")));
                else
                    config = MTable.ConfigInfo.CreateMemory(false, long.Parse(tableNode.GetAttribute("indexCapacity")), long.Parse(tableNode.GetAttribute("maxCapacity")));

                config.UseStaticColumnLength = tableNode.HasAttribute("staticColumnLength") ? bool.Parse(tableNode.GetAttribute("staticColumnLength")) : false;

                var table = new MTable(this, tableNode.GetAttribute("name"), uid, config);

                _tables.Add(table);

                // Temporary Disabled
                if (false)
                {
                    // Table Columns

                    // Create columns from scratch if the core table does not contains any fields
                    // Or add column from definition, without creating it
                    bool createColumns = table.Core.ColumnCount == 0;
                    int ordinal = 0;

                    foreach (XmlElement columnNode in tableNode.SelectNodes("column"))
                    {
                        MTable.Column column;

                        if (createColumns)
                            column = table.Columns.CreateColumn(
                                name: columnNode.GetAttribute("name"),
                                type: columnNode.HasAttribute("type") ? parseType(columnNode.GetAttribute("type")) : MTable.Column.DataTypes.Text,
                                size: columnNode.HasAttribute("size") ? int.Parse(columnNode.GetAttribute("size")) : 0);
                        else
                            table.Columns.Add(column = new MTable.Column(
                                table: table,
                                name: columnNode.GetAttribute("name"),
                                ordinal: ordinal++,
                                type: columnNode.HasAttribute("type") ? parseType(columnNode.GetAttribute("type")) : MTable.Column.DataTypes.Text,
                                size: columnNode.HasAttribute("size") ? int.Parse(columnNode.GetAttribute("size")) : 0));

                        if (columnNode.HasAttribute("textIndexed") && bool.Parse(columnNode.GetAttribute("textIndexed")))
                            table.AddTextIndex(column.Name);

                        //if (columnNode.HasAttribute("sortIndexed") && bool.Parse(columnNode.GetAttribute("sortIndexed")))
                        //    table.AddSortIndex(column.Name);

                        //if (columnNode.HasAttribute("nativeSorted") && bool.Parse(columnNode.GetAttribute("nativeSorted")))
                        //    column.NativeSorted = true;
                    }

                    // Table Indexes
                    foreach (XmlElement indexNode in tableNode.SelectNodes("sortIndex"))
                    {
                        MTable.ColumnIndex sortIndex;
                        if (indexNode.HasAttribute("nativeSorted") && bool.Parse(indexNode.GetAttribute("nativeSorted")))
                            sortIndex = new MTable.NativeSortIndex(table,
                                indexNode.HasAttribute("name") ? indexNode.GetAttribute("name") : string.Empty,
                                indexNode.GetAttribute("columns"));
                        else
                            sortIndex = new MTable.SortIndex(table,
                                indexNode.HasAttribute("name") ? indexNode.GetAttribute("name") : string.Empty,
                                indexNode.GetAttribute("columns"));

                        table.AddSortIndex(sortIndex);
                    }
                }
            }

            // Load the table associations meta data
            foreach (XmlElement associationNode in xml.DocumentElement.SelectNodes("association"))
            {
                var parentNode = associationNode.SelectSingleNode("parent") as XmlElement;
                var childNode = associationNode.SelectSingleNode("child") as XmlElement;

                var association = GetTable(parentNode.GetAttribute("table"))
                    .AddAssociation(
                        associationNode.GetAttribute("name"),
                        parentNode.GetAttribute("column"),
                        associationNode.GetAttribute("display"),
                        GetTable(childNode.GetAttribute("table")), childNode.GetAttribute("column"));
            }

            // Load the views
            foreach (XmlElement viewNode in xml.DocumentElement.SelectNodes("view"))
            {
                var view = new MView(viewNode.GetAttribute("name"),
                    GetTable(viewNode.GetAttribute("table")),
                    viewNode.HasAttribute("countSkip") ? long.Parse(viewNode.GetAttribute("countSkip")) : 0,
                    viewNode.HasAttribute("countTop") ? long.Parse(viewNode.GetAttribute("countTop")) : 0);

                foreach (XmlElement columnNode in viewNode.SelectNodes("column"))
                {
                    if (columnNode.HasAttribute("sourceTable"))
                    {
                        view.AddColumn(columnNode.GetAttribute("name"), columnNode.GetAttribute("columnKey"),
                            GetTable(columnNode.GetAttribute("sourceTable")), columnNode.GetAttribute("sourceColumn"));
                    }
                    else
                        view.AddColumn(columnNode.GetAttribute("name"));
                }

                _views.Add(view);
            }
        }

        public ITable this[string objectName, bool throwIfNotFound = true]
        {
            get
            {
                ITable table = this.GetTable(objectName);
                if (table != null)
                    return table;
            
                var dataObject = this.Views.FirstOrDefault(view => view.Name.Equals(objectName, StringComparison.InvariantCultureIgnoreCase));
                if (dataObject != null)
                    return dataObject;
                if (throwIfNotFound)
                    throw new KeyNotFoundException(string.Format("Table or view with name '{0}' not found", objectName));
                return null;
            }
        }

        private MTable GetTable(string tableName)
        {
            return (MTable) Tables.FirstOrDefault(table => table.Name.Equals(tableName, StringComparison.InvariantCultureIgnoreCase));
        }

        public IReadOnlyList<MTable> Tables
        {
            get
            {
                lock (_locker)
                {
                    return _tables;
                }
            }
        }

        public IReadOnlyList<MView> Views
        {
            get
            {
                lock (_locker)
                {
                    return _views;
                }
            }
        }

        public void Dispose()
        {
            _threadFlusher.Abort();

            lock (_locker)
            {
                this.SetSchemaModified();   //  force save metadata
                this.SavePersistentData();

                if (isMultiTable)
                {
                    // Release all tables, including the removed ones
                    foreach (var table in Enumerable.Concat(Tables, _tablesRemoved))
                        table.Dispose();

                    // Set all instances to null and clear the collections
                    for (var idx = 0; idx < Tables.Count; idx++)
                        _tables[idx] = null;
                    for (var idx = 0; idx < _tablesRemoved.Count; idx++)
                        _tablesRemoved[idx] = null;

                    _tables.Clear();
                    _tablesRemoved.Clear();
                }
                else
                {
                    this.table.Release();
                }
            }
        }

        void IDisposable.Dispose()
        {
            this.Dispose();
        }

        /// <summary>
        /// Checks if the table or view with this name already exist in the storage
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public bool Contains(string objectName)
        {
            return this.Tables.FirstOrDefault(table => String.Equals(table.Name, objectName, StringComparison.OrdinalIgnoreCase)) != null;
        }

        /// <summary>
        /// Adds new blank table into the data storage
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="indexCapacityMb"></param>
        /// <returns></returns>
        public MTable AddTable(string tableName, long indexCapacityMb)
        {
            // Disallow table with same name
            if (this.Contains(tableName))
                throw new InvalidOperationException("Table with name " + tableName + " already exists");

            // If there is table with this name in removed tables list, restore the removed table instead of creating new one.

            MTable table;

            var uid = MTable.UID.NewUid();

            if ((table = _tablesRemoved.FirstOrDefault(t => t.Name == tableName)) == null)
                table = new MTable(
                    storage: this,
                    name: tableName,
                    uid: uid,
                    config: IsPersistent
                    ? MTable.ConfigInfo.CreatePersistent(Path.Combine(this.StorageFolder, uid.ToString(), "001"), false, indexCapacityMb, 1024L * 10)
                    : MTable.ConfigInfo.CreateMemory(false, indexCapacityMb, 1024L * 10));

            this.AddTable(table);

            return table;
        }

        public void AddTable(MTable table)
        {
            lock (_locker)
            {
                if (!_tables.Contains(table))
                    _tables.Add(table);
                _tablesRemoved.Remove(table);

                SetSchemaModified();
            }
        }

        private void SetSchemaModified()
        {
            lock (_locker)
            {
                _schemaModified = true;
                _storageMetaIsDirty = true;
            }
        }

        /// <summary>
        /// Removes table from the data storage. The method does not cause exception if the table does not exist. Returns true if table was found and removed. Otherwise, returns false.
        /// </summary>
        /// <param name="tableName"></param>
        public bool RemoveTable(string tableName)
        {
            try
            {
                MTable table = (MTable)this[tableName];

                this.RemoveTableInternal(table, completely: true);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Removes table from the data storage
        /// </summary>
        /// <param name="table"></param>
        public void RemoveTable(MTable table)
        {
            this.RemoveTableInternal(table, completely: false);
        }

        /// <summary>
        /// Removes table from the storage. Normally, it only marks it as removed. If "completely" is set, the table is completely removed, including erasing all persistent data files and folders associated (in persistency mode)
        /// </summary>
        /// <param name="table"></param>
        /// <param name="completely"></param>
        internal void RemoveTableInternal(MTable table, bool completely)
        {
            lock (_locker)
            {
                _tables.Remove(table);

                if (!completely)
                    _tablesRemoved.Add(table);
                else
                {
                    // Removes persistent data too
                    if (table.Config.IsPersistent)
                    {
                        table.Dispose();

                        int trials = 5;
                        Exception exception = null;
                        while (trials-- > 0)
                        {
                            try
                            {
                                // break on successfull directory removal, or retry

                                Directory.Delete(Path.Combine(this.StorageFolder, table.GetHashCode().ToString()), true);
                                exception = null;
                                break;
                            }
                            catch (Exception e)
                            {
                                exception = e;
                                System.Threading.Thread.Sleep(100);
                            }
                        }

                        if (exception != null)
                            throw exception;
                    }

                    // Set instance to null
                    table = null;
                }

                SetSchemaModified();
                
                if (completely)
                    SaveStorageMetadata(); // force save imideately
            }
        }

        public void BuildIndices(string storageFolder)
        {
            foreach (var table in Tables)
                table.BuildIndices(storageFolder);
        }
    }
}
