﻿using System;

namespace DigitalToolworks.Data.MStorage
{
    public static class TextMatcher
    {
        public static double Similarity(string content, string matchPattern)
        {
            double dis = GetDistance(content, matchPattern) / 10;
            int maxLen = Math.Max(content.Length, matchPattern.Length);
            if (maxLen == 0)
                return 1.0;
            return (matchPattern.Length - dis) / maxLen;
        }

        private static double GetDistance(string content, string matchPattern)
        {
            int textSize = content.Length;
            int patternSize = matchPattern.Length;
            int[] d1 = new int[patternSize + 1]; // matrix
            int[] d2 = new int[patternSize + 1]; // matrix

            if (textSize == 0) return patternSize;
            if (patternSize == 0) return textSize;

            for (int j = 0; j <= patternSize; d2[j] = (j++ * 10)) ;

            for (int i = 1; i <= textSize; i++)
            {
                var temp = d1;
                d1 = d2;
                d2 = temp;
                for (int j = 1; j <= patternSize; j++)
                {
                    d2[j] =
                        Min3(d1[j] + GetDeletionCost(content[i - 1]),
                            d2[j - 1] + 10,
                            d1[j - 1] + GetReplacementCost(content[i - 1], matchPattern[j - 1]));
                }
                if (d2[patternSize] == 0)
                    return 0;
            }
            return d2[patternSize];
        }

        // This method should be replaced by a cost vector, calculated using estimated deletion cost
        private static int GetDeletionCost(char c)
        {
            return (IsVowel(c) ? 1 : 5);
        }

        // This method should be replaced by a cost matrix, calculated using estimated replacement costs.
        private static int GetReplacementCost(char t, char p)
        {
            return (p == t) ? 0 : (IsVowel(p) && IsVowel(t) ? 1 : 10);
        }

        // Can be optimized using a two dimmensional array
        private static bool IsVowel(char c)
        {
            return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'); // || (c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U');
        }

        private static int Min3(int s1, int s2, int s3)
        {
            return Math.Min(s1, Math.Min(s2, s3));
        }
    }
}