﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DigitalToolworks.Data
{
    public class ProducerConsumerQueue : IDisposable
    {
        private System.Collections.Concurrent.BlockingCollection<WorkItem> _taskQ = new BlockingCollection<WorkItem>();

        public ProducerConsumerQueue(int workerCount, string[] states)
        {
            // Create and start a separate Task for each consumer:
            for (int i = 0; i < workerCount; i++)
                Task.Factory.StartNew(Consume, states[i], TaskCreationOptions.LongRunning);
        }

        public void Dispose()
        {
            _taskQ.CompleteAdding();
        }

        public Task EnqueueTask(object state)
        {
            return EnqueueTask(null, state);
        }

        public Task EnqueueTask(CancellationToken? cancelToken, object state)
        {
            var tcs = new TaskCompletionSource<object>();
            _taskQ.Add(new WorkItem(tcs, cancelToken, state));

            return tcs.Task;
        }

        private void Consume(object state)
        {
            foreach (WorkItem workItem in _taskQ.GetConsumingEnumerable())
                if (workItem.CancelToken.HasValue && workItem.CancelToken.Value.IsCancellationRequested)
                {
                    workItem.TaskSource.SetCanceled();
                }
                else
                    try
                    {
                        DoSomeWork(workItem.State);
                        workItem.TaskSource.SetResult(workItem.State); // Indicate completion
                    }
                    catch (Exception ex)
                    {
                        workItem.TaskSource.SetException(ex);
                    }
        }

        void DoSomeWork(object state)
        {
            //string forumShortName = (string)state;
            //string url = string.Format(ForumTemplate, forumShortName);
            //WebClient wc = new WebClient();
            //try
            //{
            //    wc.DownloadString(url);
            //}
            //catch
            //{
            //    // we probably timed out here so, nada!
            //}
            //finally
            //{
            //    wc.Dispose();
            //}
            //Console.WriteLine("retrieved: " + forumShortName);
        }

        private class WorkItem
        {
            public readonly TaskCompletionSource<object> TaskSource;
            public readonly CancellationToken? CancelToken;
            public readonly object State;
            public WorkItem(TaskCompletionSource<object> taskSource, CancellationToken? cancelToken, object state)
            {
                TaskSource = taskSource;
                //Action = action;
                CancelToken = cancelToken;
                State = state;
            }
        }
    }
}
